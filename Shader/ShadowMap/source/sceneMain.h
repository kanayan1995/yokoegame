#ifndef __MAIN_H__
#define __MAIN_H__

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
#define SHADOW_MAP_NUM 2
class	sceneMain : public Scene
{
private:
	iexView*	view;

	iexShader*	shader;
	iexShader*	shaderDL;
	iexShader*	shaderPL;
	iexShader*	shaderSL;
	iexShader*  shaderSD;

	iex2DObj*	screen;
	struct ShadowMap
	{
		bool f;
		iex2DObj*	map;
	}shadow_map[128];
	
	iex2DObj*   shadow_add_map;
	iex2DObj*   shadow_memo_map;

	iex2DObj*	diffuse;
	iex2DObj*   specular;
	iex2DObj*   depth;
	iex2DObj*   normal;
	iex2DObj*   light;
	iex2DObj*   light_s;

	//視点
	float viewAngle;
	float viewHeight;
	float viewDist;

	Vector3 ViewPos;
	//ライト
	float lightAngle;
	float lightPower;

	Vector3 LightVec;

	Surface* pBackBuffer;

	//-----------------------------
	//
	//-----------------------------
	iexMesh*	stage;
	iexMesh*	stage2;
	iex3DObj*	obj;

	struct DirLightData
	{
		Vector3 pos;
		Vector3 dir;
		Vector3 color;

		void SetData(Vector3 p, Vector3 d, Vector3 c) { pos = p; dir = d; color = c; }
	}dir_data[4];
	struct PointLightData:public DirLightData
	{
		Vector3 pos;
		Vector3 color;
		float range;

		void SetData(Vector3 p, Vector3 c, float r) { pos = p; color = c; range = r; }
	}point_data[4];
	struct SpotLightData
	{
		Vector3 pos;
		Vector3 dir;
		Vector3 color;
		float inner; 
		float outer;

		void SetData(Vector3 p, Vector3 d, Vector3 c, float i, float o) { pos = p; dir = d; color = c; inner = i; outer = o; }
	}spot_data[4];

	void DirLight(const DirLightData& d)
	{
		Matrix mat = matView;
		Vector3 Lightdir;

		//view空間変換
		Lightdir.x = d.dir.x * mat._11 + d.dir.y * mat._21 + d.dir.z * mat._31;
		Lightdir.y = d.dir.x * mat._12 + d.dir.y * mat._22 + d.dir.z * mat._32;
		Lightdir.z = d.dir.x * mat._13 + d.dir.y * mat._23 + d.dir.z * mat._33;
		Lightdir.Normalize();

		//シェーダ設定
		shaderDL->SetValue("LightVec", Lightdir);
		shaderDL->SetValue("LightColor", (Vector3)d.color);
		shaderDL->SetValue("LightPower", lightPower);

		//レンダリング
		normal->Render(shaderDL, "dirlight");
	}
	void PointLight(const PointLightData& p)
	{
		Matrix mat = matView;
		Vector3 LightPos;

		//カメラ空間変換
		LightPos.x = p.pos.x * mat._11 + p.pos.y * mat._21 + p.pos.z * mat._31 + mat._41;
		LightPos.y = p.pos.x * mat._12 + p.pos.y * mat._22 + p.pos.z * mat._32 + mat._42;
		LightPos.z = p.pos.x * mat._13 + p.pos.y * mat._23 + p.pos.z * mat._33 + mat._43;

		//シェーダ設定
		shaderPL->SetValue("LightPos", LightPos);
		shaderPL->SetValue("LightColor", (Vector3)p.color);
		shaderPL->SetValue("LightRange", p.range);

		//レンダリング
		normal->Render(shaderPL, "pointlight");
	}
	void SpotLight(const SpotLightData& s)
	{
		Matrix mat = matView;
		Vector3 LightPos;
		Vector3 Lightdir;

		//カメラ空間変換
		LightPos.x = s.pos.x * mat._11 + s.pos.y * mat._21 + s.pos.z * mat._31 + mat._41;
		LightPos.y = s.pos.x * mat._12 + s.pos.y * mat._22 + s.pos.z * mat._32 + mat._42;
		LightPos.z = s.pos.x * mat._13 + s.pos.y * mat._23 + s.pos.z * mat._33 + mat._43;

		Lightdir.x = s.dir.x * mat._11 + s.dir.y * mat._21 + s.dir.z * mat._31;
		Lightdir.y = s.dir.x * mat._12 + s.dir.y * mat._22 + s.dir.z * mat._32;
		Lightdir.z = s.dir.x * mat._13 + s.dir.y * mat._23 + s.dir.z * mat._33;
		Lightdir.Normalize();

		//シェーダ設定
		shaderSL->SetValue("LightPos", LightPos);
		shaderSL->SetValue("LightDir", Lightdir);
		shaderSL->SetValue("LightColor", (Vector3)s.color);
		shaderSL->SetValue("inner", cosf(s.inner));
		shaderSL->SetValue("outer", cosf(s.outer));

		//レンダリング
		normal->Render(shaderSL, "spotlight");
	}

	void ShadowMapSet(const DirLightData& dir, int num);
	void ShadowMapAdd()
	{
		shadow_add_map->RenderTarget();
		view->Clear();

		bool f = true;
		for (int i = 0; i < SHADOW_MAP_NUM; ++i)
		{
			if (!shadow_map[i].f)continue;
			if (f)
			{
				shadow_map[i].map->Render();
				f = false;
			}
			else
			{
				shaderSD->SetValue("ShadowAddMap1", shadow_add_map);
				shaderSD->SetValue("ShadowAddMap2", shadow_map[i].map);

				shadow_memo_map->Render(shaderSD, "ShadowADD");
			}
		}
	}
	void ShadouRender(iexShader *p);
public:
	sceneMain();
	~sceneMain();

	void Update();	//	更新
	void Render();	//	描画

	iex2DObj* GetScreen() { return screen; }
	iexShader* GetShader() { return shaderSD; }

	void ShadouBufRender(iexShader *p);
};

#endif
