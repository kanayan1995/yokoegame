#include	"iextreme.h"
#include	"system/system.h"

#include    "Shadow.h"
//*****************************************************************************************************************************
//
//	グローバル変数
//
//*****************************************************************************************************************************

//*****************************************************************************************************************************
//
//	初期化
//
//*****************************************************************************************************************************
Shadow::Shadow(sceneMain *scene_p)
{
	this->scene_p = scene_p;	

	//デプステンシルバッファ作成
	iexSystem::Device->CreateDepthStencilSurface(SHADOW_SIZE, SHADOW_SIZE, D3DFMT_D24S8, D3DMULTISAMPLE_NONE, 0, FALSE, &shadow_z_buffer, nullptr);

	//影の描画用
	shadow_screen = new iex2DObj(SHADOW_SIZE, SHADOW_SIZE, IEX2D_FLOAT);
}


Shadow::~Shadow()
{	
	if (shadow_screen)delete shadow_screen;
	//if (screen)       delete screen;
}

//*****************************************************************************************************************************
//
//		更新
//
//*****************************************************************************************************************************
void Shadow::Render(Vector3 pos, Vector3 target, int rength)
{			
	light.pos = pos;
	light.target = target;
	shadow_len = (pos - target).Length();
	light.rength = rength;

	//影
	ShadowMap();
}

//*****************************************************************************************************************************
//
//		関数
//
//*****************************************************************************************************************************
//影描画
void Shadow::ShadowMap()
{
	//Zバッファ保存
	Surface *save_z_buffer;
	iexSystem::Device->GetDepthStencilSurface(&save_z_buffer);

	//View保存
	D3DVIEWPORT9 save_viewport;
	iexSystem::Device->GetViewport(&save_viewport);

	//シャドウのZバッファに変更
	iexSystem::Device->SetDepthStencilSurface(shadow_z_buffer);

	//シャドウのビューポートに変更
	D3DVIEWPORT9 shadow_viewport = { 0, 0, SHADOW_SIZE, SHADOW_SIZE, 0, 1.0f };
	iexSystem::Device->SetViewport(&shadow_viewport);

	//Projectonを何もない単位行列にする
	D3DXMatrixIdentity(&light.projection);

	//ビュー行列の設定
	LookAtLH(shadow_projetion, light.pos, light.target, light.up);

	//平行投影行列作成(ビューボリューム)
	OlthoLH(light.projection, light.rength, light.rength, 0, shadow_len);//結果の行列　投影の幅　投影の幅　最少　最大

	//影のVP行列←(ビューとプロジェクション)
	shadow_projetion *= light.projection;//合成
	scene_p->GetShader()->SetValue("ShadowProjection", shadow_projetion);//シェーダーに転送

	shadow_screen->RenderTarget();
	iexSystem::Device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xFFFFFFFF, 1.0f, 0);

	//対象描画
	allobj();

	//デプスバッファ復元
	iexSystem::Device->SetDepthStencilSurface(save_z_buffer);
	iexSystem::Device->SetViewport(&save_viewport);
}

Shadow *shadow[128];