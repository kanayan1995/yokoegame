#include	"iextreme.h"
#include	"system/system.h"

#include	"sceneMain.h"
#include    "shadow.h"

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
sceneMain::sceneMain()
{
	// フォグの設定
	iexLight::SetFog(1000, 2000, D3DFOG_LINEAR);

	// ライトの設定
	Vector3	 dir(-2.0f, -0.5f, 1.0f);
	iexLight::DirLight(0, &dir, 1.0f, 1.0f, 1.0f);
	iexLight::SetAmbient(0x808080);

	//	シェーダー読み込み
	shader = new iexShader("DATA\\SHADER\\3D.fx");
	shaderDL = new iexShader("DATA\\SHADER\\DeferredLight.fx");
	shaderPL = new iexShader("DATA\\SHADER\\PointLight.fx");
	shaderSL = new iexShader("DATA\\SHADER\\SpotLight.fx");
	shaderSD = new iexShader("DATA\\SHADER\\shadow.fx");

	//	フレームバッファのポインタ保存
	iexSystem::Device->GetRenderTarget(0, &pBackBuffer);

	//	一時レンダリング用サーフェイス
	screen = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);

	//	ビュー設定
	view = new iexView();
	view->SetProjection( 1.4f, 0.1f, 300.0f );
	
	//	視点初期設定
	viewAngle = 0;
	viewHeight = 1;
	viewDist = 10;
	//	ライト初期設定
	lightAngle = 2;
	lightPower = 1.0f;

	//深度用サーフェイス
	diffuse         = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);
	specular        = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);
	depth           = new iex2DObj(1280, 720, IEX2D_FLOAT);
	normal          = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);
	light           = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);
	light_s         = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);
	shadow_add_map  = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);
	shadow_memo_map = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);
	shaderDL->SetValue("DepthBuf", depth);
	shaderDL->SetValue("SpecularBuf", specular);
	shaderPL->SetValue("DepthBuf", depth);
	shaderPL->SetValue("SpecularBuf", specular);
	shaderSL->SetValue("DepthBuf", depth);
	shaderSL->SetValue("SpecularBuf", specular);

	for (int i = 0; i < SHADOW_MAP_NUM; ++i)
	{
		shadow[i] = new Shadow(this);
		shadow_map[i].map = new iex2DObj(1280, 720, IEX2D_RENDERTARGET);
		shadow_map[i].f = false;
	}

	//	オブジェクト読み込み
	obj   = new iex3DObj( "DATA\\OBJ\\ZERO.IEM" );
	obj->SetPos( 0,0,0 );
	obj->SetScale( 0.015f );

	stage = new iexMesh( "DATA\\STAGE\\STAGE.IMO" );
	stage2 = new iexMesh( "DATA\\STAGE\\STAGE2.IMO" );
}

sceneMain::~sceneMain()
{
	delete shadow_memo_map;
	delete shadow_add_map;
	delete light_s;
	delete light;
	delete normal;
	delete depth;
	delete specular;
	delete diffuse;

	delete screen;

	delete view;

	for (int i = 0; i < SHADOW_MAP_NUM; ++i)
	{
		delete shadow[i];
		delete shadow_map[i].map;
	}

	pBackBuffer->Release();

	delete shaderSD;
	delete shaderSL;
	delete shaderPL;
	delete shaderDL;
	delete shader;
}

//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************
void	sceneMain::Update()
{
	//	ライト強度変更
	if( GetKeyState('1') < 0 ){ lightPower+=0.01f; if( lightPower > 10.0f ) lightPower = 10; }
	if( GetKeyState('2') < 0 ){ lightPower-=0.01f; if( lightPower < 0 ) lightPower = 0; }

	//	視点
	int AxisX = KEY_GetAxisX();
	int AxisY = KEY_GetAxisY();
	int AxisX2 = KEY_GetAxisX2();
	int AxisY2 = KEY_GetAxisY2();
	if( AxisX*AxisX < 300*300 ) AxisX = 0;
	if( AxisY*AxisY < 300*300 ) AxisY = 0;
	if( AxisX2*AxisX2 < 300*300 ) AxisX2 = 0;
	if( AxisY2*AxisY2 < 300*300 ) AxisY2 = 0;
	viewAngle += AxisX * 0.00002f;
	viewHeight -= AxisY * 0.0001f;
	viewDist += AxisY2 * 0.0001f;

	ViewPos = Vector3( sinf(viewAngle)*viewDist, viewHeight, cosf(viewAngle)*viewDist);
	view->Set( ViewPos, Vector3(0, 1, 0));
	shader->SetValue("ViewPos", ViewPos );

	//	ライト
	lightAngle -= AxisX2 * 0.0001f;
	LightVec = Vector3(sinf(lightAngle), -0.7f, cosf(lightAngle));
	LightVec.Normalize();
	//iexLight::DirLight(shaderDL, 0, &LightVec, lightPower, lightPower, lightPower);
	
	obj->Animation();
	obj->SetAngle(0);
	obj->Update();

	for (int i = 0; i < SHADOW_MAP_NUM; ++i)shadow_map[i].f = false;
}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************
void sceneMain::ShadouBufRender(iexShader *p)
{
	//	物体描画
	stage->Render(p, "ShadowBuf");
	stage2->Render(p, "ShadowBuf");
	obj->Render(p, "ShadowBuf");
}
void sceneMain::ShadouRender(iexShader *p)
{
	//	物体描画
	stage->Render(p, "shadow");
	p->SetValue("adjustValue", -0.03f);
	stage2->Render(p, "shadow");
	p->SetValue("adjustValue", -0.003f);
	obj->Render(p, "shadow");
}

void sceneMain::ShadowMapSet(const DirLightData& dir, int num)
{
	shadow[num]->Render(dir.pos * 20.0f, dir.dir * 20.0f, 42);
	shaderDL->SetValue("ShadowProjection", shadow[num]->GetShadowProjetion());
	shaderDL->SetValue("ShadowMap", shadow[num]->GetShadowMap());
	shaderDL->SetValue("Projection", matProjection);
	shaderDL->SetValue("LightVec", (Vector3)dir.dir);
	shadow_map[num].map->RenderTarget();
	shadow_map[num].f = true;

	view->Clear();

	ShadouRender(shaderDL);
}

void	sceneMain::Render()
{
	view->Activate();

	shader->SetValue("matView", matView);

	Matrix Invproj;
	D3DXMatrixInverse(&Invproj, nullptr, &matProjection);
	shaderDL->SetValue("Invproj", Invproj);
	shaderPL->SetValue("Invproj", Invproj);
	shaderSL->SetValue("Invproj", Invproj);

	Vector3 vec = Vector3(-LightVec.x, LightVec.y, -LightVec.z);
	vec.Normalize();
	dir_data[0].SetData(-LightVec, LightVec, Vector3(1, 1, 1));
	dir_data[1].SetData(-vec, vec, Vector3(1, 1, 1));
	spot_data[0].SetData(Vector3(0, 4, 6), Vector3(0, -4, -6), Vector3(1, 1, 1), 0.1, 0.3f);

	ShadowMapSet(dir_data[0], 0);
	//ShadowMapSet(dir_data[1], 1);
	ShadowMapAdd();

	//ディファード設定
	diffuse->RenderTarget();
	specular->RenderTarget(1);
	depth->RenderTarget(2);
	normal->RenderTarget(3);

	view->Clear();

	//物体描画
	stage->Render(shader, "deffered");
	stage2->Render(shader, "deffered");
	obj->Render(shader, "deffered");

	//ライトバッファ作成
	light->RenderTarget();
	light_s->RenderTarget(1);
	iexSystem::Device->SetRenderTarget(2, nullptr);
	iexSystem::Device->SetRenderTarget(3, nullptr);
	view->Clear(0x000000);

	DirLight(dir_data[0]);
	//DirLight(dir_data[1]);
	//PointLight(Vector3(0, 3, 0), Vector3(0, 1, 0), 10.0f);
	//PointLight(Vector3(2, 3, 2), Vector3(1, 0, 0), 10.0f);
	//PointLight(Vector3(0, 3, 2), Vector3(0, 0, 1), 10.0f);
	//SpotLight(spot_data[0]);

	//一時バッファに切り替え
	screen->RenderTarget();

	iexSystem::Device->SetRenderTarget(1, nullptr);
	iexSystem::Device->SetRenderTarget(2, nullptr);
	iexSystem::Device->SetRenderTarget(3, nullptr);

	view->Clear(0x000000);

	diffuse->Render();
	light->  Render(0, 0, 1280, 720, 0, 0, 1280, 720, RS_MUL, 0xffffffff);
	light_s->Render(0, 0, 1280, 720, 0, 0, 1280, 720, RS_ADD, 0xffffffff);
	shadow_add_map->Render(0, 0, 1280, 720, 0, 0, 1280, 720, RS_SUB, 0xffffffff);

	iexSystem::Device->SetRenderTarget(0, pBackBuffer);

	//テスト描画
	screen->Render();	
	//diffuse->Render (  0, 0, 320, 180, 0, 0, 1280, 720);
	//specular->Render(320, 0, 320, 180, 0, 0, 1280, 720);
	//depth->Render   (640, 0, 320, 180, 0, 0, 1280, 720);
	//normal->Render  (960, 0, 320, 180, 0, 0, 1280, 720);
	//light->Render   (0, 180, 320, 180, 0, 0, 1280, 720);
	//light_s->Render(320, 180, 320, 180, 0, 0, 1280, 720);

	for (int i = 0; i < SHADOW_MAP_NUM; ++i) 
	{
		if (shadow_map[i].f)
		{
			shadow_map[i].map->Render(0 + (i * 320), 0, 320, 180, 0, 0, 1280, 720);
			//shadow[i]->GetShadowMap()->Render(0 + (256 * i), 0, 256, 256, 0, 0, SHADOW_SIZE, SHADOW_SIZE);
		}
	}
}

