#ifndef __SHADOW_H__
#define __SHADOW_H__

#include    "sceneMain.h"
//*********************************************************************************************
// 影
//*********************************************************************************************
#define SHADOW_SIZE 4096

class Shadow
{
private:
	Matrix shadow_projetion;

	//レンダーターゲット
	iex2DObj *shadow_screen;

	//バックバッファ
	Surface *shadow_z_buffer;

	sceneMain *scene_p;

	//影描画
	void ShadowMap();

	//平行光
	float shadow_len;
	struct ParallelLight
	{
	public:
		Matrix  projection;
		Vector3 up;
		Vector3 target;
		Vector3 pos;
		int rength;

		ParallelLight()
		{
			up = Vector3(0.0f, 1.0f, 0.0f);
		}
	}light;
public:
	Shadow(sceneMain *scene_p);
	~Shadow();
	void Render(Vector3 pos, Vector3 target, int rength);

	void allobj()
	{
		scene_p->ShadouBufRender(scene_p->GetShader());
	}

	iex2DObj* GetShadowMap() { return shadow_screen; }

//ゲッター
	ParallelLight GetParallelLight(){ return light; }
	Matrix GetShadowProjetion() { return shadow_projetion; }
};
extern Shadow *shadow[128];

#endif