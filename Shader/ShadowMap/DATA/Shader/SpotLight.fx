//**************************************************************************************************
//																									
//		基本シェーダー		
//
//**************************************************************************************************

//------------------------------------------------------
//		環境関連
//------------------------------------------------------
float4x4 Invproj;
float4x4 ShadowProjection;

//------------------------------------------------------
//		テクスチャサンプラー	
//------------------------------------------------------
texture Texture;	//	デカールテクスチャ
sampler DecaleSamp = sampler_state
{
    Texture = <Texture>;
    MinFilter = POINT;
    MagFilter = POINT;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};


texture DepthBuf;
sampler DepthBufSamp = sampler_state
{
	Texture = <DepthBuf>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	AddressU = CLAMP;
	AddressV = CLAMP;
};

texture SpecularBuf;
sampler SpecularBufSamp = sampler_state
{
	Texture = <SpecularBuf>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	AddressU = CLAMP;
	AddressV = CLAMP;
};

//**************************************************************************************************
//		ライト用パラメータ
//**************************************************************************************************
float3 LightPos   = { 0, 3, 0 };
float3 LightDir = { 0, -1, 0 };
float3 LightColor = { 1.0f, 1.0f, 1.0f };
float inner = 0.7f;
float outer = 0.6f;

//**************************************************************************************************
//		頂点フォーマット
//**************************************************************************************************
struct POUT_LIGHT
{
	float4 color :COLOR0;
	float4 spec  :COLOR1;
};

//**************************************************************************************************
//
//		ライティング
//
//**************************************************************************************************
//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
POUT_LIGHT PS_SpotLight(float2 Tex : TEXCOORD0) :COLOR
{
	POUT_LIGHT OUT;

	// カメラ空間変換
	float4 depth = tex2D(DepthBufSamp, Tex).r;
	float4 screen;
	screen.xy = Tex * 2 - 1;
	screen.y = -screen.y;
	screen.z = depth;
	screen.w = 1;
	float4 pos = mul(screen, Invproj);
	pos.xyz /= pos.w;

	//ライトベクトル
	float3 LightVec = pos - LightPos;
	float dist = length(LightVec);
	LightVec = normalize(LightVec);

	//法線取得
	float4 n = tex2D(DecaleSamp, Tex);
	float3 normal = n.rgb * 2 - 1;
	normal = normalize(normal);

	//減衰量
	float intensity = dot(LightVec, LightDir);
	intensity = (intensity - outer) / (inner - outer);
	intensity = max(intensity, 0.0f);
	intensity = min(intensity, 1.0f);

	//ライト計算
	float r = max(0, dot(normal, -LightVec));

	//ピクセル色決定
	OUT.color.rgb = r * LightColor * intensity;
	OUT.color.a = 1;

	///スペキュラ
	float3 E = pos.xyz;
	float3 R = normalize(-LightVec - E);
	float sp = pow(max(0, dot(R, normal)), 10);
	float4 sp_tex = tex2D(SpecularBufSamp, Tex);

	OUT.spec.rgb = sp_tex.rgb * sp * LightColor * intensity;
	OUT.spec.a = 1;

	return OUT;
}

//------------------------------------------------------
//		合成なし	
//------------------------------------------------------
technique spotlight
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = one;
		CullMode         = None;
		ZEnable          = false;

		PixelShader  = compile ps_3_0 PS_SpotLight();
    }
}
