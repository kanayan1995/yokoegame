//**************************************************************************************************
//																									
//		基本シェーダー		
//
//**************************************************************************************************

//------------------------------------------------------
//		環境関連
//------------------------------------------------------
float4x4 TransMatrix;	//	変換行列
float4x4 Projection;	//	変換行列

float4x4 Invproj;
float4x4 ShadowProjection;

//------------------------------------------------------
//		テクスチャサンプラー	
//------------------------------------------------------
texture Texture;	//	デカールテクスチャ
sampler DecaleSamp = sampler_state
{
    Texture = <Texture>;
    MinFilter = POINT;
    MagFilter = POINT;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};


texture DepthBuf;
sampler DepthBufSamp = sampler_state
{
	Texture = <DepthBuf>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	AddressU = CLAMP;
	AddressV = CLAMP;
};

texture SpecularBuf;
sampler SpecularBufSamp = sampler_state
{
	Texture = <SpecularBuf>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	AddressU = CLAMP;
	AddressV = CLAMP;
};

texture NormalMap;
sampler NormalSamp = sampler_state
{
	Texture = <NormalMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;

	AddressU = Wrap;
	AddressV = Wrap;
};

texture ShadowMap;
sampler ShadowSamp = sampler_state
{
	Texture = <ShadowMap>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	BorderColor = 0xFFFFFFFF;
	AddressU = BORDER;
	AddressV = BORDER;
};

//**************************************************************************************************
//		ライト用パラメータ
//**************************************************************************************************
float3 LightVec   = { 0.7f, -0.7f, 0 };
float3 LightColor = { 1.0f, 1.0f, 1.0f };
float LightPower = 1.0f;

inline float3 DirLight(float3 dir, float3 normal)
{
	float3 light = 1.0f;
	float rate = max(0.0f, dot(-dir, normal));
	light *= rate;

	return light;
}

//**************************************************************************************************
//		影用パラメータ
//**************************************************************************************************
float adjustValue = -0.003;
float ShadowPower = 1.0f;

inline float3 GetShadowTex(float4 Pos)
{
	float4 Tex;
	float4 p;
	p = Pos;
	p = mul(p, ShadowProjection);
	p /= p.w;

	p.y = -p.y;
	p.xy = 0.5f * p.xy + 0.5f;
	Tex = p;
	return Tex.xyz;
}

inline float GetShadow(float3 Tex)
{
	float d = tex2D(ShadowSamp, Tex.xy).r;

	float l = (d < Tex.z + adjustValue) ? max(LightPower * ShadowPower, 0.0f) : 0;
	return min(l, 1.0);
}

//**************************************************************************************************
//		頂点フォーマット
//**************************************************************************************************
struct VS_INPUT
{
	float4 Pos : POSITION;
};

struct V_FULL
{
	float4 Pos		: POSITION;
	float4 wPos		: TEXCOORD0;
};

struct POUT_LIGHT
{
	float4 color :COLOR0;
	float4 spec  :COLOR1;
};

//**************************************************************************************************
//
//		ライティング
//
//**************************************************************************************************
//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
POUT_LIGHT PS_DirLight(float2 Tex : TEXCOORD0) :COLOR
{
	POUT_LIGHT OUT;

	float4 n = tex2D(DecaleSamp, Tex);
	float3 normal = n.rgb * 2 - 1;
	normal = normalize(normal);

	//ライト率
	float r = max(0, dot(normal, -LightVec));

	//ピクセル色決定
	OUT.color.rgb = (r * LightColor) * LightPower;
	OUT.color.a = 1;

	// カメラ空間変換
	float4 NDC;
	NDC.xy = Tex * 2 - 1;
	NDC.z = tex2D(DepthBufSamp, Tex).r;
	NDC.w = 1;

	float4 pos = mul(NDC, Invproj);
	pos.xyz /= pos.w;

	//スペキュラ
	float3 E = normalize(pos.xyz);

	float3 R = normalize(-LightVec - E);
	float sp = pow(max(0, dot(R, normal)), 10);
	float4 sp_tex= tex2D(SpecularBufSamp, Tex);

	OUT.spec.rgb = sp_tex.rgb * sp * LightColor * LightPower;
	OUT.spec.a = 1;

	return OUT;
}

//------------------------------------------------------
//		合成なし	
//------------------------------------------------------
technique dirlight
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = one;
		CullMode         = None;
		ZEnable          = false;

		PixelShader  = compile ps_3_0 PS_DirLight();
    }
}

//**************************************************************************************************
//
//		影作成
//
//**************************************************************************************************
//------------------------------------------------------
//		頂点シェーダー	
//------------------------------------------------------
V_FULL VS_Shadow(VS_INPUT In)
{
	V_FULL Out = (V_FULL)0;

	Out.Pos = mul(In.Pos, Projection);
	Out.wPos = mul(In.Pos, TransMatrix);

	return Out;
}
//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
float4 PS_Shadow(V_FULL In) :COLOR
{
	float4 OUT;

	float3 shadow;
	shadow = GetShadowTex(In.wPos);

	OUT.rgb = 1;
	OUT.rgb *= GetShadow(shadow);
	OUT.a = 1.0f;
	
	return OUT;
}
//------------------------------------------------------
//		影
//------------------------------------------------------
technique shadow
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		CullMode         = CCW;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Shadow();
		PixelShader  = compile ps_3_0 PS_Shadow();
	}
}
