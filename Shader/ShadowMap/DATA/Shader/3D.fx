//**************************************************************************************************
//																									
//		基本シェーダー		
//
//**************************************************************************************************

//------------------------------------------------------
//		環境関連
//------------------------------------------------------
float4x4 TransMatrix;	//	変換行列
float4x4 matView;		//	変換行列
float4x4 Projection;	//	変換行列

float3	ViewPos;



//------------------------------------------------------
//		テクスチャサンプラー	
//------------------------------------------------------
texture Texture;	//	デカールテクスチャ
sampler DecaleSamp = sampler_state
{
    Texture = <Texture>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture NormalMap;	//	法線マップテクスチャ
sampler NormalSamp = sampler_state
{
    Texture = <NormalMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture HeightMap;		//	高さマップテクスチャ
sampler HeightSamp = sampler_state
{
    Texture = <HeightMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture SpecularMap;	//	スペキュラマップテクスチャ
sampler SpecularSamp = sampler_state
{
    Texture = <SpecularMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};


//**************************************************************************************************
//		頂点フォーマット
//**************************************************************************************************
struct VS_OUTPUT
{
    float4 Pos	 : POSITION;
    float4 Color : COLOR0;
    float2 Tex	 : TEXCOORD0;
				 
	float4 wPos  : TEXCOORD1;
	float3 N     : TEXCOORD2;
	float3 T     : TEXCOORD3;
	float3 B     : TEXCOORD4;
};

struct VS_INPUT
{
    float4 Pos    : POSITION;
	float3 Normal : NORMAL;
    float2 Tex    : TEXCOORD0;
};

//**************************************************************************************************
//		ピクセルフォーマット
//**************************************************************************************************
struct PS_OUTPUT
{
	float4 color  : COLOR;
	float4 spec   : COLOR1;
	float4 depth  : COLOR2;
	float4 normal : COLOR3;
};

//**************************************************************************************************
//
//		ライティング
//
//**************************************************************************************************
//------------------------------------------------------
//	頂点シェーダ
//------------------------------------------------------
VS_OUTPUT VS_Deffered( VS_INPUT In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	Out.Pos   = mul(In.Pos, Projection);
	Out.Color = 1.0f;
	Out.Tex   = In.Tex;

	Out.wPos = Out.Pos;

	float3x3 mat = mul(TransMatrix, matView);

	//カメラ空間座標系
	Out.N = mul(In.Normal, mat);
	Out.N = normalize(Out.N);

	float3 vy = { 0, 1, 0.001f };

	Out.T = cross(vy, Out.N);
	Out.T = -normalize(Out.T);
	Out.B = cross(Out.N, Out.T);
	Out.B = normalize(Out.B);

	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
PS_OUTPUT PS_Deffered(VS_OUTPUT In)
{
	PS_OUTPUT OUT = (PS_OUTPUT)0;
	float2 Tex = In.Tex;

	//ディフューズ色
	OUT.color = In.Color * tex2D(DecaleSamp, Tex);

	//スペキュラ
	OUT.spec = tex2D(SpecularSamp, Tex);
	OUT.spec.a = 1.0f;

	//深度
	OUT.depth.rgb = In.wPos.z / In.wPos.w;
	OUT.depth.a = 1.0f;

	//頂点空間→カメラ変換行列
	float3x3 ToView;
	ToView[0] = In.T;
	ToView[1] = In.B;
	ToView[2] = In.N;

	//法線変換
	float3 N = tex2D(NormalSamp, Tex).xyz - 0.5f;
	float3 normal = normalize(mul(N, ToView));
	normal = normal * 0.5f + 0.5f;

	//カメラ空間法線
	OUT.normal.rgb = normal;
	OUT.normal.a = 1;

	return OUT;
}

//------------------------------------------------------
//		合成なし	
//------------------------------------------------------
technique deffered
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;

		VertexShader = compile vs_3_0 VS_Deffered();
		PixelShader  = compile ps_3_0 PS_Deffered();
    }
}
