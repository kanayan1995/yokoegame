//**************************************************************************************************
//																									
//		シャドウマップ作成シェーダー		
//
//**************************************************************************************************
float4x4 TransMatrix;	//	変換行列
float4x4 ShadowProjection;
float AddShadowPower = 1.0f;

texture ShadowMap;
sampler ShadowSamp = sampler_state
{
	Texture = <ShadowMap>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	BorderColor = 0xFFFFFFFF;
	AddressU = BORDER;
	AddressV = BORDER;
};

texture ShadowAddMap1;
sampler ShadowAddSamp1 = sampler_state
{
	Texture = <ShadowAddMap1>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	BorderColor = 0xFFFFFFFF;
	AddressU = BORDER;
	AddressV = BORDER;
};

texture ShadowAddMap2;
sampler ShadowAddSamp2 = sampler_state
{
	Texture = <ShadowAddMap2>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	BorderColor = 0xFFFFFFFF;
	AddressU = BORDER;
	AddressV = BORDER;
};

struct VS_SHADOW
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
};

//------------------------------------------------------
//	頂点シェーダー
//------------------------------------------------------
VS_SHADOW VS_ShadowBuf(float4 Pos : POSITION)
{
	VS_SHADOW Out;
	
	//座標変換
	float4x4 mat = mul(TransMatrix, ShadowProjection);

	Out.Pos = mul(Pos, mat);
	Out.Color = Out.Pos.z / Out.Pos.w;

	return Out;
}

//------------------------------------------------------
//	ピクセルシェーダー	
//------------------------------------------------------
float4 PS_ShadowBuf(VS_SHADOW In) : COLOR
{
	return In.Color;
}

//------------------------------------------------------
//	テクニック
//------------------------------------------------------
technique ShadowBuf
{
	pass P0
	{
		AlphaBlendEnable = false;
		ZWriteEnable     = true;
		CullMode         = CCW;

		VertexShader = compile vs_2_0 VS_ShadowBuf();
		PixelShader  = compile ps_2_0 PS_ShadowBuf();
	}
}


//------------------------------------------------------
//	ピクセルシェーダー	
//------------------------------------------------------
float4 PS_ShadowADD(float2 Tex : TEXCOORD0) :COLOR
{
	float4 OUT;

	float4 n1 = tex2D(ShadowAddSamp1, Tex);
	float4 n2 = tex2D(ShadowAddSamp2, Tex);
	OUT = (n1 > 0.0f && n2 > 0.0f) ? AddShadowPower : n1 + n2;
	OUT.a = 1.0f;

	return OUT;
}
//------------------------------------------------------
//	テクニック
//------------------------------------------------------
technique ShadowADD
{
	pass P0
	{
		AlphaBlendEnable = false;
		ZWriteEnable = true;
		CullMode = CW;

		PixelShader = compile ps_2_0 PS_ShadowADD();
	}
}