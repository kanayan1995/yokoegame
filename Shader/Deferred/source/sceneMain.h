//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
class	sceneMain : public Scene
{
private:
	iexView*	view;
	float	viewAngle;
	float	viewHeight;
	float	viewDist;

	float	lightAngle;
	float	lightPower;
	bool	bHDR;

	iexShader*	shader;
	iexShader*	shader2D;

	iex2DObj*	screen;
	Surface* pBackBuffer;

	iex2DObj*	EnvTex;

	iex2DObj*	hdr;
	void RenderHDR();

	//-----------------------------
	//
	//-----------------------------
	iexMesh*	stage;
	iexMesh*	stage2;
	iex3DObj*	obj;
	iexMesh*	eff_obj;

	Vector3		ViewPos;

	//影用変数
	iex2DObj* m_shadowTex;
	Surface* m_shadowZ;
	static const int SHADOW_SIZE = 1024;
	void RenderShadowBuffer();

	//被写界度用変数
	iex2DObj *m_depth;
	void RenderDepth();

	//反射用変数
	iex2DObj *m_refTex;
	void RenderReflect();

	//ディファードシェーディング
	iexShader*	m_shaderDeferrred;
	iex2DObj*		m_diffuseTex;
	iex2DObj*		m_specularTex;
	iex2DObj*		m_depthTex;
	iex2DObj*		m_normalTex;
	iexShader*	m_shaderDeferredLight;
	iex2DObj*		m_lightTex;
	void DirectionLight( const Vector3 &dir , const Vector3 &color );
	iex2DObj*		m_lightSpecularTex;
	iexShader*	m_shaderDefferdPointLight;
	void PointLight( const Vector3& pos , const Vector3& color , float range );
	void SpotLight( const Vector3& pos , const Vector3& dir , const Vector3& color , float inner , float outer );
	iex2DObj* m_defferdShadowTex;
	Surface* m_defferdShadowZ;
	void DefferdRenderShadowBuffer();
public:
	sceneMain();
	~sceneMain();

	void Update();	//	更新
	void Render();	//	描画

};


