#include	"iextreme.h"
#include	"system/system.h"

#include	"sceneMain.h"

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
sceneMain::sceneMain()
{
	iexLight::SetFog( 1000 , 2000 , D3DFOG_LINEAR );

	//	シェーダー読み込み
	shader = new iexShader( "DATA\\SHADER\\3D.fx" );
	shader2D = new iexShader( "DATA\\SHADER\\2D.fx" );
	//	フレームバッファのポインタ保存
	iexSystem::GetDevice()->GetRenderTarget( 0 , &pBackBuffer );
	//	一時レンダリング用サーフェイス
	screen = new iex2DObj( 1280 , 720 , IEX2D_RENDERTARGET );

	//	(超疑似)HDR用バッファ
	hdr = new iex2DObj( 512 , 512 , IEX2D_RENDERTARGET );

	//	半球ライティング設定
	shader->SetValue( "SkyColor" , Vector3( 0.3f , 0.3f , 0.3f ) );
	shader->SetValue( "GroundColor" , Vector3( 0.2f , 0.2f , 0.2f ) );
	//	環境テクスチャ設定
	EnvTex = new iex2DObj( "DATA\\Env01.png" );
	shader->SetValue( "EnvMap" , EnvTex );

	//	ポストエフェクト設定
	float contrast = 1.6f;		//	コントラスト
	float saturate = 0.7f;		//	彩度
	float brightness = 0.0f;	//	輝度
	Vector3 screen_color = Vector3( 1.0f , 1.03f , 1.05f );
	shader2D->SetValue( "contrast" , contrast );
	shader2D->SetValue( "saturate" , saturate );
	shader2D->SetValue( "brightness" , brightness );
	shader2D->SetValue( "ScreenColor" , &screen_color );

	//	ビュー設定
	view = new iexView();
	view->SetProjection( 1.4f , 0.1f , 300.0f );

	//	視点初期設定
	viewAngle = 0;
	viewHeight = 1;
	viewDist = 10;
	//	ライト初期設定
	lightAngle = 2;
	lightPower = 0.1f;


	//	オブジェクト読み込み
	obj = new iex3DObj( "DATA\\OBJ\\ZERO.IEM" );
	obj->SetPos( 0 , 0 , 0 );
	obj->SetScale( 0.015f );

	stage = new iexMesh( "DATA\\STAGE\\STAGE.IMO" );
	stage2 = new iexMesh( "DATA\\STAGE\\STAGE2.IMO" );
	eff_obj = new iexMesh( "DATA\\EFFECT\\TEST.IMO" );

	//シャドーマップ作成
	m_shadowTex = new iex2DObj( SHADOW_SIZE , SHADOW_SIZE , IEX2D_RENDERTARGET );
	//影用Zバッファ作成
	iexSystem::Device->CreateDepthStencilSurface( SHADOW_SIZE , SHADOW_SIZE , D3DFMT_D16 , D3DMULTISAMPLE_NONE , 0 , FALSE , &m_shadowZ , NULL );
	//深度用サーフェイス
	m_depth = new iex2DObj( 1280 , 720 , IEX2D_FLOAT );
	//反射用
	m_refTex = new iex2DObj( 1024 , 1024 , IEX2D_RENDERTARGET );

	//ディファード用
	m_diffuseTex = new iex2DObj( 1280 , 720 , IEX2D_RENDERTARGET );
	m_specularTex = new iex2DObj( 1280 , 720 , IEX2D_RENDERTARGET );
	m_depthTex = new iex2DObj( 1280 , 720 , IEX2D_FLOAT );
	m_normalTex = new iex2DObj( 1280 , 720 , IEX2D_RENDERTARGET );

	//ディファードライト
	m_shaderDeferredLight = new iexShader( "DATA/Shader/DeferredLight.fx" );
	m_lightTex = new iex2DObj( 1280 , 720 , IEX2D_RENDERTARGET );

	m_lightSpecularTex = new iex2DObj( 1280 , 720 , IEX2D_RENDERTARGET );
	m_shaderDeferredLight->SetValue( "DepthBuf" , m_depthTex );
	m_shaderDeferredLight->SetValue( "SpecularBuf" , m_specularTex );

	//ディファードポイントライト
	m_shaderDefferdPointLight = new iexShader( "DATA/Shader/DeferredPointLight.fx" );

	//ディファードシャドウ
	m_defferdShadowTex = new iex2DObj( SHADOW_SIZE , SHADOW_SIZE , IEX2D_FLOAT );
	iexSystem::GetDevice()->CreateDepthStencilSurface( SHADOW_SIZE , SHADOW_SIZE ,
		D3DFMT_D24S8 , D3DMULTISAMPLE_NONE , 0 , FALSE , &m_defferdShadowZ , NULL );
}

sceneMain::~sceneMain()
{
	delete	view;
	delete	screen;

	pBackBuffer->Release();

	delete	shader;
	delete	shader2D;

	delete	EnvTex;
	delete	hdr;

	delete	m_shadowTex;
	delete	m_depth;
	delete	m_refTex;

	delete	m_diffuseTex;
	delete	m_specularTex;
	delete	m_depthTex;
	delete	m_normalTex;
	delete	m_shaderDeferredLight;

	delete m_lightSpecularTex;
}

//*****************************************************************************************************************************
//
//		主処理
//
//*****************************************************************************************************************************
void	sceneMain::Update()
{
	//	ライト強度変更
	if ( GetKeyState( '1' ) < 0 ){ lightPower += 0.01f; if ( lightPower > 1.0f ) lightPower = 1; }
	if ( GetKeyState( '2' ) < 0 ){ lightPower -= 0.01f; if ( lightPower < 0 ) lightPower = 0; }
	//	ＨＤＲ切り替え
	if ( KEY_Get( KEY_C ) == 3 ) bHDR = !bHDR;

	//	視点
	int AxisX = KEY_GetAxisX();
	int AxisY = KEY_GetAxisY();
	int AxisX2 = KEY_GetAxisX2();
	int AxisY2 = KEY_GetAxisY2();
	if ( AxisX*AxisX < 300 * 300 ) AxisX = 0;
	if ( AxisY*AxisY < 300 * 300 ) AxisY = 0;
	if ( AxisX2*AxisX2 < 300 * 300 ) AxisX2 = 0;
	if ( AxisY2*AxisY2 < 300 * 300 ) AxisY2 = 0;
	viewAngle += AxisX * 0.00002f;
	viewHeight -= AxisY * 0.0001f;
	viewDist += AxisY2 * 0.0001f;

	ViewPos = Vector3( sinf( viewAngle )*viewDist , viewHeight , cosf( viewAngle )*viewDist );
	view->Set( ViewPos , Vector3( 0 , 1 , 0 ) );
	shader->SetValue( "ViewPos" , ViewPos );

	//	ライト
	lightAngle -= AxisX2 * 0.0001f;
	iexLight::DirLight( shader , 0 , &Vector3( sinf( lightAngle ) , -0.7f , cosf( lightAngle ) ) , lightPower , lightPower , lightPower );

	//
	obj->Animation();
	obj->SetAngle( 0 );
	obj->Update();

	//	物体設定
	eff_obj->SetPos( 0 , 0 , 0 );
	eff_obj->SetAngle( 0 );
	eff_obj->SetScale( 0.03f );
	eff_obj->Update();

	shader->SetValue( "matView" , matView );

	m_shaderDeferredLight->SetValue( "ViewPos" , ViewPos );
	m_shaderDeferredLight->SetValue( "Target" , Vector3( 0 , 0 , 0 ) );
}

//*****************************************************************************************************************************
//
//		HDR関連
//
//*****************************************************************************************************************************
void	sceneMain::RenderHDR()
{
	//	ＨＤＲ用バッファへ切り替え
	hdr->RenderTarget();
	view->SetViewport( 0 , 0 , 512 , 512 );
	view->SetProjection( 0.8f , 0.1f , 300.0f , 1280.0f / 720.0f );
	view->Activate();
	view->Clear();
	//	物体の描画
	stage->Render( shader , "specular" );
	stage2->Render( shader , "specular" );
	obj->Render( shader , "specular" );

	//	ぼかし
	DWORD	color = ((8 * 6) << 24) | 0xFFFFFF;
	for ( int i = 4 ; i<24 ; i += 4 )
	{
		hdr->Render( -i , 0 , 512 , 512 , 0 , 0 , 512 , 512 , RS_ADD , color );
		hdr->Render( i , 0 , 512 , 512 , 0 , 0 , 512 , 512 , RS_ADD , color );
		hdr->Render( 0 , -i , 512 , 512 , 0 , 0 , 512 , 512 , RS_ADD , color );
		hdr->Render( 0 , i , 512 , 512 , 0 , 0 , 512 , 512 , RS_ADD , color );
		color -= 8 << 24;
	}

}

//*****************************************************************************************************************************
//
//		シャドーマップ関連
//
//*****************************************************************************************************************************
void sceneMain::RenderShadowBuffer()
{
	m_shadowTex->RenderTarget();
	//Zバッファ設定
	Surface* orgZ;

	iexSystem::GetDevice()->GetDepthStencilSurface( &orgZ );
	iexSystem::GetDevice()->SetDepthStencilSurface( m_shadowZ );

	//ライト方向
	Vector3 dir( sinf( lightAngle ) , -1.7f , cosf( lightAngle ) );
	dir.Normalize();

	//シャドウ作成
	Vector3 target( 0 , 0 , 0 );
	Vector3 pos = target - dir * 10;
	Vector3 up( 0 , 1 , 0 );

	//視点とライト位置へ
	D3DXMATRIX shadowMat , work;
	LookAtLH( shadowMat , pos , target , up );				//行列にカメラ情報を与えている
	D3DXMatrixOrthoLH( &work , 20 , 20 , 1 , 30.0f );	//平行投影行列（範囲ｘ100）
	shadowMat *= work;	//ビュー行列に平行行列をかける

	shader->SetValue( "ShadowProjection" , &shadowMat );

	D3DVIEWPORT9 vp = { 0,0,SHADOW_SIZE,SHADOW_SIZE,0,1.0f };
	iexSystem::GetDevice()->SetViewport( &vp );
	//レンダリング
	iexSystem::GetDevice()->Clear( 0 , NULL , D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER , 0xFFFFFFFF , 1.0f , 0 );

	//描画
	stage2->Render( shader , "ShadowBuf" );
	obj->Render( shader , "ShadowBuf" );

	//作ったシャドウテクスチャをシェーダーにセット
	shader->SetValue( "ShadowMap" , m_shadowTex );

	//レンダーターゲットの復元
	iexSystem::GetDevice()->SetRenderTarget( 0 , pBackBuffer );
	iexSystem::GetDevice()->SetDepthStencilSurface( orgZ );
}

void sceneMain::DefferdRenderShadowBuffer()
{
	m_defferdShadowTex->RenderTarget();
	//Zバッファ設定
	Surface* orgZ;
}


//*****************************************************************************************************************************
//
//		深海度関連
//
//*****************************************************************************************************************************
void sceneMain::RenderDepth()
{
	m_depth->RenderTarget();
	view->Activate();
	view->Clear();

	//物体描画
	stage->Render( shader , "depth" );
	obj->Render( shader , "depth" );

	shader2D->SetValue( "DepthTex" , m_depth );
}


//*****************************************************************************************************************************
//
//		深海度関連
//
//*****************************************************************************************************************************
void sceneMain::RenderReflect()
{
	//m_refTex->RenderTarget();
	//view->Activate();
	//view->Clear();

	////物体描画
	//stage2->Render( shader , "Reflect" );
	//obj->Render( shader , "Reflect" );

	//shader->SetValue( "RefMap" , m_refTex );

	m_refTex->RenderTarget();
	//Zバッファ設定
	Surface* orgZ;

	iexSystem::GetDevice()->GetDepthStencilSurface( &orgZ );
	iexSystem::GetDevice()->SetDepthStencilSurface( m_shadowZ );

	D3DVIEWPORT9 vp = { 0,0,SHADOW_SIZE,SHADOW_SIZE,0,1.0f };
	iexSystem::GetDevice()->SetViewport( &vp );
	//レンダリング
	iexSystem::GetDevice()->Clear( 0 , NULL , D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER , 0xFFFFFFFF , 1.0f , 0 );

	//描画
	stage2->Render( shader , "Reflect" );
	obj->Render( shader , "Reflect" );

	//作ったシャドウテクスチャをシェーダーにセット
	shader->SetValue( "RefMap" , m_refTex );

	//レンダーターゲットの復元
	iexSystem::GetDevice()->SetRenderTarget( 0 , pBackBuffer );
	iexSystem::GetDevice()->SetDepthStencilSurface( orgZ );
}

//*****************************************************************************************************************************
//
//		平行光処理関数
//
//*****************************************************************************************************************************
void sceneMain::DirectionLight( const Vector3 &dir , const Vector3 &color )
{
	Matrix mat = matView;
	Vector3 lightDirectiont; //ライト方向
	//カメラ空間変換
	lightDirectiont.x = dir.x*mat._11 + dir.y*mat._21 + dir.z*mat._31;
	lightDirectiont.y = dir.x*mat._12 + dir.y*mat._22 + dir.z*mat._32;
	lightDirectiont.z = dir.x*mat._13 + dir.y*mat._23 + dir.z*mat._33;
	lightDirectiont.Normalize();
	//シェーダー設定
	m_shaderDeferredLight->SetValue( "LightVec" , lightDirectiont );
	m_shaderDeferredLight->SetValue( "LightColor" , (Vector3) color );
	//レンダリング
	m_normalTex->Render( m_shaderDeferredLight , "dirlightspecular" );
}

//*****************************************************************************************************************************
//
//		点光源関数
//
//*****************************************************************************************************************************
void sceneMain::PointLight( const Vector3& pos , const Vector3& color , float range )
{
	//変換行列設定
	Matrix invProjection;
	D3DXMatrixInverse( &invProjection , NULL , &matProjection );
	m_shaderDeferredLight->SetValue( "InverseProjection" , invProjection );

	Matrix mat = matView;
	//カメラ空間変換
	Vector3 lightPos;
	lightPos.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41; 
	lightPos.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
	lightPos.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;

	//シェーダー設定
	m_shaderDeferredLight->SetValue( "LightPos" , lightPos );
	m_shaderDeferredLight->SetValue( "LightColor" , (Vector3) color );
	m_shaderDeferredLight->SetValue( "LightLange" , range );

	//レンダリング
	m_normalTex->Render( m_shaderDeferredLight , "dirPointLight" );
}


//*****************************************************************************************************************************
//
//		スポットライト関数
//
//*****************************************************************************************************************************

void sceneMain::SpotLight( const Vector3& pos , const Vector3& dir , const Vector3& color , float inner , float outer )
{
	//変換行列設定
	Matrix invProjection;
	D3DXMatrixInverse( &invProjection , NULL , &matProjection );
	m_shaderDeferredLight->SetValue( "InverseProjection" , invProjection );

	Matrix mat = matView;
	Vector3 lightPos;
	//カメラ空間変換
	lightPos.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
	lightPos.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
	lightPos.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;

	Vector3 lightDir;
	//カメラ空間変換
	lightDir.x = dir.x*mat._11 + dir.y*mat._21 + dir.z*mat._31;
	lightDir.y = dir.x*mat._12 + dir.y*mat._22 + dir.z*mat._32;
	lightDir.z = dir.x*mat._13 + dir.y*mat._23 + dir.z*mat._33;

	//シェーダー設定
	m_shaderDeferredLight->SetValue( "SpotLightPos" , lightPos );
	m_shaderDeferredLight->SetValue( "SpotLightDir" , lightDir );
	m_shaderDeferredLight->SetValue( "SpotLightColor" , (Vector3)color );
	//m_shaderDeferredLight->SetValue( "inner" , cosf( inner ) );
	//m_shaderDeferredLight->SetValue( "outer" , cosf( outer ) );
	//レンダリング
	m_normalTex->Render( m_shaderDeferredLight , "spotLight" );
}



//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************
void	sceneMain::Render()
{
	//	ビュー設定
	view->SetViewport();
	view->SetProjection( 0.8f , 1.0f , 80.0f );
	view->Activate();
	view->Clear( 0x505050 , 1 );

	//変換行列設定
	Matrix inverseProjection;
	D3DXMatrixInverse( &inverseProjection , NULL , &matProjection );
	m_shaderDeferredLight->SetValue( "InverseProjection" , inverseProjection );

	//ディファード設定
	m_diffuseTex->RenderTarget();
	m_specularTex->RenderTarget( 1 );
	m_depthTex->RenderTarget( 2 );
	m_normalTex->RenderTarget( 3 );
	view->Clear();

	////	物体描画
	stage->Render( shader , "differed" );
	stage2->Render( shader , "differed" );
	obj->Render( shader , "differed" );

	//ライト用バッファに切り替え
	m_lightTex->RenderTarget();
	m_lightSpecularTex->RenderTarget( 1 );
	//iexSystem::Device->SetRenderTarget( 1 , NULL );
	iexSystem::Device->SetRenderTarget( 2 , NULL );
	iexSystem::Device->SetRenderTarget( 3 , NULL );
	view->Clear(0x000000);

	//ライト描画
	DirectionLight( Vector3( 0 , 0.5f , 1 ) , Vector3( 1 , 1 , 1 ) );
	DirectionLight( Vector3( -1 , -1 , 0 ) , Vector3( 1 , 1 , 1 ) );
	//for ( int i = 0 ; i < 100; i++ ){
	//	PointLight( Vector3( rand()%10 - 5 , 2 ,  rand()%10-5 ) , Vector3( rand() % 10 *0.01f , rand() % 10 * 0.01f , rand() % 10 * 0.01f ) , 5.0f );
	//}
	PointLight( Vector3( 0 , 0 , 0 ) , Vector3( 1 , 0 , 0 ) , 0.5f );
	PointLight( Vector3( 0 , 3 , 2 ) , Vector3( 0 , 0 , 1 ) , 5.0f );
	//SpotLight( Vector3( 0 , 4 , 0 ) , Vector3( 0 , -1 , 0 ) , Vector3( 0 , 0.5f , 0 ) , 0.1f , 0.3f );

	iexSystem::GetDevice()->SetRenderTarget( 0 , pBackBuffer );

	m_diffuseTex->Render();
	m_lightTex->Render( 0 , 0 , 1280 , 720 , 0 , 0 , 1280 , 720 , RS_MUL , 0xFFFFFFFF );
	m_lightSpecularTex->Render( 0 , 0 , 1280 , 720 , 0 , 0 , 1280 , 720 , RS_ADD , 0xFFFFFFFF );

	m_diffuseTex->Render( 0 , 0 , 320 , 180 , 0 , 0 , 1280 , 720 ,shader2D , "copy");
	m_specularTex->Render( 320 , 0 , 320 , 180 , 0 , 0 , 1280 , 720 , shader2D , "copy" );
	m_depthTex->Render( 640 , 0 , 320 , 180 , 0 , 0 , 1280 , 720 , shader2D , "copy" );
	m_normalTex->Render( 960 , 0 , 320 , 180 , 0 , 0 , 1280 , 720 , shader2D , "copy" );
	m_lightTex->Render( 0 , 180 , 320 , 180 , 0 , 0 , 1280 , 720 , shader2D , "copy" );
	m_lightSpecularTex->Render( 960 , 180 , 320 , 180 , 0 , 0 , 1280 , 720 , shader2D , "copy" );
}

