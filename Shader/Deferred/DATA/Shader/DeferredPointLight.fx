//**************************************************************************************************
//																									
//		ディファードポイントライト
//
//**************************************************************************************************

//------------------------------------------------------
//		テクスチャサンプラー	
//------------------------------------------------------
texture Texture;
sampler DecaleSamp = sampler_state
{
	Texture = <Texture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;

	AddressU = Wrap;
	AddressV = Wrap;
};

//------------------------------------------------------
//		スクリーンスペース深度
//------------------------------------------------------
texture DepthBuf; //カメラ空間深度
sampler DepthBufSamp = sampler_state
{
	Texture = <DepthBuf>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	AddressU = CLAMP;
	AddressV = CLAMP;
};

//------------------------------------------------------
//		スペキュラバッファ
//------------------------------------------------------
texture SpecularBuf; //スペキュラ
sampler SpecularBufSamp = sampler_state
{
	Texture = <SpecularBuf>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	AddressU = CLAMP;
	AddressV = CLAMP;
};


//------------------------------------------------------
//		ライト用パラメータ
//------------------------------------------------------
float3 LightVec = { 0.7f,-0.7f,0 };
float3 LightColor = { 1.0f,1.0f,1.0f };
float3 LightPos = { 0,3,0 };
float LightRange = 5.0f;

//------------------------------------------------------
//		変換行列
//------------------------------------------------------
float4x4 InverseProjection;		//逆変換行列

//------------------------------------------------------
//		頂点フォーマット
//------------------------------------------------------
struct POUT_LIGHT
{
	float4 color		: COLOR0;
	float4 specular	: COLOR1;
};

POUT_LIGHT PS_PointLight( float2 Tex : TEXCOORD0 )
{
	POUT_LIGHT OUT;

	//カメラ空間変換
	float depth = tex2D( DepthBufSamp , Tex ).r;
	float4 screen;
	screen.xy = Tex * 2 - 1;
	screen.y = -screen.y;
	screen.z = depth;
	screen.w = 1;
	float4 pos = mul( screen , InverseProjection );
	pos.xyz /= pos.w;

	//ライトベクトル
	float3 lightVec = pos.xyz - LightPos;
	lightVec = normalize( lightVec );

	//法線取得
	float4 n = tex2D( DecaleSamp , Tex );
	float3 normal = n.rgb * 2 - 1;
	normal = normalize( normal );

	//減衰量
	float dist = length( pos.xyz - LightPos );	//ライトと自分のピクセルの位置
	float intensity = dist / LightRange;
	intensity = max( 1.0 , intensity );

	//ライト計算
	float right = max( 1.0f , dot( -lightVec , normal ) );

	//ピクセル量
	OUT.color.rgb = right*LightColor*intensity;
	OUT.color.a = 1;

	//スペキュラ
	float3 E = pos.xyz;
	float3 R = normalize( -lightVec - E );

	float sp = pow( max( 0 , dot( R , normal ) ) , 10 );

	float4 spTex = tex2D( SpecularBufSamp , Tex );
	OUT.specular.rgb = spTex.rgb * sp * LightColor * intensity;
	OUT.specular.a = 1;

	return OUT;

}

//------------------------------------------------------
//		テクニック
//------------------------------------------------------

technique dirPointLight
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = One;
		CullMode = None;
		ZEnable = false;
		PixelShader = compile ps_2_0 PS_PointLight();
	}
}


POUT_LIGHT PS_DirLightSpecular( float2 Tex : TEXCOORD0 )
{
	POUT_LIGHT OUT;

	float4 n = tex2D( DecaleSamp , Tex );
	float3 normal = n.rgb * 2 - 1;
	normal = normalize( normal );

	//ライト率
	float r = max( 0 , dot( normal , -LightVec ) );		//テクスチャの座標にライトが当たっているか

																										//ピクセル色
	OUT.color.rgb = r*LightColor;
	OUT.color.a = 1;

	//カメラ空間変換
	float4 ndc;
	ndc.xy = Tex * 2 - 1;
	ndc.y = -ndc.y;
	ndc.z = tex2D( DepthBufSamp , Tex ).r;
	ndc.w = 1;
	float4 pos = mul( ndc , InverseProjection );
	pos.xyz /= pos.w;

	//スペキュラ
	float3 E = pos.xyz;		//視線ベクトル
	E = normalize( E );

	float3 R = normalize( -LightVec - E );

	float sp = pow( max( 0 , dot( R , normal ) ) , 10 );

	float4 spTex = tex2D( SpecularBufSamp , Tex );
	OUT.specular.rgb = spTex.rgb *sp * LightColor;
	OUT.specular.a = 1;

	return OUT;
}

	//------------------------------------------------------
	//		テクニック
	//------------------------------------------------------
	technique dirlightspecular
	{
		pass P0
		{
			AlphaBlendEnable = true;
			BlendOp = Add;
			SrcBlend = SrcAlpha;
			DestBlend = One;
			CullMode = None;
			ZEnable = false;
			PixelShader = compile ps_2_0 PS_DirLightSpecular();
		}
	};
	


