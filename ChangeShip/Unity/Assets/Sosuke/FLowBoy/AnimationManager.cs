﻿using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour {
    bool jump;

	// Use this for initialization
	void Start () {
        jump = false;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
        GetComponent<Animator>().SetBool("Jump", jump);
	}
}
