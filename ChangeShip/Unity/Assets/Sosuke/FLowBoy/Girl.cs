﻿using UnityEngine;
using System.Collections;

using System;
//***********************************************
//製作者：　山中　壮介
//***********************************************


/// キャラの移動・回転・モード変数を管理
/// キャラの行動はGIRL_MODE、ジャンプはJUMP_MODEで管理してます。
/// ゲッターセッターは一番下に記述してます。

public class Girl : MonoBehaviour
{
	/*ガール行動管理用列挙型*/
	public enum GIRL_MODE
	{
		MOVE,   //移動
		JUMP,   //ジャンプ
		VORTEX, //渦に沈む
		SHELL,  //砲弾に当たった時
		ROCK,
		CLEAR, //クリア
	}
	/*ジャンプ管理用列挙型*/
	public enum JUMP_MODE
	{
		MOVE,
		JUMP,
		STAY,
	}

	/*沈没管理用列挙型*/
	public enum SINK_MODE
	{
		SINK_BEFORE,
		SINK,
		DETHE ,				//死んだとき
	}


	// Use this for initialization
	private Quaternion start_rotation;//初期angle
	private Vector3 start_pos;        //初期位置
	private Vector3 vec;              //速度
	private Vector3 line_vec;         //補間
	private Vector3 circle_vec;
	private Vector3 hit_vec;          //当たったギミックの位置
	private float elapsedTime;        //経過時間
	private float t;                  //媒介変数
									  //public AnimatorStateInfo animInfo;       //アニメーション変数
	public bool animInfo;
	AnimatorStateInfo Sink;             //沈むアニメーション


	public Vector3 b_vec;             //速度保存 
	private Vector3 splashdown_pos;  //着水位置

	/*モード管理用*/
	public JUMP_MODE jump_mode;
	public GIRL_MODE girl_mode;    //ガール行動管理
	public SINK_MODE sink_mode;

	/*ゲームオーバー用変数*/
	private GameObject go;

	public bool start;

	GameSE gameSE;
	public ParticleSystem fireEffect;
	public GameObject ship;
	private float shellTimeMax = 2.0f;		//撃たれてからGameOverを出すまで
	private float shellTime;                //撃たれてからGameOverを出すまでをカウントする
	private bool  shellFlag;                //撃たれたから一度だけ通ってほしい

	private float deathCountMax = 1.5f;
	private float deathCount;

	void Start()
	{
		//初期位置
		start_pos = transform.position;
		//初期向き
		start_rotation = transform.rotation;
		//初速度
		vec = b_vec;
		//開始フラグ
		start = false;
		//モード設定
		jump_mode = JUMP_MODE.MOVE;
		go = GameObject.Find( "Canvas" );
		//SE取得
		gameSE = GameObject.Find( "Main Camera" ).GetComponent<GameSE>();
		shellTime = 0;
		deathCount = 0;
    }

    //------------------------------------------
    //              線形補間
    //------------------------------------------

    Vector3 Liner_interpolation(Vector3 start, Vector3 end)
    {
        elapsedTime += Time.deltaTime;   // 前フレームからの経過時間

        t = elapsedTime / 1.0f;   // 時間を媒介変数に
        if (t > 1.0f) t = 1.0f;    // クランプ
        float rate = t * t * (3.0f - 2.0f * t);   // 3次関数補間値に変換
        Vector3 p = start * (1.0f - rate) + end * rate;   // いわゆるLerp
        return p;
    }


    //------------------------------------------
    //               移動関数
    //------------------------------------------
    void Move()
    {
        //今向いてる方向の値を取得
        Vector3 playerDir = b_vec;   //値を保存
        //向きたい方角をQuaternion型に直す
        Quaternion q = Quaternion.LookRotation(playerDir);
        //プレイヤーの角度を変える
        transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 360.0f * Time.deltaTime);
    }

    //------------------------------------------
    //         ジャンプ関数(アニメーション)
    //------------------------------------------
    public void Jump()
    {
        switch (jump_mode)
        {
            case JUMP_MODE.MOVE:
                line_vec = Liner_interpolation(this.transform.position, hit_vec);  //補間率の計算
                this.transform.position = line_vec;                                 //移動の補間
                //アニメーション再生
                gameObject.transform.FindChild("player").GetComponent<Animator>().SetBool("Jump", true);

                //補間が終われば
                if (t >= 1.0f)
                {
                    t = 0;
                    elapsedTime = 0;      //時間と初期化
                    line_vec = Vector3.zero;
                    jump_mode = JUMP_MODE.JUMP; //次のモードへ
                }
                break;

            case JUMP_MODE.JUMP:
                ////アニメーターの状態の代入
                //animInfo = gameObject.transform.FindChild("player").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);

                //アニメーションが終わったら
                if (animInfo == true)
                {
                    //アニメーション停止
                    gameObject.transform.FindChild("player").GetComponent<Animator>().enabled = false;

                    //子を参照
                    foreach (Transform child in this.transform)
                    {
                        if (child.name == ("Splashdown_pos"))
                        {
                            splashdown_pos = child.transform.position;    //位置座標取得
                            //当たり判定ＯＦＦ
                            this.transform.GetComponent<BoxCollider>().enabled = false;
                            jump_mode = JUMP_MODE.STAY;
                        }
                    }
                }
                break;

            case JUMP_MODE.STAY:
                line_vec = Liner_interpolation(this.transform.position, splashdown_pos);
                this.transform.position = line_vec;        //補間

                if (t >= 1.0f)
                {
                    //アニメーション関連
                    gameObject.transform.FindChild("player").GetComponent<Animator>().enabled = true;
                    gameObject.transform.FindChild("player").GetComponent<Animator>().SetBool("Jump", false);
                    animInfo = false;
                    //あたり判定ＯＮ
                    this.transform.GetComponent<BoxCollider>().enabled = true;
                    //移動値初期化
                    line_vec = Vector3.zero;
                }
                break;
        }
    }

	//------------------------------------------
	//         渦に当たって沈む
	//------------------------------------------
	public void Sink_Vortex()
	{
		//自分自身を削除
		transform.eulerAngles -= new Vector3( 0 , 20f , 0 );
		transform.localScale -= new Vector3( 0.015f , 0.015f , 0.015f );
		b_vec = Vector3.zero;

		if( transform.localScale.x < 0 ) {
			gameSE.WaterSound();
			GameModeManager.SetGameMode( GameModeManager.GAMEMODE.GAMEOVER );
			go.GetComponent<UI_Controltest>().OnGameOver();
			Destroy( this.gameObject );		
		}
	}

	//------------------------------------------
	//         岩に当たって沈む
	//------------------------------------------
	public void Sink_Rock()
	{
		switch( sink_mode )
		{
			case SINK_MODE.SINK_BEFORE:
				//移動値を0に
				b_vec = Vector3.zero;
				//アニメーション変更
				gameObject.transform.FindChild( "player" ).GetComponent<Animator>().SetBool( "Sink" , true );
				sink_mode = SINK_MODE.SINK;
				break;

			case SINK_MODE.SINK:
				deathCount += Time.deltaTime;
				if (deathCount > deathCountMax)
				{
					sink_mode = SINK_MODE.DETHE;
				}
				break;
			case SINK_MODE.DETHE:
				go.GetComponent<UI_Controltest>().OnGameOver();
				gameSE.HitSound();
				break;

		}//switch
	}

	//------------------------------------------
	//         砲弾に当たって沈む
	//------------------------------------------
	public void Sink_Shell()
	{
		//自分自身を削除
		if (shellFlag == false)
		{
			shellFlag = true;
			fireEffect.Emit( 20 );
			ship.SetActive( false );
		}
	
		shellTime += Time.deltaTime;
		if( shellTime > shellTimeMax )
		{
			gameSE.WaterSound();
			go.GetComponent<UI_Controltest>().OnGameOver();
			Destroy( this.gameObject );
		}
	}

    //------------------------------------------
    //         クリアしたとき
    //------------------------------------------
    public void ClearPlayer()
    {
        b_vec = new Vector3(0, 0, 0);
		GameModeManager.SetGameMode( GameModeManager.GAMEMODE.GAMECLEAR );
    }

    //------------------------------------------
    //         海賊船と接触
    //------------------------------------------
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Pirate")
        {
            //a = 1;
            girl_mode = GIRL_MODE.ROCK;
        }

        if (collider.gameObject.tag == "Stage")
        { //ゴールとのあたり判定
            girl_mode = GIRL_MODE.CLEAR;
        }
    }

    //------------------------------------------
    //              更新
    //------------------------------------------
    void Update()
    {
		//ゲーム状の速度管理判定
		if( GameModeManager.GetGameMode() != GameModeManager.GAMEMODE.MAIN ) return;  
        if (start)
        {
			switch (girl_mode)
            {
                case GIRL_MODE.MOVE:    //移動
                    Move();
                    //座標更新
                    transform.position += b_vec * Time.deltaTime * 50;
                    break;

                case GIRL_MODE.JUMP:    //ジャンプ
                    Jump();
                    break;

                case GIRL_MODE.VORTEX:  //渦
                    Sink_Vortex();
                    break;

                case GIRL_MODE.SHELL:   //砲弾
					Sink_Shell();
                    break;

                case GIRL_MODE.ROCK:    //岩に当たった時
                    Sink_Rock();
                    break;

                case GIRL_MODE.CLEAR:   //クリアしたとき
                    ClearPlayer();
                    break;
            }
        }
    }



    //流れを受けた時の関数
    public void SetVec(Vector3 vec) { b_vec = vec; }
    //当たった竜巻の座標をhit_posにセット
    public void SetPos(Vector3 pos) { hit_vec = pos; }
    //外部から状態を変えるとき用
    public void SetMode(int mode) { girl_mode = (GIRL_MODE)mode; }
	public void SetStart( bool org )
	{
		start = org;
		GameModeManager.SetGameMode( GameModeManager.GAMEMODE.MAIN );
	}
    public bool GetStart() { return start; }
    public GIRL_MODE GetMode() { return girl_mode; }
}

