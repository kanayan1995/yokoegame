﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour
{

    GameObject player;
		GameSE gameSE;

    // Use this for initialization
    void Start()
    {
		//SE取得
		gameSE = GameObject.Find( "Main Camera" ).GetComponent<GameSE>();
	}

    //------------------------------------------
    //              モードセット変更
    //------------------------------------------
    public void JUMP_SET()
    {
        player = this.transform.parent.gameObject;
        player.GetComponent<Girl>().jump_mode = Girl.JUMP_MODE.MOVE;
        player.GetComponent<Girl>().girl_mode = Girl.GIRL_MODE.MOVE;
    }

    public void NEXTJUMP()
    {
        player = this.transform.parent.gameObject;
        player.GetComponent<Girl>().animInfo = true;
    }

    //------------------------------------------
    //              消去
    //------------------------------------------
    void Delete()
    {
        player = this.transform.parent.gameObject;
				gameSE.WaterSound();
        Destroy(player);
    }
}
