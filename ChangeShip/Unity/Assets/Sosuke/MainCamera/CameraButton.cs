﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//***********************************************
//製作者：　山中　壮介
//***********************************************


/// <summary>
/// 
/// ボタンに触れた時のスクリプト
/// 触れた場合、親のオブジェクトの関数
/// SetInteractive(string name, bool b)
/// を参照するように組んでます
/// ちなみにボタンの有効までの時間決めたい場合はここの
/// count変数の値をいじるとok
/// 
/// </summary>

public class CameraButton : MonoBehaviour {

    private GameObject parent;
    private bool flag_right;
    private bool flag_left;

    public float count;
    
    void Start()
    {
        //親取得
        parent = transform.root.gameObject;
        flag_right = false;
        flag_left = false;
        
    }

    //左ボタンクリックしたときの処理
    public void OnLeftClick() 
    {
        flag_left = true;
        parent.GetComponent<SwichButton>().SetInteractive("Left",false);
        Camera.main.GetComponent<ChangeCamera>().LeftTarget(); //一行追加 小松
    }

    //右ボタンクリックしたときの処理
    public void OnRightClick()
    {
        flag_right = true;
        parent.GetComponent<SwichButton>().SetInteractive("Right", false);
        Camera.main.GetComponent<ChangeCamera>().RightTarget(); //一行追加 小松
    }


    void Update()
    {
        //左ボタン処理
        if(flag_left ==true )
        {
            count += Time.deltaTime;

            if(count>1.0f)
            {
                flag_left = false;
                parent.GetComponent<SwichButton>().SetInteractive("Left", true);
                count = 0;
            }

        }
        //右ボタン処理
        if (flag_right == true)
        {
            count += Time.deltaTime;

            if (count > 1.0f)
            {
                flag_right = false;
                parent.GetComponent<SwichButton>().SetInteractive("Right", true);
                count = 0;
            }

        }

    }

}
