﻿using UnityEngine;
using System.Collections;

//***********************************
//  製作者：山中　壮介
//***********************************

/// <summary>
/// 
///  カメラのタッチ操作を管理するスクリプト
///  一本指でスワイプ操作
///  二本指でピンチ操作をするように組んでます
///  ※なおまだ改良の余地ありなので適用はしてません
/// 
/// </summary>


public class MainCamera : MonoBehaviour
{

    const float ROTATION_SPEED = 1.0f; //回転速度
    const float ZOOM_SPEED = 500.0f;    //ズーム速度

    public Transform target;
    private Vector3 original;
    private float angle_x = 0.0f;
    private float angle_y = 0.0f;

    private bool isDragging = false;    //ドラッグ中の操作
    private bool isDragged = false;     //ドラッグ後の操作
    private bool isPinched = false;     //ピンチ操作
    private float interval = 0.0f;      //二点間の距離

    //---------------------------
    //   円の情報
    //---------------------------
    //private Vector2 p;              //円の中心座標
    //private float rot = 0;          //円の角度（0～360）
    //private float rot_speed = 5;    //円の角度変化量
    //private float radius = 30;      //円の半径


    //****************************************************
    //
    //                   初期化
    //
    //***************************************************
    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        angle_x = angles.y;
        angle_y = angles.x;
        original = transform.position;
    }


    /*Input.touchCount…タッチ数、そのフレームでは変化しない*/


    //****************************************************
    //
    //                      更新
    //
    //*****************************************************
    void Update()
    {
        //----------------------------
        //      ドラッグ操作
        //----------------------------
        if (Input.touchCount == 1 && !isPinched)
        {
            /*マウスが移動していた場合の処理（今回は指）*/
            if (Event.current.type == EventType.MouseDrag)
            {
                isDragging = true;
                isDragged = true;
            }
            /*そうじゃなければ*/
            else
            {
                isDragging = false;
                /*タップして離れた時*/
                if (Event.current.type == EventType.MouseUp)
                {
                    /*ドラッグ中でなければ現在のマウスの位置をTap関数に送る*/
                    if (!isDragged) Tap(Input.mousePosition);
                    else isDragged = false;
                }
            }
        }
        else if (Input.touchCount == 0) { isPinched = false; }

        //----------------------------
        //      スワイプ操作
        //----------------------------
        if (Input.touchCount == 2)
        {
            /*画面に指が一本でも触れた時*/
            if (Input.touches[0].phase == TouchPhase.Began || Input.touches[1].phase == TouchPhase.Began)
            {
                /*タッチされた二点間の距離を求めてintervalに代入*/
                interval = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
            }
            /*タッチされた二点間の距離を求めてtmpintervalに代入*/
            float tmpInterval = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
            //座標変換
            original.z += (tmpInterval - interval) / ZOOM_SPEED;


            if (original.z > 0) original.z = 0;
            interval = tmpInterval;

            transform.position =  transform.rotation * original;
            isPinched = true;
        }

        /*ドラッグ中なら処理*/
        else if (isDragging)
        {
            angle_x += Input.GetAxis("Mouse X") * ROTATION_SPEED;
            angle_y -= Input.GetAxis("Mouse Y") * ROTATION_SPEED;


            transform.rotation = Quaternion.Euler(angle_y, angle_x, 0);
            transform.position = transform.rotation * original;
        }
        //this.transform.LookAt(target);
    }

    private void Tap(Vector3 point)
    {
        // タップ時の処理を記述
    }
}