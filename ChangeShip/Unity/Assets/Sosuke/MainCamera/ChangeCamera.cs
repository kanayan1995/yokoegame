﻿using UnityEngine;
using System.Collections;

//***************************************
// 製作者：山中　壮介
//***************************************

/// <summary>
/// 
/// カメラの位置変更の動作を管理してるスクリプト
/// それぞれの位置に空のオブジェクトを配置して
/// 自分のいた位置と目標地点で線形補間させてます
/// 
/// </summary>


public class ChangeCamera : MonoBehaviour
{
    //------------------------
    //オブジェクト宣言
    //------------------------
    public GameObject right;   //右
    public GameObject left;    //左
    public GameObject back;    //後
    public GameObject front;   //前
    private float elapsedTime;  //ゲーム内時間の変数

    float t;                 //媒介変数
    Vector3 vec;             //移動量
    Vector3 start_pos;       //現在位置

    private float waite_time; 

    //------------------------
    //カメラの状態宣言
    //------------------------
    public enum CAMERA_Mode
    {
        RIGHT,  //右
        LEFT,   //左
        BACK,   //後
        FRONT,  //前
        START   //初期位置
    }
    //------------------------
    //     モード変数宣言
    //------------------------
   private CAMERA_Mode mode;
   private CAMERA_Mode right_mode;
   private CAMERA_Mode left_mode;
   private bool flag;

    //------------------------
    //        初期化
    //------------------------
    void Start()
    {
        mode = CAMERA_Mode.START;   //カメラ初期位置
      //  waite_time = Time.deltaTime;

    }

    /*線形補間(開始地点、終点)*/
    Vector3 Liner_interpolation(Vector3 start, Vector3 end)
    {
        elapsedTime += Time.deltaTime;   // 経過時間

          t = elapsedTime / 1.0f;   // 時間を媒介変数に
        if (t > 1.0f)
            t = 1.0f;    // クランプ
    float rate = t * t * (3.0f - 2.0f * t);   // 3次関数補間値に変換
    Vector3 p = start * (1.0f - rate) + end * rate;   // いわゆるLerp
        return p;
    }

    //------------------------
    //    変数初期化関数
    //------------------------
    public void Variable_ZeroSet()
    {
        t = 0;
        elapsedTime = 0;
        vec = Vector3.zero;
        flag = true;
        
    }

    //-------------------------------
    //ボタンによるターゲットチェンジ
    //-------------------------------
   void ModeChange()
    {
        if (mode == CAMERA_Mode.START)
        {
            right_mode = CAMERA_Mode.RIGHT;
            left_mode = CAMERA_Mode.LEFT;
        }

       if(mode==CAMERA_Mode.RIGHT)
       {
           right_mode = CAMERA_Mode.FRONT;
           left_mode = CAMERA_Mode.BACK;
       }

       if(mode==CAMERA_Mode.LEFT)
       {
           right_mode = CAMERA_Mode.BACK;
           left_mode = CAMERA_Mode.FRONT;
       }

       if(mode==CAMERA_Mode.BACK)
       {
           right_mode = CAMERA_Mode.RIGHT;
           left_mode = CAMERA_Mode.LEFT;
       }

       if(mode==CAMERA_Mode.FRONT)
       {
           right_mode = CAMERA_Mode.LEFT;
           left_mode = CAMERA_Mode.RIGHT;
       }
    }

    
    //------------------------
    //    右ボタンタッチ
    //------------------------
    public void RightTarget()
    {
        Variable_ZeroSet();
        ModeChange();
        mode = right_mode;
        start_pos = this.transform.position;
    }

    //------------------------
    //    左ボタンタッチ
    //------------------------
    public void LeftTarget()
    {
            Variable_ZeroSet();
            ModeChange();
            mode = left_mode;
            start_pos = this.transform.position;
     }

    //------------------------
    //         更新
    //------------------------
    void Update()
    {
        //注視点
        //;this.transform.LookAt(new Vector3(0, 0, 0));
        //カメラの座標更新
        switch (mode)
        {
             case CAMERA_Mode.START:    //初期位置
                //初期値維持のための空欄
                //何もしてないです
                break;

            case CAMERA_Mode.RIGHT:     //右方向に変更
                vec = Liner_interpolation(start_pos, right.transform.position);
                  this.transform.position = vec;
                break;

            case CAMERA_Mode.LEFT:      //左に変更
                vec = Liner_interpolation(start_pos, left.transform.position);
                  this.transform.position = vec;
                break;

            case CAMERA_Mode.BACK:      //後ろに変更
                vec = Liner_interpolation(start_pos, back.transform.position);
                this.transform.position = vec;
                break;

            case CAMERA_Mode.FRONT:     //前に変更
                vec = Liner_interpolation(start_pos, front.transform.position);
                this.transform.position = vec; 
                break;
         }
        }//switch
    }
