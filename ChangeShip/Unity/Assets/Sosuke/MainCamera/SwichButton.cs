﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//***********************************************
//製作者：　山中　壮介
//***********************************************

/// <summary>
/// 
/// ボタンの表示・非表示を決めるスクリプト
/// CameraButtonスクリプト（子オブジェクト）から参照してます
/// 
/// </summary>

public class SwichButton : MonoBehaviour {

    //ボタンの表示非表示設定
    public void SetInteractive(string name, bool b)
    {
        foreach (Transform child in this.transform)
        {
            //子の要素を辿る
            if (child.name == name)
            {
                //Buttonのコンポーネント取得
                Button btn = child.GetComponent<Button>();
                //有効・無効フラグ設定
                btn.interactable = b;
                return;
            }
        }
    }
}
