﻿using UnityEngine;
using System.Collections;

public class Rock_Collision : MonoBehaviour {


    private GameObject player;
	// Use this for initialization
	void Start () {
	
        player = GameObject.Find("ship_player");
	}
	


    void OnTriggerEnter(Collider collider)
    {
        switch(collider.tag)
        { 
            case "Player":
               
            //当たったら「沈む」に変更
            collider.GetComponent<Girl>().SetMode(4);
            break;

            case "Pirate":
            collider.GetComponent<Pirate>().SetMode(4);

            break;
        }
    }

}
