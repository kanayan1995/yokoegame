﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

    // Use this for initialization
    public Gimmick My_wind;

    public void SetActive(bool arg) { gameObject.SetActive(arg); }

    void Start () {
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        Update_Rotate();
	}

    public void Update_Rotate()
    {
        transform.rotation = My_wind.GetRotate();
    }
}
