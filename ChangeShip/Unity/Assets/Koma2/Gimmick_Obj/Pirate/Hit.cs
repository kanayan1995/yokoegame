﻿using UnityEngine;
using System.Collections;

public class Hit : MonoBehaviour
{

    // Use this for initialization
    public GameObject parent;
    void Start()
    {
        transform.SetParent(parent.transform);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            transform.parent.GetComponent<Pirate>().Hit_Player();
        }
    }
}
