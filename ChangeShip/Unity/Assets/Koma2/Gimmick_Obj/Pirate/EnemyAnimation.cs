﻿using UnityEngine;
using System.Collections;

public class EnemyAnimation : MonoBehaviour
{

    GameObject enemy;

    // Use this for initialization
    void Start()
    {
      
    }

    //------------------------------------------
    //              モードセット変更
    //------------------------------------------
    public void JUMP_SET()
    {
        enemy = this.transform.parent.gameObject;
        enemy.GetComponent<Pirate>().jump_mode = Pirate.JUMP_MODE.MOVE;
        enemy.GetComponent<Pirate>().enemy_mode = Pirate.ENEMY_MODE.MOVE;
    }

    public void NEXTJUMP()
    {
        enemy = this.transform.parent.gameObject;
        enemy.GetComponent<Pirate>().animInfo = true;
    }

    //------------------------------------------
    //              消去
    //------------------------------------------
    void Delete()
    {
        enemy = this.transform.parent.gameObject;
        Destroy(enemy);
    }

}
