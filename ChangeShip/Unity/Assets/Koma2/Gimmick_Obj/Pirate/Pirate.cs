﻿using UnityEngine;
using System.Collections;

// 山中
public class Pirate : MonoBehaviour
{

    private AudioSource Fire;

    /*海賊船列挙型*/
    public enum ENEMY_MODE
    {
        STAY,       //通常
        MOVE,       //移動
        JUMP,       //ジャンプ
        WHIRL,      //渦
        SINK,       //沈没
        HIT_GOAL,
		GIRL_GAMEOVER //船が死んだとき
    }

    public enum JUMP_MODE
    {
        MOVE,
        JUMP,
        STAY,
    }
    /*沈没時用列挙型*/
    public enum SINK_MODE
    {
        SINK_BEFORE,
        SINK,
    }

    /*列挙型変数*/
    public ENEMY_MODE enemy_mode;
    public JUMP_MODE jump_mode;
    public SINK_MODE sink_mode;

    /*移動用変数*/
    public Vector3 l_vec;           //移動量
    public Vector3 line_vec;        //補間用


    /*線形補間用変数*/
    private float elapsedTime;      //経過時間
    private float t;                //媒介変数
    public bool animInfo;
    private Vector3 splashdown_pos;     //ジャンプ着地地点

    public GameObject Right_impact_point;	//水しぶきを出す場所（右）
    public GameObject Left_impact_point;	//水しぶきを出す場所（左）
    public GameObject Right_Fire_place;		//爆撃の場場所
    public GameObject Left_Fire_place;		//爆撃の場所

    private Vector3 tornade_pos;
    private Vector3 RShot_Efect_Pos;		//爆撃の場所（右）
    private Vector3 LShot_Efect_Pos;        //爆撃の場所（左）

	const int SHOT_NUM = 10;				//撃つ回数を（右4　休憩　左4　休憩　で10）
    private int SHOT_COUNT_MAX	 = 300;		//ここにくるとリセット
    private int SHOT_CHANGE_SIDE = 150;     //ここにくると撃つ方向返る
	private int setShotCount;               //SHOT_COUNT_MAXを10
	private int shot_count = 0;

	private GameObject go;
    private bool HitTornad = false;  // 仮
    GameObject ship_player;

	public ParticleSystem splash;			//水しぶき
	public ParticleSystem splash1;			//水しぶき
	public ParticleSystem fire;				//爆炎
	private Vector3 enemyToSplashRightVec;	//敵船から水しぶきまでのベクトル(右)　これを4分割したものを入れる
	private Vector3 enemyToSplashLeftVec;    //敵船から水しぶきまでのベクトル(左)　これを4分割したものを入れる

	void Start()
    {
        //プレイヤー設定
        ship_player = GameObject.Find("ship_player");
        //初期モード設定
        enemy_mode = ENEMY_MODE.STAY;

        go = GameObject.Find("Canvas");
        RShot_Efect_Pos = Right_Fire_place.transform.position;
        LShot_Efect_Pos = Left_Fire_place.transform.position;

        Fire = GetComponent<AudioSource>();
		splash.transform.position = transform.position;
		splash1.transform.position = transform.position;
		enemyToSplashRightVec = ( Left_impact_point.transform.position - transform.position ) / 4;
		enemyToSplashLeftVec = ( Right_impact_point.transform.position - transform.position ) / 4;
		setShotCount = SHOT_COUNT_MAX / SHOT_NUM;
	}


    //------------------------------------------
    //              線形補間
    //------------------------------------------

    Vector3 Liner_interpolation(Vector3 start, Vector3 end)
    {
        elapsedTime += Time.deltaTime;   // 前フレームからの経過時間

        t = elapsedTime / 1.0f;   // 時間を媒介変数に
        if (t > 1.0f) { t = 1.0f; }// クランプ
        float rate = t * t * (3.0f - 2.0f * t);   // 3次関数補間値に変換
        Vector3 p = start * (1.0f - rate) + end * rate;   // いわゆるLerp
        return p;
    }

    // 当たり判定内に入ったら
    public void Hit_Player()
    {
        if (enemy_mode == ENEMY_MODE.STAY)
        {
            Fire.PlayOneShot(Fire.clip);
			fire.Emit( 20 );
			splash.transform.position = ship_player.transform.position;
			splash1.transform.position = ship_player.transform.position;
			splash.Emit( 15 );
			splash1.Emit( 15 );

			ship_player.GetComponent<Girl>().SetMode( (int)Girl.GIRL_MODE.SHELL );
			//ship_player.GetComponent<Girl>().Sink_Shell();
			//go.GetComponent<UI_Controltest>().OnGameOver();
		}
    }

    // 視認範囲に入ったら
    //public void Visibility_Player()
    //{
    //    // 発砲を始める
    //    if (enemy_mode == ENEMY_MODE.STAY)
    //    {
    //        shot_count++;
    //        if (shot_count >= Left_Shot_Time)
    //        {
    //            Fire.PlayOneShot(Fire.clip);
    //            GameObject work_ShotEfect = (GameObject)Instantiate(Shot_Efect, LShot_Efect_Pos, Quaternion.identity);
    //            GameObject work_Impact = (GameObject)Instantiate(Impact_Point, Left_impact_point.transform.position, Quaternion.identity);
    //            Destroy(work_ShotEfect, SHOT_EFECT_LIFE_TIME);
    //            Destroy(work_Impact, IMPACT_LIFE_TIME);
    //            shot_count = -60;
    //        }

    //        else if (shot_count == Right_Shot_Time)
    //        {
    //            Fire.PlayOneShot(Fire.clip);
    //            GameObject work_ShotEfect = (GameObject)Instantiate(Shot_Efect, RShot_Efect_Pos, Quaternion.identity);
    //            GameObject work_Impact = (GameObject)Instantiate(Impact_Point, Right_impact_point.transform.position, Quaternion.identity);
    //            Destroy(work_ShotEfect, SHOT_EFECT_LIFE_TIME);
    //            Destroy(work_Impact, IMPACT_LIFE_TIME);
    //        }
    //    }
    //}

	//右に撃つ
	public void RigthSplash()
	{
		Fire.PlayOneShot( Fire.clip );
		fire.transform.position = LShot_Efect_Pos;
		fire.Emit( 20 );
		splash.transform.position  += enemyToSplashRightVec;
		splash1.transform.position += enemyToSplashRightVec;
		splash.Emit( 15 );
		splash1.Emit( 15 );
	}

	public void LeftSplash()
	{
		Fire.PlayOneShot( Fire.clip );
		fire.transform.position = RShot_Efect_Pos;
		fire.Emit( 20 );
		splash.transform.position  += enemyToSplashLeftVec;
		splash1.transform.position += enemyToSplashLeftVec;
        splash.Emit( 15 );
		splash1.Emit( 15 );
	}

	public void Visibility_Player()
	{
		// 発砲を始める
		if( enemy_mode == ENEMY_MODE.STAY )
		{
			//Right_Shot_Time
			shot_count++;
			if( shot_count == setShotCount * 1 ) LeftSplash();
			if( shot_count == setShotCount * 2 ) LeftSplash();
			if( shot_count == setShotCount * 3 ) LeftSplash();
			//if( shot_count == setShotCount * 4 ) LeftSplash();

			if( shot_count == SHOT_CHANGE_SIDE )  //五回目は撃つ方向返る
			{
				splash.transform.position = transform.position;
				splash1.transform.position = transform.position;
			}

			if( shot_count == setShotCount * 6 ) RigthSplash();
			if( shot_count == setShotCount * 7 ) RigthSplash();
			if( shot_count == setShotCount * 8 ) RigthSplash();
			//if( shot_count == setShotCount * 9 ) RigthSplash();
			if( shot_count == SHOT_COUNT_MAX )
			{
				splash.transform.position = transform.position;
				splash1.transform.position = transform.position;
				shot_count = 0;
			}
		}
	}
	//------------------------------------------
	//         海賊船の向き変更
	//------------------------------------------
	void Rotate()
    {
        //エネミーの向き変換
        Vector3 enemyDir = l_vec;
        Quaternion q = Quaternion.LookRotation(enemyDir);
        //プレイヤーの角度を変える
        transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 360.0f * Time.deltaTime);
    }

    //------------------------------------------
    //         渦に当たって沈む
    //------------------------------------------
    public void Hit_Whirl()
    {
        transform.eulerAngles -= new Vector3(0, 10f, 0);
        transform.localScale -= new Vector3(0.001f, 0.001f, 0.001f) * Time.time;
        if (transform.localScale.y <= 0) Destroy(gameObject);
    }

    //------------------------------------------
    //         ジャンプ関数(アニメーション)
    //------------------------------------------
    public void Hit_Tornad()
    {
        if (ship_player)
        {
            switch (jump_mode)
            {
                case JUMP_MODE.MOVE:
                    line_vec = Liner_interpolation(this.transform.position, tornade_pos);  //補間率の計算
                    this.transform.position = line_vec;                                 //移動の補間
                    //アニメーション再生
                    gameObject.transform.FindChild("enemy").GetComponent<Animator>().SetBool("Jump", true);

                    //補間が終われば
                    if (t >= 1.0f)
                    {
                        t = elapsedTime = 0;      //時間と初期化
                        line_vec = Vector3.zero;
                        jump_mode = JUMP_MODE.JUMP; //次のモードへ
                    }
                    break;

                case JUMP_MODE.JUMP:
                    ////アニメーターの状態の代入
                    //animInfo = gameObject.transform.FindChild("enemy").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
                    //アニメーションが終わったら
                    if (animInfo == true)
                    {
                        //アニメーション停止
                        gameObject.transform.FindChild("enemy").GetComponent<Animator>().enabled = false;
                        //子を参照
                        foreach (Transform child in this.transform)
                        {
                            if (child.name == ("Splashdown_pos"))
                            {
                                splashdown_pos = child.transform.position;    //位置座標取得
                                //当たり判定ＯＦＦ
                                this.transform.GetComponent<BoxCollider>().enabled = false;
                                jump_mode = JUMP_MODE.STAY;
                            }
                        }
                    }
                    break;

                case JUMP_MODE.STAY:
                    line_vec = Liner_interpolation(this.transform.position, splashdown_pos);
                    this.transform.position = line_vec;        //補間

                    if (t >= 1.0f)
                    {
                        //アニメーション関連
                        gameObject.transform.FindChild("enemy").GetComponent<Animator>().enabled = true;
                        gameObject.transform.FindChild("enemy").GetComponent<Animator>().SetBool("Jump", false);

                        //あたり判定ＯＮ
                        this.transform.GetComponent<BoxCollider>().enabled = true;

                        line_vec = Vector3.zero;

                    }
                    break;
            }//swich
        }
    }


    //------------------------------------------
    //         岩に当たって沈む
    //------------------------------------------
    public void Hit_Rock()
    {
        switch (sink_mode)
        {
            case SINK_MODE.SINK_BEFORE:
                //移動値を0に
                l_vec = Vector3.zero;
                //アニメーション変更
                //子のアニメーションＯＮ
                this.transform.GetComponentInChildren<Animator>().SetBool("Sink", true);
                sink_mode = SINK_MODE.SINK;

                break;

            case SINK_MODE.SINK:
                //アニメーションの状態取得
                break;
        }//switch
    }





	//------------------------------------------
	//              更新
	//------------------------------------------
	//void Update()
	void FixedUpdate()
	{
		if( ship_player ) {
			{ //とりあえずバグ直すためのごみコード
				if( GameModeManager.GetGameMode() != GameModeManager.GAMEMODE.MAIN ) return;
			}
			if( ship_player.transform.GetComponent<Girl>().GetStart() ) {
				switch( enemy_mode ) {
					case ENEMY_MODE.STAY:       //待機
						Visibility_Player();    //
						break;

					case ENEMY_MODE.MOVE:       //移動
						Rotate();
						transform.position += l_vec ;
						break;

					case ENEMY_MODE.JUMP:       //ジャンプ
						Hit_Tornad();
						break;

					case ENEMY_MODE.SINK:
						Hit_Rock();
						break;

					case ENEMY_MODE.HIT_GOAL:     //プレイヤーと当たったとき
						go.GetComponent<UI_Controltest>().OnGameOver_byEnemy();
						l_vec = Vector3.zero;
						break;
					case ENEMY_MODE.GIRL_GAMEOVER:
						break;
					case ENEMY_MODE.WHIRL:
						Hit_Whirl();
						break;
				}//switch
			}
		}
	}

    //vecの向きに変更
    public void SetVec(Vector3 vec)
    {
        //移動量を代入
        l_vec = vec;

        //モードを移動に
        if (enemy_mode == ENEMY_MODE.STAY)
        {
            enemy_mode = ENEMY_MODE.MOVE;
        }

    }

    //移動量設定
    public void SetPos(Vector3 pos) { tornade_pos = pos; }      //竜巻の位置設定
    //外部から状態を変えるとき用
    public void SetMode(int mode) { enemy_mode = (ENEMY_MODE)mode; }
    public ENEMY_MODE GetMode()   { return enemy_mode; }
    // public void SetRotate(Quaternion qua) { transform.rotation = qua; }
}