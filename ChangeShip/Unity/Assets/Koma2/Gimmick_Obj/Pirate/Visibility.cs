﻿using UnityEngine;
using System.Collections;

public class Visibility : MonoBehaviour
{

    // Use this for initialization
    public bool shot = false;
    public GameObject parent;
    void Start()
    {
        transform.SetParent(parent.transform);

    }

    // Update is called once per frame
    void Update()
    {
        if (shot)
        {
            transform.parent.SendMessage("Visibility_Player");
        }
    }


}
