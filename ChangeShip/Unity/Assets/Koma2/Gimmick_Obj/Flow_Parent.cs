﻿using UnityEngine;
using System.Collections;

public class Flow_Parent : MonoBehaviour {

    public GameObject flow;
    public GameObject swirl;
    public enum MODE
    {
        流れ,
        渦
    }
    public MODE mode = 0;
    // Use this for initialization
    void Start()
    {
        flow.transform.parent = transform;
        flow.transform.name = transform.name + "_" + flow.transform.name;
        swirl.transform.parent = transform;
        swirl.transform.name = transform.name + "_" + swirl.transform.name;

        switch (mode)
        {
            case MODE.流れ:
                flow.SetActive(true);
                swirl.SetActive(false);
                break;
            case MODE.渦:
                flow.SetActive(false);
                swirl.SetActive(true);
                break;
        }
	}
    
    public void Change_OBJ()
    {
        if (flow.active)
        {
            flow.SetActive(false);
            swirl.SetActive(true);
			ChangeEffect.SetChangetObjWorld( 0 , swirl );
        }
        else
        {
            flow.SetActive(true);
            swirl.SetActive(false);
			ChangeEffect.SetChangetObjWorld( 1 , flow );
		}
    }

    public GameObject GetFlow() { return flow; }
    public GameObject GetSwirl() { return swirl; }
}
