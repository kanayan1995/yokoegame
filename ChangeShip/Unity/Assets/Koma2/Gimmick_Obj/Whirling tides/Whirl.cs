﻿using UnityEngine;
using System.Collections;

public class Whirl : MonoBehaviour {

    // Use this for initialization
    private GameObject go;
    public GameObject clair;    //予測線用オブジェクト
    public GameObject child;
    private GameObject workSmoke1 = null;
    void Start ()
    {
        child.transform.parent = transform;
        child.transform.name = "UI";
        //clair.transform.position = transform.position;
        //clair.SetActive(false);
        go = GameObject.Find("Canvas");

    }

    // Update is called once per frame


    void OnTriggerStay(Collider collider)
    {
        switch (collider.tag)
        {
            case "Pirate":
                collider.GetComponent<Pirate>().SetMode(3);
                break;
            case "Player":
                collider.GetComponent<Girl>().Sink_Vortex();
				collider.GetComponent<Girl>().SetMode( (int)Girl.GIRL_MODE.VORTEX );
				//go.GetComponent<UI_Controltest>().OnGameOver();
                break;
        }
    }

    public void OnClairvoyance(bool arg)
    {
        if (arg)
        {
            if(!workSmoke1)
            workSmoke1 = (GameObject)Instantiate(clair, transform.position, Quaternion.identity);
        }
        else
        {
            Destroy(workSmoke1);
        }
    }
}
