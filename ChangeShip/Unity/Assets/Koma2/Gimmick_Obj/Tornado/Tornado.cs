﻿using UnityEngine;
using System.Collections;


public class Tornado : MonoBehaviour
{
    // Use this for initialization
    public GameObject clair;    //予測線用オブジェクト
    public GameObject child;
    private GameObject workSmoke1 = null;
    private GameObject player;
    private GameObject pirate;
    void Start()
    {
        child.transform.parent = transform;
        child.transform.name = "UI";
        player = GameObject.Find("ship_player");
    }


    void OnTriggerStay(Collider collider)
    {
        switch (collider.tag)
        {
            case "Pirate":
                if (collider.GetComponent<Pirate>().enemy_mode != Pirate.ENEMY_MODE.JUMP)
                {
                    collider.GetComponent<Pirate>().SetPos(this.transform.position);  //竜巻の座標を送る
                    collider.GetComponent<Pirate>().SetMode(2);                       //モード変更
                }
                break;

            case "Player":
                if (player.GetComponent<Girl>().girl_mode != Girl.GIRL_MODE.JUMP)
                {
                    collider.GetComponent<Girl>().SetPos(this.transform.position);  //竜巻の座標を送る 
                    collider.GetComponent<Girl>().SetMode(1);                       //モード変更
                }
                break;
        }
    }

    public void OnClairvoyance(bool arg)
    {
        if (arg)
        {
            if (!workSmoke1)
                workSmoke1 = (GameObject)Instantiate(clair, transform.position, Quaternion.identity);
        }
        else
        {
            Destroy(workSmoke1);
        }
    }
}


