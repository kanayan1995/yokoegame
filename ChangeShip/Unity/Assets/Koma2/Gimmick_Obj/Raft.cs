﻿using UnityEngine;
using System.Collections;

public class Raft : MonoBehaviour {
    private Vector3 start_pos;      //開始位置
    public Vector3 r_vec;           //イカダのベクトル
    private Vector3 vec;            //力の向き

	// Use this for initialization
	void Start () {
        start_pos = transform.position;
        vec = r_vec;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += r_vec;
        if(transform.position.y<=-30.0f)
        {
            r_vec = vec;
            transform.position = start_pos;
        }
	}

    public void SetVec(Vector3 vec) { r_vec = vec; }
}

