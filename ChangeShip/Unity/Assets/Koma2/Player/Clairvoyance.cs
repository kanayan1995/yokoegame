﻿using UnityEngine;
using System.Collections;

public class Clairvoyance : MonoBehaviour {

    public Test clair_1;
    public Test clair_2;
    public Test clair_3;

    private bool tgr;
    private bool tgr_flg;


    public void SetActive(bool arg) { gameObject.SetActive(arg); }


    // Use this for initialization
    void Start () {
        tgr = false;
        tgr_flg = false;
	}

    public void OnClick_Main()
    {
        if (tgr)
        {
            clair_1.SetActive(false);
            clair_2.SetActive(false);
            clair_3.SetActive(false);
            tgr = false;
        }
        else
        {
            clair_1.Update_Rotate();
            clair_2.Update_Rotate();
            clair_3.Update_Rotate();

            clair_1.SetActive(true);
            clair_2.SetActive(true);
            clair_3.SetActive(true);
            tgr = true;
        }
    }
}
