﻿using UnityEngine;
using System.Collections;

// 小松
public class TutorialGimmick : MonoBehaviour
{

    public enum Dir
    {
        UP,
        DOWN,
        RIGHT,
        LEFT
    }
    public Dir dir;

    public static float SPEED = 0.01f;
    // Use this for initialization
    public GameObject clair;    //予測線用オブジェクト
    public GameObject child;
    private GameObject workSmoke1 = null;
    // public Raft raft_1;
    Vector3 vec = Vector3.zero;
    void Start()
    {
        child.transform.parent = transform;
        //transform.name = transform.parent.name + "_" + transform.name;

        switch (dir)
        {
            case Dir.UP:
                vec.z = SPEED;
                break;
            case Dir.DOWN:
                vec.z = -SPEED;
                break;
            case Dir.RIGHT:
                vec.x = SPEED;
                break;
            case Dir.LEFT:
                vec.x = -SPEED;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            c.GetComponent<Girl>().SetVec(vec);
        }

        //　海賊船
        if (c.gameObject.tag == "Pirate")
        {
            c.GetComponent<Pirate>().SetVec(vec);
           
        }
    }
    public void OnClairvoyance(Quaternion angle, bool arg)
    {
        if (arg)
        {
            if (!workSmoke1)
                workSmoke1 = (GameObject)Instantiate(clair, transform.position + new Vector3(0, 0.1f, 0), angle);
        }
        else
        {
            if (workSmoke1)
                Destroy(workSmoke1);
        }
    }
    public void OnClairvoyance(bool arg)
    {
        if (arg)
        {
            if (!workSmoke1)
                workSmoke1 = (GameObject)Instantiate(clair, transform.position + new Vector3(0, 0.1f, 0), Quaternion.identity);
        }
        else
        {
            if (workSmoke1)
                Destroy(workSmoke1);
        }
    }

    public void SetVec(Vector3 v) { vec = v; }
    public Vector3 GetVec() { return vec; }

    public void SetRotate(Quaternion angle) { transform.rotation = angle; }
    public Quaternion GetRotate() { return transform.rotation; }

    public GameObject GetClair() { return clair; }
}

