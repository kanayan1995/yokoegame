﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSpeed : MonoBehaviour
{

	public GameObject icon;
	// Use this for initialization
	void Start()
	{
		icon.SetActive( false );
	}

	// Update is called once per frame
	void Update()
	{
		if( Time.timeScale == 3 )
		{
			icon.SetActive( false );
		}
	}

	void OnTriggerEnter( Collider collider )
	{
		if( Time.timeScale != 3 )
		{
			icon.SetActive( true );
		}
	}

	public void OnButton()
	{
		icon.SetActive( false );
	}
}
