﻿using UnityEngine;
using System.Collections;

public class TutorialCollider : MonoBehaviour {

    // Use this for initialization
    public static int OBJ_MAX = 2;
    public GameObject[] tap = new GameObject[OBJ_MAX];
    private bool OnTap;
    public GameObject player;
	void Start () {
        for(int i = 0; i < OBJ_MAX; i++)
        {
            tap[i].SetActive(false);
        }
        OnTap = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnTriggerEnter(Collider collider)
    {
        StartTap();
        player.GetComponent<Girl>().SetStart(false);
        tap[0].SetActive(true);
    }

    public void OnFirstTap()
    {
        tap[0].SetActive(false);
        tap[1].SetActive(true);
    }

    public void OnSecondTap()
    {
        tap[1].SetActive(false);
        player.GetComponent<Girl>().SetStart(true);
    }

    //public void OnThirdTap()
    //{
    //    tap[2].SetActive(false);
    //    player.GetComponent<Girl>().SetStart(true);
    //}



    public bool GetOnTap() { return OnTap; }
    public void SetOnTap(bool org) { OnTap = org; }
    public void StartTap() { OnTap = true; }
}
