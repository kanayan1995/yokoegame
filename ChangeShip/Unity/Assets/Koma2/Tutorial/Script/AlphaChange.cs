﻿using UnityEngine;
using System.Collections;

public class AlphaChange : MonoBehaviour
{
	private GameObject changeAlphaObject; //アルファを変えるオブジェクト
	private Color objectColor;            //変えたいオブジェクトの色
	public float flasheTime = 2.0f;       //点滅する時間	秒数でいい
	private float turnTime;               //点滅時間
	private float keepObjectAlpha;				//始めのオブジェクトのアルファ値を保持する

	// Use this for initialization
	void Start()
	{
		changeAlphaObject = this.gameObject;
		objectColor = changeAlphaObject.GetComponent<SpriteRenderer>().material.color;
		keepObjectAlpha = objectColor.a;
		turnTime = -( keepObjectAlpha / ( flasheTime * 60.0f ) );    //アルファ値を削る時間を分割してる
	}

	// Update is called once per frame
	void Update()
	{
		objectColor.a += turnTime;
		changeAlphaObject.GetComponent<SpriteRenderer>().material.color = objectColor;
		if( objectColor.a < 0.0f ) {
			turnTime = keepObjectAlpha / ( ( flasheTime * 60.0f ) * 2 );
			objectColor.a = 0.0f;
		}
		if( objectColor.a > 1.0f ) {
			turnTime = -( keepObjectAlpha / ( flasheTime * 60.0f ) );
			objectColor.a = 1.0f;
		}
	}
}
