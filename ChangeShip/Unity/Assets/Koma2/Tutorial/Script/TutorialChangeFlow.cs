﻿using UnityEngine;
using System.Collections;

public class TutorialChangeFlow : MonoBehaviour
{

	public GameObject Tap_C;
	static class TapConstant
	{
		public const int tap_max = 2;     // タップ最大数
		public const float distance = 10000.0f;   // タップ認識範囲
	}

	enum Tap
	{   // タップ定数

		/// <summary>
		/// タップしてないとき
		/// </summary>
		NONE = -1,
		/// <summary>
		/// １タップ目
		/// </summary>
		FIRST,
		/// <summary>
		/// ２タップ目
		/// </summary>
		SECOND,

	}


	// エフェクト用
	Vector3[] ButtonUI = new Vector3[TapConstant.tap_max];
	TutorialChangeEffect changeEffect;    //入れ替えのエフェクト
	GameObject[] changeEffectObj = new GameObject[TapConstant.tap_max];

	private int tap;

	private GameObject[] obj = new GameObject[TapConstant.tap_max];
	private Vector3 vec;
	private Vector3 temp_vec;
	private Quaternion rotate;
	private Quaternion temp_rotate;

	private RaycastHit hit;

	private int swirl;  //渦のオブジェクト番号
	private int flow;  //流のオブジェクト番号

	GameObject ship_player;

	private AudioSource 入れ替え;
	private AudioSource タッチ;

	public GameObject wind;
	public GameObject wind1;

	public int changeCount;

	// 気分でやってみた
	public void 入れ替えサウンド()
	{
		入れ替え.PlayOneShot( 入れ替え.clip );
	}
	public void タッチサウンド()
	{
		タッチ.PlayOneShot( タッチ.clip );
	}

	/// <summary>
	/// obj[]をNULLに初期化
	/// </summary>
	void NULL_OBJ() { for( int i = 0 ; i < TapConstant.tap_max ; i++ ) { obj[i] = null; } }

	void OFF_UI() { for( int i = 0 ; i < TapConstant.tap_max ; i++ ) { obj[i].transform.FindChild( "UI" ).GetComponent<ChangeChoice>().SetChoice( false ); } }

	void Start()
	{

		AudioSource[] audioSources = GetComponents<AudioSource>();
		入れ替え = audioSources[0];
		タッチ = audioSources[1];

		ship_player = GameObject.Find( "ship_player" );

		hit = new RaycastHit();

		vec = new Vector3( 0 , 0 , 0 );
		temp_vec = new Vector3( 0 , 0 , 0 );

		rotate = new Quaternion( 0 , 0 , 0 , 0 );
		temp_rotate = new Quaternion( 0 , 0 , 0 , 0 );

		tap = (int)Tap.NONE;

		NULL_OBJ();

		for( int i = 0 ; i < TapConstant.tap_max ; i++ )
		{
			ButtonUI[i] = Vector3.zero;
			changeEffectObj[i] = null;
		}
		changeEffect = GetComponent<TutorialChangeEffect>();
		changeCount = 0;
	}

	void Update()
	{
		if( ship_player )
		{
			if( Tap_C.GetComponent<TutorialCollider>().GetOnTap() )
			{
				On_Tap();
			}
		}
	}

	/// <summary>
	/// タップしたかどうか
	/// </summary>
	void On_Tap()
	{
		// 左クリックを取得
		if( Input.GetMouseButtonDown( 0 ) )
		{
			// クリックしたスクリーン座標をrayに変換
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			// Rayの当たったオブジェクトの情報を格納する
			// オブジェクトにrayが当たった時
			if( Physics.Raycast( ray , out hit , TapConstant.distance ) )
			{
				tap++;
				// rayが当たったオブジェクトの名前を取得;
				obj[tap] = hit.collider.gameObject;
				if( obj[tap].tag == "Button" )
				{
					タッチサウンド();// １タップ目
					if( tap == (int)Tap.FIRST )
					{
						if( obj[tap].name == "WindUI1" )
						{
							Tap_C.GetComponent<TutorialCollider>().OnFirstTap();
							obj[tap] = GameObject.Find( obj[tap].transform.parent.name );
							ButtonUI[tap] = obj[tap].transform.position;
						}
						else
						{
							obj[tap] = null;
							tap--;
						}
					}
					// ２タップ目
					else if( tap == (int)Tap.SECOND )
					{

						if( obj[tap].name == "WindUI3" )
						{
							Tap_C.GetComponent<TutorialCollider>().SetOnTap( false );
							Tap_C.GetComponent<TutorialCollider>().OnSecondTap();
							obj[tap] = GameObject.Find( obj[tap].transform.parent.name );
							ButtonUI[tap] = obj[tap].transform.position;
						}
						else
						{
							obj[tap] = null;
							tap--;
						}
					}
					else
					{
						obj[tap] = null;
						tap--;
					}
				}
				else
				{
					obj[tap] = null;
					tap--;

				}

				Tap_Event();
			}

		}

	}
	/// <summary>
	/// ギミックをタップしてたら
	/// </summary>
	void Tap_Event()
	{
		switch( tap )
		{   // タップしていない状態
			case (int)Tap.NONE: break;
			//１タップ目
			case (int)Tap.FIRST:
				obj[tap].transform.FindChild( "WindUI1" ).GetComponent<ChangeChoice>().SetChoice( true );
				obj[tap].transform.FindChild( "WindUI1" ).GetComponent<ChangeUI>().SetChangeScaleFlag( true );
				changeEffect.SetFirstTap( Flow_Parent.MODE.流れ );
				Fast_Tap();

				break;

			//２タップ目
			case (int)Tap.SECOND:
				wind.transform.GetComponent<SpriteRenderer>().material.color = new Color( 0.5f , 0.5f , 0.5f , 1.0f );
				wind1.transform.GetComponent<SpriteRenderer>().material.color = new Color( 0.5f , 0.5f , 0.5f , 1.0f );
				obj[tap].transform.FindChild( "WindUI3" ).GetComponent<ChangeChoice>().SetChoice( true );
				//obj[tap].transform.FindChild( "WindUI3" ).GetComponent<ChangeUI>().SetChangeScaleFlag( true );
				changeEffect.SetSecondTap( Flow_Parent.MODE.流れ );
				Second_Tap();
				changeCount++;
				break;
		}
	}


	void Fast_Tap()
	{
		// tempに保存
		switch( obj[tap].tag )
		{
			case "Gimmick":
				temp_vec = obj[tap].GetComponent<TutorialGimmick>().GetVec();
				temp_rotate = obj[tap].GetComponent<TutorialGimmick>().GetRotate();
				break;

			case "Swirl":
				break;
		}
	}

	void Second_Tap()
	{

		// １タップ目と２タップ目が同じなら
		if( obj[(int)Tap.FIRST].name == obj[(int)Tap.SECOND].name )
		{
			obj[0].transform.FindChild( "WindUI1" ).GetComponent<ChangeChoice>().SetChoice( false );
			for( int i = 0 ; i < 0 ; i++ )
			{
				obj[i] = null;
			}
			tap = (int)Tap.NONE;
		}
		// １タップ目と２タップ目が渦なら
		else if( obj[(int)Tap.FIRST].tag == "Swirl" && obj[(int)Tap.SECOND].tag == "Swirl" )
		{
			obj[tap].transform.FindChild( "UI" ).GetComponent<ChangeChoice>().SetChoice( false );
			obj[tap] = null;
			tap--;
		}
		else
		{

			Change();   // 入れ替える
			obj[0].transform.FindChild( "WindUI1" ).GetComponent<ChangeChoice>().SetChoice( false );
			obj[0].transform.FindChild( "WindUI1" ).GetComponent<ChangeUI>().SetChangeScaleFlag( false );
			obj[0].transform.FindChild( "WindUI1" ).GetComponent<ChangeUI>().Reset();
			obj[1].transform.FindChild( "WindUI3" ).GetComponent<ChangeChoice>().SetChoice( false );
			obj[1].transform.FindChild( "WindUI3" ).GetComponent<ChangeUI>().SetChangeScaleFlag( false );
			obj[1].transform.FindChild( "WindUI3" ).GetComponent<ChangeUI>().Reset();
			changeEffectObj[0] = obj[0];
			changeEffect.SetChangetObj( 0 );
			changeEffectObj[1] = obj[1];
			changeEffect.SetChangetObj( 1 );

			//OFF_UI();
			NULL_OBJ();

			changeEffect.ChangeFlagTrue();
		}
	}

	/// <summary>
	/// 入れ替える
	/// </summary>
	void Change()
	{
		入れ替えサウンド();
		// SmokeEffectCreate(obj[(int)Tap.FIRST].transform.position, obj[(int)Tap.SECOND].transform.position);

		if( obj[(int)Tap.FIRST].tag == "Swirl" || obj[(int)Tap.SECOND].tag == "Swirl" )
		{
			Search_Swirl();
			// 渦の方の親をよんでChange_OBJ関数を使う
			obj[swirl].transform.FindChild( "UI" ).GetComponent<ChangeChoice>().SetChoice( false );
			obj[swirl].GetComponentInParent<Flow_Parent>().Change_OBJ();
			// 渦の方に流のvecとrotateを代入
			obj[swirl] = obj[swirl].GetComponentInParent<Flow_Parent>().GetFlow();
			Vector3 test = obj[flow].GetComponent<Gimmick>().GetVec();
			obj[swirl].GetComponent<Gimmick>().SetVec( obj[flow].GetComponent<Gimmick>().GetVec() );
			obj[swirl].GetComponent<Gimmick>().SetRotate( obj[flow].GetComponent<Gimmick>().GetRotate() );

			obj[tap].transform.FindChild( "WindUI1" ).GetComponent<ChangeUI>().SetChangeScaleFlag( false );
			// 流の親を読んでChange_OBJ関数を使う
			obj[flow].GetComponentInParent<Flow_Parent>().Change_OBJ();
		}
		else
		{
			vec = obj[(int)Tap.SECOND].GetComponent<TutorialGimmick>().GetVec();
			obj[(int)Tap.SECOND].GetComponent<TutorialGimmick>().SetVec( temp_vec );
			rotate = obj[(int)Tap.SECOND].GetComponent<TutorialGimmick>().GetRotate();
			obj[(int)Tap.SECOND].GetComponent<TutorialGimmick>().SetRotate( temp_rotate );

			obj[(int)Tap.FIRST].GetComponent<TutorialGimmick>().SetVec( vec );
			obj[(int)Tap.FIRST].GetComponent<TutorialGimmick>().SetRotate( rotate );
		}

		tap = (int)Tap.NONE;

	}


	/// <summary>
	/// 渦の方を検索
	/// </summary>
	private void Search_Swirl()
	{
		if( obj[(int)Tap.FIRST].tag == "Swirl" )
		{
			swirl = (int)Tap.FIRST;
			flow = (int)Tap.SECOND;
		}
		else if( obj[(int)Tap.SECOND].tag == "Swirl" )
		{
			swirl = (int)Tap.SECOND;
			flow = (int)Tap.FIRST;
		}
	}

	public Vector3 GetButtonUI( int タップ番号 )
	{
		return ButtonUI[タップ番号 - 1];
	}

	public GameObject GetChangeObject( int objNum )
	{
		return changeEffectObj[objNum];
	}
}
