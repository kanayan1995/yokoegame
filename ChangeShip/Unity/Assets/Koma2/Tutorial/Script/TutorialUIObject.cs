﻿using UnityEngine;
using System.Collections;

public class TutorialUIObject : MonoBehaviour
{

    public Camera targetCamera;
    public GameObject finger;
    // Use this for initialization

    void Start()
    {

        Color col = new Color(1, 1, 1, 1);
        transform.GetComponent<Renderer>().sharedMaterial.color = col;
    }

    // Update is called once per frame
    void Update()
    {

        this.transform.LookAt(this.targetCamera.transform.position);
    }
}
