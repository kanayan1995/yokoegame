﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishManager : MonoBehaviour {

    public int instantiateTimer;  // 出現時間

    public const int KIND_FISH_MAX = 3;
    public const int KIND_TIME_MAX = 3;
    public const int KIND_POS_MAX = 5;
    private const int LIMIT_TIME = 0;

    public int[] InstantiateTIME = new int[KIND_TIME_MAX];
    public GameObject[] Fish = new GameObject[KIND_FISH_MAX];
    public GameObject[] InstantiatePos = new GameObject[KIND_POS_MAX];

    enum KIND_FISH          
    {
        SAKANA,
        EI
    }
    enum KIND_TIME
    {
        TIME360,
        TIME500,
        TIME660,
    }

	// Use this for initialization
	void Start ()
    {

        instantiateTimer = InstantiateTIME[Random.Range(0, KIND_TIME_MAX)];
	}

	// Update is called once per frame
	void FixedUpdate()
	{
        instantiateTimer--;
        if(instantiateTimer <= LIMIT_TIME)
        {
            int kind = Random.Range(0, KIND_FISH_MAX);
            int pos = Random.Range(0, KIND_POS_MAX);

            Instantiate(Fish[kind], InstantiatePos[pos].transform.position, InstantiatePos[pos].transform.localRotation);
            instantiateTimer = InstantiateTIME[Random.Range(0, KIND_TIME_MAX)];
        }
	}

}


