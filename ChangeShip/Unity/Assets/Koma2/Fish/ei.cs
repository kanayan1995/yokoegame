﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ei : MonoBehaviour {

    public int DELETE_TIME = 900;
    public int deleteTime;

    private float speed;
    private Vector3 move;

    Quaternion from;
    Quaternion to;
    float time = 0;
    // Use this for initialization
    void Start()
    {
        deleteTime = DELETE_TIME;
        speed = 0.01f;
        move = Vector3.forward * speed;
        from = transform.localRotation;
        to = Quaternion.Euler(0, -180, 0);

    }
	// Update is called once per frame
	void FixedUpdate()
	{
        if(speed > 0)
        {
            this.transform.Translate(move);
        }
        else
        {
            if (time < 1)
                time += Time.deltaTime;
            transform.rotation = Quaternion.Slerp(from, to, time);
            this.transform.Translate(move);
        }

        deleteTime--;
        if (deleteTime <= 0) Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider collider)
    {
        speed = -0.01f;

    }
}
