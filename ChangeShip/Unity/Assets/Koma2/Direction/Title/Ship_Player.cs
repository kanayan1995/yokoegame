﻿using UnityEngine;
using System.Collections;

public class Ship_Player : MonoBehaviour {


    // Use this for initialization
    public bool isStart { get; set; }
	void Start () {
        isStart = false;
	}

	// Update is called once per frame
	void FixedUpdate()
	{
        if (isStart)
        {
            transform.position += transform.forward * 0.07f;
            FadeManager.Instance.LoadScene("Stage_Select", 2.0f);
        }
       
    }
}
