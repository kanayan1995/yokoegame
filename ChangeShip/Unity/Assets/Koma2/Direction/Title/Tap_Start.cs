﻿using UnityEngine;
using System.Collections;

public class Tap_Start : MonoBehaviour {

    // Use this for initialization
    public GameObject ship_player;
    public GameObject wind;
	void Start () {
        wind.SetActive(false);
    }
	
	// Update is called once per frame
	public void OnStart()
    {
        ship_player.GetComponent<Ship_Player>().isStart = true;
        wind.SetActive(true);
    }
}
