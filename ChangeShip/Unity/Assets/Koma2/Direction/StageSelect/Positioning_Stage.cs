﻿using UnityEngine;
using System.Collections;

public class Positioning_Stage : MonoBehaviour {

    public static int STAGE_MAX;
    public GameObject[] stage = new GameObject[STAGE_MAX];
    private static Vector3 SelectLocation;
    private static Vector3 MovePos;
    private int NowSelectStage = 0;
    // stage1     0
    // stage2     81.9226
    // stage3     166.2568
    // stage4     166.2568
    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void NextStage()
    {
        NowSelectStage++;
    }
    public void PreviousStage()
    {
        NowSelectStage--;
    } 
}
