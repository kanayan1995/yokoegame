﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {


    public AudioSource audioSource;
    // Use this for initialization
    void Start ()
    {
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	public void Play_Sound() {
        audioSource.PlayOneShot(audioSource.clip);
	}
}
