﻿using UnityEngine;
using System.Collections;


public static class GameObjectExtension
{
    public static void SetLayer(this GameObject gameObject, int layerNo, bool needSetChildrens = true)
    {
        if (!gameObject)
        {
            return;
        }
        gameObject.layer = layerNo;

        if (!needSetChildrens)
        {
            return;
        }
        
        foreach (Transform childTransform in gameObject.transform)
        {
            SetLayer(childTransform.gameObject, layerNo, needSetChildrens);
        }
    }
    
    public static void SetMaterial(this GameObject gameObject, Material setMaterial, bool needSetChildrens = true)
    {
        if (!gameObject)
        {
            return;
        }
        
        if (gameObject.GetComponent<Renderer>())
        {
            gameObject.GetComponent<Renderer>().material = setMaterial;
        }
        
        if (!needSetChildrens)
        {
            return;
        }
        
        foreach (Transform childTransform in gameObject.transform)
        {
            SetMaterial(childTransform.gameObject, setMaterial, needSetChildrens);
        }

    }


}