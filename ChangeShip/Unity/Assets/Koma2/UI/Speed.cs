﻿using UnityEngine;
using System.Collections;

public class Speed : MonoBehaviour {

    public GameObject x1, x2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    public void OnSpeed()
    {
        if(Time.timeScale == 3)
        {
            x1.SetActive(true);
            x2.SetActive(false);
            Time.timeScale = 1;
        }
		else if (Time.timeScale == 0)
		{
			x1.SetActive( true );
			x2.SetActive( false );
		}
        else
        {
            x1.SetActive(false);
            x2.SetActive(true);
            Time.timeScale = 3;
        }
    }
}
