﻿using UnityEngine;
using System.Collections;

//　小松
public class UI : MonoBehaviour
{
    // Use this for initialization
    public GameObject next_ui;
    private GameObject ship_player;
    public void SetActive(bool arg) { gameObject.SetActive(arg); }

    //カメライベント用
    private MainCamera main_camera;
    private Vector3 back_pos;
    //public GameObject main_camera;
    private float elapsedTime;
    private float t;
    Vector3 vec;
    //------------------------------------------
    //              線形補間
    //------------------------------------------

    Vector3 Liner_interpolation(Vector3 start, Vector3 end)
    {
        elapsedTime += Time.deltaTime;   // 前フレームからの経過時間

        t = elapsedTime / 1.0f;   // 時間を媒介変数に
        if (t > 1.0f)
            t = 1.0f;    // クランプ
        float rate = t * t * (3.0f - 2.0f * t);   // 3次関数補間値に変換
        Vector3 p = start * (1.0f - rate) + end * rate;   // いわゆるLerp
        return p;
    }



    void Start()
    {
        ship_player = GameObject.Find("ship_player");
        back_pos = new Vector3(0, 2.5f, -4.0f);
       
    }
    //---------------------------------------------------
    //  コルーチン(引数の値分のフレームの間処理しない)
    //---------------------------------------------------
    private IEnumerator wait(float second)
    {
        while(true)
        {
            yield return new WaitForSeconds(second);
        }
    }

    public void OnClick_Main()
    {
        Time.timeScale = 0;
        ship_player.GetComponent<Girl>().SetStart(false);
        if (next_ui) next_ui.SetActive(true);
    }

	public void OnClick_Start()
	{
		Time.timeScale = 1;
		ship_player.GetComponent<Girl>().SetStart( true );
		gameObject.SetActive( false );
		if( next_ui ) next_ui.SetActive( true );
		GameModeManager.SetGameMode( GameModeManager.GAMEMODE.MAIN ); //付け足しました
	}


    public void OnStartCamera()
    {
        //コルーチン呼び出し(3フレーム間待機)
        StartCoroutine(wait(3.0f));

        //初期位置から後方の位置までの補間率計算
        // vec = Liner_interpolation(main_camera.transform.position,　back_pos.transform.position);
        vec = Liner_interpolation(main_camera.transform.position, back_pos);

        main_camera.transform.position = vec;

        if(t >= 1.0f)
        {
            t = 0;
            elapsedTime = 0;
            vec = Vector3.zero;

            //ここに移動が終わった後の処理記述
        }

    }

    public void OnClick_Back()
    {
        Time.timeScale = 1;
        ship_player.GetComponent<Girl>().SetStart(true);
        GameObject Control = GameObject.Find("Canvas");
        Control.GetComponent<UI_Controltest>().OnBack();
        if (next_ui) next_ui.SetActive(true);
		Control.GetComponentInChildren<Speed>().x1.SetActive( true );
		Control.GetComponentInChildren<Speed>().x2.SetActive( false );
	}
}
