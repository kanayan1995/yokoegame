﻿using UnityEngine;
using System.Collections;

public class UI_Controltest : MonoBehaviour {

    public GameObject start, play, menu, gameover, gameoverEnemy, clear;
    public int StageNum;
    private int NowStageNum;

    void Start()
    {
        start.SetActive(true);
        play.SetActive(false);
        menu.SetActive(false);
        gameover.SetActive(false);
        gameoverEnemy.SetActive(false);
        clear.SetActive(false);
    }
    public void OnStart()
    {
        start.SetActive(false);
        play.SetActive(true);
    }

	public void OnGameOver()
	{
		//同時に中った場合とか考えてMAINからしか動かない
		//if( GameModeManager.GetGameMode() == GameModeManager.GAMEMODE.MAIN )
		{
			Time.timeScale = 1;
			SetRightandLeft( false );
			start.SetActive( false );
			play.SetActive( false );
			menu.SetActive( false );
			gameover.SetActive( true );
			GameModeManager.SetGameMode( GameModeManager.GAMEMODE.GAMEOVER );
		}
	}

    public void OnGameOver_byEnemy()
    {
		//同時に中った場合とか考えてMAINからしか動かない
		//if( GameModeManager.GetGameMode() == GameModeManager.GAMEMODE.MAIN )
		{
			Time.timeScale = 1;
			SetRightandLeft( false );
			start.SetActive( false );
			play.SetActive( false );
			menu.SetActive( false );
			gameoverEnemy.SetActive( true );
			GameModeManager.SetGameMode( GameModeManager.GAMEMODE.GAMEOVER );
		}
    }

	public void OnClear()
	{
		//同時に中った場合とか考えてMAINからしか動かない
		//if( GameModeManager.GetGameMode() == GameModeManager.GAMEMODE.MAIN )
		{
			NowStageNum = PlayerPrefs.GetInt( "STAGE" );
			if( StageNum > NowStageNum )
			{
				PlayerPrefs.SetInt( "STAGE" , StageNum );
			}

			SetRightandLeft( false );
			start.SetActive( false );
			play.SetActive( false );
			menu.SetActive( false );
			clear.SetActive( true );
			GameModeManager.SetGameMode( GameModeManager.GAMEMODE.GAMECLEAR );
		}
	}

	public void OnMenu()
    {
        SetRightandLeft(false);
        start.SetActive(false);
        play.SetActive(false);
        menu.SetActive(true);
		GameModeManager.SetGameMode( GameModeManager.GAMEMODE.MENU );
    }

    public void OnBack()
    {
        SetRightandLeft(true);
        play.SetActive(true);
        menu.SetActive(false);
		GameModeManager.SetGameMode( GameModeManager.GAMEMODE.MAIN );
    }

    public void OnHelp()
    {
        start.SetActive(true);
        play.SetActive(false);
        menu.SetActive(false);
        gameover.SetActive(false);
        gameoverEnemy.SetActive(false);
        clear.SetActive(false);
    }
    
    public void SetRightandLeft(bool set)
    {
        //right.SetActive(set);
        //left.SetActive(set);
    }

    public void OffPlay()
    {
        play.SetActive(false);
    }
}
