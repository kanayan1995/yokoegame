﻿using UnityEngine;
using System.Collections;

public class ChangeChoice : MonoBehaviour
{

	// Use this for initialization
	public GameObject Choice;
	void Start()
	{
		Choice.SetActive( false );
	}

	// Update is called once per frame
	public void SetChoice( bool org ) { Choice.SetActive( org ); }
}
