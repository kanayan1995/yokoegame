﻿using UnityEngine;
using System.Collections;

public class Help : MonoBehaviour {

    public static int PAGE_MAX = 5;
    public static int PAGE_MIN = 0;

    public GameObject[] Page = new GameObject[PAGE_MAX];
    public GameObject right, left;
    private int num;
    private int save_num;
    void Start()
    {
        right.SetActive(true);
        left.SetActive(false);
        save_num = num = 0;
        Page[0].SetActive(true);
        for(int i = 1; i < PAGE_MAX; i++)
        {
            Page[i].SetActive(false);
        }
    }

    void Update()
    {

        SearchRightActive();
        SearchLeftActive();

        if (save_num != num) CheckPage();
        save_num = num;
    }

    void CheckPage()
    {
        for (int i = 0; i < PAGE_MAX; i++)
        {
            if (i == num) Page[i].SetActive(true);
            else Page[i].SetActive(false);

        }
    }

    void SearchRightActive()
    {
        if (num >= PAGE_MAX - 1)
        {
            right.SetActive(false);
            num = PAGE_MAX - 1;
        }
        else
        {
            right.SetActive(true);
        }
    }

    void SearchLeftActive()
    {
        if (num <= 0)
        {
            left.SetActive(false);
            num = PAGE_MIN;
        }
        else
        {
            left.SetActive(true);

        }
    }

    public void AddPage() { num++; }
    public void SubPage() { num--; }
}
