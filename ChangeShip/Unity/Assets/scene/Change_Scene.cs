﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

// 小松
public class Change_Scene : MonoBehaviour {

    public string scene;
    public void SceneLoad()
    {
        Time.timeScale = 1;
        FadeManager.Instance.LoadScene(scene, 1.5f);
    }

    public void SceneRetry()
    {
        Time.timeScale = 1;
        FadeManager.Instance.LoadScene(SceneManager.GetActiveScene().name, 1.0f);
    }
}
