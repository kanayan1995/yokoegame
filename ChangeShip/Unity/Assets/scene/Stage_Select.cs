﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Stage_Select : MonoBehaviour
{

    public static int STAGE_MAX = 4;
    private GameObject obj;
    private RaycastHit hit;
    Quaternion toRot = new Quaternion(0, 0, 0, 0);
    public int speed = 5;
    private int ANGLE_MODE = 0;
    private int tap_num = 0;

    public AudioSource touch;

    public bool DebugMode = false;
    enum Vec
    {
        None,
        Right,
        Left
    }
    // stage1     0
    // stage2     81.9226
    // stage3     166.2568
    // stage4     257.1253
    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        obj = null;
        hit = new RaycastHit();
    }

    // Update is called once per frame
    void Update()
    {
        switch (ANGLE_MODE)
        {
            case (int)Vec.None:
                TapStage();
                break;
            case (int)Vec.Right:
                toRot = Quaternion.Euler(11, 45 * tap_num, 0);
                transform.rotation = Quaternion.Slerp(transform.rotation, toRot, Time.deltaTime * 5f);
                if (transform.rotation == toRot) ANGLE_MODE = (int)Vec.None;
                break;
            case (int)Vec.Left:
                toRot = Quaternion.Euler(11, 45 * tap_num, 0);
                transform.rotation = Quaternion.Slerp(transform.rotation, toRot, Time.deltaTime * 5f);
                if (transform.rotation == toRot) ANGLE_MODE = (int)Vec.None;
                break;
        }
    }

    // ステージを選択
    void TapStage()
    {
        // 左クリックを取得
        if (!FadeManager.Instance.GetisFading())
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("TapStage 1");
                // クリックしたスクリーン座標をrayに変換
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                // Rayの当たったオブジェクトの情報を格納する
                // オブジェクトにrayが当たった時
                if (Physics.Raycast(ray, out hit, 1000.0f))
                {
                    Debug.Log(hit.transform.tag);
                    //debug.text = hit.transform.name;
                    // クリックしたスクリーン座標
                    //obj = hit.collider.gameObject;

                    if (DebugMode)
                    {
                        if (hit.collider.gameObject.tag == "Stage")
                        {
                            touch.PlayOneShot(touch.clip);
                            FadeManager.Instance.LoadScene(hit.collider.gameObject.name, 2.0f);
                        }
                    }
                    else
                    {
                        if (hit.collider.gameObject.tag == "Stage")
                        {
                            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("ON"))
                            {
                                touch.PlayOneShot(touch.clip);
                                FadeManager.Instance.LoadScene(hit.collider.gameObject.name, 2.0f);
                            }
                        }
                    }
                    //debug.text = obj.transform.name;
                }
            }
        }
    }
    public void NextStage()
    {
        ANGLE_MODE = (int)Vec.Right;
        tap_num++;
    }
    public void PreviousStage()
    {
        ANGLE_MODE = (int)Vec.Left;
        tap_num--;
    }
}

