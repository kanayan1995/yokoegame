﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeCount : MonoBehaviour
{
	public Sprite[] number;         //画像を入れる
	public GameObject ten;          //十の位
	public GameObject one;			//位置の位
	private int changeCount;		//回数

	//******************************************************
	// 初期化
	//******************************************************
	void Start()
	{
		changeCount = 0;
	}

	//******************************************************
	// 更新
	//******************************************************
	void Update()
	{
		changeCount = GameObject.Find( "Main Camera" ).GetComponent<change_flow>().GetChangeCount();
		
		//とりあえず一の位から
		int workOne = changeCount % 10; //右から順に数字だす
		one.GetComponent<Image>().sprite = number[workOne];

		int workTen = changeCount / 10; //十の位の数値を出す
		ten.GetComponent<Image>().sprite = number[workTen];
	}
}
