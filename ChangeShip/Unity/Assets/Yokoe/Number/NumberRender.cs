﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//**********************************************//
//												//		
//				製作者：　横江					//
//												//
//**********************************************//

//***********************************************************************
// 画像を読み込み指定した数字を描画する
//***********************************************************************
public class NumberRender : MonoBehaviour
{
	public Sprite[] number;			//画像を入れる
	public Image setSize;           //サイズをこれに合わせる
	public GameObject oneDigitPos;	//数字を出す場所(一桁の場合)
	public GameObject twoDigitPos;  //数字を出す場所(二桁の場合)
	public float clearance;         //文字の隙間
	public GameObject numberObj;	//こいつを生成する
	private int changeCount;        //入れ替えた回数を入れる(change_flow)にある
	private int missingNumber;		//最少までに足りない数値

	//******************************************************
	// 初期化
	// 数字を出すのは一回だけでいいので初期化で終わらす
	// 最少設定と入れ替えた回数をはかり回数が最少より
	// 少なければ描画しない
	//******************************************************
	void Start()
	{
		changeCount = GameObject.Find( "Main Camera" ).GetComponent<change_flow>().GetChangeCount();
		int changeCountMin = GameObject.Find("Canvas").GetComponentInChildren<ClearDirection>().changeCountMin;
		if( changeCount <= changeCountMin ) return;
		missingNumber = changeCount - changeCountMin;
		Render();
	}

	//******************************************************
	// 数字を一つ一つ描画
	//******************************************************
	public void SetNumber(Vector3 position)
	{
		int i = 0;
		int workNum = missingNumber;    //スコアを代入
		while( true )
		{
			int num = workNum % 10; //余りを出すことで一番右の数字を出す
			GameObject imageNumber = Instantiate(numberObj);
			imageNumber.GetComponent<Image>().sprite = number[num];
			imageNumber.transform.parent = this.transform;
			Vector3 workPos = new Vector3();
			workPos = position;
			workPos.x -= i * clearance;
			imageNumber.GetComponent<Image>().rectTransform.position = workPos;
			workNum = workNum / 10;
			i++;
			if( workNum - 1 < 0 ) break;
		}
	}

	//******************************************************
	// まとめて描画
	//******************************************************
	void Render()
	{
		int digit = 1;
		int workNum = missingNumber;
		while( true )
		{
			workNum = workNum / 10;
			if( workNum - 1 < 0 ) break;
			digit++;
		}
		switch (digit)
		{
			case 1:
				SetNumber( oneDigitPos.transform.position );
				break;
			case 2:
				SetNumber( twoDigitPos.transform.position );
				break;
		}
	}


	//******************************************************
	// 更新
	//******************************************************
	void Update()
	{

	}
}