﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//**********************************************//
//												//		
//				製作者：　横江					//
//												//
//**********************************************//

//***********************************************************************
//　ステージのウィンドウを出す
//　クリア済みは最初から出す
//***********************************************************************
public class StageSelectWindow : MonoBehaviour
{

	public int stageNum = 0;        //ステージ番号を入れる
	private int ClearStageNum;      //どこまでクリアしたかを取得する
	private float   scaleUpTime;    //ウィンドウが大きくなる時間
	private Vector3 keepScale;      //ステージの大きさを保持
	private Vector3 scaleUpValue;   //ウィンドウが大きくなるスピード
	private bool    stageCheck;     //ステージが選択できるかのチェック

	//******************************************************
	// 初期化
	//******************************************************
	void Start()
	{
		stageCheck = false;
		scaleUpTime = 1.0f;
		keepScale = transform.localScale;

		ClearStageNum = PlayerPrefs.GetInt( "STAGE" );
		//クリアしてない奴はまず移さない
		if( ClearStageNum < stageNum )
		{
			transform.localScale = Vector3.zero;
		}
		else if( ClearStageNum > stageNum )
		{
			stageCheck = false;
		}
		else if( ClearStageNum == stageNum )
		{
			scaleUpValue = transform.localScale / scaleUpTime / 50.0f;
			transform.localScale = Vector3.zero;
		}
	}

	//******************************************************
	// 更新
	//******************************************************
	//void Update()
	void FixedUpdate()
	{
		if( transform.localScale.x > keepScale.x ) return;

		OnCamera();
	}

	//******************************************************
	// カメラの視野範囲に入ると宝箱を開く
	//******************************************************
	public void OnCamera()
	{
		Vector3 cameraForward = Camera.main.transform.forward;
		Vector3 cameraVec = transform.position - Camera.main.transform.position;
		cameraVec.Normalize();
		float dot = Vector3.Dot(cameraForward , cameraVec);
		if( dot > 0.9f )
		{
			if( ClearStageNum == stageNum )
			{
				transform.localScale += scaleUpValue;
			}
		}
	}
}
