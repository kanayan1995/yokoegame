﻿using UnityEngine;
using System.Collections;

//***********************************************
//製作者：　横江
//***********************************************

public class PlayerSE : MonoBehaviour
{
	public AudioSource hit;     //壁に中ったとき
	public AudioSource water;   //溺れる音
	public AudioSource goal;    //ゴールの時
															//ここから下のフラグは一回だけ鳴らしたいのでフラグを入れる
	private bool soundHitFlag;
	private bool soundWaterFlag;
	private bool soundGoalFlag;

	private Girl player;        //プレイヤ-の状態を見たい方取得

	void Start()
	{
		soundHitFlag = true;
		soundGoalFlag = true;
		soundWaterFlag = true;

		player = GameObject.Find( "ship_player" ).GetComponent<Girl>();
	}

	//--------------------------------------
	//					一度だけ鳴らす
	//--------------------------------------
	void OneShout( AudioSource sound , bool soundFlag )
	{
		if( soundFlag == true ) {
			sound.PlayOneShot( sound.clip , sound.volume );
		}
	}

	//--------------------------------------
	//上の関数を使ってそれぞれ鳴らすだけ
	//--------------------------------------
	public void HitSound()
	{
		OneShout( hit , soundHitFlag );
		soundHitFlag = false;
	}
	public void WaterSound()
	{
		OneShout( water , soundWaterFlag );
		soundWaterFlag = false;
	}
	public void GoalSound()
	{
		OneShout( goal , soundGoalFlag );
		soundGoalFlag = false;
	}
}
