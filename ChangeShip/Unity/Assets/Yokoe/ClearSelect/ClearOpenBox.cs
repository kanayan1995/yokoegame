﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearOpenBox : MonoBehaviour
{
	private Animator animator;    //アニメータ

	//SE関連
	public bool         isOpen;         //開いているか？
	public AudioSource  open;           //宝箱開く
	private bool        soundOpenFlag;  //サウンド

	// Use this for initialization
	void Start()
	{
		animator = gameObject.GetComponent<Animator>();
		animator.speed = 0; //スピード0にして一時停止
		this.enabled = false;
		isOpen = false;
		soundOpenFlag = true;
	}

	//これを呼び出すとアニメーションが再開する
	public void BoxOpen()
	{
		isOpen = true;
		this.enabled = true;
		animator.speed = 0.4f;
		if( soundOpenFlag == true )
		{
			open.PlayOneShot( open.clip , open.volume );
			soundOpenFlag = false;
		}
	}
}
