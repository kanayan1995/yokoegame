﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//**********************************************//
//												//		
//				製作者：　横江					//
//												//
//**********************************************//

//***********************************************************************
// クリアした後の演出
//***********************************************************************
public class ClearCheak : MonoBehaviour
{
	public int stageNumber;         //何番目のステージかを入れる
	public GameObject starSummary;  //星をまとめているオブジェクト
	public GameObject starOne;      //一個目の☆
	public GameObject starTwo;      //二個目の☆
	public GameObject starThree;    //三個目の☆
	public GameObject genius;       //天才

	//******************************************************
	// 初期化
	//******************************************************
	void Start()
	{
		if( stageNumber == 5 )
		{
			int a = 0;
		}
		int clearCheack =  PlayerPrefs.GetInt("STAGECLEAR" + stageNumber.ToString());
		//if( clearCheack >= 1 ) GetComponentInChildren<ClearOpenBox>().BoxOpen();
		if( clearCheack >= 2 ) starOne.SetActive( true );
		if( clearCheack >= 3 ) starTwo.SetActive( true );
		if( clearCheack >= 4 ) starThree.SetActive( true );
	}

	//******************************************************
	// 更新
	//******************************************************
	void Update()
	{
		if( GetComponentInChildren<ClearOpenBox>().isOpen == false )
		{
			OnCamera();
		}
	}

	//******************************************************
	// カメラの視野範囲に入ると宝箱を開く
	//******************************************************
	public void OnCamera()
	{
		Vector3 cameraForward = Camera.main.transform.forward;
		Vector3 cameraVec = transform.position - Camera.main.transform.position;
		cameraVec.Normalize();
		float dot = Vector3.Dot(cameraForward , cameraVec);
		if( dot > 0.9f )
		{
			int clearCheack =  PlayerPrefs.GetInt("STAGECLEAR" + stageNumber.ToString());
			if( clearCheack >= 1 ) GetComponentInChildren<ClearOpenBox>().BoxOpen();
		}
	}
}
