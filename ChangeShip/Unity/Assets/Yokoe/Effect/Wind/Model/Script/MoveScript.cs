﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour
{
  public Vector3 moveVec;
  public float moveSpeed;
  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    //Vector3 front = this.gameObject.transform.forward;
    Vector3 workPos = this.gameObject.transform.position;
    Quaternion aaa =  this.gameObject.transform.rotation;
    workPos += moveVec;
    this.gameObject.transform.position = workPos;
  }

  void Retry()
  {
    
  }
}
