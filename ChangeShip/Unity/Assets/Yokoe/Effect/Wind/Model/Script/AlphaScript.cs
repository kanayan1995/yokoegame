﻿using UnityEngine;
using System.Collections;

public class AlphaScript : MonoBehaviour
{
  public float alphaDownTime;     //消え始める時間
  public float alphaUpTime;       //出始める時間
  public float durationTime;      //効果時間
  private float AlphaCount;       //変わるまでの時間
  private bool changeAlphaFlag;   

  private Color objectColor;              //オブジェクトの色を変化させる
  // Use this for initialization
  void Start()
  {
    objectColor = gameObject.GetComponent<SkinnedMeshRenderer>().material.GetColor("_TintColor");
    AlphaCount = 0;
    changeAlphaFlag = false;
    //objectColor = startColor;   //初期色を代入
  }

  // Update is called once per frame
  void Update()
  {
    AlphaCount += Time.deltaTime;  //時間更新
    if ((int)AlphaCount == 1) changeAlphaFlag = true;
    if(changeAlphaFlag == true) objectColor.a -= 0.005f;
    if (objectColor.a == 0) changeAlphaFlag = false;

    if(AlphaCount > 3.5) {
      objectColor.a += 0.01f;
    }
    gameObject.GetComponent<SkinnedMeshRenderer>().material.SetColor("_TintColor" , objectColor);
  }
}
