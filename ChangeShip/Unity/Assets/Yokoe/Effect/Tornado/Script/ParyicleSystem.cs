﻿using UnityEngine;
using System.Collections;

public class ParyicleSystem : MonoBehaviour
{
	ParticleSystem particle;  //パーティクルシステム

	// Use this for initialization
	void Start()
	{
		ParticleSystem ps = GetComponent<ParticleSystem>();
		var vel = ps.velocityOverLifetime;
		vel.enabled = true;
		vel.space = ParticleSystemSimulationSpace.Local;

		AnimationCurve curve = new AnimationCurve();
		curve.AddKey( 0.0f , 1.0f );	
		curve.AddKey( 1.0f , 0.0f );
		vel.x = new ParticleSystem.MinMaxCurve( 10.0f , curve );
	}

	// Update is called once per frame
	void Update()
	{

	}
}
