﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//***********************************************
//製作者：　横江　朝海
//***********************************************
public class Barrel : MonoBehaviour
{
	public GameObject		hitObj;			//当たったら樽を消す
	public ParticleSystem   smokeEffect;	//煙を出す
	public ParticleSystem   breakEffect;    //壊れた樽を出す
	public Vector3 moveVec;

	private float detheCountMax = 4.0f;
	private float detheCount;

	private GameObject enemy;

	// Use this for initialization
	void Start()
	{
		enemy = GameObject.Find( "ship_enemy" );
		detheCount = 0;
    }

	// Update is called once per frame
	void Update()
	{
		if( GameModeManager.GetGameMode() == GameModeManager.GAMEMODE.MAIN )
		{
			//transform.position += b_vec * Time.deltaTime * 50;
			transform.position += moveVec * Time.deltaTime * 50;
		}
		if( enemy.GetComponent<Pirate>().GetMode() == Pirate.ENEMY_MODE.MOVE )
		{
			detheCount += Time.deltaTime;
			moveVec = new Vector3( 0 , -0.001f , 0 );
		}
		if (detheCount > detheCountMax)
		{
			Destroy( this.gameObject );
		}
	}

	void OnTriggerEnter( Collider col )
	{
		if( enemy.GetComponent<Pirate>().GetMode() == Pirate.ENEMY_MODE.STAY )
		{
			if( col.tag == "Hit" )
			{
				hitObj.SetActive( false );
				smokeEffect.Emit( 20 );
				breakEffect.Emit( 20 );
			}
		}

		if( enemy.GetComponent<Pirate>().GetMode() == Pirate.ENEMY_MODE.MOVE )
		{
			if( col.tag == "Pirate" )
			{
				moveVec = new Vector3( 0 , -0.003f , 0 );
			}
		}
	}
}
