﻿using UnityEngine;
using System.Collections;

//***********************************************
//製作者：　横江
//***********************************************

//時間を少し遅らせて出現する
public class Late : MonoBehaviour
{
	public  GameObject obj;             //こいつの存在を遅らせてだす
	public  float playTimeMax   = 1.0f; //発生する時間遅れる
	private float playTime;             //時間進める
	private bool startFlag;             //時間を進めるフラグ

	// Use this for initialization
	void OnEnable()
	{
		playTime = 0.0f;
		startFlag = false;
		obj.SetActive( false );
	}

	// Update is called once per frame
	void Update()
	{
		if( startFlag == true ) return;

		playTime += Time.deltaTime;
		if( playTime > playTimeMax )
		{
			obj.SetActive( true );
			playTime = 0.0f;
			startFlag = true;
		}
	}
}
