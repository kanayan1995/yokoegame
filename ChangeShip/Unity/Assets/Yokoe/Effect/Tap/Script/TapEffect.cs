﻿using UnityEngine;
using System.Collections;

public class TapEffect : MonoBehaviour
{
	public ParticleSystem coinEffect;			//タッチした時に出るエフェクト
	public ParticleSystem lightEffect;          //タッチした時に出るエフェクト
	private const float effectZPos = 0.4f;		//カメラからみた時のZ値

	//--------------------------------------
	//	更新
	//	Menuの時には発生させない
	//--------------------------------------
	void Update()
	{
		if( GameModeManager.GetGameMode() == GameModeManager.GAMEMODE.MENU ) return;
		if( Input.GetMouseButtonDown( 0 ) )
		{
			//タッチした画面座標からワールド座標へ変換
			Vector3 workPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x , Input.mousePosition.y , effectZPos));
			transform.position = workPos;
			transform.position += new Vector3( 0 , -0.01f , 0 );
			
			coinEffect.Emit( 4 );
			lightEffect.Emit( 10 );
        }
	}

	void OnMouseDown()
	{

	}

	void OnMouseDrag()
	{

	}
}
