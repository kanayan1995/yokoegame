﻿using UnityEngine;
using System.Collections;

//***********************************************
//製作者：　横江　朝海
//***********************************************

//ビルボードとカメラの距離によって長さをかえている
public class ChangeUI : MonoBehaviour
{
	private bool changeScaleFlag;           //大きさ変えるフラグ
	private Vector3 keepScale;              //元の大きさ保持
	private Vector3 changeScaleValue;       //スケールサイズ変更の値
	private float changeTimeMax = 0.8f;     //変える時間の最大
	private float changeTime;               //変える時間

	void Awake()
	{
		keepScale = this.transform.localScale;
		changeScaleFlag = false;
		changeScaleValue = new Vector3( 0.01f , 0.01f , 0.01f );
		changeTime = 0;
		ChangeSize();
	}

	//--------------------------------------
	//	更新
	//	選ばれてないときと
	//	Mainじゃない時は動かさない
	//--------------------------------------
	void Update()
	{
		if( GameModeManager.GetGameMode() != GameModeManager.GAMEMODE.MAIN ) return;
		if( changeScaleFlag == false ) return;

		changeTime += Time.deltaTime;
		this.transform.localScale += changeScaleValue;

		if( changeTime > changeTimeMax )
		{
			changeTime = 0;
			changeScaleValue = -changeScaleValue;
		}
	}

	//カメラの向きによって大きさとか調整
	public void ChangeSize()
	{
		this.transform.forward = Camera.main.transform.forward;
		float dist = Vector3.Distance(this.gameObject.transform.position , Camera.main.transform.position);
		dist = ( dist * 0.08f ) + 1.0f;   //距離を0.8倍奴に1を足してる　　例（ 5.0を0.5にして1.0足して1.5にしてる
		this.transform.localScale = keepScale * dist;
	}

	public void SetChangeScaleFlag( bool setFlag )
	{
		changeScaleFlag = setFlag;
	}

	public void Reset()
	{
		changeTime = 0;
		changeScaleValue = new Vector3( 0.01f , 0.01f , 0.01f );
		ChangeSize();
	}
}
