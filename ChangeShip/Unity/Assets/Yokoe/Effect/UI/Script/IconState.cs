﻿using UnityEngine;
using System.Collections;

//***********************************************
//製作者：　横江　朝海
//***********************************************

public class IconState : MonoBehaviour
{
	void Start()
	{

	}

	void Update()
	{
		switch( GameModeManager.GetGameMode() )
		{
			case GameModeManager.GAMEMODE.WAIT:
				this.gameObject.GetComponent<SpriteRenderer>().material.color = new Color( 0.5f , 0.5f , 0.5f , 1.0f );
				break;
			case GameModeManager.GAMEMODE.MAIN:
				this.gameObject.GetComponent<SpriteRenderer>().material.color = new Color( 1.0f , 1.0f , 1.0f , 1.0f );
				break;
		}
	}

	public void SetIconState()
	{

	}
}
