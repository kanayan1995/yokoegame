﻿//half 16ビット　ショートベクトル　方向　オブジェクト空間位置　HDRカラーにしようされる
//fixed 低精度　固定少数点数　一般的に11ビット　-2.0から+2.0の範囲で、1/256精度です　

Shader "Custom/WaveShader"{
	Properties{
		_Color( "Main Color", Color ) = (1,1,1,1)
		_MainTex( "MainTex", 2D ) = "white" {}
		_AlphaTex( "AlphaTex", 2D ) = "white" {}
	}

	SubShader{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma multi_compile_fog

		#include "UnityCG.cginc"

		struct appdata_t
		{
			float4 vertex : POSITION;
			float2 texcoord : TEXCOORD0;
		};

		struct v2f
		{
			float4 vertex : SV_POSITION;
			half2 texcoord : TEXCOORD0;
			UNITY_FOG_COORDS( 1 )
		};

		sampler2D _MainTex;
		float4 _MainTex_ST;
		fixed4 _Color;

		v2f vert( appdata_t v )
		{
			v2f o;
			o.vertex = mul( UNITY_MATRIX_MVP, v.vertex );
			o.texcoord = TRANSFORM_TEX( v.texcoord, _MainTex );
			UNITY_TRANSFER_FOG( o,o.vertex );
			return o;
		}

		fixed4 frag( v2f i ) : SV_Target
		{
			fixed4 col = tex2D( _MainTex, i.texcoord ) * _Color;
			UNITY_APPLY_FOG( i.fogCoord, col );
			return col;
			}
				ENDCG
			}
		}
}

