﻿using UnityEngine;
using System.Collections;

public class UVAnimation : MonoBehaviour
{
	public float moveX = 1;   //Uに移動
	public float moveY = 1;   //Vに移動		Unityは左手系座標	ワールドに合わせるため-方向に動かす
	public int materialNumber = 0; //materialが複数あった場合何番目を動かすか指定
	Vector2 uv;
	public bool skinnedMeshFlag = false;    //スキンメッシュかメッシュかの選択　trueならスキンメッシュ　falseならメッシュ

	// Use this for initialization
	void Start()
	{
		Debug.Assert( moveX < 100 , "100以上はつかえないよ!!" );
		Debug.Assert( moveY < 100 , "100以上はつかえないよ!!" );
	}

	// Update is called once per frame
	void Update()
	{
		uv.x += moveX * 0.01f * Time.deltaTime * 50;						//値が1からでも出来るように100/1してる 
		uv.y += moveY * -0.01f * Time.deltaTime * 50;						//値が1からでも出来るように-100/1してる ワールドに合わせるため-方向に動かす
		if( uv.x > 1.0f ) { uv.x = uv.x * 0.01f * Time.deltaTime * 50; }    //1より大きくなったら100/1にする
		if( uv.y < -1.0f ) { uv.y = uv.y * 0.01f * Time.deltaTime * 50; }   //-1より小さくなったら100/1にする

		if( skinnedMeshFlag == false ) {
			MeshUVAnimetion();
		}
		else {
			SkinnedMeshUVAnimetion();
		}
	}

	//MeshRendererでのUVアニメーション
	void MeshUVAnimetion()
	{
		if( materialNumber == 0 ) {
			gameObject.GetComponent<MeshRenderer>().material.SetTextureOffset( "_MainTex" , uv );
		}
		else {
			for( int i = 0 ; i < materialNumber ; i++ ) {
				gameObject.GetComponent<MeshRenderer>().materials[i].SetTextureOffset( "_MainTex" , uv * i );
			}
		}
	}

	//SkinnedMeshRendererでのUVアニメーション
	void SkinnedMeshUVAnimetion()
	{
		if( materialNumber == 0 ) {
			gameObject.GetComponent<SkinnedMeshRenderer>().material.SetTextureOffset( "_MainTex" , uv );
		}
		else {
			for( int i = 0 ; i < materialNumber ; i++ ) {
				gameObject.GetComponent<SkinnedMeshRenderer>().materials[i].SetTextureOffset( "_MainTex" , uv * i );
			}
		}
	}

}
