﻿using UnityEngine;
using System.Collections;


//***********************************************
//製作者：　横江　朝海
//***********************************************

public class HitEffectEnemy : MonoBehaviour
{
	public ParticleSystem particle;   //パーティクル
	private bool hitFlag = false;

	private Pirate.ENEMY_MODE enemyMode;

	void Update()
	{
		if( hitFlag == true ) return;
		enemyMode = transform.root.GetComponent<Pirate>().enemy_mode;
		if( enemyMode == Pirate.ENEMY_MODE.SINK ) {
			particle.Play();
			hitFlag = true;
		}
	}
}
