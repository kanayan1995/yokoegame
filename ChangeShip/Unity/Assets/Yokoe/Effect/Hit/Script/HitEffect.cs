﻿using UnityEngine;
using System.Collections;


//***********************************************
//製作者：　横江　朝海
//***********************************************

public class HitEffect : MonoBehaviour
{
	public ParticleSystem particle;   //パーティクル
	private bool hitFlag = false;

	private Girl.GIRL_MODE shipMode;  //モード

	void Update()
	{
		if( hitFlag == true ) return;
		shipMode = transform.root.GetComponent<Girl>().girl_mode;
		if( shipMode == Girl.GIRL_MODE.ROCK ) {
			particle.Play();
			hitFlag = true;
		}
	}
}
