﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//***********************************************
//製作者：　横江　朝海
//***********************************************

public class ShipLocus : MonoBehaviour
{
	public ParticleSystem particle;             //パーティクルシステム
	private float changeRadian = 3.1415f / 180; //一度
	private float firstAngle;                   //初期角度

	private Girl.GIRL_MODE shipMode;    //船の状態
	private Vector3 shipSpeed;          //船の速さ
	private bool shipStart = false;     //スタートを押すまで
	private float EmiiTime = 0.0f;
	private const float emitTimeMax = 0.5f;

	void Start()
	{
		particle.Stop();
		firstAngle = 180 * changeRadian;  //向きは180回転させとく
	}

	void Update()
	{

		if( shipStart == false ) {	
			shipStart = gameObject.transform.root.GetComponent<Girl>().start;
			return;
		}

		CheakPlay();
		SpeedAdjustment();
		RotatonAdjustment();
	}

	//ガールの状態によって動かすか決める
	void CheakPlay()
	{
		shipMode = transform.root.GetComponent<Girl>().girl_mode;
		if( shipMode == Girl.GIRL_MODE.MOVE ) { //移動してない時以外はparticleが動く
			EmiiTime += Time.deltaTime;
			if( EmiiTime > emitTimeMax ) {
				particle.Emit( 1 );
				EmiiTime = 0.0f;
			}
		}
	}

	//速度調整
	void SpeedAdjustment()
	{
		shipSpeed = transform.root.GetComponent<Girl>().b_vec;
		float workSpeed = -shipSpeed.z;
		particle.startSpeed = workSpeed;
	}

	//向き調整
	void RotatonAdjustment()
	{
		float workAngle = transform.rotation.eulerAngles.y * changeRadian;
		particle.startRotation3D = new Vector3( 0 , firstAngle + workAngle , 0 );
	}
}
