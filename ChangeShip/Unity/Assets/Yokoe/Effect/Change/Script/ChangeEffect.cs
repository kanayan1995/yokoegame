﻿using UnityEngine;
using System.Collections;

//***********************************************
//製作者：　横江　朝海
//***********************************************

//入れ替えの時に使うスクリプト
public class ChangeEffect : MonoBehaviour
{
	//マジックナンバー使いたくないからとりあえず付けとく
	public enum NUMBER
	{
		FIRST = 1,
		SECOND = 2
	}

	public ParticleSystem firstChangeEffectFlow;			//1タップ目の流れエフェクト
	public ParticleSystem firstChangeEffectWhirlpool;		//1タップ目の渦エフェクト
	public ParticleSystem secondChangeEffectFlow;			//2タップ目の流れエフェクト
	public ParticleSystem secondChangeEffectWhirlpool;		//2タップ目の渦エフェクト
	private ParticleSystem firstChangeEffectMove;			//2タップ目のエフェクト
	private ParticleSystem secondChangeEffectMove;			//2タップ目のエフェクト
	public ParticleSystem LightEffect;						//ボタンの位置に出るキラキラエフェクト
	private Vector3 firstButtonPos;							//1タップ目のボタンの位置
	private Vector3 secondButtonPos;						//2タップ目のボタンの位置

	private bool changeFlag;								//入れ替えのエフェクトを出す
	private change_flow changeFlow;							//入れ替えを実行しているスクリプト
	private Vector3 posUp = new Vector3(0,0.2f,0);          //ちょっと上にあげる

	private const float changeTimeMax = 90.0f;			//変わるまでの時間
	//private const float changeTimeMax = 1.5f;				//変わるまでの時間
	private Vector3 workVec;								//ボタン1からボタン2の向き
	private float workDist;									//ボタンとボタンの距離
	private float changeTime;								//変わっていく時間
	private float workTime;									//ボタンに行くまでの時間

	static private Vector3[] chageScaleValue = new Vector3[2];
	static private Vector3[] keepScale = new Vector3[2];
	private const int TAPCOUNT = 2;
	static public GameObject[] tapObj = new GameObject[TAPCOUNT];
	static private float changeScaleTimeMax = 45.0f;
	//static private float changeScaleTimeMax = 0.75f;
	//static private int changeScaleTimeMax = 14;

	static public bool flowChangeFlag;						//これがtrueなら選択ができる

	private float dirFirstFlow;
	private float dirSecondFlow;

	void Start()
	{
		firstChangeEffectFlow.Stop();
		firstChangeEffectWhirlpool.Stop();
		secondChangeEffectFlow.Stop();
		secondChangeEffectWhirlpool.Stop();
		firstChangeEffectMove = null;
		secondChangeEffectMove = null;
		changeFlag = false;
		changeFlow = GetComponent<change_flow>();
		changeTime = 0.0f;
		flowChangeFlag = true;
	}

	//--------------------------------------
	//	更新
	//	メニュー画面の時だけ動かない
	//--------------------------------------
	//void Update()
	void FixedUpdate()
	{
		if( GameModeManager.GetGameMode() == GameModeManager.GAMEMODE.MENU ) return;
		if( changeFlag == false ) return;

		//firstChangeEffect.transform.position += workVec * workTime;
		//secondChangeEffect.transform.position += -workVec * workTime;
		firstChangeEffectMove.startRotation = ( dirFirstFlow ) * 3.1415f / 180;
		secondChangeEffectMove.startRotation = ( dirSecondFlow ) * 3.1415f / 180;
		MoveEffect( firstButtonPos , secondButtonPos , firstChangeEffectMove , 0.5f );
		MoveEffect( secondButtonPos , firstButtonPos , secondChangeEffectMove , -0.5f );

		changeTime++;
		//changeTime += Time.deltaTime;
		if( changeTime > changeTimeMax )
		{
			changeFlag = false;
			changeTime = 0.0f;
			firstChangeEffectMove.Stop();
			secondChangeEffectMove.Stop();
			LightEffect.transform.position = firstButtonPos;
			LightEffect.Emit( 30 );
			LightEffect.transform.position = secondButtonPos;
			LightEffect.Emit( 30 );
			flowChangeFlag = true;

			tapObj[0].transform.localScale = keepScale[0];
			chageScaleValue[0] = Vector3.zero;
			tapObj[1].transform.localScale = keepScale[1];
			chageScaleValue[1] = Vector3.zero;
		}

		if( changeTime <= changeScaleTimeMax )
		{
			tapObj[0].transform.localScale -= chageScaleValue[0];
			tapObj[1].transform.localScale -= chageScaleValue[1];
			if( tapObj[0].transform.localScale.x < 0 )
			{
				tapObj[0].transform.localScale = Vector3.zero;
				tapObj[1].transform.localScale = Vector3.zero;
			}
		}
		else
		{
			tapObj[0].transform.localScale += chageScaleValue[0];
			tapObj[1].transform.localScale += chageScaleValue[1];
			if( tapObj[0].transform.localScale.x > keepScale[0].x )
			{
				tapObj[0].transform.localScale = keepScale[0];
				chageScaleValue[0] = Vector3.zero;
			}
			if( tapObj[1].transform.localScale.x > keepScale[1].x )
			{
				tapObj[1].transform.localScale = keepScale[1];
				chageScaleValue[1] = Vector3.zero;
			}
		}
	}

	public void SetFirstTap( Flow_Parent.MODE mode )
	{
		if( mode == Flow_Parent.MODE.流れ ) firstChangeEffectMove = firstChangeEffectFlow;
		if( mode == Flow_Parent.MODE.渦 ) firstChangeEffectMove = secondChangeEffectWhirlpool;
		firstButtonPos = changeFlow.GetComponent<change_flow>().GetButtonUI( (int)NUMBER.FIRST );
		firstChangeEffectMove.transform.position = firstButtonPos;
		firstChangeEffectMove.transform.position += posUp;
	}

	public void SetSecondTap( Flow_Parent.MODE mode )
	{
		if( mode == Flow_Parent.MODE.流れ ) secondChangeEffectMove = secondChangeEffectFlow;
		if( mode == Flow_Parent.MODE.渦 ) secondChangeEffectMove = secondChangeEffectWhirlpool;
		secondButtonPos = changeFlow.GetComponent<change_flow>().GetButtonUI( (int)NUMBER.SECOND );
		secondChangeEffectMove.transform.position = secondButtonPos;
		secondChangeEffectMove.transform.position += posUp;
	}

	public void ChangeFlagTrue()
	{
		changeFlag = true;
		workVec = secondButtonPos - firstButtonPos;           //1から2への向き
		workVec.Normalize();
		workDist = Vector3.Distance( firstButtonPos , secondButtonPos ); //距離入れる
		workTime = workDist / changeTimeMax;  //時間で区切る
		flowChangeFlag = false;
		firstChangeEffectMove.Play();
		secondChangeEffectMove.Play();
	}

	//タップした
	public void SetChangetObj( int number )
	{
		tapObj[number] = changeFlow.GetChangeObject( number );
		keepScale[number] = tapObj[number].transform.localScale;
		chageScaleValue[number] = tapObj[number].transform.localScale / changeScaleTimeMax;
	}


	//タップした
	static public void SetChangetObjWorld( int number , GameObject flow )
	{
		tapObj[number] = flow;
		keepScale[number] = tapObj[number].transform.localScale;
		chageScaleValue[number] = tapObj[number].transform.localScale / changeScaleTimeMax;
	}

	//線形補間
	void MoveEffect( Vector3 firstPos , Vector3 lastPos , ParticleSystem moveObj , float dir )
	{
		Vector3 center = (firstPos + lastPos) * 0.5f;
		center -= new Vector3( dir , 0 , 0 );
		Vector3 workPos1 = firstPos - center + posUp;
		Vector3 workPos2 = lastPos - center + posUp;
		moveObj.transform.position = Vector3.Slerp( workPos1 , workPos2 , changeTime / changeTimeMax );
		moveObj.transform.position += center;
	}

	public void EffectDir( Gimmick.Dir dir , NUMBER num )
	{
		float ang = 0;
		switch( dir )
		{
			case Gimmick.Dir.UP:
				ang = 270;
				break;
			case Gimmick.Dir.DOWN:
				ang = 90;
				break;
			case Gimmick.Dir.RIGHT:
				ang = 0;
				break;
			case Gimmick.Dir.LEFT:
				ang = 180;
				break;
		}
		if( num == NUMBER.FIRST ) dirFirstFlow = ang;
		if( num == NUMBER.SECOND ) dirSecondFlow = ang;
	}
}
