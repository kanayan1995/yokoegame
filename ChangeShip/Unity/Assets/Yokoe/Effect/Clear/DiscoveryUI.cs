﻿using UnityEngine;
using System.Collections;

//***********************************************
//製作者：　横江　朝海
//***********************************************

public class DiscoveryUI : MonoBehaviour
{
	public GameObject UI;
	Girl.GIRL_MODE shipMode;		//船の状態
	void Update()
	{
		shipMode = gameObject.transform.root.GetComponent<Girl>().girl_mode;
		if(shipMode == Girl.GIRL_MODE.CLEAR) {
			UI.SetActive( true );
		}
	}
}
