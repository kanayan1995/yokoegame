﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//***********************************************
//製作者：　横江　朝海
//***********************************************

public class GoalArea : MonoBehaviour
{
	public string scene;

	public bool goalFlag = false;  //ゴールしているかどうか
	private enum GOAL_DIRECTION     //演出のモード
	{
		CLEARSTRAT,
		BOXFLY,
		OPEN,
		SETCANVAS,
		END
	}
	private GOAL_DIRECTION mode = GOAL_DIRECTION.CLEARSTRAT;

	public GameObject goalText;         //ゴールした時に出る文字エフェクト
	private Vector3 clearTextPos;       //ゴールの文字のエフェクトの位置	

	public GameObject openEffect;       //宝箱を開けたきらきら
	private Vector3 opemEffectPos;      //きらきらエフェクトの位置

	private Vector3 treasureBoxtPos;    //宝箱の位置
	private GameObject treasureBox;     //宝箱
	private Vector3 firstPos;           //始めの宝箱の場所を保持
	private Vector3 lastPos;            //宝箱の終着点
	private float moveTime = 0.0f;      //移動さすのに使う

	public GameObject islandEffect;     //島にキラキラが出る

	private GameObject clear;

	private GameSE gameSE;							//カメラにあるSE取得

	//------------------------------------------
	//     初期化
	//------------------------------------------
	void Start()
	{
		clear = GameObject.Find( "Canvas" );
		//SE取得
		gameSE = GameObject.Find( "Main Camera" ).GetComponent<GameSE>();
	}

	//------------------------------------------
	//     宝箱の位置をいじるのに使うやつセット
	//------------------------------------------
	void SetTreasureBoxPos( Vector3 cameraPos )
	{
		treasureBox = GameObject.Find( "treasure_box" );
		firstPos = treasureBox.transform.position;
		cameraPos.z -= 0.14f;
		lastPos = cameraPos + Camera.main.transform.forward * 0.5f;
		//lastPos += Camera.main.transform.right * 0.32f;
		//lastPos -= Camera.main.transform.up * 0.13f;
		//lastPos += Camera.main.transform.forward * 0.13f;
	}

	//------------------------------------------
	//     テキストエフェクトの位置調整
	//------------------------------------------
	void SetOpenEffectPos( Vector3 cameraPos )
	{
		cameraPos.y -= 0.1f;
		opemEffectPos = cameraPos + Camera.main.transform.forward * 0.5f;
		opemEffectPos.y += 0.1f;
	}

	//------------------------------------------
	//     箱あける時のきらきらエフェクトの位置調整
	//------------------------------------------
	void SetTextEffectPos( Vector3 cameraPos )
	{
		clearTextPos = cameraPos + Camera.main.transform.forward * 0.4f;
		clearTextPos.y += 0.02f;
	}

	//------------------------------------------
	//     エフェクトの位置決め
	//------------------------------------------
	void SetEffectPos()
	{
		Vector3 cameraPos = Camera.main.transform.position;
		SetTreasureBoxPos( cameraPos );
		SetOpenEffectPos( cameraPos );
		SetTextEffectPos( cameraPos );

	}

	//------------------------------------------
	//     放物線に動かす
	//------------------------------------------
	void MoveTreasureBox()
	{
		Vector3 center = (firstPos + lastPos) * 0.5f;
		center -= new Vector3( 0 , 2 , 0 );
		Vector3 workPos1 = firstPos - center;
		Vector3 workPos2 = lastPos - center;
		moveTime += ( Time.deltaTime * 0.5f );
		treasureBox.transform.position = Vector3.Slerp( workPos1 , workPos2 , moveTime );
		treasureBox.transform.position += center;
		treasureBox.GetComponent<TreasureBoxOpen>().SetPos( treasureBox.transform.position );
		treasureBox.transform.forward = Camera.main.transform.forward;
		treasureBox.transform.rotation *= new Quaternion( 0 , 180 , 0 , 1 );
	}


	//------------------------------------------
	//    ゴール演出の設置
	//------------------------------------------
	void SetEffect()
	{
		treasureBox.GetComponent<TreasureBoxOpen>().BoxOpen();
		//treasureBox.transform.rotation *= new Quaternion( 0 , 1 , 0 , 50 );
		//ゴールテキスト
		//GameObject workGoal = (GameObject)Instantiate(goalText,clearTextPos, Quaternion.identity);
		//workGoal.transform.forward = Camera.main.transform.forward;
		//開けてのきらきら
		GameObject workOpenEffect = (GameObject)Instantiate(openEffect , opemEffectPos , openEffect.transform.rotation);
	}

	//------------------------------------------
	//     クリアしたかの判断
	//------------------------------------------
	void SetCanvas()
	{
		clear.GetComponent<UI_Controltest>().OnClear();
	}

	//------------------------------------------
	//     更新処理
	//------------------------------------------
	//void Update()
	void FixedUpdate()
	{
		if( goalFlag == false ) return;
		Time.timeScale = 1;
		switch( mode ) {
			case GOAL_DIRECTION.CLEARSTRAT:
				SetEffectPos();
				islandEffect.SetActive( true );
				if( islandEffect.GetComponent<IslandEffect>().animeEndFlag ) {
					mode = GOAL_DIRECTION.BOXFLY;
				}
				break;
			case GOAL_DIRECTION.BOXFLY:
				MoveTreasureBox();
				if( moveTime > 1.2f ) {
					mode = GOAL_DIRECTION.OPEN;
				}
				break;
			case GOAL_DIRECTION.OPEN:
				SetEffect();
				mode = GOAL_DIRECTION.SETCANVAS;
				break;
			case GOAL_DIRECTION.SETCANVAS:
				SetCanvas();
				mode = GOAL_DIRECTION.END;
				break;
			case GOAL_DIRECTION.END:
				break;
		}
	}

	//------------------------------------------
	//     あたり判定
	//------------------------------------------
	void OnTriggerEnter( Collider col )
	{
		if( col.gameObject.tag == "Player" ) {
			goalFlag = true;
			clear.GetComponent<UI_Controltest>().SetRightandLeft( false );
			clear.GetComponent<UI_Controltest>().OffPlay();
			gameSE.GoalSound();
			//gameObject.transform.Find( "ship_player" ).GetComponent<Girl>().girl_mode = Girl.GIRL_MODE.CLEAR;
		}

		if( col.gameObject.tag == "Pirate" ) {
			col.GetComponent<Pirate>().SetMode( 5 );
		}
	}


}

