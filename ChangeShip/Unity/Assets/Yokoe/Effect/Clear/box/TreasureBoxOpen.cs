﻿using UnityEngine;
using System.Collections;

public class TreasureBoxOpen : MonoBehaviour
{
	private Animator animator;    //アニメータ
	private GameSE gameSE;        //SE取得

	// Use this for initialization
	void Start()
	{
		animator = gameObject.GetComponent<Animator>();
		animator.speed = 0; //スピード0にして一時停止
		this.enabled = false;
		//SE取得
		gameSE = GameObject.Find( "Main Camera" ).GetComponent<GameSE>();
	}

	//これを呼び出すとアニメーションが再開する
	public void BoxOpen()
	{
		this.enabled = true;
		animator.speed = 1;
		gameSE.OpenSound();
		gameSE.GorgeousSound();
	}

	public void SetPos( Vector3 setPos )
	{
		transform.position = setPos;
	}
}
