﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearDirectionThotrial : MonoBehaviour
{
	public int stageNumber;         //何番目のステージかを入れる
	public int changeCountMin;      //最少回数を設定(GoalArea)に入れる
	public GameObject starOne;      //一個目の☆
	public GameObject starTwo;      //二個目の☆
	public GameObject starThree;    //三個目の☆
	public GameObject perfectFont;  //PerFectを表示するImage
	public GameObject countFont;    //回数を出す文字
	public GameObject strongest;    //こっちが予想していたのを超えたときに出す

	private int changeCount;        //chanege_flowから入れ替えた回数を取得
	private int getClearStar;       //星を取得した数

	//******************************************************
	// 入れ替えた回数に応じての描画選択
	//******************************************************
	//--------------------------------------
	//	入れ替えた回数に対しての星の数描画
	//	☆一つ　最少の2倍
	//	☆二つ　最少の半分 + 最少の数
	//	☆三つ　最少以下
	//--------------------------------------
	public void SetStar()
	{
		getClearStar = 0;
		if( changeCount < changeCountMin * 2 )
		{
			starOne.SetActive( true );
			getClearStar = 2;
		}
        if( changeCount <= changeCountMin / 2 + changeCountMin )
		{
			starTwo.SetActive( true );
			getClearStar = 3;
		}
		if( changeCount <= changeCountMin )
		{
			starThree.SetActive( true );
			getClearStar = 4;
		}
	}

	//--------------------------------------
	//	回数でのうまさを文字で表す
	//--------------------------------------
	public void SetFont()
	{
		if( changeCount > changeCountMin ) countFont.SetActive( true );
		if( changeCount == changeCountMin ) perfectFont.SetActive( true );
		if( changeCount < changeCountMin ) strongest.SetActive( true );

	}

	//******************************************************
	// 初期化
	//******************************************************
	void Start()
	{
		//チュートリアルは一発成功なのでこいつで十分
		changeCount = 1;
		SetStar();
		SetFont();

		int cheackStar;
		cheackStar = PlayerPrefs.GetInt( "STAGECLEAR" + stageNumber.ToString() );
		if( cheackStar < getClearStar )
		{
			PlayerPrefs.SetInt( "STAGECLEAR" + stageNumber.ToString() , getClearStar );
			PlayerPrefs.Save();
		}
	}

	//******************************************************
	// 更新
	//******************************************************
	void Update()
	{

	}
}
