﻿using UnityEngine;
using System.Collections;

//***********************************************
//製作者：　横江　朝海
//***********************************************
public class GameModeManager : MonoBehaviour
{
	public enum GAMEMODE //ゲームの状態
	{
		WAIT,		//GO押す前の待機状態
		MAIN,		//GO押した後の遊ぶ時
		GAMEOVER,	//死んだとき
		GAMECLEAR,	//クリアした時
		MENU,		//メニュー画面
	}
	static private GAMEMODE gameMode;    //ゲームの状態を確認できる 

	// Use this for initialization
	void OnLevelWasLoaded()
	{
		gameMode = GAMEMODE.WAIT;
	}

	public static void SetGameMode( GAMEMODE setMode ) { gameMode = setMode; }
	public static GAMEMODE GetGameMode() { return gameMode; }
}
