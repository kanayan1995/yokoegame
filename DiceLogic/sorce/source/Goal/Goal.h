//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __GOAL_H__
#define __GOAL_H__

#include "../Character/Character.h"
#include "../Collision/Sphere.h"

//*************************************************************
//	Goalクラス
//	プレイヤーのPEOPLEがTWOになれば存在する
//	こいつでGAMEMODEをGOALに変える
//*************************************************************
class Goal
{
private:
	unique_ptr<iexMesh> m_obj;	//goalのオブジェクト
	Vector3 m_pos;				//位置
	Vector3 m_angle;			//向き
	Vector3 m_scale;			//スケール

	Sphere m_sphere;					//あたり判定球を設置
	weak_ptr<PlayerCharacter> m_player;	//プレイヤーのポインタ
public:
	Goal()
	{
		m_obj = nullptr;
		m_pos = Vector3Zero;
		m_angle = Vector3Zero;
		m_scale = Vector3One;
	}
	~Goal()
	{

	}

	void Initialize();
	void Update();
	void Render();
private:
	void GoalCheck();		//プレイヤーの状態を見てゴールできるか判断
public: //-----セッター-----//
	void SetPos( Vector3 pos );
	void SetPlayerPointer( shared_ptr<PlayerCharacter> player ) { m_player = player; }
public: //-----ゲッター-----//
	Sphere GetSphere() { return m_sphere; }
};

#endif 
