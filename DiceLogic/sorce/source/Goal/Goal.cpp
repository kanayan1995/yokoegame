#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"

#include "../GameMode/GameMode.h"
#include "../Sound/Sound.h"

#include "Goal.h"

//*****************************************************************************
//	初期化
//*****************************************************************************
void Goal::Initialize()
{
	char* fileName = "DATA/Gimmick/Goal/Goal.IMO";
	m_obj = make_unique<iexMesh>( fileName );
	m_scale = Vector3One * 2.5f;
	m_obj->SetAngle( m_angle );
	m_obj->SetScale( m_scale );

	//あたり判定セット
	m_sphere.SetRadius( 10 );
}

//*****************************************************************************
//	更新
//*****************************************************************************

//-----------------------------------------------------
//	とりあえず消してるけど
//	演出つけるならまた使うからまだおいとく
//-----------------------------------------------------
void Goal::GoalCheck()
{
	//if ( m_player.lock()->GetPeoples() != PEOPLES::TWO ) return;

	//if ( Collision::CheackSphereToSphere( m_player.lock()->GetSphere() , m_sphere  , m_sphere.GetRadius() ) == true )
	//{
	//	Sound::StopBGM( Sound::BGM::MAIN );
	//	Sound::PlayBGM( Sound::BGM::RESULT );
	//	GameMode::SetGameMode( GAMEMODE::CLEAR );
	//}
}

//-----------------------------------------------------
//	更新
//-----------------------------------------------------
void Goal::Update()
{
	GoalCheck();
	m_obj->Update();
}

//*****************************************************************************
//	描画
//*****************************************************************************
void Goal::Render()
{
	m_obj->Render( shader , "makeDepth" );
	//m_sphere.TestRender();
}

//*****************************************************************************
//	特殊なセットするもの
//*****************************************************************************
void Goal::SetPos( Vector3 pos )
{
	m_pos = pos;
	m_pos += Vector3( 0 , 1 , 0 );
	m_obj->SetPos( m_pos );
	m_sphere.SetPos( m_pos );
}