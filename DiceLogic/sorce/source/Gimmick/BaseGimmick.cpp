#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../Utilty/Utility.h"

#include "BaseGimmick.h"
#include "Dice\BaseDice.h"
#include "../Gimmick/GimmickManager.h"

//*****************************************************************************
//	初期化
//*****************************************************************************

void BaseGimmick::Initialize()
{

}

//*****************************************************************************
//	更新
//*****************************************************************************

//-----------------------------------------------------
//	更新(angle)
//-----------------------------------------------------
void BaseGimmick::Update()
{
	m_obj->SetPos( m_pos );
	m_obj->SetAngle( m_angle );
	m_obj->SetScale( m_scale );
	m_obj->Update();
}

//-----------------------------------------------------
//	更新(Quaternion)
//-----------------------------------------------------
void BaseGimmick::UpdateQuaternion( Quaternion rotation )
{
	m_obj->SetPos( m_pos );
	m_obj->SetRotation( rotation );
	m_obj->SetScale( m_scale );
	m_obj->UpdateQuaternion();
}

//*****************************************************************************
//	描画
//*****************************************************************************

//-----------------------------------------------------
//	描画
//-----------------------------------------------------
void BaseGimmick::Render()
{
	m_obj->Render();
	//m_obb.TestRender();
	FontRender();
}

//-----------------------------------------------------
//	影描画
//-----------------------------------------------------
void BaseGimmick::RenderShadow()
{
	m_obj->Render(myshader, "ShadowBuf");
}

void BaseGimmick::RenderDepth()
{
	m_obj->Render( myshader , "makeDepth" );
}

//-----------------------------------------------------
//	テスト用に文字出す
//-----------------------------------------------------
void BaseGimmick::FontRender()
{
}