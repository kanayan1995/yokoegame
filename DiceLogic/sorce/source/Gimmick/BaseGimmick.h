//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __BASEGIMMICK_H__
#define __BASEGIMMICK_H__

//別クラス作るたびにinclude書くの忘れそうややから大事なものだけ書く
#include "../Utilty/Utility.h"
#include "../Character/Character.h"
#include "../Collision/OBB.h"
#include "../Collision/AABB.h"
#include "../Graphics/Graphics.h"
#include "../Stage/Stage.h"

//****************************************************
//	列挙体
//****************************************************

//-----------------------------------------------------
//	サイコロで使う
//-----------------------------------------------------
#define FACE_MAX 6		//面の数　どっかで使うかもしれんからdefine宣言
enum class DICEWARPTYPE	//面の種類
{
	ONE = 0 ,			//1入り口
	SIX ,				//6出口
	TWO ,				//2入り口
	FIVE ,				//5出口
	THREE ,				//3入り口
	FOWR ,				//4出口
	NONE ,				//何もないとき
	MAX ,
};

//どの面につけているかを判断する 
//ちなみに場所はmaterial番号に合わしている
enum DICEFACE : int
{
	DICE_UP = 0 ,	
	DICE_DOWN ,
	DICE_FORWARD ,
	DICE_BACK ,
	DICE_RIGHT ,
	DICE_LEFT ,
};

//掴んでから転がすまで
enum class HOLDSTATE
{
	NONE ,				//何もしていない状態
	SETROLL ,			//転がるための情報を入れる
	ROLLCHICE ,			//どっちの向きに転がるか選ぶ
	ROLL ,				//転がる
};

//-----------------------------------------------------
//　ギミックの種類
//-----------------------------------------------------
enum class GIMMICKTYPE	//ギミックの種類　追加されるたびにここを増やしていく
{
	ONEPERSONDICE ,		//一人用サイコロ
	TWOPERSONDICE ,		//二人用サイコロ
	NONE ,				//この時は何も入ってない for分で回す時とかに使えばいいかも
};

//****************************************************************************
//	ギミックのベース
//	新しくギミックを追加したかったらこいつを継承して作る
//	そのたびにGIMMICKTYPEの中身も追加していく
//	設置はGimmickManagerからSetGimmickの中を増やしていく
//****************************************************************************
class BaseGimmick
{
protected:
	//****************************************************
	//	メンバ変数
	//****************************************************
	shared_ptr<iexMesh>	m_obj;				//メッシュ(描画用)  
	shared_ptr<iexMesh> m_collisionMesh;	//メッシュ(あたり判定用) //CollisionCheackでは使わない　壁ずりで使っている
	Vector3		m_pos;						//位置
	Vector3		m_scale;					//大きさ
	Vector3		m_angle;					//回転（ラジアン）

	AABB    m_aabb;							//AABB(四角形)あたり判定用
	bool	m_isCollision;					//当たっているかいないか

	GIMMICKTYPE m_gimmickType;				//自分が何のギミックかを持っておく
	StageObjType m_objType;					//自分が何のオブジェクトか判断
public:
	//****************************************************
	//	メンバ関数
	//****************************************************
	BaseGimmick()
	{
		m_obj = nullptr;
		m_pos = Vector3Zero;
		m_scale = Vector3Zero;
		m_angle = Vector3Zero;
		m_isCollision = false;
		m_gimmickType = GIMMICKTYPE::NONE;
		m_objType = StageObjType::NONE;
	}
	virtual ~BaseGimmick()
	{

	}

	virtual void Initialize();								//初期化
	virtual void Update();									//更新（angleでの場合）
	virtual void Render();									//描画
	virtual void RenderShadow();							//影の描画
	virtual void RenderDepth();								//深度描画
	virtual void FontRender();								//テストのため描画
	virtual void Smashed() = 0;

private:
	virtual void UpdateQuaternion( Quaternion rotation );	//更新（Quaternionでの場合）

public:	//-----セッター-----//
	void SetIsCollision( bool isCollision ) { m_isCollision = isCollision; }		//当たっているかいないか
	void SetGimmickType( GIMMICKTYPE gimmickType ) { m_gimmickType = gimmickType; }	//ギミックの種類を持たせる
	virtual void SetPos( Vector3 pos ) { this->m_pos = pos; }						//位置
	virtual void SetPlayerParameters(  weak_ptr<PlayerCharacter> player ) = 0;		//プレイヤーの情報まとめて更新		
	virtual void SetFaceType( DICEFACE face , DICEWARPTYPE faceType ) = 0;			//面に種類を埋め込む
	virtual void SetDiceRayVec( int materialNumber ) = 0;							//サイコロがうつレイピックの向きを決める
	virtual void SetHoldPlayerPos( int materialNumber , GIMMICKTYPE type ) = 0;		//転がす時にプレイヤーの位置をSETする(CollisionCheack) 種類によって場所返る
	virtual void SetRollReset() = 0;												//転がり状態(holdState)をリセットする
	virtual void SetRollChoice( Vector3 move , Vector3 rollAxis ) = 0;				//trueで前falseで後ろ
	virtual void SetHold() = 0;														//あたっているサイコロを転がす状態に指定する
	virtual void SetRotation( Vector3 axis , float angle ) = 0;
public:	//-----ゲッター-----//
	bool GetIsCollsion()					{ return m_isCollision; }
	shared_ptr<iexMesh> GetObj()			{ return m_obj; }
	shared_ptr<iexMesh> GetCollisionMesh()	{ return m_collisionMesh; }	
	GIMMICKTYPE GetGimmickType()			{ return m_gimmickType; }
	StageObjType GetObjType()				{ return m_objType; }
	AABB GetAABB()		{ return m_aabb;}
	Vector3 GetPos()	{ return m_pos; }
	//サイコロの面の取るために使う
	virtual Vector3 GetFacePos( int faceNum ) = 0;				//面の場所
	virtual Vector3 GetHoldPlayerPos( ) = 0;					//転がるときにPlayerPosを入れる
	virtual Vector3 GetFaceForward( int faceNum ) = 0;			//面の向き
	virtual DICEWARPTYPE GetFaceWarpType( int faceNum ) = 0;	//ワープのパネルの種類
	virtual DICEWARPTYPE GetFaceType( int materialNumber ) = 0;	//サイコロの面のタイプを取得
	virtual Quaternion GetQuaternion() = 0;						//回転情報取得
	virtual HOLDSTATE GetDiceHoldState() = 0;					//転がるときに使うStateを取得する
	virtual Vector3 GetRayVec() = 0;							//レイを撃つ向きを取得
	virtual float GetRadius() = 0;
};

#endif 
