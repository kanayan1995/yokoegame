//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __GIMMICKMANAGER_H__
#define __GIMMICKMANAGER_H__

#include "BaseGimmick.h"
#include "Dice/BaseDice.h"
#include "Dice/ForOnePersonDice.h"
#include "Dice/ForTwoPersonDice.h"

#include <list>
#include <map>
using namespace std;

//--------------------------------------------------
//	BaseGimmickから作ったギミックをmultimapで生成する
//	更新の処理順番重要（一部分）
//	プレイヤー更新してからGimmickManagerを更新する
//--------------------------------------------------
class GimmickManager
{
private:
	//****************************************************
	//	メンバ変数
	//****************************************************
	multimap<GIMMICKTYPE , shared_ptr<BaseGimmick>> m_gimmickMap;	//firstにギミックの種類、secondにはギミックのポインタ

	list<Vector3> m_worpPosList;				//ワープ先の場所を格納する。複数ある場合を想定してlistで	
	shared_ptr<Vector3> m_worpPos;				//位置座標をポインタ配列で格納
	int m_worpPosNum;							//今何番目を選択しているかを選択
	int	m_worpPosNumMax;						//何個見つけたかを選択する
	bool m_isWorpFind;

	GIMMICKTYPE	 m_colGimmickType;				//当たっているサイコロのタイプを判断する
	DICEWARPTYPE m_colDiceFaceType;				//当たっているサイコロの面の種類を格納する

	DICEWARPTYPE m_diceFaceType[ FACE_MAX ];	//設置したギミックに格納する

	//(＠_＠;) : 横江 テスト用かな
	shared_ptr<iexMesh> m_arrow;
	weak_ptr<PlayerCharacter> m_player;

	//パネルの位置を取得するときに使う
	enum class WORPMODE 
	{
		DO_NOT,		//何もしない
		GET,		//触れているパネルを取得
		CHOICE,		//位置を選ぶ
		ERASE,		//初期化
	}m_worpMode;

	struct PlayerParmater
	{
		PLAYERSTATE playerState;
	} m_playerParm;

	/*小さいサイコロの個数カウント*/
	int SmallDiceCount;
	/*大きいサイコロの個数カウント*/
	int BigDiceCount;

public:
	//****************************************************
	//	メンバ関数
	//****************************************************
	GimmickManager()
	{
		m_gimmickMap.clear();
		m_worpPosList.clear();
		m_worpPos = nullptr;
		m_worpPosNum = 0;
		m_worpPosNumMax = 0;
		m_isWorpFind = true;

		m_colGimmickType = GIMMICKTYPE::TWOPERSONDICE;
		m_colDiceFaceType = DICEWARPTYPE::NONE;

		m_arrow = nullptr;
		m_worpMode = WORPMODE::DO_NOT;

		m_playerParm.playerState = PLAYERSTATE::Move;
		m_player.lock() = nullptr;

		//追加
		SmallDiceCount = 0;
		BigDiceCount = 0;

	}
	~GimmickManager()
	{

	}

	void Initialize();					//初期化
	void Update();						//更新
	void Render();						//描画
	void RenderShadow();				//影の描画
	void RenderDepth();					//深度描画

	void FacePosListClear() { m_worpPosList.clear(); }
	void CheakMeshToWorp(shared_ptr<BaseGimmick> dice , int faceNum );	//ワープ先にメッシュがおいてあるかの確認する
	void SerchDiceFace();				//当たったサイコロの面をみて他の奴も探す
	void SetFacePos();					//位置からposの位置を選択する
	void GetFacePosChoice();			//面を選択する
	void CheackFacePos();				//面の情報を格納する //(＠_＠;) : 横江 名前考える
	void ChoiceWorpPos();				//どこにワープするかを場所を選べる
	void WorpPosNumPlus();				//ワープ先をプラスして選択する
	void WorpPosNumMinus();				//ワープ先をマイナスして選択する
	void WorpReset();					//ワープ状態をリセットする
	void EraseFacePos();				//面の場所初期化

public: //---------セッター--------//
	void SetPlayerParameters( weak_ptr<PlayerCharacter> player );
	void SetPlayerPointer( shared_ptr<PlayerCharacter> player ) { m_player = player; }
	void SetGimmick( Vector3 pos ,Vector3 axis,float angle, GIMMICKTYPE type );	//位置とギミックの種類決定
	void SetGimmickType( GIMMICKTYPE type ) { m_colGimmickType = type; }		//プレイヤーと中っているギミックを保持する
	void SetWarpFaceType( DICEWARPTYPE type ) { m_colDiceFaceType = type; }		//プレイヤーと中っているワープに使う面を保持
	void SetDiceFaceType( DICEFACE face , DICEWARPTYPE worpType );
	void SetDiceFace( GIMMICKTYPE gimmickType , int gimmickNum );				//設置したサイコロに対して面の種類を合わせる

	/*小さいサイコロのダイス面決定する関数*/
	void SetSmallDiceFaceType(int DiceTopNum)
	{
		switch (DiceTopNum)
		{
		case 0:		//1が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::SIX );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::TWO );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::FIVE);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::THREE);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::FOWR);
			break;
		case 1:		//2が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::TWO );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::FIVE );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::SIX);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::FOWR);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::THREE);
			break;
		case 2:		//3が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::THREE );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::FOWR );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::SIX);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::TWO);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::FIVE);
			break;
		case 3:		//4が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::FOWR );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::THREE );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::SIX);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::TWO);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::FIVE);
			break;
		case 4:		//5が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::FIVE);
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::TWO );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::SIX);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::FOWR);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::THREE);
			break;
		case 5:		//6が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::SIX );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::TWO );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::FIVE);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::FOWR);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::THREE);
			break;

		default:
			break;
		}
		/*ダイスにテクスチャセット*/
		SetDiceFace( GIMMICKTYPE::ONEPERSONDICE , SmallDiceCount );
		/*次のダイスへ*/
		SmallDiceCount++;
	}

	/*大きいサイコロのダイス面決定する関数*/
	void SetBigDiceFaceType(int DiceTopNum)
	{
		switch (DiceTopNum)
		{
		case 0:		//1が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::SIX );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::TWO );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::FIVE);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::FOWR);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::THREE);
			break;
		case 1:		//2が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::TWO );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::FIVE );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::SIX);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::FOWR);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::THREE);
			break;
		case 2:		//3が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::THREE );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::FOWR );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::SIX);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::TWO);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::FIVE);
			break;
		case 3:		//4が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::FOWR );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::THREE );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::SIX);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::TWO);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::FIVE);
			break;
		case 4:		//5が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::FIVE);
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::TWO );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::SIX);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::FOWR);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::THREE);
			break;
		case 5:		//6が一番上の時
		SetDiceFaceType( DICE_UP	, DICEWARPTYPE::SIX );
		SetDiceFaceType( DICE_DOWN	, DICEWARPTYPE::ONE );
		SetDiceFaceType( DICE_FORWARD , DICEWARPTYPE::TWO );
		SetDiceFaceType( DICE_BACK	, DICEWARPTYPE::FIVE);
		SetDiceFaceType( DICE_RIGHT	, DICEWARPTYPE::FOWR);
		SetDiceFaceType( DICE_LEFT	, DICEWARPTYPE::THREE);
			break;
		default:
			break;
		}
		/*ダイスにテクスチャセット*/
		SetDiceFace( GIMMICKTYPE::TWOPERSONDICE , BigDiceCount );
		/*次のダイスへ*/
		BigDiceCount++;
	}


public: //---------ゲッター--------//
	multimap<GIMMICKTYPE , shared_ptr<BaseGimmick>> GetGimmickMap() { return m_gimmickMap; }
	GIMMICKTYPE		GetGimmickType()	{ return m_colGimmickType; }
	DICEWARPTYPE	GetFaceType()		{ return m_colDiceFaceType; }
	Vector3 GetWorpPos( int num )	{ return m_worpPos.get()[ num ]; }
	Vector3 GetWorpPos() { return m_worpPos.get()[ m_worpPosNum ]; }
	bool	GetIsWorpFind()  { return m_isWorpFind; }
	int		GetGimmickCount(){ return m_gimmickMap.size(); }
	Vector3 GetGimmckPos( int num );

};

#endif 

