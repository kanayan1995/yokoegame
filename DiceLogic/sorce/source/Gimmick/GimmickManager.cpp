#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"

#include "GimmickManager.h"

//**************************************************************************************************
//	シングルトン 準備と解放
//**************************************************************************************************
////shared_ptr<GimmickManager> GimmickManager::instance;
//GimmickManager* GimmickManager::instance;
////-----------------------------------------------------
////	インスタンス取得（シングルトン）
////-----------------------------------------------------
//GimmickManager* GimmickManager::GetInstance()
//{
//	if ( !instance )
//	{
//		instance = new GimmickManager();
//	}
//	//static GimmickManager instance;
//	return instance;
//}
//
////-----------------------------------------------------
////	自動的にスマポで消すと
////	順番的にうまくdelete出来ないのでこいつを呼ぶ
////-----------------------------------------------------
//void GimmickManager::Release()
//{
//	//delete instance;
//	//instance.reset();
//}
//
////-----------------------------------------------------
////	シーンの終わりで呼ぶ
////	(＠_＠;) : 横江　もしかしたら事故るかもしんないメモリーリークで
////-----------------------------------------------------
//void GimmickManager::Clear()
//{
//	for ( auto it : m_gimmickMap )
//	{
//		(&it)->second.reset();
//	}
//	m_worpPos.reset();
//}

//**************************************************************************************************
//	初期化
//**************************************************************************************************

//-----------------------------------------------------
//	初期化
//	シーンの使う前とかによんで
//-----------------------------------------------------
void GimmickManager::Initialize()
{
	m_gimmickMap.clear();
	m_worpPosList.clear();
	m_arrow.reset( new iexMesh( "DATA/Gimmick/Arrow/arrow.IMO" ) );
}

//**************************************************************************************************
//	更新
//**************************************************************************************************
void GimmickManager::Update()
{
	GetFacePosChoice();
	for ( auto it = begin( m_gimmickMap ); it != end( m_gimmickMap ); it++ )
	{
		it->second->Update();

		//大きいのが小さいのを潰す
		if ( it->second->GetGimmickType() != GIMMICKTYPE::TWOPERSONDICE ) continue;
		for ( auto itGimmckType = begin( m_gimmickMap ); itGimmckType != end( m_gimmickMap ); itGimmckType++ )
		{
			if ( Collision::CheackAABBToAABB( it->second->GetAABB() , itGimmckType->second->GetAABB() , -1.0f ) == true )
			{
				itGimmckType->second->Smashed();
			}
		}
	}
}

//**************************************************************************************************
//	描画
//**************************************************************************************************
void GimmickManager::Render()
{
	for ( auto it = begin( m_gimmickMap ) ; it != end( m_gimmickMap ); it++ )
	{
		it->second->Render();
	}

	if ( m_worpMode == WORPMODE::CHOICE )
	{
		int i = 0;
		float worpChoice;
		for ( auto it = m_worpPosList.begin(); it != m_worpPosList.end(); it++ )
		{
			if ( i == m_worpPosNum ) worpChoice = 2;
			else worpChoice = 1;
			m_arrow->SetPos( m_worpPos.get()[ i ] + Vector3( 0 , 18 , 0 ));
			m_arrow->SetAngle( Vector3( PI , 0 , 0 ) );
			m_arrow->SetScale( Vector3One * worpChoice );
			m_arrow->Update();
			m_arrow->Render();
			i++;
		}
	}
}

void GimmickManager::RenderShadow()
{
	for ( auto it = begin( m_gimmickMap ) ; it != end( m_gimmickMap ); it++ )
	{
		it->second->RenderShadow();
	}
}

void GimmickManager::RenderDepth()
{
	for ( auto it = begin( m_gimmickMap ) ; it != end( m_gimmickMap ); it++ )
	{
		it->second->RenderDepth();
	}

	if ( m_worpMode == WORPMODE::CHOICE )
	{
		int i = 0;
		float worpChoice;
		for ( auto it = m_worpPosList.begin(); it != m_worpPosList.end(); it++ )
		{
			if ( i == m_worpPosNum ) worpChoice = 2;
			else worpChoice = 1;
			m_arrow->SetPos( m_worpPos.get()[ i ] + Vector3( 0 , 18 , 0 ) );
			m_arrow->SetAngle( Vector3( PI , 0 , 0 ) );
			m_arrow->SetScale( Vector3One * worpChoice );
			m_arrow->Update();
			m_arrow->Render();
			i++;
		}
	}
}

//**************************************************************************************************
//	サイコロ関連に使う
//**************************************************************************************************
//**********************************************************************************
//	ワープでのState管理
//**********************************************************************************

//******************************************************************
//	GET状態
//******************************************************************

//-----------------------------------------------------
//	ワープ先の面の向きからレイピックを撃って
//	メッシュがあったらそこにはワープしてはいけない
//	確認してメッシュがなければ位置を追加する
//-----------------------------------------------------
void GimmickManager::CheakMeshToWorp( shared_ptr<BaseGimmick> dice , int faceNum )
{
	Vector3 out;
	Vector3 dicePos = dice->GetPos();
	dicePos -= Vector3( 0 , dice->GetRadius() , 0 );
	Vector3 ficeVec = dice->GetFaceForward( faceNum );
	float	dist	= Vector3Distance( dicePos , dice->GetFacePos( faceNum ) );	//サイコロの位置からワープ先の位置までの距離

	if ( Collision::RayPickLocal( &out , dicePos , ficeVec , dist ) == true )
	{
		return;
	}
	//存在しているメッシュにあたらなかったらワープする位置を追加する
	m_worpPosList.push_back( dice->GetFacePos(faceNum) );
}

//-----------------------------------------------------
//	保持したギミックの種類と面のタイプから
//	面の位置を保存する
//	(＠_＠;) : 横江　ifとforでひどいことになっている
//-----------------------------------------------------
void GimmickManager::SerchDiceFace()
{
	//入り口を見て出口を探して位置を記録する　ラムダ式関数
	auto CheakDiceFace = [this]( shared_ptr<BaseGimmick> dice , int faceNum , DICEWARPTYPE in , DICEWARPTYPE out )
	{
		if ( m_colDiceFaceType == in )
		{
			if ( dice->GetFaceWarpType( faceNum ) == out )
			{
				CheakMeshToWorp( dice , faceNum );
				return true;
			}
		}
		return false;
	};

	//出てるギミック分だけfor分回す
	for ( auto it = begin( m_gimmickMap ); it != end( m_gimmickMap ); it++ )
	{
		//当たっているギミックと同じタイプのギミックか判断する
		if ( m_colGimmickType != it->first ) continue;

		//サイコロの面の情報が欲しいので面の数だけfor分を回す
		for ( int faceNum = 0; faceNum < FACE_MAX; faceNum++ )
		{
			if		( CheakDiceFace( it->second , faceNum , DICEWARPTYPE::ONE , DICEWARPTYPE::SIX ) == true ) {}
			else if ( CheakDiceFace( it->second , faceNum , DICEWARPTYPE::TWO , DICEWARPTYPE::FIVE ) == true ){}
			else if ( CheakDiceFace( it->second , faceNum , DICEWARPTYPE::THREE , DICEWARPTYPE::FOWR ) == true ){}
		}
	}
}

//-----------------------------------------------------
//	面の位置をポインタ配列で取得して
//	位置を番号指定で探せるようにする
//	shared_ptrをresetにしているのはワープするたびに作り直すから
//-----------------------------------------------------
void GimmickManager::SetFacePos()
{
	m_worpPosNum = 0;
	m_worpPosNumMax = m_worpPosList.size() - 1;	//配列に合わしたいから-1しないとひとつ多くなる
	//m_worpPos = make_shared<Vector3>( m_worpPosList.size() );
	m_worpPos.reset( new Vector3[ m_worpPosList.size() ] );
	int i = 0;
	for ( auto it : m_worpPosList )
	{
		m_worpPos.get()[ i ] = it;
		i++;
	}
}

//-----------------------------------------------------
//	SerchDiceFaceを使った後sizeの確認をして
//	0やったらCHOICEモードにいってはいけない
//	サイズが1以上なら位置をm_worpPos格納して
//	CHOICEモードにいっていい
//-----------------------------------------------------
void GimmickManager::CheackFacePos()
{
	SerchDiceFace();
	if ( m_worpPosList.size() <= 0 )
	{
		m_player.lock()->SetState( PLAYERSTATE::Move );
		m_worpMode = WORPMODE::ERASE;
		m_isWorpFind = false;
		return;
	}
	//ワープ先を見つけたあとの処理
	m_player.lock()->SetWarpState( WARPSTATE::WARPCHOICE );
	m_isWorpFind = true;
 	SetFacePos();
	m_worpMode = WORPMODE::CHOICE;
}

//******************************************************************
//	CHOICE状態
//	その状態の時に使われてほしい関数
//******************************************************************

//-----------------------------------------------------
//	どこにワープするかを選ぶことができる
//	m_worpPosNumを++したり--するのはPlayerの中がいい
//-----------------------------------------------------
void GimmickManager::ChoiceWorpPos()
{
	if ( m_playerParm.playerState != PLAYERSTATE::Worp ) m_worpMode = WORPMODE::ERASE;
}

//-----------------------------------------------------
//	選択をする時に最大超えたら最初に戻って
//	イライラをなくす
//-----------------------------------------------------
void GimmickManager::WorpPosNumPlus()
{
	m_worpPosNum++;
	if ( m_worpPosNum > m_worpPosNumMax ) m_worpPosNum = 0;
}

//-----------------------------------------------------
//	選択をする時に最少をこえたら最大に進んで
//	イライラをなくす
//-----------------------------------------------------
void GimmickManager::WorpPosNumMinus()
{
	m_worpPosNum--;
	if ( m_worpPosNum < 0 ) m_worpPosNum = m_worpPosNumMax;
}

//-----------------------------------------------------
//	選択状態を消す
//-----------------------------------------------------
void GimmickManager::WorpReset()
{

}

//******************************************************************
//	ERASEの時に使われてほしい
//******************************************************************

//-----------------------------------------------------
//	選んだ後に消す
//-----------------------------------------------------
void GimmickManager::EraseFacePos()
{
	m_worpPosList.clear();
	m_worpPosNum = 0;
	m_worpPosNumMax = 0;
	m_worpMode = WORPMODE::DO_NOT;
	m_isWorpFind = true;
	m_worpPos.reset();
}

//-----------------------------------------------------
//	面の位置を選択する
//	Stateで管理する
//-----------------------------------------------------
void GimmickManager::GetFacePosChoice()
{
	switch ( m_worpMode )
	{
		case WORPMODE::DO_NOT:
			if ( m_playerParm.playerState == PLAYERSTATE::Worp )m_worpMode = WORPMODE::GET;
			break;
		case WORPMODE::GET:
			CheackFacePos();
			break;
		case WORPMODE::CHOICE:
			ChoiceWorpPos();
			break;
		case WORPMODE::ERASE:
			EraseFacePos();
			break;
		default:
			break;
	}
}

//**************************************************************************************************
//	配置関連
//**************************************************************************************************

//-----------------------------------------------------
//	ギミックの設置
//	設置と同時に種類も入れる
//	ギミックの生成と一緒にタイプとかも一緒に入れる
//-----------------------------------------------------
void GimmickManager::SetGimmick( Vector3 pos ,Vector3 axis,float angle, GIMMICKTYPE type )
{
	shared_ptr<BaseGimmick> gimmick;
	switch ( type )
	{
		case GIMMICKTYPE::ONEPERSONDICE:
			gimmick = make_shared<ForOnePersonDice>();
			break;
		case GIMMICKTYPE::TWOPERSONDICE:
			gimmick = make_shared<ForTwoPersonDice>();
			break;
		default:
			assert( !"ギミックの種類間違えてる\n ゲームなりたたん\n　" );
			break;
	}
	gimmick->Initialize();
	gimmick->SetGimmickType( type );
	gimmick->SetPos( pos );
	gimmick->SetRotation(axis, angle);
	gimmick->Update();
	m_gimmickMap.emplace( type , gimmick );
	//m_gimmickMap.insert( make_pair( type , move(gimmick) ) );
}

void GimmickManager::SetDiceFaceType( DICEFACE face , DICEWARPTYPE worpType )
{
	m_diceFaceType[ face ] = worpType;
}

//-----------------------------------------------------
//	サイコロの面に対して
//	面のテクスチャとワープする種類を張る
//-----------------------------------------------------
void GimmickManager::SetDiceFace( GIMMICKTYPE gimmickType , int gimmickNum )
{
	int i = 0;
	for ( auto it = begin( m_gimmickMap ) ; it != end( m_gimmickMap ); it++ )
	{
		if ( it->first != gimmickType ) continue;
		if ( i == gimmickNum )
		{
			it->second->SetFaceType( DICE_UP	, m_diceFaceType[ DICE_UP ]		);
			it->second->SetFaceType( DICE_DOWN	, m_diceFaceType[ DICE_DOWN ]	);
			it->second->SetFaceType( DICE_FORWARD, m_diceFaceType[ DICE_FORWARD ]);
			it->second->SetFaceType( DICE_BACK	, m_diceFaceType[ DICE_BACK ]	);
			it->second->SetFaceType( DICE_RIGHT	, m_diceFaceType[ DICE_RIGHT ]	);
			it->second->SetFaceType( DICE_LEFT	, m_diceFaceType[ DICE_LEFT ]	);
			return;
		}
		else i++;
	}
}


//**************************************************************************************************
//	プレイヤーの情報を取得
//**************************************************************************************************

//-----------------------------------------------------
//	ギミックの中にプレイヤーの情報が欲しいときに使う
//	SetPlayerParametersを純粋仮想関数にしているので
//	継承先で欲しい情報だけをGetする
//-----------------------------------------------------
void GimmickManager::SetPlayerParameters( weak_ptr<PlayerCharacter> player )
{
	for ( auto it = begin( m_gimmickMap ) ; it != end( m_gimmickMap ); it++ )
	{
		it->second->SetPlayerParameters( player );
		m_playerParm.playerState = player.lock()->GetPlayerState();
	}
}

//**************************************************************************************************
//	ゲッター関連
//**************************************************************************************************
Vector3 GimmickManager::GetGimmckPos( int num )
{
	int cheackCount = 0;	//指定した番号の
	for ( auto it = begin( m_gimmickMap ) ; it != end( m_gimmickMap ); it++ )
	{
		if ( cheackCount == num )
		{
			return it->second->GetPos();
		}
		cheackCount++;
	}
	return Vector3( 0 , 0 , 0);
}