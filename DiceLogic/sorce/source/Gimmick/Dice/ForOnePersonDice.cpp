#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../../Utilty/Utility.h"
#include "../../ObjStorage/ObjStorage.h"

#include "ForOnePersonDice.h"

//-----------------------------------------------------
//		������
//-----------------------------------------------------
void ForOnePersonDice::Initialize()
{
	char* fileName = "DATA/Dice/dice.IMO" ;
	m_obj = make_shared<iexMesh>( fileName );
	fileName = "DATA/Dice/diceCollision.IMO" ;
	m_collisionMesh = OBJSTORAGE->GetGimmickMesh( GIMMICKTYPE::ONEPERSONDICE );
	m_scale = Vector3One;
	m_radius = 4;	
	m_objType = StageObjType::SMALL_DICE;

	m_smashedTime	= FPS( 0.2f );
	m_smashedScale	= m_scale / m_smashedTime;
	m_smashedPos	= m_radius / m_smashedTime;

	BaseDice::Initialize();
}

void ForOnePersonDice::Smashed()
{
	switch ( m_smasedState )
	{
		case SMASEDSTATE::SET:
			SetSmashed();
			break;
		case SMASEDSTATE::SMASHING:
			Smashing();
			break;
		case SMASEDSTATE::END:
			SmashEnd();
			break;
		default:
			break;
	}
}

void ForOnePersonDice::SetSmashed()
{
	m_smashedKeepPos = m_pos;
	m_smashedScale = m_scale / m_smashedTime;
	m_smashedPos = m_radius / m_smashedTime;
	if ( fabsf( m_obj->GetForwardVec().y - Vector3Up.y ) < FLT_EPSILON )
	{
		m_smashedScale.x = 0;
		m_smashedScale.y = 0;
	}
	else if ( fabsf( m_obj->GetUpVec().y - Vector3Up.y ) < FLT_EPSILON )
	{
		m_smashedScale.x = 0;
		m_smashedScale.z = 0;
	}
	else if ( fabsf( m_obj->GetRightVec().y - Vector3Up.y ) < FLT_EPSILON )
	{
		m_smashedScale.y = 0;
		m_smashedScale.z = 0;
	}
	m_smasedState = SMASEDSTATE::SMASHING;
}

void ForOnePersonDice::Smashing()
{
	auto SizeCheack = [& , this]( float &cheackScale , float setSize )
	{
		if ( cheackScale < 0 )
		{
			cheackScale = setSize;
			m_smasedState = SMASEDSTATE::END;
			m_pos.y = m_smashedKeepPos.y - m_radius;
			m_collisionMesh->SetScale( 0.0f );
			m_collisionMesh->Update();
		}
	};

	m_scale -= m_smashedScale;
	m_pos.y -= m_smashedPos;
	SizeCheack( m_scale.x , m_smashedScale.x );
	SizeCheack( m_scale.y , m_smashedScale.y );
	SizeCheack( m_scale.z , m_smashedScale.z );
	if ( m_pos.y < m_smashedKeepPos.y - m_radius ) m_pos.y = m_smashedKeepPos.y - m_radius;
}

void ForOnePersonDice::SmashEnd()
{
	m_aabb.SetMinAndMax( Vector3Zero );
	m_aabb.UpdatePos( Vector3Zero );
}

