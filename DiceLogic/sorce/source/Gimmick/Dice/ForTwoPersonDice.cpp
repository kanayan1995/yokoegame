#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../../Utilty/Utility.h"
#include "../../ObjStorage/ObjStorage.h"

#include "ForTwoPersonDice.h"

void ForTwoPersonDice::Initialize()
{
	//m_obj = new iexMesh( "DATA/Gimmick/diceOrigin.IMO" );
	//char* fileName = "DATA/Dice/diceBig.IMO" ;			//materialミスったから描画が事故る
	char* fileName = "DATA/Dice/dice.IMO" ;
	m_obj = make_shared<iexMesh>( fileName );
	m_collisionMesh = OBJSTORAGE->GetGimmickMesh( GIMMICKTYPE::TWOPERSONDICE );
	m_collisionMesh->SetScale( Vector3One );
	m_scale = Vector3One*2;
	m_radius = 8;
	m_objType = StageObjType::BIG_DICE;

	BaseDice::Initialize();
}

void ForTwoPersonDice::Smashed()
{
}

