//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __FORONEPERSONDICE_H__
#define __FORONEPERSONDICE_H__

#include "BaseDice.h"

class ForOnePersonDice : public BaseDice
{
private:
	float	m_smashedTime;		//潰れる時間(フレーム)
	float	m_smashedPos;		//潰れながら位置調整
	Vector3	m_smashedScale;		//潰れる値
	Vector3 m_smashedKeepPos;	//潰れる前に場所を保持
private:
	enum class SMASEDSTATE //潰れる時に動き
	{
		SET,		//潰れる前に状態を保存	
		SMASHING,	//潰れている時
		END,		//何もしない
	} m_smasedState;
public:
	ForOnePersonDice()
	{
		m_smasedState = SMASEDSTATE::SET;
	}
	~ForOnePersonDice()
	{

	}

	void Initialize()override;
private:
	void Smashed()override;

	void SetSmashed();
	void Smashing();
	void SmashEnd();
};



#endif