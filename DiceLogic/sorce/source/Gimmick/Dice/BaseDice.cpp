#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../../Utilty/Utility.h"

#include "../../Stage/Stage.h"
#include "../../ObjStorage/ObjStorage.h"
#include "../../Character/Character.h"
#include "../../Collision/Collision.h"
#include "../BaseGimmick.h"
#include "../GimmickManager.h"
#include "BaseDice.h"
#include "../../Input/Input.h"
#include "../../Sound/Sound.h"

//*****************************************************************************
//	定数
//*****************************************************************************
const float BaseDice::ROLL_CHEACK_ANGLE = 0.8f;

//*****************************************************************************
//	初期化
//*****************************************************************************
void BaseDice::Initialize()
{
	m_moveTimeMax = FPS( 0.5f );									//何秒で動くかの設定
	m_rollSpeed   = TO_RADIAN(90) / m_moveTimeMax;					//90度単位で回るから90を最大時間で割る
	m_moveDist    = m_radius * 2 ;									//サイコロやし半径の2倍の値を入れる		
	m_fallDownCheack = m_radius + m_radius*0.65f;					//半径より少し大きくしておく
	m_aabb.SetMinAndMax( Vector3One * m_radius );					//AABBセット

	//転がす時に使うやつ初期化
	char* fileName;
	fileName	= "DATA/Gimmick/Arrow/arrowRoll.IMO" ;
	m_arrow = make_unique<iexMesh>( fileName );
	m_arrow->SetScale( Vector3( 0.6f , 0.3f , 0.4f ) );

}

//*****************************************************************************
//	更新
//*****************************************************************************

//-----------------------------------------------------
//	状態の更新
//	Stateパターンのほうが良いのかな？
//-----------------------------------------------------
void BaseDice::StateUpdate()
{
	switch ( m_state )
	{
		case DICESTATE::IDEL:
			Idel();
			break;
		case DICESTATE::HOLD:
			Hold();
			break;
		case DICESTATE::DROPDOWN:
			DropDown();
		default:
			break;
	}
}

//-----------------------------------------------------
//	obj関連の更新
//-----------------------------------------------------
void BaseDice::ObjUpdate()
{	

	//自分の座標更新
	m_obj->SetPos( m_pos );
	m_obj->SetRotation( m_rotation );
	m_obj->SetScale( m_scale );
	m_obj->UpdateQuaternion();	
	//あたり判定用メッシュ更新
	m_collisionMesh->SetPos( m_pos );
	m_collisionMesh->UpdateQuaternion();
	//AABB更新
	m_aabb.UpdatePos( m_pos );
}

//-----------------------------------------------------
//	まとめて更新
//-----------------------------------------------------
void BaseDice::Update()
{
	StateUpdate();
	ObjUpdate();

	WorpFaceSet(); //(＠_＠;) : 横江 とりあえずここに書く
}

//*****************************************************************************
//	描画
//*****************************************************************************
void BaseDice::Render()
{
	myshader->SetValue( "OutlineSize" , 0.125f );
	m_obj->Render( myshader , "toon" );

	//(＠_＠;) : 横江 テスト描画用
	//m_aabb.TestRender( m_pos );
	//m_collisionMesh->Render();
}

//-----------------------------------------------------
//	影作る奴
//-----------------------------------------------------
void BaseDice::RenderShadow()
{
	m_obj->Render( myshader , "ShadowBuf" );
}

//-----------------------------------------------------
//	どっちに転がりかの矢印を描画する
//-----------------------------------------------------
void BaseDice::ArrowRender()
{
	if ( m_holdState == HOLDSTATE::ROLLCHICE )
	{
		m_arrow->SetPos( m_pos + Vector3( 0 , m_radius + 6 , 0 ) );
		Vector3 workAngle = DirCorrection( m_rayVec );
		float setAnglePlus = workAngle.x * TO_RADIAN( 90 );
		setAnglePlus -= TO_RADIAN( workAngle.z );
		if ( m_cheakFront == true )
		{
			if ( workAngle.z < 0 ) m_arrow->SetAngle( setAnglePlus + PI );
			else m_arrow->SetAngle( setAnglePlus );
			m_arrow->Update();
			m_arrow->Render( shader , "copy_fx" );
		}
		if ( m_cheakBack == true )
		{
			if ( workAngle.z < 0 ) m_arrow->SetAngle( setAnglePlus );
			else m_arrow->SetAngle( setAnglePlus + PI );
			m_arrow->Update();
			m_arrow->Render( shader , "copy_fx" );
		}
	}
}

//-----------------------------------------------------
//	デプス作成
//-----------------------------------------------------
void BaseDice::RenderDepth()
{
	m_obj->Render( shader , "makeDepth" );
	//m_aabb.TestRender( m_pos );
	ArrowRender();
}

//-----------------------------------------------------
//	テスト用に文字出す
//-----------------------------------------------------
void BaseDice::FontRender()
{
	switch ( m_state )
	{
		case DICESTATE::IDEL:
			if ( !m_isMove )
			{
				//m_font->Render( 0,0,"Dont Push" );
			}
			break;
		default:
			break;
	}
}

//*****************************************************************************
//	状態関連
//*****************************************************************************

//-----------------------------------------------------
//	待機状態
//-----------------------------------------------------
void BaseDice::Idel()
{	
	//動いていない時 ここの条件式上手く書きたい
	if ( m_playerParameters.state == PLAYERSTATE::Push && m_isCollision == true )
	{
		//m_state = DICESTATE::HOLD;
		//m_holdState = HOLDSTATE::SETROLL;
	}
}


//-----------------------------------------------------
//	ワープ状態になったとき
//-----------------------------------------------------
void BaseDice::Warp()
{

}

//*****************************************************************************
//	Hold関連
//*****************************************************************************
void BaseDice::Hold()
{
	if ( m_playerParameters.state == PLAYERSTATE::Move )
	{
		m_state = DICESTATE::IDEL;
		m_holdState = HOLDSTATE::NONE;
	}

	switch ( m_holdState )
	{
		case HOLDSTATE::NONE:		
			//この時はなにもしない（SETROLL）に行くのはプレイヤーが指示する
			break;
		case HOLDSTATE::SETROLL:
			RollCheack();
			break;
		case HOLDSTATE::ROLLCHICE:
			//ここはプレイヤーが指示する
			break;
		case HOLDSTATE::ROLL:
			RollAndMove();
			break;
		default:
			break;
	}
}

//-----------------------------------------------------
//	転がる状態にするのを指定する
//	プレイヤーの中で指定する
//-----------------------------------------------------
void BaseDice::SetHold()
{
	m_state = DICESTATE::HOLD;
	m_holdState = HOLDSTATE::SETROLL;
}

//-----------------------------------------------------
//	転がれる状態かを判定する
//	サイコロから四方に撃って転がれる向きを出す
//	(＠_＠;)横江 : どれくらいの長さを撃つかは考えないといかん
//-----------------------------------------------------
void BaseDice::RollCheack()
{
	//レイが当たると動ける体制なるので動く時に使う情報を保存
	auto RayHitCheack = [this]( Vector3 rayVec , float dist , bool &m_checkRoll , int &count )
	{
		Vector3 out;
		Vector3 workPos = m_pos - Vector3( 0 , m_radius , 0 );
		if ( Collision::RayPickLocal( &out , workPos , rayVec , dist ) == true )
		{
			m_checkRoll = false;
			return false;
		}
		else
		{
			m_checkRoll = true;
			count++;
			return true;
		}
	};

	//内積判定入れてバグ少なく
	float workdDist;
	m_playerParameters.forward.Normalize();
	workdDist = Vector3Dot( m_playerParameters.forward , m_rayVec );

	int cheack = 0;
	bool cheackBackObj = true;
	if ( workdDist > ROLL_CHEACK_ANGLE )
	{
		Vector3 out;
		float dist = m_moveDist + m_radius / 2;							//進む分の距離と中心から撃つから判型分足す
		RayHitCheack( m_rayVec , dist , m_cheakFront , cheack );
		dist = m_moveDist + m_radius * 2;								//後ろの場合プレイヤー位置を考慮してさらに判型分撃つ	
		RayHitCheack( -m_rayVec , dist , m_cheakBack , cheack );
	}

	if ( cheack != 0 && cheackBackObj == true )
	{
		m_holdState = HOLDSTATE::ROLLCHICE;
		m_keepPos = m_pos;
		m_keepRotation = m_rotation;
	}
	else
	{
		RollChoiceReset();
	}
}

//-----------------------------------------------------
//	外部から転がる向きを指定する
//	setVecがtrueなら前に撃つ
//	setVecがfalseなら後ろに撃つ
//-----------------------------------------------------
void BaseDice::SetRollChoice( Vector3 move , Vector3 rollAxis )
{
	Vector3 out;
	float dist;			//レイを撃つ距離
	//プレイヤーから見て前に転がすか後ろに転がすかの判定
	if ( fabsf( move.x - m_rayVec.x ) < FLT_EPSILON
		&& fabsf( move.z - m_rayVec.z ) < FLT_EPSILON )
	{
		dist = m_moveDist + m_radius / 2;
	}
	else
	{
		dist = m_moveDist + m_radius * 2.5f;
	}

	//転がす方向を見て転がせるか転がせないか判定
	Vector3 workPos = m_pos - Vector3( 0 , m_radius , 0 );
	if ( Collision::RayPickLocal( &out , workPos , move , dist ) == false )
	{
		m_rollVec = rollAxis;
		m_moveSpeed = (move*m_moveDist) / m_moveTimeMax;
		m_holdState = HOLDSTATE::ROLL;
	}
}

//-----------------------------------------------------
//	転がる方向を決めるのに使った値リセット
//-----------------------------------------------------
void BaseDice::RollChoiceReset()
{
	m_state = DICESTATE::IDEL;
	m_holdState = HOLDSTATE::NONE;
}

//-----------------------------------------------------
//	回ってさらに移動する
//-----------------------------------------------------
void BaseDice::RollAndMove()
{
	if ( m_moveTimeMax > m_moveTime )
	{
		Quaternion workQ;
		QuaternionRotationAxis( workQ , m_rollVec , m_rollSpeed );
		m_rotation = workQ * m_rotation;
		m_pos += m_moveSpeed;
		m_moveTime++;
	}
	else
	{
		Sound::PlaySE( Sound::SE::DICE );
		RollReset();
	}
}

//-----------------------------------------------------
//	転がるために使った値をリセットする
//	最後に場所、回転の値が誤差が出ないようにしておく
//-----------------------------------------------------
void BaseDice::RollReset()
{
	m_holdState = HOLDSTATE::SETROLL;
	m_state = DICESTATE::DROPDOWN;
	//少しずれてたりしたらヤバいからここで回転を90度足した状態を入れるべき
	m_moveTime = 0;
	//m_moveSpeed.Normalize();
	//m_pos = m_keepPos + ( m_moveSpeed * m_moveDist );

	Quaternion workQ;
	QuaternionRotationAxis( workQ , m_rollVec , TO_RADIAN( 90 ) );
	m_rotation = workQ * m_keepRotation;

	//動いた後の値を保存
	m_keepPos = m_pos;
	m_keepRotation = m_rotation;
}

//*****************************************************************************
//	ワープ関連
//*****************************************************************************

//-----------------------------------------------------
//	ワープのパネルセット
//	パネルの位置、向きをセットする
//-----------------------------------------------------
//	(＠_＠;)種類は別のところからのほうが良い　これやと固定やから
void BaseDice::WorpFaceSet()
{
	//Vector3 downPos = -m_obj->GetUpVec() * m_radius;	//半径分位置を下げて地面と合わせないといけない
	float worpPosPlus = m_radius + (m_radius);		//半径のままだとめり込むから少し前に出す
	//前
	m_face[ DICE_FORWARD ].forward  = m_obj->GetForwardVec();
	m_face[ DICE_FORWARD ].worpPos  = m_pos + (m_obj->GetForwardVec() * worpPosPlus);
	//後ろ
	m_face[ DICE_BACK ].forward  = -m_obj->GetForwardVec();
	m_face[ DICE_BACK ].worpPos  = m_pos + (-m_obj->GetForwardVec() * worpPosPlus);
	//右
	m_face[ DICE_RIGHT ].forward  = m_obj->GetRightVec();
	m_face[ DICE_RIGHT ].worpPos  = m_pos + (m_obj->GetRightVec() * worpPosPlus);
	//左
	m_face[ DICE_LEFT ].forward  = -m_obj->GetRightVec();
	m_face[ DICE_LEFT ].worpPos  = m_pos + (-m_obj->GetRightVec() * worpPosPlus);
	//上
	m_face[ DICE_UP ].forward  = m_obj->GetUpVec();
	m_face[ DICE_UP ].worpPos  = m_pos + (m_obj->GetUpVec() * worpPosPlus);
	//下
	m_face[ DICE_DOWN ].forward  = -m_obj->GetUpVec();
	m_face[ DICE_DOWN ].worpPos  = m_pos + (-m_obj->GetUpVec() * worpPosPlus);
}

//-----------------------------------------------------
//	自分のワープするパネルを返す
//	あたり判定クラスの中で動かしてもらう
//	ついでに転がす時に使うレイを撃つ方向を決める
//-----------------------------------------------------
DICEWARPTYPE BaseDice::GetFaceType( int materialNumber )
{
	float workDot;
	workDot = Vector3Dot( -m_playerParameters.forward , m_face[ materialNumber ].forward );
	if ( workDot > ROLL_CHEACK_ANGLE )
	{
		m_rayVec = -m_face[ materialNumber ].forward;
		return m_face[ materialNumber ].warpType;
	}
	else
	{
		m_rayVec = Vector3Zero;
		return DICEWARPTYPE::NONE;
	}
}

//*****************************************************************************
//	DropDown関連
//*****************************************************************************

//-----------------------------------------------------
//	下にレイを撃って何もなければ落ちていく
//-----------------------------------------------------
void BaseDice::DownCheack()
{
	Vector3 out;
	if ( Collision::RayPickLocal( &out , m_pos , Vector3Down , m_fallDownCheack ) == false )
	{
		m_dropDownState = DROPDOWNSTATE::FALLING;
	}
	else
	{
		m_holdState = HOLDSTATE::SETROLL;
		m_state = DICESTATE::HOLD;
	}
}

//-----------------------------------------------------
//	下にレイを撃って何もなければ落ちていく
//-----------------------------------------------------
void BaseDice::Falling()
{
	Vector3 out;
	if ( Collision::RayPickLocal( &out , m_pos , Vector3Down , m_fallDownCheack ) == false )
	{
		m_pos += Vector3Down*0.4f;
	}
	else
	{
		if ( Collision::RayPickLocal( StageObjType::SMALL_DICE , m_pos , Vector3Down , m_fallDownCheack ) == true )
		{
			m_pos += Vector3Down*0.4f;
		}
		else 
		{
			m_pos = out + Vector3( 0 , m_radius , 0 );
			m_holdState = HOLDSTATE::NONE;
			m_state = DICESTATE::HOLD;
			m_dropDownState = DROPDOWNSTATE::CHEACK;
		}
	}
}

//-----------------------------------------------------
//	落ちていく処理
//-----------------------------------------------------
void BaseDice::DropDown()
{
	switch ( m_dropDownState )
	{
		case DROPDOWNSTATE::CHEACK:
			DownCheack();
			break;
		case DROPDOWNSTATE::FALLING:
			Falling();
			break;
		default:
			break;
	}
}

//*****************************************************************************
//	セット関連
//*****************************************************************************

//-----------------------------------------------------
//	プレイヤーの情報をセットする
//	今よりプレイヤーの情報を追加したかったら
//	こことメンバ変数の中いじる
//-----------------------------------------------------
void BaseDice::SetPlayerParameters( weak_ptr<PlayerCharacter> player )
{
	m_playerParameters.forward	= player.lock()->GetForwardVec();
	m_playerParameters.right	= player.lock()->GetRightVec();
	m_playerParameters.state	= player.lock()->GetPlayerState();
	m_playerParameters.pos		= player.lock()->GetPos();
	m_playerParameters.people	= player.lock()->GetPeoples();
}

//-----------------------------------------------------
//	位置情報のセット
//	サイコロのセットのなかですでに半径分足して置く（中心は原点にあるから）
//-----------------------------------------------------
void BaseDice::SetPos( Vector3 pos )
{
	this->m_pos = pos;
	m_pos = m_pos + Vector3( 0 , m_radius , 0 );
}

//-----------------------------------------------------
//	面に種類を付ける
//-----------------------------------------------------
void BaseDice::SetFaceType( DICEFACE faceNum , DICEWARPTYPE faceType )
{
	m_face[ faceNum ].warpType = faceType;
	m_obj->SetTexture( faceNum , OBJSTORAGE->GetDiceTexture( faceType ) );
}

//-----------------------------------------------------
//	転がす時にプレイヤーの位置を固定さすために使う
//-----------------------------------------------------
void BaseDice::SetHoldPlayerPos( int materialNumber , GIMMICKTYPE type )
{
	float   workLength;				//どれくらいの距離をとるか
	if ( type == GIMMICKTYPE::ONEPERSONDICE ) workLength = m_radius*2.1f;
	if ( type == GIMMICKTYPE::TWOPERSONDICE ) workLength = m_radius*2.0f;

	Vector3 workPos;
	m_playerParameters.forward = DirCorrection( m_playerParameters.forward );
	Vector3 diceForPlayer = m_playerParameters.pos - m_pos;
	diceForPlayer.y = 0;
	diceForPlayer.Normalize();
	diceForPlayer = DirCorrection( diceForPlayer );
	workPos = m_pos + (diceForPlayer*workLength);		//少し後ろに置く
	workPos -= Vector3Up*m_radius;						//真ん中に座標があるから少し下げる

	m_holdPlayerPos = workPos;
}

//-----------------------------------------------------
//	回転をさす
//	回転軸にそって回転するので
//	axisに回転軸
//	angleに回転量（度）を入れる
//-----------------------------------------------------
void BaseDice::SetRotation( Vector3 axis , float degree )
{
	//少しずれてたりしたらヤバいからここで回転を90度足した状態を入れるべき
	Quaternion workQ;
	QuaternionRotationAxis( workQ , axis ,TO_RADIAN( degree ) );
	m_rotation = workQ * m_rotation;
}
