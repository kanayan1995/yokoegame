//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __BASEDICE_H__
#define __BASEDICE_H__

#include "../BaseGimmick.h"
#include "../../Character/Character.h"

class PlayerCharacter;
//****************************************************************************
//	サイコロの基本的な動きが入っている
//	Boxの中心が原点になっているのでy座標を使っての計算は気を付けるべき
//	初めにSetする時でY座標を半径分、足しているので気を付ける
//****************************************************************************
class BaseDice : public BaseGimmick
{
public:
	//****************************************************
	//	列挙体
	//****************************************************
	//サイコロの状態
	enum class DICESTATE
	{
		IDEL ,		//待機
		WARP ,		//ワープ状態　(＠_＠;)（プレイヤーの中でもいいかも）
		HOLD ,		//プレイヤーが捕まえてどっちの転がすかを選ぶ
		DROPDOWN,	//落ちていく状態
	}m_state;

	//転がる方向
	enum ROLLVEC : int
	{
		FORWARD = 0 ,
		BACK ,
		RIGHT ,
		LEFT ,
		MAX ,
	};

	//落ちるか確認
	enum class DROPDOWNSTATE
	{
		CHEACK,		//落ちるかどうかのチェック
		FALLING,	//落ちてる最中
	} m_dropDownState;
protected:
	//****************************************************
	//	メンバ変数
	//****************************************************
	Quaternion	m_rotation;		//姿勢( 回転 )
	float		m_radius;		//半径( FBXで作ったときの直径に合わせる )
	Vector3		m_footPos;		//モデル座標が中心にあるので足元の座標に合わせる

	//動くとき使う
	float m_moveTimeMax;		//転がる最大時間
	float m_moveTime;			//転がっている時に使う時間
	//回転(Rotate)				
	float m_rollSpeed;			//転がるスピードを決める
	Vector3 m_rollVec;			//どの向きに回転するかを判断する
	Quaternion m_keepRotation;	//転がる前の値を保持する
	HOLDSTATE m_holdState;		//転がるときに使う状態
	//移動(Translate)
	float m_moveDist;			//移動する距離(サイコロの半径から作る)
	Vector3 m_moveSpeed;		//最大に合わして移動する距離
	Vector3 m_moveVec;			//転がる方向
	Vector3 m_keepPos;			//転がる前の値を保持する
	//落ちる時に使う
	float m_fallDownCheack;		//落ちる時に下に何かあるか調べる時に使う
	
	//転がす前に矢印を出してわかりやすく
	unique_ptr<iexMesh>	m_arrow;	//転がる向きを出す時に使う矢印
	Vector3 m_rayVec;				//転がす方向を出す時にどの方向にレイを撃つかを決める(CheackDiceFaceType)の中で取得(プレイヤーの向きに合わせる)
	float	m_arrowAngle;			//転がしたい方向向かすために使う
	bool	m_cheakFront;			//転がる方向の確認（前）
	bool	m_cheakBack;			//転がる方向の確認（後ろ）

	//面が持つ情報
	struct  Face
	{
		DICEWARPTYPE	warpType;	//種類　ワープするときに使う情報
		Vector3			worpPos;	//位置	面の中心に今ある		(＠_＠;)横江 : 位置は地面に置いたらいいか悩む
		Vector3			forward;	//正面	面からみての正面
	}m_face[ FACE_MAX ];
	
	//プレイヤーの情報 (＠_＠;)ここ今思うとweak_ptrもたせたらいい
	struct PlayerParameters				//サイコロで中で使うプレイヤーの情報
	{
		Vector3				forward;	//プレイヤーの正面
		Vector3				right;		//プレイヤーの右
		PLAYERSTATE			state;		//プレイヤーの状態
		Vector3				pos;		//プレイヤーの位置
		PEOPLES             people;		//プレイヤーが今何人いるか
	}m_playerParameters;
	Vector3 m_holdPlayerPos;			//転がるときにプレイヤーの位置をSETする

	weak_ptr<PlayerCharacter> m_player;
	//----------------------------------------------------
	//	定数
	//----------------------------------------------------
	static const float ROLL_CHEACK_ANGLE; //内積で転がる向きを決定するので0.0〜1.0の値を入れる

	//----------------------------------------------------
	//	定数 : (＠_＠;) : 横江 テスト用
	//----------------------------------------------------
	//unique_ptr<iexMesh>	m_arrow;		//入り口、出口を解りやすくする
	bool m_isMove;
public:
	//****************************************************
	//	メンバ関数
	//****************************************************

	BaseDice()
	{
		m_rotation.Identity();
		m_radius = 0;

		m_moveTimeMax	= 0;
		m_moveTime		= 0;
		m_rollSpeed		= 0;
		m_rollVec		= Vector3Zero;
		m_keepRotation.Identity();
		m_moveDist		= 0;
		m_moveSpeed		= Vector3Zero;
		m_moveVec		= Vector3Zero;
		m_keepPos		= Vector3Zero;

		m_arrow = nullptr;
		m_arrowAngle = 0;
		m_cheakFront = true;
		m_cheakBack  = true;


		for ( int i = 0; i < FACE_MAX; i++ )
		{
			m_face[ i ].warpType = DICEWARPTYPE::NONE;
			m_face[ i ].worpPos = Vector3Zero;
			m_face[ i ].forward = Vector3Zero;
		}
		m_playerParameters.forward = Vector3Zero;
		m_playerParameters.state = PLAYERSTATE::Idle;
		m_playerParameters.pos = Vector3Zero;
		m_state			= DICESTATE::IDEL;
		m_holdState		= HOLDSTATE::NONE;
		m_dropDownState = DROPDOWNSTATE::CHEACK;
	}
	virtual ~BaseDice()
	{
		//SafeDelete( &m_arrow );
	}

	virtual void Initialize()override;	//初期化
	virtual void Update()override;		//更新
	virtual void Render()override;		//描画
	void ArrowRender();					//矢印の描画
	virtual void RenderShadow()override;//影の描画
	virtual void RenderDepth()override;	//震度バッファ作成
	virtual void FontRender()override;

	//転がるときに使う
	void RollChoiceReset();	//選択するときに使った変数リセット

public:
	//Updateの中に書く
	void StateUpdate();					//サイコロの状態によって更新処理を変える
	void ObjUpdate();					//obj関連の更新

	//State関連			
	void Idel();			//待機状態	
	void Warp();			//プレイヤーがワープ状態になったらすること
	void Test();			//テスト用

	//捕まえて転がす		//転がるのをやめるのはプレイヤーの中で指定する
	void Hold();
	void SetHold()override;	//転がるのをプレイヤーが命令する
	void RollCheack();		//転がることができるかを判定する
	void RollAndMove();		//回って移動する
	void RollReset();		//転がるときに使った変数リセット
	void SetRollChoice( Vector3 move , Vector3 rollAxis );

	//ワープ関連
	void WorpFaceSet();		//ワープに使う設定をすう

	//転がった先で下に何もなかったら
	void DownCheack();			//転がった先で下を確認する
	virtual void Falling();		//落ちている状態
	void DropDown();			//落ちる状態

	//潰れる処理
	virtual void Smashed() = 0;

public:	//-----セッター-----//
	void SetPlayerParameters( weak_ptr<PlayerCharacter> player )override;
	void SetPos( Vector3 pos )override;
	void SetFaceType( DICEFACE faceNum , DICEWARPTYPE faceType )override;
	void SetDiceRayVec( int materialNumber ) { m_rayVec = -m_face[ materialNumber ].forward; }	//CollisionCheackの中で動かす
	void SetHoldPlayerPos( int materialNumber , GIMMICKTYPE type )override;						//CollisionCheackの中で動かす
	void SetRollReset() { RollChoiceReset(); }
	void SetRotation( Vector3 axis , float degree )override;
public:	//-----ゲッター-----//
	Vector3 GetFacePos( int faceNum )override				{ return m_face[ faceNum ].worpPos; }			//面の位置
	Vector3 GetHoldPlayerPos()override						{ return m_holdPlayerPos; }						//転がる時に使う位置
	Vector3 GetFaceForward( int faceNum )override			{ return m_face[ faceNum ].forward; }			//面の向き
	DICEWARPTYPE GetFaceWarpType( int faceNum )override		{ return m_face[ faceNum ].warpType; }			//面の種類の取得
	DICEWARPTYPE GetFaceType( int materialNumber )override;													//material番号からワープパネルを取得　CollisionCheackで動かす
	Quaternion GetQuaternion()override						{ return m_rotation; }							//Quaternion取得
	HOLDSTATE GetDiceHoldState()override					{ return m_holdState; }							//転がるときに使うStateを取得						
	Vector3 GetRayVec()override								{ return m_rayVec; }							//レイを撃つ向きを取得
	float GetRadius()override { return m_radius; }
	DICESTATE GetDiceState() { return m_state; }
	DROPDOWNSTATE GetDropDownState() { return m_dropDownState; }
};

#endif 
