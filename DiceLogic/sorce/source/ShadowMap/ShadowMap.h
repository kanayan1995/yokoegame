#pragma once

#include "../IEX/iextreme.h"

class ShadowMap
{
private:
	shared_ptr<iex2DObj> ShadowWork;
	shared_ptr<iex2DObj> ShadowTex;
	Surface* ShadowZ;
	Matrix  ShadowMat;

	Surface* orgZ;
	Surface* BackBuffer;
	int ShadowSize;
	float lightAngle;
	Vector3 target;

	ShadowMap();
public:
	ShadowMap(int ShadowSize);
	~ShadowMap();

	void RenderShadowBuff(float w, float h, float zn, float zf,Vector3 dir, shared_ptr<iex3DObj> obj);
	void RestoreShadowBuff();

	void ShadowSetting(Vector3 t, Vector3 p, Vector3 light);
	void ActivateShadow();
	void ActivateShadowL();

	void EndShadow();

	void DebugRenderShadowBuff(int x, int y, int w, int h);

	void SetLughtAngle(float radian)
	{
		lightAngle = radian;
	}
	void SetTarget(const Vector3 pos)
	{
		target = pos;
	}
	shared_ptr<iex2DObj> GetShadowTex(){ return ShadowTex; }

	void Reset()
	{
		iexSystem::Device->SetRenderTarget(0, BackBuffer );
	}
};