//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　橋口										//
//																					//
//---------------------------------------------------------------------------------	//
#include "ShadowMap.h"

#include "system\System.h"

//***********************************************
// 初期化
//***********************************************
ShadowMap::ShadowMap(int ShadowSize) :ShadowTex(nullptr), ShadowSize(ShadowSize), lightAngle(0.2f), target(0, 0, 0)
{
	ShadowWork = make_shared<iex2DObj>(512, 512, IEX2D_FLOAT2);
	ShadowTex = make_shared<iex2DObj>(ShadowSize, ShadowSize, IEX2D_RENDERTARGET);
	iexSystem::GetDevice()->CreateDepthStencilSurface(ShadowSize, ShadowSize, D3DFMT_D16, D3DMULTISAMPLE_NONE, 0, FALSE, &ShadowZ, nullptr);
	iexSystem::GetDevice()->GetRenderTarget(0, &BackBuffer);
}

ShadowMap::~ShadowMap()
{
	if (ShadowTex)
	{
		ShadowTex = nullptr;
	}

	if (ShadowZ)
	{
		ShadowZ->Release();
	}
}

//***********************************************
// シャドウバッファ
//***********************************************
//　影を付ける範囲の指定
//　w,hがobj(ここでは主人公を中心座標としている)から範囲の指定
//　解像度みたいなもの、数値が上がれば上がるほど影の描画できる範囲は広がるが影のジャギィが目立つ
//　dirはそのままライト方向、znが影の奥行の最近値、zfが最奥地
void ShadowMap::RenderShadowBuff(float w, float h, float zn, float zf, Vector3 dir, shared_ptr<iex3DObj> obj)
{
	//　レンダーターゲット設定
	ShadowTex->RenderTarget();

	//　Zバッファ設定
	iexSystem::GetDevice()->GetDepthStencilSurface(&orgZ);
	iexSystem::GetDevice()->SetDepthStencilSurface(ShadowZ);

	//　ライト方向
	//Vector3 dir(sinf(lightAngle), -2.5f, cosf(lightAngle));
	dir.Normalize();

	//　シャドウ作成
	target = obj->GetPos();
	Vector3 pos = target - dir * 150;
	Vector3 up(0, 1, 0);

	//　視点とライト位置へ
	D3DXMATRIX work;
	LookAtLH(ShadowMat, pos, target, up);
	D3DXMatrixOrthoLH(&work, w, h, zn, zf);
	ShadowMat *= work;
	myshader->SetValue("ShadowProjection", ShadowMat);
	
	//　3D ボリュームを射影するレンダリング ターゲット サーフェイスのウィンドウの大きさを定義する
	D3DVIEWPORT9 vp = { 0, 0, ShadowSize, ShadowSize, 0, 1.0f };
	iexSystem::GetDevice()->SetViewport(&vp);

	//　レンダリング
	iexSystem::GetDevice()->Clear(0, nullptr, 
		D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xFFFFFFFF, 1.0f, 0);
}

void ShadowMap::ActivateShadow()
{
	//　レンダーターゲット設定
	ShadowTex->RenderTarget();
	//　近距離シャドウ作成
	Matrix mat, work;
	D3DXMatrixOrthoLH(&work, 15, 15, 0.5f, 40.0f);
	mat = ShadowMat*work;

	shader->SetValue("ShadowProjection", &mat);
}

void ShadowMap::ActivateShadowL()
{
	//　レンダーターゲット設定
	ShadowTex->RenderTarget();
	//　近距離シャドウ作成
	Matrix mat, work;
	D3DXMatrixOrthoLH(&work, 60, 60, 2.0f, 100.0f);
	mat = ShadowMat*work;

	shader->SetValue("ShadowProjection", &mat);
}

void ShadowMap::EndShadow()
{
	//　近距離影ー後処理
	ShadowWork->RenderTarget();
	ShadowTex->Render(0, 0, 512, 512, 0, 0, ShadowSize, ShadowSize);
	ShadowTex->RenderTarget();
	ShadowWork->Render(0, 0, 512, 512, 0, 0, 512, 512);
}

void ShadowMap::ShadowSetting( Vector3 t, Vector3 p, Vector3 light )
{
	Vector3 target = t - p;
	
	float v = sqrtf( target.x*target.x + target.z*target.z );
	target.x = target.x / v * 8 + p.x;
	target.y = p.y;
	target.z = target.z / v * 8 + p.z;

	Vector3	pos = target - light*30.0f;
	Vector3	up( 0, 1, 0 );

	LookAtLH( ShadowMat, pos, target, up );
}

//***********************************************
// レンダーターゲット復元
//***********************************************
void ShadowMap::RestoreShadowBuff()
{
	//　シャドウテクスチャをシェーダにセット
	myshader->SetValue("ShadowMap", ShadowTex);

	//　レンダーターゲットの復元
	iexSystem::GetDevice()->SetRenderTarget(0, BackBuffer);
	iexSystem::GetDevice()->SetDepthStencilSurface(orgZ);
}

//***********************************************
// シャドウマップ描画
//***********************************************
void ShadowMap::DebugRenderShadowBuff( int x, int y, int w, int h )
{
	ShadowTex -> Render( x, y, w, h, 0, 0, ShadowSize, ShadowSize );
}