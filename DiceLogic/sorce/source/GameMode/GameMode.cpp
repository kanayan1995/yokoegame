#include "iextreme.h"
#include "iextremePlus.h"

#include "../Sound/Sound.h"
#include "GameMode.h"

//*****************************************************************************
//	初期化
//*****************************************************************************
void GameMode::Initialize()
{
}

//*****************************************************************************
//	GameModeの状態を管理する
//	他クラスの状態を見たり
//	自分の中で操作して変わるほうが良いかな
//*****************************************************************************

//-----------------------------------------------------
//	メニューへの変更
//-----------------------------------------------------
void GameMode::ChangeMenu()
{
	
}

//-----------------------------------------------------
//	ゴール状態への変更
//	こいつの中であたり判定処理をする
//	もしかしゴールの状態を見てに変えるかもしれん
//-----------------------------------------------------
void GameMode::ChangeClear()
{
	if ( m_gameState == GAMESTATE::CLEAR ) return;
	if ( m_player.lock()->GetPeoples() != PEOPLES::TWO ) return;

	if ( Collision::CheackSphereToSphere( m_player.lock()->GetSphere() ,
		m_goal.lock()->GetSphere() ,
		m_goal.lock()->GetSphere().GetRadius() ) == true )
	{
		Sound::StopBGM( Sound::BGM::MAIN );
		Sound::PlayBGM( Sound::BGM::RESULT );
		m_gameState = GAMESTATE::CLEAR;
	}
}

//-----------------------------------------------------
//	カメラの状態を見て変わる
//	カメラの中でモードわけしてるからそいつを見て変わるようにする
//-----------------------------------------------------
void GameMode::ChangeCamera()
{
	if ( m_gameState == GAMESTATE::CLEAR ) return;

	if ( m_camera.lock()->GetCameraMode() == Camera::CAMERAMODE::FREE ||
		 m_camera.lock()->GetCameraMode() == Camera::CAMERAMODE::GIMMICK )
	{
		m_gameState = GAMESTATE::CAMERA;
	}
	else
	{
		m_gameState = GAMESTATE::MAIN;
	}
}

//*****************************************************************************
//	更新
//*****************************************************************************
void GameMode::Update()
{
	ChangeClear();
	ChangeCamera();
}
