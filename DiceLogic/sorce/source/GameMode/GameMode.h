//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __GAMEMODE_H__
#define __GAMEMODE_H__

#include "../Goal/Goal.h"
#include "../Character/Character.h"
#include "../Camera/Camera.h"

enum class GAMESTATE
{
	MAIN,		//メイン(普通に動くとき)
	MENU,		//メニュー画面
	CAMERA,		//カメラで動かす
	CLEAR,		//ゲームクリア状態
	RESET,		//リセット
};

//*****************************************************************************
//	GameMode
//	ゲーム中に動かすとか止まるとかの状態だけを持つクラス
//	こいつの中でゲームの状態を操作する
//	他クラスの状態やらを見て自分で変わる
//*****************************************************************************
class  GameMode
{
private:
	GAMESTATE m_gameState;

	weak_ptr<Goal>				m_goal;
	weak_ptr<PlayerCharacter>	m_player;
	weak_ptr<Camera>			m_camera;
public:
	GameMode()
	{
		m_gameState = GAMESTATE::MAIN;
		m_goal.lock()	= nullptr;
		m_player.lock() = nullptr;
		m_camera.lock() = nullptr;
	}
	~GameMode()
	{

	}

	void Initialize();	//初期化
	void Update();		//更新
private:
	void ChangeMenu();
	void ChangeClear();
	void ChangeCamera();
public://-----セッター-----//
	void SetGameMode( GAMESTATE gameState )						{ m_gameState = gameState; }
	void SetPlayerPointer( shared_ptr<PlayerCharacter> player )	{ m_player = player; }
	void SetGoalPointer( shared_ptr<Goal> goal )				{ m_goal = goal; }
	void SetCameraPointer(shared_ptr<Camera> camera)			{ m_camera = camera; }
public://-----ゲッター-----//
	GAMESTATE GetGameState()		{ return m_gameState; }
};

#endif