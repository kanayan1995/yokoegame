#include "Graphics.h"
#include "Utilty/Utility.h"

void GraphicFont::Init()
{
	m_obj = nullptr;
	m_width = 0;
	m_height = 0;
	m_scale = 0;
	m_margin = 0;
}

GraphicFont::GraphicFont()
{
	Init();
}

GraphicFont::GraphicFont(char *filename)
{
	Init();
	m_obj = new iex2DObj(filename);
}

GraphicFont::~GraphicFont()
{
	SafeDelete(&m_obj);
}

void GraphicFont::setGraphic(char *filename)
{
	SafeDelete(&m_obj);
	m_obj = new iex2DObj(filename);
}

void GraphicFont::setSize(float w, float h)
{
	m_width = w;
	m_height = h;
}

void GraphicFont::setMargin(float m)
{
	m_margin = m;
}

void GraphicFont::setScale(float s)
{
	m_scale = s;
}

void GraphicFont::Render(float x, float y, char *str)
{
	char c;

	for (int i = 0;; i++) {
		c = str[i];

		if (c == '\0')
			break;

		m_obj->Render((int)(x + (i * (m_width * m_scale + m_margin))),
			(int)y,
			(int)(m_width * m_scale),
			(int)(m_height * m_scale), 
			(int)((c % 16) * m_width), 
			(int)((c / 16) * m_height),
			(int)m_width,
			(int)m_height);
	}
}
