#include "Graphics.h"
#include "Utilty/Utility.h"

extern DWORD ScreenMode;

Fader::Fader()
{
	m_frame = 0;
	m_timer = 0;
	m_type = Type::None;
	m_state = State::Pause;
}

void Fader::Start(int frame, Fader::Type type)
{
	m_type = type;
	m_frame = m_timer = frame;
	m_state = State::Play;
}

void Fader::Stop()
{
	m_state = (m_state == State::Play)? State::Pause : State::Play;
}

void Fader::Delete()
{
	m_type = Type::None;
	m_state = State::Pause;
	m_frame = 0;
	m_timer = 0;
}

void Fader::Update()
{
	if (m_state == State::Pause) {
		return;
	}

	if (m_timer <= 0) {
		m_state = State::Pause;
		return;
	}

	m_timer--;
}

void Fader::Render()
{
	float alpha;

	switch (m_type) {
	case Type::In :
		alpha = (float)(m_frame - m_timer) / (float)m_frame;
		break;

	case Type::Out :
		alpha = (float)m_timer / (float)m_frame;
		break;

	case Type::None :
		return;

	default:
		OutputDebugString("[Fader]エラー：タイプエラー");
		assert(false);
		return;
	}

	m_color.a = (unsigned char)(alpha * 0xff);

	RECT r;
	iexSystem::GetScreenRect(ScreenMode, r);

	TLVERTEX v[4];

	v[0].sx = v[2].sx = (float)r.left;
	v[1].sx = v[3].sx = (float)(r.left + r.right) - 0.5f;
	v[0].sy = v[1].sy = (float)r.top;
	v[2].sy = v[3].sy = (float)(r.top + r.bottom) - 0.5f;

	for (int i = 0; i < 4; i++) {
		v[i].sz = 0.0f;
		v[i].rhw = 1.0f;
		v[i].color = m_color.color;
	}

	iexPolygon::Render2D(v, 2, NULL, RS_COPY);
}

void Fader::setColor(Color color)
{
	m_color = color;
	m_color.a = 0x0;
}

void Fader::setColor(DWORD color)
{
	m_color.color = color & 0x00ffffff;
}

void Fader::setColor(u8 r, u8 g, u8 b)
{
	m_color.a = 0x0;
	m_color.r = r;
	m_color.g = g;
	m_color.b = b;
}
