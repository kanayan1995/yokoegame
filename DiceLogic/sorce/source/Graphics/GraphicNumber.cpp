#include "Graphics.h"
#include "Utilty/Utility.h"

void GraphicNumbers::Init()
{
	m_obj = nullptr;
	m_width = 0;
	m_height = 0;
	m_margin = 0;
	m_digit = 0;
	m_scale = 1.0f;
	m_value = 0;
}

GraphicNumbers::GraphicNumbers()
{
	Init();
}

GraphicNumbers::GraphicNumbers(char *filename)
{
	Init();
	m_obj = new iex2DObj(filename);
}

GraphicNumbers::~GraphicNumbers()
{
	SafeDelete(&m_obj);
}

void GraphicNumbers::setGraphic(char *filename)
{
	SafeDelete(&m_obj);
	m_obj = new iex2DObj(filename);
}

void GraphicNumbers::setSize(float w, float h)
{
	m_width = w;
	m_height = h;
}

void GraphicNumbers::setMargin(float m)
{
	m_margin = m;
}

void GraphicNumbers::setDigit(int d)
{
	m_digit = d;
}

void GraphicNumbers::setScale(float s)
{
	m_scale = s;
}

void GraphicNumbers::Render(float x, float y)
{
	int value = m_value;
	int digit = (m_digit != 0)? m_digit : (value != 0)? (int)log10((double)value) + 1 : 1;
	int n;

	for (int i = digit - 1; i >= 0; i--) {
		n = value % 10;
		value /= 10;

		m_obj->Render(
			(int)(x + (i * (m_width * m_scale + m_margin))),
			(int)(y * m_scale),
			(int)(m_width * m_scale),
			(int)(m_height * m_scale),
			(int)(n * m_width),
			0, 
			(int)(m_width), 
			(int)(m_height) );
	}
}
