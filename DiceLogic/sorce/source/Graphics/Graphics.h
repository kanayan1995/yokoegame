//----------------------------------------------------------------------------------//
//																					//
//			製作者	:　嶋津															//
//																					//
//---------------------------------------------------------------------------------	//
#pragma once

#include "iextreme.h"
#include "iextremePlus.h"

//	テクスチャによる数字描画クラス
class GraphicNumbers
{
private:
	iex2DObj *m_obj;					//	横並びの数字(0-9-)
	float m_width;						//	1文字の横
	float m_height;						//	1文字の縦
	float m_scale;						//	スケール
	float m_margin;						//	文字間隔調整
	int m_digit;						//	桁数(0で現在の桁数)

	void Init();
	
public:
	GraphicNumbers();
	GraphicNumbers(char *filename);
	~GraphicNumbers();

	int m_value;						//	表示する値

	void setGraphic(char *filename);	//	画像セット
	void setSize(float w, float h);		//	サイズセット
	void setMargin(float m);			//	マージンセット
	void setDigit(int d);				//	桁数セット
	void setScale(float s);				//	スケールセット

	void Render(float x, float y);
};

//	テクスチャによる文字描画クラス
class GraphicFont
{
private:
	iex2DObj *m_obj;					//	横並びの数字(0-9-)
	float m_width;						//	1文字の横
	float m_height;						//	1文字の縦
	float m_scale;						//	スケール
	float m_margin;						//	文字間隔調整

	void Init();

public:
	GraphicFont();
	GraphicFont(char *filename);
	~GraphicFont();

	void setGraphic(char *filename);	//	画像セット
	void setSize(float w, float h);		//	サイズセット
	void setMargin(float m);			//	マージンセット
	void setScale(float s);				//	スケールセット

	void Render(float x, float y, char *str);
};
