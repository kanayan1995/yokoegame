//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __UI_H__
#define __UI_H__

#include "../Camera/Camera.h"
#include "../Gimmick/GimmickManager.h"

//*****************************************************************************
//	BaseUI
//	Uiがひとつのクラスの中で何個も呼ぶ可能性があるので
//	継承ではなくメソッドとして持たせるようにして使う
//*****************************************************************************
class BaseUI
{
private:
	//unique_ptr<iex2DObj> m_obj;		//とりあえず一つ
public:	
	unique_ptr<iex2DObj> m_obj;		//とりあえず一つ
	int m_x		, m_y;				//張る位置
	int m_width	, m_height;			//大きさ
	int m_cutX	, m_cutY;			//切りだし位置
	int m_cutWidth , m_cutHeight;	//切りだし位置から範囲を指定

	//スケール調整
	float m_scale;					//大きさを変化させるために使う
	int m_scaleMax , m_scaleMin;	//最大の大きさと小ささを入れる
	float m_scaleChangeSecond;		//サイズを返る時の時間（秒を入れる）
public:
	//大きさを変えるアニメーションをする
	enum class SCALEANIME
	{
		BIG ,		//小さいから大きい
		SMALL ,		//大きいから小さい
		BIGLOOP ,	//大きいから大小にループ
		SMALLLOOP ,	//小さいから大小にループ
	};
public:
	BaseUI()
	{
		m_obj				= nullptr;
		m_x					= 0;
		m_y					= 0;
		m_width				= 0;
		m_height			= 0;
		m_cutX				= 0;
		m_cutY				= 0;
		m_cutWidth			= 0;
		m_cutHeight			= 0;
		m_scale				= 0;
		m_scaleMax			= 10;
		m_scaleMin			= 0;
		m_scaleChangeSecond = 1;
	}
	~BaseUI()
	{

	}

	//ひとつのクラスからnewを2個かかなあかんから書く
	void LoadPng( char* filename ) { m_obj = make_unique<iex2DObj>( filename ); }	//こいつは絶対呼ばないといかん
	void Render();
	void RenderScale( SCALEANIME animation );
};

class ScaleAnimationUI : public BaseUI
{

};

//*****************************************************************************
//	CameraUI
//	その名の通りCameraで使うUI
//*****************************************************************************
class CameraUI
{
private:
	unique_ptr<BaseUI> m_freeCamera;	//フリーカメラの時に出すUI
	unique_ptr<BaseUI> m_freeArrow;		//四方に4つ置く
	unique_ptr<BaseUI> m_gimmickCamera;	//ギミックカメラの時に出す
	unique_ptr<BaseUI> m_choice;		//ギミックカメラの時に出す

	weak_ptr<Camera> m_camera;			//カメラがどのモードなのか知りたいからいる
public:
	CameraUI()
	{
		m_freeCamera		= nullptr;
		m_freeArrow			= nullptr;
		m_gimmickCamera		= nullptr;
		m_choice			= nullptr;
	}
	~CameraUI()
	{

	}

	void Initialize();

	void FreeCameraRender();
	void GimmickCameraRender();
	void Render();
public: //-----セッター-----//
	void SetCameraPointer( shared_ptr<Camera> camera ) { m_camera = camera; }
};

//*****************************************************************************
//	ClearUI
//	クリア画面　カーソルでセレクトに戻るとかつけれたらいいね
//*****************************************************************************
class ClearUI
{
private:
	unique_ptr<BaseUI> m_clear;			//クリアのUI
	unique_ptr<BaseUI> m_selectBack;	//スタートに戻るUI
public:
	ClearUI()
	{
		m_clear = nullptr;
		m_selectBack = nullptr;
	}
	~ClearUI()
	{

	}

	void Initialize();
	void Render();

};

//*****************************************************************************
//	GameMainで表示するUI
//*****************************************************************************
class MainUI
{
private:
	unique_ptr<BaseUI> m_optionUI;		//リスタートのUI

	//ワープUI
	unique_ptr<BaseUI> m_warpUI;		//ワープの画像を表示させてる
	unique_ptr<BaseUI> m_smallUI;		//小さい
	unique_ptr<BaseUI> m_bigUI;			//大きい
	unique_ptr<BaseUI> m_warpFace4;		//4の面
	unique_ptr<BaseUI> m_warpFace5;		//5の面
	unique_ptr<BaseUI> m_warpFace6;		//6の面
	unique_ptr<BaseUI> m_dontWarpUI;	//ワープできません
	unique_ptr<BaseUI> m_explanationUI;	//サイコロの説明のUI
	unique_ptr<BaseUI> m_personUI;		//今何人いてるか
	unique_ptr<BaseUI> m_rollUI;		//転がすのを解らすUI

	weak_ptr<GimmickManager> m_gimmickManager;
public:
	MainUI()
	{
		m_warpUI	= nullptr;
		m_optionUI	= nullptr;
		m_smallUI	= nullptr;
		m_bigUI		= nullptr;
		m_warpFace4 = nullptr;
		m_warpFace5 = nullptr;
		m_warpFace6 = nullptr;
		m_explanationUI = nullptr;
		m_personUI = nullptr;
		m_rollUI = nullptr;
		m_gimmickManager.lock() = nullptr;
	}
	~MainUI()
	{

	}

	void Initialize();
	void Render();
public: //-----セッター-----//
	void SetGimmickManagerPointer( shared_ptr<GimmickManager> gimmickManager ) { m_gimmickManager = gimmickManager; }
};

#endif