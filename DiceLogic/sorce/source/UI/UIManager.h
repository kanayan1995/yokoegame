//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __UIMANAGER_H__
#define __UMANAGERI_H__

#include "UI.h"
#include "../GameMode/GameMode.h"
#include "../Gimmick/GimmickManager.h"

class UIManager
{
private:
	unique_ptr<CameraUI> m_cameraUI;	//カメラUI
	unique_ptr<ClearUI>  m_clearUI;		//クリアUI
	unique_ptr<MainUI>	 m_mainUI;		//メインでのUI

	weak_ptr<GameMode>	m_gameMode;		//ゲームモードの状態を見て出すUIを決める
public:
	UIManager()
	{
		m_cameraUI	= nullptr;
		m_clearUI	= nullptr; 
		m_mainUI	= nullptr;
	}
	~UIManager()
	{

	}

	void Initialize();
	void Update();
	void Render();

public://---------セッター--------//
	void SetCameraPointer( shared_ptr<Camera> camera )						{ m_cameraUI->SetCameraPointer( camera ); }				//カメラのポインタ格納
	void SetGameModePointer( shared_ptr<GameMode> gameMode )				{ m_gameMode = gameMode; }								//ゲームの状態を格納
	void SetGimmickManagerPointer( shared_ptr<GimmickManager> gimmickMNG )	{ m_mainUI->SetGimmickManagerPointer( gimmickMNG ); }	//面の情報を入れる
};

#endif 