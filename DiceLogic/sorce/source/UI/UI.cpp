#include "iextreme.h"
#include "iextremePlus.h"
#include "../GameMode/GameMode.h"

#include "UI.h"
//*****************************************************************************
//	BaseUI
//*****************************************************************************

//-----------------------------------------------------
//	とりあえず描画
//-----------------------------------------------------
void BaseUI::Render()
{
	m_obj->Render( m_x , m_y , m_width , m_height , m_cutX , m_cutY , m_cutWidth , m_cutHeight );
}

void BaseUI::RenderScale( SCALEANIME animation )
{
	float changeScale;
	switch ( animation )
	{
		case SCALEANIME::SMALL:
			changeScale = -m_scaleMax / FPS( m_scaleChangeSecond );
			m_scale += changeScale;
			if ( m_scale < m_scaleMin ) m_scale = m_scaleMin;
			break;
		case SCALEANIME::BIG:
			changeScale = m_scaleMax / FPS( m_scaleChangeSecond );
			m_scale += changeScale;
			if ( m_scale > m_scaleMax ) m_scale = m_scaleMax;
			break;
		case SCALEANIME::BIGLOOP:
			break;
		case SCALEANIME::SMALLLOOP:
			break;
		default:
			break;
	}
	m_obj->RenderScale( m_x , m_y , m_cutX , m_cutY , m_cutWidth , m_cutHeight , (int)m_scale );
}

//*****************************************************************************
//	CameraUI
//*****************************************************************************

//-----------------------------------------------------
//	初期化
//-----------------------------------------------------
void CameraUI::Initialize()
{
	char* fileName;
	fileName = "DATA/UI/Camera/FreeCamera.png";
	m_freeCamera = make_unique<BaseUI>();
	m_freeCamera->LoadPng( fileName );
	m_freeCamera->m_x = 0;
	m_freeCamera->m_y = 0;
	m_freeCamera->m_width = 400;
	m_freeCamera->m_height = 100;
	m_freeCamera->m_cutX = 0;
	m_freeCamera->m_cutY = 0;
	m_freeCamera->m_cutWidth = 512;
	m_freeCamera->m_cutHeight = 128;

	fileName = "DATA/UI/Camera/Arrow.png";
	m_freeArrow = make_unique<BaseUI>();
	m_freeArrow->LoadPng( fileName );
	m_freeArrow->m_x = 0;
	m_freeArrow->m_y = 0;
	m_freeArrow->m_width = 80;
	m_freeArrow->m_height = 80;
	m_freeArrow->m_cutX = 0;
	m_freeArrow->m_cutY = 0;
	m_freeArrow->m_cutWidth = 64;
	m_freeArrow->m_cutHeight = 64;

	fileName = "DATA/UI/Camera/GimmickCamra.png";
	m_gimmickCamera = make_unique<BaseUI>();
	m_gimmickCamera->LoadPng( fileName );
	m_gimmickCamera->m_x = 0;
	m_gimmickCamera->m_y = 0;
	m_gimmickCamera->m_width = 400;
	m_gimmickCamera->m_height = 100;
	m_gimmickCamera->m_cutX = 0;
	m_gimmickCamera->m_cutY = 0;
	m_gimmickCamera->m_cutWidth = 512;
	m_gimmickCamera->m_cutHeight = 128;

	fileName = "DATA/UI/Camera/GimmckChoice.png";
	m_choice = make_unique<BaseUI>();
	m_choice->LoadPng( fileName );
	m_choice->m_x = 0;
	m_choice->m_y = 80;
	m_choice->m_width = 400;
	m_choice->m_height = 100;
	m_choice->m_cutX = 0;
	m_choice->m_cutY = 0;
	m_choice->m_cutWidth = 512;
	m_choice->m_cutHeight = 128;
}

//-----------------------------------------------------
//	フリーカメラ
//-----------------------------------------------------
void CameraUI::FreeCameraRender()
{
	m_freeCamera->Render();
	//右
	m_freeArrow->m_x = iexSystem::ScreenWidth - m_freeArrow->m_width;
	m_freeArrow->m_y = iexSystem::ScreenHeight / 2 - m_freeArrow->m_height;
	m_freeArrow->m_cutX = 64 * 0;
	m_freeArrow->Render();
	//左
	m_freeArrow->m_x = 0;
	m_freeArrow->m_y = iexSystem::ScreenHeight / 2 - m_freeArrow->m_width;
	m_freeArrow->m_cutX = 64 * 1;
	m_freeArrow->Render();
	//上
	m_freeArrow->m_x = iexSystem::ScreenWidth / 2 - m_freeArrow->m_width/2;
	m_freeArrow->m_y = 0;
	m_freeArrow->m_cutX = 64 * 2;
	m_freeArrow->Render();
	//下
	m_freeArrow->m_x = iexSystem::ScreenWidth / 2 - m_freeArrow->m_width/2;
	m_freeArrow->m_y = iexSystem::ScreenHeight - m_freeArrow->m_height;
	m_freeArrow->m_cutX = 64 * 3;
	m_freeArrow->Render();
}

//-----------------------------------------------------
//	ギミックカメラ
//-----------------------------------------------------
void CameraUI::GimmickCameraRender()
{
	m_gimmickCamera->Render();
	m_choice->Render();
}

//-----------------------------------------------------
//	描画
//-----------------------------------------------------
void CameraUI::Render()
{
	switch ( m_camera.lock()->GetCameraMode() )
	{
		case Camera::CAMERAMODE::FREE:
			FreeCameraRender();
			break;
		case Camera::CAMERAMODE::GIMMICK:
			GimmickCameraRender();
			break;
		default:
			break;
	}
}




//*****************************************************************************
//	ClearUI
//*****************************************************************************

//-----------------------------------------------------
//	初期化
//-----------------------------------------------------
void ClearUI::Initialize()
{
	m_clear = make_unique<BaseUI>();
	m_clear->LoadPng( "DATA/UI/Clear/Clear.png" );
	m_clear->m_x = 400 - 128;
	m_clear->m_y = 200;
	m_clear->m_height = 256;
	m_clear->m_width = 512 + 250;
	m_clear->m_cutX = 0;
	m_clear->m_cutY = 0;
	m_clear->m_cutWidth = 512;
	m_clear->m_cutHeight = 128;

	m_selectBack = make_unique<BaseUI>();
	m_selectBack->LoadPng( "DATA/UI/Clear/Select.png" );
	m_selectBack->m_x = 450;
	m_selectBack->m_y = 450;
	m_selectBack->m_height = 150;
	m_selectBack->m_width = 450;
	m_selectBack->m_cutX = 0;
	m_selectBack->m_cutY = 0;
	m_selectBack->m_cutWidth = 512;
	m_selectBack->m_cutHeight = 256;
}

//-----------------------------------------------------
//	描画
//-----------------------------------------------------
void ClearUI::Render()
{
	m_clear->Render();
	m_selectBack->Render();
}

//*****************************************************************************
//	ClearUI
//*****************************************************************************

//-----------------------------------------------------
//	初期化
//-----------------------------------------------------
void MainUI::Initialize()
{
	char* fileName;

	//リスタートを促すUI
	fileName = "DATA/UI/GameMain/Option.png";
	m_optionUI = make_unique<BaseUI>();
	m_optionUI->LoadPng( fileName );
	m_optionUI->m_x = 1280 - 300;
	m_optionUI->m_y = 0;
	m_optionUI->m_width	 = 300;
	m_optionUI->m_height = 50;
	m_optionUI->m_cutX = 0;
	m_optionUI->m_cutY = 0;
	m_optionUI->m_cutWidth = 512;
	m_optionUI->m_cutHeight = 128;	
	
	//ワープUI
	fileName = "DATA/UI/GameMain/UI_WARP.png";
	m_warpUI = make_unique<BaseUI>();	
	m_warpUI->LoadPng( fileName );
	m_warpUI->m_x = 950 + 20;
	m_warpUI->m_y = 510 + 20;
	m_warpUI->m_width	= 320 - 20;
	m_warpUI->m_height	= 210 - 20;
	m_warpUI->m_cutX = 0;
	m_warpUI->m_cutY = 0;
	m_warpUI->m_cutWidth = 320;
	m_warpUI->m_cutHeight = 192;


	//転がすを解らすUI
	fileName = "DATA/UI/GameMain/Rolling.png";
	m_rollUI = make_unique<BaseUI>();
	m_rollUI->LoadPng( fileName );
	m_rollUI->m_x = 955 + 20;
	m_rollUI->m_y = 300 + 30;
	m_rollUI->m_width = 324 - 20;
	m_rollUI->m_height = 210 - 20;
	m_rollUI->m_cutX = 0;
	m_rollUI->m_cutY = 0;
	m_rollUI->m_cutWidth = 384;
	m_rollUI->m_cutHeight = 256;


	//4の面
	fileName = "DATA/UI/GameMain/Warp4.png";
	m_warpFace4 = make_unique<BaseUI>();
	m_warpFace4->LoadPng( fileName );
	m_warpFace4->m_x		 = 1032;
	m_warpFace4->m_y		 = 630;
	m_warpFace4->m_width	 = 64;
	m_warpFace4->m_height	 = 64;
	m_warpFace4->m_cutX		 = 0;
	m_warpFace4->m_cutY		 = 0;
	m_warpFace4->m_cutWidth	 = 64;
	m_warpFace4->m_cutHeight = 64;
	m_warpFace4->m_scaleMax	 = 32;
	m_warpFace4->m_scaleMin	 = 0;
	m_warpFace4->m_scaleChangeSecond = 0.5f;

	//5の面
	fileName = "DATA/UI/GameMain/Warp5.png";
	m_warpFace5 = make_unique<BaseUI>();
	m_warpFace5->LoadPng( fileName );
	m_warpFace5->m_x		 = m_warpFace4->m_x;
	m_warpFace5->m_y		 = m_warpFace4->m_y;
	m_warpFace5->m_width	 = m_warpFace4->m_width;
	m_warpFace5->m_height	 = m_warpFace4->m_height;
	m_warpFace5->m_cutX		 = m_warpFace4->m_cutX;
	m_warpFace5->m_cutY		 = m_warpFace4->m_cutY;
	m_warpFace5->m_cutWidth  = m_warpFace4->m_cutWidth;
	m_warpFace5->m_cutHeight = m_warpFace4->m_cutHeight;
	m_warpFace5->m_scaleMax  = m_warpFace4->m_scaleMax;
	m_warpFace5->m_scaleMin  = m_warpFace4->m_scaleMin;
	m_warpFace5->m_scaleChangeSecond = m_warpFace4->m_scaleChangeSecond;

	//6の面
	fileName = "DATA/UI/GameMain/Warp6.png";
	m_warpFace6 = make_unique<BaseUI>();
	m_warpFace6->LoadPng( fileName );
	m_warpFace6->m_x		 = m_warpFace4->m_x;
	m_warpFace6->m_y		 = m_warpFace4->m_y;
	m_warpFace6->m_width	 = m_warpFace4->m_width;
	m_warpFace6->m_height	 = m_warpFace4->m_height;
	m_warpFace6->m_cutX		 = m_warpFace4->m_cutX;
	m_warpFace6->m_cutY		 = m_warpFace4->m_cutY;
	m_warpFace6->m_cutWidth  = m_warpFace4->m_cutWidth;
	m_warpFace6->m_cutHeight = m_warpFace4->m_cutHeight;
	m_warpFace6->m_scaleMax  = m_warpFace4->m_scaleMax;
	m_warpFace6->m_scaleMin  = m_warpFace4->m_scaleMin;
	m_warpFace6->m_scaleChangeSecond = m_warpFace4->m_scaleChangeSecond;

	//ワープ出来ません
	fileName = "DATA/UI/GameMain/NotWarp.png";
	m_dontWarpUI = make_unique<BaseUI>();
	m_dontWarpUI->LoadPng( fileName );
	m_dontWarpUI->m_x		 = m_warpFace4->m_x;
	m_dontWarpUI->m_y		 = m_warpFace4->m_y;
	m_dontWarpUI->m_width	 = m_warpFace4->m_width;
	m_dontWarpUI->m_height	 = m_warpFace4->m_height;
	m_dontWarpUI->m_cutX	 = m_warpFace4->m_cutX;
	m_dontWarpUI->m_cutY	 = m_warpFace4->m_cutY;
	m_dontWarpUI->m_cutWidth = m_warpFace4->m_cutWidth;
	m_dontWarpUI->m_cutHeight= m_warpFace4->m_cutHeight;
	m_dontWarpUI->m_scaleMax = m_warpFace4->m_scaleMax;
	m_dontWarpUI->m_scaleMin = m_warpFace4->m_scaleMin;
	m_dontWarpUI->m_scaleChangeSecond = m_warpFace4->m_scaleChangeSecond;

	//サイコロに関するUI
	fileName = "DATA/UI/GameMain/Explanation.png";
	m_explanationUI = make_unique<BaseUI>();
	m_explanationUI->LoadPng( fileName );
	m_explanationUI->m_x = 0;
	m_explanationUI->m_y = 720 - 256;
	m_explanationUI->m_width = 256;
	m_explanationUI->m_height = 256;
	m_explanationUI->m_cutX = 0;
	m_explanationUI->m_cutY = 0;
	m_explanationUI->m_cutWidth = 512;
	m_explanationUI->m_cutHeight = 512;

	//今何人を表すか
	fileName = "DATA/UI/GameMain/Now.png";
	m_personUI = make_unique<BaseUI>();
	m_personUI->LoadPng( fileName );
	m_personUI->m_x = 26;
	m_personUI->m_y = 720 - 220;
	m_personUI->m_width  = 64;
	m_personUI->m_height = 64;
	m_personUI->m_cutX = 0;
	m_personUI->m_cutY = 0;
	m_personUI->m_cutWidth  = 64;
	m_personUI->m_cutHeight = 64;

}

//-----------------------------------------------------
//	描画
//-----------------------------------------------------
void MainUI::Render()
{
	m_warpUI->Render();
	m_optionUI->Render();
	m_explanationUI->Render();
	m_rollUI->Render();
	m_personUI->Render();

	switch ( m_gimmickManager.lock()->GetFaceType() )
	{
		case DICEWARPTYPE::ONE:
			m_warpFace4->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_warpFace5->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_warpFace6->RenderScale( BaseUI::SCALEANIME::BIG );
			m_dontWarpUI->RenderScale( BaseUI::SCALEANIME::SMALL );
			break;
		case DICEWARPTYPE::TWO:
			m_warpFace4->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_warpFace5->RenderScale( BaseUI::SCALEANIME::BIG );
			m_warpFace6->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_dontWarpUI->RenderScale( BaseUI::SCALEANIME::SMALL );
			break;
		case DICEWARPTYPE::THREE:
			m_warpFace4->RenderScale( BaseUI::SCALEANIME::BIG );
			m_warpFace5->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_warpFace6->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_dontWarpUI->RenderScale( BaseUI::SCALEANIME::SMALL );
			break;
		case DICEWARPTYPE::FOWR:
		case DICEWARPTYPE::FIVE:
		case DICEWARPTYPE::SIX:	
			m_warpFace4->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_warpFace5->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_warpFace6->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_dontWarpUI->RenderScale( BaseUI::SCALEANIME::BIG );
			break;
		case DICEWARPTYPE::NONE:
			m_warpFace4->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_warpFace5->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_warpFace6->RenderScale( BaseUI::SCALEANIME::SMALL );
			m_dontWarpUI->RenderScale( BaseUI::SCALEANIME::SMALL );
		default:
			//m_dontWarpUI->Render();
			break;
	}
}
