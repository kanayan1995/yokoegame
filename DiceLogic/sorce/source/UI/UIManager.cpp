#include "iextreme.h"
#include "iextremePlus.h"

#include "UIManager.h"

//*****************************************************************************
//	初期化
//*****************************************************************************
void UIManager::Initialize()
{
	//カメラUI
	m_cameraUI = make_unique<CameraUI>();
	m_cameraUI->Initialize();

	//クリアUI
	m_clearUI = make_unique<ClearUI>();
	m_clearUI->Initialize();

	//メインUI
	m_mainUI = make_unique<MainUI>();
	m_mainUI->Initialize();
}

//*****************************************************************************
//	更新
//*****************************************************************************
void UIManager::Update()
{
}

//*****************************************************************************
//	描画
//*****************************************************************************
void UIManager::Render()
{
	switch ( m_gameMode.lock()->GetGameState() )
	{
		case GAMESTATE::MAIN:
			m_mainUI->Render();
			break;
		case GAMESTATE::MENU:
			break;
		case GAMESTATE::CAMERA:
			m_cameraUI->Render();
			break;
		case GAMESTATE::CLEAR:
			m_clearUI->Render();
			break;
		case GAMESTATE::RESET:	
			break;
		default:
			break;
	}		

}
