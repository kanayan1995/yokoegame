//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　山中										//
//																					//
//---------------------------------------------------------------------------------	//
#include "iextreme.h"
#include "iextremePlus.h"
#include "../System/System.h"
#include "../Collision/Collision.h"
#include "../Collision/Sphere.h"
#include "../Gimmick/GimmickManager.h"
#include "Stage.h"


////データ保存用
//StageData data;



Stage::Stage()
{
	//書き出し用の配列初期化
	for (int i = 0; i < MAX; i++)		{
			excel_data[i].type = StageObjType::NONE;
	}
	
};

//------------------------------------------------
//			オブジェクトの生成と読み込み
//-------------------------------------------------
bool Stage::SetIndex(int type, char* filename)
{
	orgObj[type] = make_unique<iexMesh>(filename);
	if (orgObj[type] == nullptr)return false;
	

	return true;
}

//------------------------------------------------
//			ステージデータ読み込み
//-------------------------------------------------
bool Stage::StageDataLoad(char* filename,shared_ptr<GimmickManager> m_gimmickManager)
{
	FILE* fp;
	fp = fopen(filename,"r");
	
	//例外処理
	if (fp == null){
		return false;
	}
	
	int ret;				//値を格納する変数
	int i = 0;				//行数

	
	//ファイルの要素の読み込み
		while (ret = fscanf(fp,"%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
		&data.orgType,
		&data.TopNum,
		&data.pos_x,
		&data.pos_y,
		&data.pos_z,
		&data.AABBsize_x,
		&data.AABBsize_y,
		&data.AABBsize_z,
		&data.axis_x,
		&data.axis_y,
		&data.axis_z,
		&data.angle
		) != EOF)
	{
			//オブジェクトの設置
			//オブジェクトの種類・頂点の出目・位置情報
			//回転軸・回転角度・ギミックマネージャーのポインタ
			Set((StageObjType)data.orgType,
				data.TopNum,
				Vector3(data.pos_x,data.pos_y,data.pos_z),
				Vector3(data.AABBsize_x,data.AABBsize_y,data.AABBsize_z),
				Vector3(data.axis_x,data.axis_y,data.axis_z),
				data.angle,
				m_gimmickManager);
						
			if (data.orgType == 8)
			{
				Vector3 test = Vector3(data.AABBsize_x, data.AABBsize_y, data.AABBsize_z);
			}

			//オブジェクトが存在していなければ飛ばす
			if (excel_data[i].obj != nullptr)
			{
				data.aabb.SetMax(Vector3(data.AABBsize_x,data.AABBsize_y,data.AABBsize_z) );
				data.aabb.SetMin(Vector3(data.AABBsize_x,0,data.AABBsize_z) );
				data.aabb.UpdatePos( Vector3( data.pos_x , data.pos_y , data.pos_z ) );
				Collision::SetCollisionMesh( (StageObjType)data.orgType , excel_data[ i ].obj );
				Collision::SetCollisionAABB( (StageObjType)data.orgType , data.aabb );
			}
			i++;
	}
	fclose(fp);
	return 0;
}

//------------------------------------------------
//			読み込まれたオブジェクトの設置
//-------------------------------------------------
void Stage::Set( StageObjType type,int TopNum, const Vector3 pos,Vector3 AABBSize, Vector3 axis,float angle,shared_ptr<GimmickManager> m_gimmickManager)
{
	for (int i = 0; i < MAX; i++)
	{ 
		//存在しなければ飛ばす
		if (excel_data[i].type != StageObjType::NONE)continue;
		
		//プレイヤーとヘルプちゃんのタイプなら位置情報保存
		if (type == StageObjType::PLAYER){
			excel_data[i].type = StageObjType::PLAYER;
			playerPos = pos;
			break;
		}

		if (type == StageObjType::HELPWOMAN){
			excel_data[i].type = StageObjType::HELPWOMAN;
			helpPos = pos;
			break;
		}

		//プレイヤー・ヘルプちゃん・大小サイコロ以外は処理通す
		if (type != StageObjType::PLAYER && type != StageObjType::HELPWOMAN && type != StageObjType::SMALL_DICE && type != StageObjType::BIG_DICE)
		{
			//種類
			excel_data[i].type = type;
			
			//クローンの生成
			excel_data[i].obj = orgObj[(int)type]->CloneShared();
			
			//位置情報
			excel_data[i].pos.x = pos.x;
			excel_data[i].pos.y = pos.y;
			excel_data[i].pos.z = pos.z;
			i++;
			break;
		}

		//上の条件式に入らないものはギミックであるため
		//これより下でギミック機能を付与
		
		//ギミックの設定
		switch (type)
		{
		case StageObjType::SMALL_DICE:

			//オブジェの種類と頂点の出目決める
			excel_data[i].type = type;
			excel_data[i].TopNum = TopNum;

			//回転軸と回転角度の決定(Quaternion)
			excel_data[i].axis = axis;
			excel_data[i].angle = angle;

			/*ギミック機能の追加*/
			m_gimmickManager->SetGimmick(Vector3(pos.x, pos.y, pos.z), excel_data[i].axis,
				excel_data[i].angle,GIMMICKTYPE::ONEPERSONDICE);

			/*上面を軸にしてダイスの面決定*/
			m_gimmickManager->SetSmallDiceFaceType(excel_data[i].TopNum);
			break;

		case StageObjType::BIG_DICE:
			
			//オブジェの種類と頂点の出目決める
			excel_data[i].type = type;
			excel_data[i].TopNum = TopNum;

				//回転軸と回転角度の決定(Quaternion)
			excel_data[i].axis = axis;
			excel_data[i].angle = angle;

			/*ギミック機能の追加*/
			m_gimmickManager->SetGimmick(Vector3(pos.x, pos.y, pos.z),excel_data[i].axis,
				excel_data[i].angle, GIMMICKTYPE::TWOPERSONDICE);
			
			/*上面を軸にしてダイスの面決定*/
			m_gimmickManager->SetBigDiceFaceType(excel_data[i].TopNum);
			break;

		default:
			break;
		}
		break;
	}
}

//------------------------------------------------
//					更新
//-------------------------------------------------
void Stage::Update()
{
	for (int i = 0; i < MAX; i++)
	{

		//生成されていないオブジェクトと
		//プレイヤー・ヘルプちゃん・大小サイコロは処理飛ばす
		if (excel_data[i].type == StageObjType::NONE)continue;
		if (excel_data[i].type == StageObjType::PLAYER)continue;
		if (excel_data[i].type == StageObjType::HELPWOMAN)continue;
		if (excel_data[i].type == StageObjType::SMALL_DICE)continue;
		if (excel_data[i].type == StageObjType::BIG_DICE)continue;
			
		excel_data[i].obj->SetScale(1.0f);
		excel_data[i].obj->SetPos(excel_data[i].pos);
		excel_data[i].obj->Update();
	}
}


//------------------------------------------------
//					描画
//-------------------------------------------------
void Stage::Render()
{
	for (int i = 0; i < MAX; i++)
	{
		//生成されていないオブジェクトと
		//プレイヤー・ヘルプちゃん・大小サイコロは処理飛ばす
		if (excel_data[i].type == StageObjType::NONE)continue;
		if (excel_data[i].type == StageObjType::PLAYER)continue;
		if (excel_data[i].type == StageObjType::HELPWOMAN)continue;
		if (excel_data[i].type == StageObjType::SMALL_DICE)continue;
		if (excel_data[i].type == StageObjType::BIG_DICE)continue;
		excel_data[ i ].obj->Render( shader , "depth" );
	}
}

void Stage::RenderShadow()
{
	for (int i = 0; i < MAX; i++)
	{
		//生成されていないオブジェクトと
		//プレイヤー・ヘルプちゃん・大小サイコロは処理飛ばす
	if (excel_data[i].type	   == StageObjType::NONE)continue;
		if (excel_data[i].type == StageObjType::PLAYER)continue;
		if (excel_data[i].type == StageObjType::HELPWOMAN)continue;
		if (excel_data[i].type == StageObjType::SMALL_DICE)continue;
		if (excel_data[i].type == StageObjType::BIG_DICE)continue;
		excel_data[i].obj->Render(myshader,"ShadowBuf");
	}
}

