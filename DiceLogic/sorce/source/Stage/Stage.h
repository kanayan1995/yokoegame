//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　山中										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __STAGE_H__
#define __STAGE_H__

#include "../Collision/AABB.h"
class GimmickManager;

//***************************************
//			
//***************************************
enum class StageObjType : int
{
	NONE = -1 ,
	PLAYER ,		//プレイヤー
	HELPWOMAN ,		//ヘルプちゃん
	SMALL_DICE ,	//小さいサイコロ
	BIG_DICE ,		//大きいサイコロ
	CUBE_WALL ,		//立方体の壁
	WIDEWALL_X ,	//横方向に長いオブジェクト
	WIDEWALL_Z ,	//縦方向に長いオブジェクト
	GRID ,
	WALL_LONG_Y ,	//縦に細長いオブジェクト
	DOWNHILL_LEFT ,	//左に坂道があるオブジェクト
	DOWNHILL_RIGHT	//右に坂道があるオブジェクト
};

//***************************************
//			Excelクラス
//***************************************
class  ExcelData
{ 

public:
	ExcelData(){
		obj = nullptr;
		type = StageObjType::NONE;
		//TopNum - 1;
	}

	
public:	//メンバ変数
	shared_ptr<iexMesh> obj;
	StageObjType type;
	int TopNum;
	Vector3 pos;
	Vector3 axis;
	float angle;
	AABB aabb;

};


//***************************************
//		データ受取り用構造体
//***************************************
typedef struct
{
	int orgType;
	int TopNum;
	float pos_x;
	float pos_y;
	float pos_z;
	float AABBsize_x;
	float AABBsize_y;
	float AABBsize_z;
	//回転軸決めるためのVector3 axis
	float axis_x;
	float axis_y;
	float axis_z;
	float angle;	//回転角度
	AABB aabb;
}StageData;


//***************************************
//			ステージクラス
//***************************************
class Stage
{
public:
	//データ保存用
	StageData data;

	/*出現できるオブジェクトの個数の最大値*/
	static const int MAX = 500;
	/*出現できるオブジェクトの種類の最大値*/
	static const int KIND = 20;

private:

	/*プレイヤーの位置情報*/
	Vector3 playerPos;
	/*ヘルプちゃんの位置情報*/
	Vector3 helpPos;

	/*@1現在のステージ番号 @2オブジェの種類 @3オブジェの位置情報 @4回転する軸の値 
	　@5回転角度(必ず1f以上を入れる) @6ギミックマネージャーのポインタ変数
	　[details]ステージオブジェクトのセット*/
	void Set( StageObjType type,
		int TopNum,
		const Vector3 pos,
		const Vector3 AABBSize,
		Vector3 axis,
		float angle,
		shared_ptr<GimmickManager> m_gimmickManager);

public:

	/*読み込んだデータを格納する変数*/
	ExcelData excel_data[MAX];

	/*ステージオブジェ*/
	shared_ptr<iexMesh> orgObj[KIND];
	

	//--------------------------------以下関数-------------------------------------//

	Stage();
	~Stage(){};
	/*@1オブジェクトの種類番号　@2読み込むファイルの名前(iexMesh型)
	[details] iesMesh型のオリジナルオブジェクトの生成。引数で送ったtypeの番号でオブジェクトを管理。*/
	bool SetIndex(int type, char* filename);
	void Update();
	void Render();
	void RenderShadow();
	/*@1ファイル名読み込み @2ギミックマネージャーのポインタ
	[details]ステージデータ読み込み関数*/
	bool StageDataLoad(char* filename,shared_ptr<GimmickManager> m_gimmickManagerm);

	/*type[0] プレイヤーの位置情報 type[1] ヘルプちゃんの位置情報
	[details]設定したオブジェクトの位置情報を取得*/
	Vector3 GetPos_Chara( StageObjType type)
	{
		if (type == StageObjType::PLAYER){
			return playerPos;
		}

		if (type == StageObjType::HELPWOMAN){
			return helpPos;
		}
		return Vector3Zero;
	}
};
#endif 