#ifndef _CURSOR_H_
#define _CURSOR_H_


#include "../Scene/StageSelect_ObjGroup.h"


//----------------------------------------------------------------------
//
//						カーソルクラス
//
//-----------------------------------------------------------------------
class Cursor
{
private:
	shared_ptr<iex2DObj> cursor;
	Vector3 target;
	Vector3 pos;
	int num;
	
	//画像の横幅高さの固定値
	static const int Image_Height = 128;
	static const int Image_Width = 128;

public:
	/*カーソルの位置補正の為の値の固定値*/
	const float CURSOR_TRANSLATION_UP = 5.0f;
	const float CURSOR_TRANSLATION_RIGHT = 3.0f;

	/*@1目標物の位置座標*/
	Cursor(int num,Vector3 target)
	{
		//オブジェクトの種類分け
		this->num = num;

		//カーソルの座標調整
		this->target.x = target.x - CURSOR_TRANSLATION_RIGHT;
		this->target.y = target.y + CURSOR_TRANSLATION_UP;
		this->target.z = target.z;
	}

	bool Init(char* filename);
	void Update(shared_ptr<SelectObj_Manager> manager);
	void Render();
	void CursorControl(shared_ptr<SelectObj_Manager> manager);
	void CursorTarget();
	void TargetRender();
	void CursorTlanslation();
	bool TransferOn(bool flag);

	/*@1 帰ってくる値　@2ターゲットの位置座標　@3　カメラとワールド座標のマトリクス*/
	bool ScreenToWorld(Vector3& out,Vector3&pos,const Matrix&mat);

	//ステージ番号取得
	int GetNum(){ return this->num; }

	char* GetStageName(int num)
	{
		char* n;
		switch (num)
		{
		case 0:
			n = "DATA/Stage/Stage1/Stage1.csv";
			break;
		case 1:
			n = "DATA/Stage/Stage2/Stage2.csv";
			break;
		case 2:
			n = "DATA/Stage/Stage3/Stage3.csv";
			break;
		case 3:
			n = "DATA/Stage/Stage4/Stage4.csv";
			break;
		case 4:
			n = "DATA/Stage/Stage5/Stage5.csv";
			break;
		case 5:
			n = "DATA/Stage/Stage6/Stage6.csv";
			break;
		case 6:
			n = "DATA/Stage/Stage7/Stage7.csv";
			break;
		case 7:
			n = "DATA/Stage/Stage8/Stage8.csv";
			break;
		case 8:
			n = "DATA/Stage/Stage9/Stage9.csv";
			break;
		}
		return n;
	}
	
};

#endif