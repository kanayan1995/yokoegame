#include "../../IEX/iextreme.h"
#include "../Input/Input.h"
#include "Cursor.h"

//-----------------------------------------------
//				初期化
//-----------------------------------------------
bool Cursor::Init(char* filename)
{
	cursor = make_shared<iex2DObj>(filename);
	if (cursor == nullptr) return false;

	return true;
}
//-----------------------------------------------
//				更新
//-----------------------------------------------
void Cursor::Update(shared_ptr<SelectObj_Manager> manager)
{
	//カーソルの移動処理
	CursorControl( manager);
	

}
//-----------------------------------------------
//				描画
//-----------------------------------------------
void Cursor::Render()
{
	//カーソル描画
	Vector3 out;
	Vector3 p = target;
	ScreenToWorld(out, p, matView*matProjection);

	cursor->Render((int)out.x,(int)out.y, Image_Width, Image_Height, 0, 0, Image_Width, Image_Height);
}

//-----------------------------------------------
//				カーソルの処理
//-----------------------------------------------
void Cursor::CursorControl(shared_ptr<SelectObj_Manager> manager)
{

	//アナログスティック左[押したときに判定]
	if (GAMECONTROLER->AnalogStickLeftPush())
	{
		//ステージ番号減算
		num--;
		if (num < 0)num = 8;
		target = manager->GetPos(num);
		//補正
		CursorTlanslation();
	}

	//アナログスティック右[押したときに判定]
	if (GAMECONTROLER->AnalogStickRightPush())
	{
		//ステージ番号加算
		num++;
		if (num > 8)num = 0;
		target = manager->GetPos(num);
		//補正
		CursorTlanslation();
	}

	//アナログスティック上[押したときに判定]
	if (GAMECONTROLER->AnalogStickUpPush())
	{
		//ステージ番号加算
		num -= 3;
		if (num < 0)num = 0;
		target = manager->GetPos(num);
		//補正
		CursorTlanslation();

	}

	//アナログスティック下[押したときに判定]
	if (GAMECONTROLER->AnalogStickDownPush())
	{
		//ステージ番号加算
		num += 3;
		if (num > 8)num = 8;
		target = manager->GetPos(num);
		//補正
		CursorTlanslation();
	}
}
//-----------------------------------------------
//		カーソルの座標を補正
//----------------------------------------------
void Cursor::CursorTlanslation()
{
		target.x = target.x - CURSOR_TRANSLATION_RIGHT;
		target.y = target.y + CURSOR_TRANSLATION_UP;
		target.z = target.z;
}

//-----------------------------------------------
//		スクリーン座標をワールド座標に変換
//----------------------------------------------
void Cursor::TargetRender()
{
	Vector3 out;
	Vector3 p = Vector3(0, 0, 0);
	ScreenToWorld(out, p, matView*matProjection);
}

bool Cursor::ScreenToWorld(Vector3& out,Vector3&pos,const Matrix&mat)
{
	//頂点×行列で射影する
	Vector3 temp;
	temp.x = pos.x * mat._11 + pos.y * mat._21 + pos.z * mat._31 + 1.0f * mat._41;
	temp.y = pos.y * mat._12 + pos.y * mat._22 + pos.z * mat._32 + 1.0f * mat._42;
	temp.z = pos.z * mat._13 + pos.y * mat._23 + pos.z * mat._33 + 1.0f * mat._43;
	float w = pos.x * mat._14 + pos.y * mat._24 + pos.z * mat._34 + 1.0f * mat._44;
	temp.x /= w;
	temp.y /= w;
	temp.z /= w;

	//画面座標に換算(ビューポート変換)
	long WinWide, WinHeight;
	WinWide = iexSystem::ScreenWidth;
	WinWide /= 2;
	WinHeight = iexSystem::ScreenHeight;
	WinHeight /= 2;

	out.x = (temp.x*WinWide + WinWide);
	out.y = (-temp.y*WinHeight + WinHeight);
	out.z = temp.z;

	if (temp.x > 1.0f)return false;
	if (temp.x <-1.0f)return false;
	if (temp.y > 1.0f)return false;
	if (temp.y <-1.0f)return false;
	if (temp.z < 0)return false;
	return true;

}