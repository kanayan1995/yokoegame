//----------------------------------------------------------------------------------//
//																					//
//						�����	:�@���]												//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __UTILITY_H__
#define __UTILITY_H__

template <typename T>
inline void SafeDelete( T** p )
{
	if ( *p != nullptr )
	{
		delete *p;
		*p = nullptr;
	}
}

inline D3DXVECTOR3 ConvertD3DVec3(const Vector3 &vec)
{
	return D3DXVECTOR3(vec.x, vec.y, vec.z);
}

inline Vector3 ConvertVec3(const D3DXVECTOR3 &vec)
{
	return Vector3(vec.x, vec.y, vec.z);
}


#endif