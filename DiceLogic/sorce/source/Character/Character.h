//----------------------------------------------------------------------------------//
//																					//
//			製作者	:　横江								//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include "../Collision/Sphere.h"
#include "../Collision/AABB.h"
#include "../Collision/Collision.h"
#include "../Graphics/Graphics.h"
#include "Camera/Camera.h"


#define HELP_ACCESS_DISTANCE	(10.0f)		//	アクセス可能な範囲
#define HELP_FOLLOW_OFFSET		(5.0f)		//	フォロー時、対象との距離
#define HELP_MAX_CHANGE_POS		(0.2f)		//	位置の最大変位量
#define HELP_MAX_CHANGE_ANGLE	(0.05f)		//	角の最大変位量


//前方宣言
class PlayerCharacter;
class HelpCharacter;
class GimmickManager;
class BaseGimmick;
class BaseDice;
class Camera;

//****************************************************
//	列挙体
//****************************************************
//-----プレイヤー関係-----//
enum class PLAYERSTATE
{
	Idle ,		//アイドル(待機状態)
	Move ,		//移動
	Push ,		//押す
	Accsess ,	//姫に接触
	Worp,		//ワープ状態
};

//フラグやと見にくいから列挙体
enum class PEOPLES
{
	ONE, 
	TWO
};

//-----ヘルプちゃん関係-----//
enum class HELPSTATE
{
	Wait ,		//待ち
	Follow		//追従
};

//プレイヤーがワープするかを選ぶ
enum  class WARPSTATE
{
	WARPCHEACK,		//ワープできるかの確認
	WARPCHOICE,		//ワープを選択する
};

//****************************************************
//	キャラクターのベース
//****************************************************
class BaseCharacter
{
protected:

	//iex3DObj *obj;			//	モデル
	shared_ptr<iex3DObj> m_obj;	//	モデル
	Vector3 m_pos;				//位置
	Vector3 m_twoPeoplePos;		//二人になったときの位置
	float	m_angle;			//角（rad）
	float	m_setAngle;			//この角度にm_angleを補正する
	Vector3 m_scale;			//大きさ
	Vector3 m_move;				//移動量
	Vector3 m_forward;			//前向き

	Sphere	m_sphere;			//あたり判定に使う
	Vector3 m_spherePosUp;		//球の位置を少しだけ上にあげて中心になるように調整
	AABB	m_aabb;				//あたり判定(四角形)
	Vector3 m_aabbPosUp;		//あたり判定を少しだけ上にあげる

	//定数
	static const float TWO_PERSON_DIST;

	//(＠_＠;) : 横江 テスト
	unique_ptr<GraphicFont>	m_font;
public:
	BaseCharacter()
	{
		m_obj			= nullptr;
		m_pos			= Vector3Zero;
		m_twoPeoplePos	= Vector3Zero;
		m_angle			= 0;
		m_setAngle		= 0;
		m_scale			= Vector3One;
		m_move			= Vector3Zero;
		m_forward		= Vector3Zero;
		m_spherePosUp	= Vector3Zero;
		m_aabbPosUp		= Vector3Zero;
	}
	~BaseCharacter()
	{

	}

	virtual void Initialize();		//初期化
	virtual void Update();			//更新
	virtual void Render();			//描画
	virtual void RenderShadow();	//影を作る
	virtual void RenderDepth();		//深度を作る
protected:
	virtual void StateUpdate() = 0;	//ステート更新
	virtual void ObjUpdate() = 0;	//オブジェ関連の動き
	virtual void SetMotion( int motion );
public: //-----セッター-----//
	void SetPos( const Vector3 &pos )			{ m_pos = pos; }
	void SetPos( float x , float y , float z );
	void SetAngle( float angle )				{ m_angle = angle; }
public: //-----ゲッター-----//
	shared_ptr<iex3DObj> getObj()	{ return m_obj; }
	Vector3 GetPos()				{ return m_pos; }
	float	GetAngle()				{ return m_angle; }
	Sphere  GetSphere()				{ return m_sphere; }
};

//更新の処理順番重要（一部分）
//プレイヤー更新してからGimmickManager
class PlayerCharacter :public BaseCharacter
{
private://---------メンバ変数--------//	
	PEOPLES				m_peoples;					//何人でいてるかをチェックする
	PLAYERSTATE			m_state;					//ステート
	WARPSTATE			m_warpState;				//ワープを選ぶ時に使う

	//あとで別マネージャーで管理するように作るべき
	weak_ptr<Camera>			m_camera;			//現在のカメラ
	weak_ptr<HelpCharacter>		m_help;				//ヘルプちゃんのポインタ
	weak_ptr<GimmickManager>	m_gimmickManager;	//ギミックマネージャーのポインタ（ワープ先を検知するため）
	weak_ptr<BaseGimmick>		m_gimmick;			//ギミックを保存してステートを変えるために使う(CollisionCheackの中で参照)
private:
	enum MOTION : int 
	{
		IDEL			= 0,	//待機
		WALK			= 1,	//歩き
		ROLLCHOICE		= 2,	//転がすの考える
		ROLLWALK		= 3,	//転がしながら進む
		WARPOCHOICE		= 5,	//ワープの選択
		SIT				= 6,	//座っているモーション
		CLEAR			= 7,	//クリアの時のモーション
		HOLDHANDSIDEL	= 8,	//手を繋いでの待機
		HOLDHANDSWALK	= 9,	//手を繋いでの歩き
	};
private://---------メンバ関数--------//
	void UpdateIdle();
	void UpdateAccsess();

	//Move状態での関数
	void PlayerMove();				//スティックで操作
	void ChangeStatePush();			//キー操作で転がすStateに
	void ChangeStateWorp();			//キー入力でワープStateに
	void ToHelp();					//ヘルプちゃんを助ける m_peoplesの人数もTWOにする
	void UpdateMove();				//上の関数まとめて更新

	//Push状態での関数
	void DiceRollChoice();			//選択したサイコロをどこに転がすか選択する
	void PlayerAngleCorrection();	//プレイヤーの向きを補正する
	void UpdatePush();

	//Worp状態での関数
	void WorpCheack();				//ワープ先があるかどうかの確認
	void WarpChoice();				//ワープ先を選択する
	void UpdateWorp();	
	
	//ずっと更新してたほうがいい
	void GroundCheack();			//下にレイ撃って地面判定
	void AABBShaer();				//AABBでの壁ずり
	void OnePersonMove();			//一人でいる時の正面行動
	void TwoPersonMove();			//二人いるとこの行動
	void ForwardCheack();			//前にレイを撃って進行方向チェック

	//自分の状態更新
	void StateUpdate()override;		//ステート更新
	void ObjUpdate()override;		//オブジェ関連の動き
	void MoveObjUpdate();
public:
	PlayerCharacter()
	{
		m_peoples	= PEOPLES::ONE;
		m_state		= PLAYERSTATE::Move;
		m_warpState = WARPSTATE::WARPCHEACK;
		m_camera.lock()			= nullptr;
		m_help.lock()			= nullptr;
		m_gimmickManager.lock() = nullptr;
		m_gimmick.lock()		= nullptr;
	};
	
	void Initialize()override;
	void Update()override;
	void Render()override ;
	void RenderShadow()override;

	void WarpState();
public: //---------セッター--------//
	void SetState( PLAYERSTATE setState )		{ m_state = setState; }
	void SetWarpState( WARPSTATE warpState )	{ m_warpState = warpState; }
	void SetGimmickManagerPointer( shared_ptr<GimmickManager> setGimmick )	{ m_gimmickManager = setGimmick; }
	void SetGimmickPointer( shared_ptr<BaseGimmick> gimmick )				{ m_gimmick = gimmick; }
	void SetCameraPointer(shared_ptr<Camera> camera)						{ m_camera = camera; }
	void SetHelpPointer( weak_ptr<HelpCharacter> help )						{ m_help = help; }
public: //---------ゲッター--------//
	PEOPLES	GetPeoples()		{ return m_peoples; }
	PLAYERSTATE GetPlayerState(){ return m_state; }
	WARPSTATE	GetWarpState()	{ return m_warpState; }
	Vector3 GetForwardVec()		{ return m_obj->GetForwardVec(); }
	Vector3 GetRightVec()		{ return m_obj->GetRightVec(); }
	Vector3 GetMove()			{ return m_move; }
};

class HelpCharacter :public BaseCharacter
{
private:
	weak_ptr<PlayerCharacter> m_player;	//プレイヤーのポインタ
	HELPSTATE m_state;					//ステート

private:
	void UpdateWait();
	void UpdateFollow();

	void StateUpdate()override;	//ステート更新
	void ObjUpdate()override;	//オブジェ関連の動きpublic:

private:
	enum MOTION : int
	{
		HELP = 1,				//助けて欲しい
		HOLDHANDSIDEL = 2,		//手をつないでの待機
		HOLDHANDSWALK = 3,		//手をつないでの歩き
	};
public:
	HelpCharacter()
	{
		m_player.lock() = nullptr;
		m_state = HELPSTATE::Wait;
		m_sphere.SetRadius( 10 );
	};

	void Initialize()override;
	void Update()override;
public: //---------セッター--------//
	void SetPlayerPointer( shared_ptr<PlayerCharacter> player )	{ m_player = player; }
	void SetState( HELPSTATE state )							{ m_state = state; }
public: //---------ゲッター--------//
	HELPSTATE GetState() { return m_state; }

};

#endif 