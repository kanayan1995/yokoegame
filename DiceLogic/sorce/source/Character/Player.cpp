#include "iextreme.h"
#include "iextremePlus.h"
#include "Utilty/Utility.h"

#include "../Gimmick/BaseGimmick.h"
#include "../Gimmick/Dice/BaseDice.h"
#include "../Gimmick/GimmickManager.h"
#include "Character.h"
#include "Input/Input.h"
#include "../Sound/Sound.h"
#include "../Fade/Fade.h"


//*****************************************************************************
//	初期化
//*****************************************************************************
void PlayerCharacter::Initialize()
{	
	char* fileName = "DATA/CHR/Player/player.IEM";
	m_obj = make_unique<iex3DObj>( fileName );
	m_scale = Vector3One * 5.0f;
	m_obj->SetScale( m_scale );
	BaseCharacter::Initialize();
}

//*****************************************************************************
//	Move関連
//*****************************************************************************
void PlayerCharacter::UpdateIdle()
{
	if		( m_peoples == PEOPLES::ONE )	SetMotion( MOTION::IDEL );
	else if (m_peoples == PEOPLES::TWO)		SetMotion( MOTION::HOLDHANDSIDEL );
	if ( GAMECONTROLER->AxisLeftStickX() != 0 ||
		GAMECONTROLER->AxisLeftStickY() != 0 )
	{
		m_state = PLAYERSTATE::Move;
	}

	ChangeStatePush();
	ChangeStateWorp();
}

void PlayerCharacter::UpdateAccsess()
{
	//if (help->getState() == PLAYERSTATEHelp::Wait) {
	//	Vector3 v = help->getPos() - pos;

	//	if (v.Length() < HELP_ACCESS_DISTANCE) {
	//		help->setFollow(this);
	//	}
	//}
	//else {
	//	help->setFollow(nullptr);
	//}

	//state = PLAYERSTATE::Move;
}

//*****************************************************************************
//	Move関連
//*****************************************************************************

//-----------------------------------------------------
//　コントローラーで操作
//	ここはあくまで移動量を取得するだけ
//	更新処理は別のところ
//-----------------------------------------------------
void PlayerCharacter::PlayerMove()
{
	m_forward = m_obj->GetForwardVec();
	m_move = Vector3Zero;

	//	カメラベクトル
	Vector3 cVec = m_pos - m_camera.lock()->GetPos();
	cVec.y = 0;

	//	移動
	Vector3 front( 0 , 0 , 1 );	//	ワールド座標の正面
	Vector3 move( GAMECONTROLER->AxisLeftStickX() , 0 , -GAMECONTROLER->AxisLeftStickY() );
	if ( move.LengthSq() > 1.0f )
	{
		move.Normalize();
	}

	if ( move.LengthSq() != 0 )
	{
		D3DXVECTOR3 dmove = ConvertD3DVec3( move );
		Vector3 nmove = move;
		nmove.Normalize();

		float angle = acosf( Vector3Dot( front , cVec ) / cVec.Length() );
		if ( cVec.x < 0 )
		{
			angle *= -1;
		}

		Matrix RMatY;	//	Y軸回転行列
		D3DXMatrixRotationY( &RMatY , angle );
		D3DXVec3TransformCoord( &dmove , &dmove , &RMatY );
		move = ConvertVec3( dmove );
		m_move = move;					//移動量入れる

		move.Normalize();
		angle = acosf( Vector3Dot( front , move ) );
		if ( move.x < 0 )
		{
			angle *= -1;
		}
		m_setAngle = angle;

		if ( m_peoples == PEOPLES::ONE )		SetMotion( MOTION::WALK );
		else if ( m_peoples == PEOPLES::TWO )	SetMotion( MOTION::HOLDHANDSWALK );
	}
}

//-----------------------------------------------------
//　転がる状態にする
//-----------------------------------------------------
void PlayerCharacter::ChangeStatePush()
{
	//CollisionCheackの中でギミックのポインタを入れてる
	//レイピックにあたっていたらギミックのポインタを入れる
	if ( m_gimmick.lock() == nullptr ) return;

	auto DicePush = [this]()
	{
		if ( GAMECONTROLER->SquareButtonPush() )
		{
			m_state = PLAYERSTATE::Push;
			m_gimmick.lock()->SetHold();
		}
	};

	if ( m_gimmick.lock()->GetGimmickType() == GIMMICKTYPE::ONEPERSONDICE
		&&m_peoples == PEOPLES::ONE )
	{
		DicePush();
	}
	else if ( m_gimmick.lock()->GetGimmickType() == GIMMICKTYPE::TWOPERSONDICE
		&& m_peoples == PEOPLES::TWO )
	{
		DicePush();
	}
}

//-----------------------------------------------------
//　ワープ状態にする
//-----------------------------------------------------
void PlayerCharacter::ChangeStateWorp()
{
	if ( m_gimmick.lock() == nullptr ) return;

	auto DiceWorp = [this]()
	{
		if ( GAMECONTROLER->CircleButtonPush() ) m_state = PLAYERSTATE::Worp;
	};

	if ( m_gimmick.lock()->GetGimmickType() == GIMMICKTYPE::ONEPERSONDICE
		&&m_peoples == PEOPLES::ONE )
	{
		DiceWorp();
	}
	else if ( m_gimmick.lock()->GetGimmickType() == GIMMICKTYPE::TWOPERSONDICE
		&& m_peoples == PEOPLES::TWO )
	{
		DiceWorp();
	}
}

//-----------------------------------------------------
//　ヘルプちゃんと接触している時に
//	ボタンを押して助ける
//	助けたときに人数を変わる
//-----------------------------------------------------
void PlayerCharacter::ToHelp()
{
	if ( Fade::GetFadeMode() == FADEMODE::MOVE ) return;		//フェード中は何もしない
	if ( m_help.lock() == nullptr ) return;
	if ( m_help.lock()->GetState() == HELPSTATE::Follow ) return;

	if ( Collision::CheackSphereToSphere( m_sphere , m_help.lock()->GetSphere() , m_help.lock()->GetSphere().GetRadius() ) == true )
	{
		if ( GAMECONTROLER->CircleButtonPush() )
		{
			m_help.lock()->SetState( HELPSTATE::Follow );
			m_peoples = PEOPLES::TWO;
		}
	}
}

//-----------------------------------------------------
//	Move更新
//-----------------------------------------------------
void PlayerCharacter::UpdateMove()
{
	if (  GAMECONTROLER->AxisLeftStickX() == 0 &&
		GAMECONTROLER->AxisLeftStickY() == 0)
	{
		m_state = PLAYERSTATE::Idle;
	}


	PlayerMove();
	Vector3 out;
	Vector3 workPos = m_pos;
	workPos += m_obj->GetForwardVec() * 4;
	if ( Collision::RayPickLocal( &out , workPos , Vector3Down , 4.0f ) == false )
	{
		m_move = Vector3Zero;
	}
	ChangeStatePush();
	ChangeStateWorp();
	if ( m_peoples == PEOPLES::ONE )
	{
		ToHelp();
	}
}

//*****************************************************************************
//	Push関連
//*****************************************************************************

//-----------------------------------------------------
//	向きを選択してボタンを押すことでその方向に転がる
//	解除もここでする
//	誰の向きからが重要になる
//　(＠_＠;)　横江 : ここいろいろとゴリゴリ過ぎてやばし 斜めからの時が特に
//	でもこだわった
//-----------------------------------------------------
void PlayerCharacter::DiceRollChoice()
{
	//カメラの向き取得
	Matrix mat = matView;
	Vector3 cameraForwardVec( -mat._31 , 0 , mat._33 );
	cameraForwardVec.Normalize();
	Vector3 cameraRightVec( mat._11 , 0 , -mat._13 );
	cameraRightVec.Normalize();
	Vector3 playerForDice = m_gimmick.lock()->GetPos() - m_pos;
	playerForDice.Normalize();
	playerForDice.y = 0;
	Vector3 rollPlayerAxis;

	//カメラの向きからどっちに動かすか決める
	auto WhereCamera = [](Vector3 playerForDice , Vector3 cameraVec )
	{
		if ( 0.80f < abs( Vector3Dot( playerForDice , cameraVec ) ) ) return true;
		else return false;
	};
	//前後ろ判定
	if ( WhereCamera( playerForDice , cameraForwardVec ) )
	{
		Vector3 cameraForward( -mat._31 , 0 , mat._33 );
		cameraForward.Normalize();
		cameraForward = DirCorrection( cameraForward );
		Vector3 rollForwardVec( cameraForward.z , 0 , -cameraForward.x );

		//縦
		if ( GAMECONTROLER->AxisLeftStickUpHold() )	   m_gimmick.lock()->SetRollChoice( cameraForward  , rollForwardVec );
		if ( GAMECONTROLER->AxisLeftStickDownHold() )  m_gimmick.lock()->SetRollChoice( -cameraForward , -rollForwardVec );

		playerForDice = DirCorrection( playerForDice );
		rollPlayerAxis = Vector3( playerForDice.z , 0 , -playerForDice.x );
		//(＠_＠;)　横江 : プレイヤーを左から見たときだけ調整
		if ( -0.05f >= Vector3Dot( -m_obj->GetRightVec() , cameraForwardVec ) )
		{
			playerForDice = -playerForDice;
			rollPlayerAxis = -rollPlayerAxis;
		}

		//横
		if ( GAMECONTROLER->AxisLeftStickRightHold() ) m_gimmick.lock()->SetRollChoice( playerForDice , rollPlayerAxis );
		if ( GAMECONTROLER->AxisLeftStickLeftHold() )  m_gimmick.lock()->SetRollChoice( -playerForDice , -rollPlayerAxis );
	}
	//横に動かせる
	else if ( WhereCamera( playerForDice , cameraRightVec ) )
	{
		Vector3 cameraRight( mat._11 , 0 , -mat._13 );
		cameraRight.Normalize();
		cameraRight = DirCorrection( cameraRight );
		Vector3 rollRightVec( cameraRight.z , 0 , -cameraRight.x );
		playerForDice = DirCorrection( playerForDice );
		rollPlayerAxis = Vector3( playerForDice.z , 0 , -playerForDice.x );

		//縦
		if ( GAMECONTROLER->AxisLeftStickUpHold() )		m_gimmick.lock()->SetRollChoice( playerForDice , rollPlayerAxis );
		if ( GAMECONTROLER->AxisLeftStickDownHold() )	m_gimmick.lock()->SetRollChoice( -playerForDice , -rollPlayerAxis );
		//横
		if ( GAMECONTROLER->AxisLeftStickRightHold() )	m_gimmick.lock()->SetRollChoice(  cameraRight , rollRightVec );
		if ( GAMECONTROLER->AxisLeftStickLeftHold() )	m_gimmick.lock()->SetRollChoice( -cameraRight , -rollRightVec );
	}

	//斜めになるとプレイヤーの向きを優先して回転さす
	else
	{
		//プレイヤーの向き優先で考えるからプレイヤーの向きを転がす方向に加える
		playerForDice = DirCorrection( playerForDice );
		Vector3 rollPlayerVec( playerForDice.z , 0 , -playerForDice.x );
		//カメラの向き取得
		Vector3 cameraForward( -mat._31 , 0 , mat._33 );
		cameraForward.Normalize();
		//横関係
		Vector3 rightMove = playerForDice;
		Vector3 rightRoll = rollPlayerVec;

		float abjustmentAngle = 0.25f;
		auto WherePlayerForGimmickVec = [](Vector3 axis , Vector3 playerForGimmick)	//プレイヤ-がどの向きを向いているかの判定
		{
			if ( 0.7f < Vector3Dot( axis , playerForGimmick ) )  return true;
			else return false;
		};
		//縦スティック関係
		//サイコロからをZ軸方面の見たとき
		if ( WherePlayerForGimmickVec(Vector3Forward , playerForDice) )
		{
			if ( abjustmentAngle < Vector3Dot( Vector3Back  , cameraForward ) )
			{
				playerForDice.z = -playerForDice.z;
				rollPlayerVec.x = -rollPlayerVec.x;
			}
			if ( abjustmentAngle < Vector3Dot( Vector3Right , cameraForward ) )
			{
				rightMove.z = -rightMove.z;
				rightRoll.x = -rightRoll.x;
			}
		}
		//サイコロをX軸方面にみたとき
		else if ( WherePlayerForGimmickVec(Vector3Right , playerForDice) )
		{
			if ( abjustmentAngle < Vector3Dot( Vector3Left , cameraForward ) )
			{
				playerForDice.x = -playerForDice.x;
				rollPlayerVec.z = -rollPlayerVec.z;
			}
			if ( abjustmentAngle < Vector3Dot( Vector3Back , cameraForward ) )
			{
				rightMove.x = -rightMove.x;
				rightRoll.z = -rightRoll.z;
			}
		}
		//サイコロを-Z軸方向に見たとき
		else if ( WherePlayerForGimmickVec(Vector3Back , playerForDice) )
		{
			if ( abjustmentAngle < Vector3Dot( Vector3Forward	, cameraForward ) )
			{
				playerForDice.z = -playerForDice.z;
				rollPlayerVec.x = -rollPlayerVec.x;
			}
			if ( abjustmentAngle < Vector3Dot( Vector3Left		, cameraForward ) )
			{
				rightMove.z = -rightMove.z;
				rightRoll.x = -rightRoll.x;
			}
		}
		//サイコロを-X軸方面に見たとき
		else if ( WherePlayerForGimmickVec(Vector3Left , playerForDice) )
		{
			if ( abjustmentAngle < Vector3Dot( Vector3Right		, cameraForward ) )
			{
				playerForDice.x = -playerForDice.x;
				rollPlayerVec.z = -rollPlayerVec.z;
			}
			if ( abjustmentAngle < Vector3Dot( Vector3Forward	, cameraForward ) )
			{
				rightMove.x = -rightMove.x;
				rightRoll.z = -rightRoll.z;
			}
		}

		if ( GAMECONTROLER->AxisLeftStickUpHold() )	   m_gimmick.lock()->SetRollChoice(  playerForDice ,  rollPlayerVec );
		if ( GAMECONTROLER->AxisLeftStickDownHold() )  m_gimmick.lock()->SetRollChoice( -playerForDice , -rollPlayerVec );
		if ( GAMECONTROLER->AxisLeftStickRightHold() ) m_gimmick.lock()->SetRollChoice(  rightMove ,  rightRoll );
		if ( GAMECONTROLER->AxisLeftStickLeftHold() )  m_gimmick.lock()->SetRollChoice( -rightMove , -rightRoll );
	}	

	if ( GAMECONTROLER->CrossButtonPush() )
	{
		m_gimmick.lock()->SetRollReset();
		m_state = PLAYERSTATE::Move;
	}
}

//-----------------------------------------------------
//	向きをサイコロの向きに補正する
//-----------------------------------------------------
void PlayerCharacter::PlayerAngleCorrection()
{
	Vector3 playerForDice = m_gimmick.lock()->GetPos() - m_pos;
	playerForDice.Normalize();
	float cheackAngle = 0.6f;
	float correctionAngle;			//補正する向きを入れる

	if		( cheackAngle < Vector3Dot( playerForDice , Vector3Forward ) )	correctionAngle = 0;
	else if ( cheackAngle < Vector3Dot( playerForDice , Vector3Back ) )		correctionAngle = PI;
	else if ( cheackAngle < Vector3Dot( playerForDice , Vector3Right ) )	correctionAngle = HALF_PI;
	else if ( cheackAngle < Vector3Dot( playerForDice , Vector3Left ) )		correctionAngle = -HALF_PI;
	m_angle = correctionAngle;
	m_setAngle = correctionAngle;
}

//-----------------------------------------------------
//	Pushまとめ
//-----------------------------------------------------
void PlayerCharacter::UpdatePush()
{
	//まだ何も触れていなかったときに使う
	if ( m_gimmick.lock() == nullptr )
	{
		m_state = PLAYERSTATE::Move;
		return;
	}


	switch ( m_gimmick.lock()->GetDiceHoldState() )
	{
		case HOLDSTATE::NONE:
			m_state = PLAYERSTATE::Idle;
		case HOLDSTATE::SETROLL:
			break;
		case HOLDSTATE::ROLLCHICE:
			PlayerAngleCorrection();
			DiceRollChoice();
			SetMotion( MOTION::ROLLCHOICE );
			m_pos = m_gimmick.lock()->GetHoldPlayerPos();
			break;
		case HOLDSTATE::ROLL:
			m_pos = m_gimmick.lock()->GetHoldPlayerPos();
			SetMotion( MOTION::ROLLWALK );
			break;
		default:
			break;
	}
}

//*****************************************************************************
//	Worp関連
//*****************************************************************************
//-----------------------------------------------------
//	ワープ先を探す
//	なかったらStateをMoveに戻す
//-----------------------------------------------------
void PlayerCharacter::WorpCheack()
{
	if ( m_gimmickManager.lock()->GetIsWorpFind() == false )
	{
		m_state = PLAYERSTATE::Move;
	}
}

//-----------------------------------------------------
//	ワープ先を選択する
//	処理順番（更新の）を間違えないように
//-----------------------------------------------------
void PlayerCharacter::WarpChoice()
{
	SetMotion( MOTION::WARPOCHOICE );
	//コントローラーで場所選択
	if ( GAMECONTROLER->AxisLeftStickRightPush() )	m_gimmickManager.lock()->WorpPosNumMinus();
	if ( GAMECONTROLER->AxisLeftStickLeftPush() )	m_gimmickManager.lock()->WorpPosNumPlus();

	//場所決定
	if ( GAMECONTROLER->CircleButtonPush() )
	{
		Sound::PlaySE( Sound::SE::WARP );
		m_state = PLAYERSTATE::Move;
		m_warpState = WARPSTATE::WARPCHEACK;
		Vector3 worpPos;
		m_pos = m_gimmickManager.lock()->GetWorpPos();
	}
	//場所選択リセット
	if ( GAMECONTROLER->CrossButtonPush() )
	{
		m_gimmickManager.lock()->EraseFacePos();
		m_state = PLAYERSTATE::Move;
		m_warpState = WARPSTATE::WARPCHEACK;
	}
}

//-----------------------------------------------------
//	ワープ先を選択する
//	更新順番はPlayer先でGimmickManagerはあと
//-----------------------------------------------------
void PlayerCharacter::UpdateWorp()
{

	switch ( m_warpState )
	{
		case WARPSTATE::WARPCHEACK:
			//ここのStateを次にかえるのはGimmickManagerのCheackFacePosの中で判断
			//WorpCheack();
			break;
		case WARPSTATE::WARPCHOICE:
			WarpChoice();
			break;
		default:
			break;
	}
	//ワープ出来るかのチェック
}

//*****************************************************************************
//	Update (ずっと更新してたほうがよい)
//*****************************************************************************

//-----------------------------------------------------
//　State更新
//-----------------------------------------------------
void PlayerCharacter::StateUpdate()
{	switch ( m_state )
	{
		case PLAYERSTATE::Idle:
			UpdateIdle();
			break;
		case PLAYERSTATE::Move:
			UpdateMove();
			break;
		case PLAYERSTATE::Push:
			UpdatePush();
			break;
		case PLAYERSTATE::Accsess:
			UpdateAccsess();
			break;
		case PLAYERSTATE::Worp:
			UpdateWorp();
			break;
		default:
			break;
	}
}

//-----------------------------------------------------
//　下にレイ撃って地面判定
//-----------------------------------------------------
void PlayerCharacter::GroundCheack()
{
	Vector3 out;
	float dist = 5.0f;
	m_pos.y -= 0.5f;
	//下にむかってレイをうって地面判定をもつ	(＠_＠;) : 横江 落ちるスピードとレイピックの長さは要調整
	Collision::RayPickLocal( &out , m_pos , Vector3Down , 2.0f );
	m_pos = out;
}

//-----------------------------------------------------
// AABBでのかべずり
//-----------------------------------------------------
void PlayerCharacter::AABBShaer()
{
	multimap< StageObjType , AABB > aabbMap = Collision::GetColisionAABBMap();
	m_move *= 0.5f;
	Vector3 out = m_move;
	Vector3 moveNormal = DirCorrection( m_move*1.5f );
	for ( auto it = begin( aabbMap ); it != end( aabbMap ); it++ )
	{
		out = Collision::AABBShear( out , moveNormal , m_aabb , it->second );
	}
	//常に更新しないといけない奴は分ける
	multimap<GIMMICKTYPE , shared_ptr<BaseGimmick>> aabbGimmickMap = m_gimmickManager.lock()->GetGimmickMap();
	for ( auto it = begin( aabbGimmickMap ); it != end( aabbGimmickMap ); it++ )
	{
		out = Collision::AABBShear( out , moveNormal , m_aabb , it->second->GetAABB() );
	}

	m_pos += out;
}

//-----------------------------------------------------
// 一人の時は正面に撃つだけ
//-----------------------------------------------------
void PlayerCharacter::OnePersonMove()
{
	//壁ずりレイピック
	Vector3 out;
	float dist = 6.0f;
	m_move *= 0.5f;
	Collision::WallShear( &out , m_pos , m_move , &dist );
	m_pos += out;
}

//-----------------------------------------------------
// 二人になったときにレイを撃つのを変える
// プレイヤーとヘルプちゃん両方からレイを撃つ
// 処理内容は考えるべき
// (＠_＠;) : 横江 今のところがばがばゴミコード
//-----------------------------------------------------
void PlayerCharacter::TwoPersonMove()
{
	Vector3 rayPos;
	Vector3 playerForward = DirCorrection( m_obj->GetForwardVec() );
	//rayPos = m_pos;
	rayPos.x += playerForward.z * 5.0f;
	rayPos.z += playerForward.x * 5.0f;
	Vector3 helpPos = m_help.lock()->GetPos();
	Vector3 playerPos = m_twoPeoplePos - m_obj->GetForwardVec() * 5.0f;

	int cheak = 0;
	Vector3 out;		//壁ずりの値が返ってくる
	float dist = 8.0f;	//レイを撃つ距離
	m_move *= 0.5f;
	if ( Collision::WallShear( &out , playerPos , m_move , &dist ) == true )
	{
		m_move = out;
		cheak++;
	}
	m_move *= 0.85f;
	if ( Collision::WallShear( &out , helpPos , m_move , &dist ) == true )
	{
		m_move = out;
		cheak++;
	}	

	if ( cheak == 2 )
	{

	}
	m_pos += m_move;
}

//-----------------------------------------------------
//　前にレイを撃って壁判定チェック
//	壁ずり判定
//-----------------------------------------------------
void PlayerCharacter::ForwardCheack()
{
	switch ( m_peoples )
	{
		case PEOPLES::ONE:
			//OnePersonMove();
			AABBShaer();
			break;
		case PEOPLES::TWO:
			//TwoPersonMove();
			AABBShaer();
			break;
		default:
			break;
	}
}

//-----------------------------------------------------
//　オブジェ関連の更新
//	当たり判定メッシュやらいろいろ
//-----------------------------------------------------
void PlayerCharacter::ObjUpdate()
{	
	if ( m_peoples == PEOPLES::ONE )  //一人の時はposの中心
	{
		m_obj->SetPos( m_pos );
		//m_aabb.SetMinAndMax( Vector3One * 3.5 );

		//細い道抜けられないため変更、もし不都合あったら山中まで連絡ください
		m_aabb.SetMinAndMax( Vector3(3.5f,5.0f,3.5f) );
	}
	else if ( m_peoples == PEOPLES::TWO )					//二人の時は円を中心にいる
	{
		Vector3 right	= m_obj->GetRightVec();
		float workAngle	= m_angle - HALF_PI;
		m_twoPeoplePos	= m_pos + Vector3( sinf( workAngle ) * TWO_PERSON_DIST , 0 , cosf( workAngle ) * TWO_PERSON_DIST );
		m_obj->SetPos( m_twoPeoplePos );
		m_aabb.SetMinAndMax( Vector3( 6.5f , 5.0f , 6.5f ) );
	}			
	if		( m_angle > 2 * PI ) m_angle -= 2 * PI;
	else if ( m_angle < -2 * PI )m_angle += 2 * PI;

	m_obj->SetAngle( m_angle );
	m_obj->Animation();
	m_obj->Update();

	m_sphere.SetPos(  m_pos + m_spherePosUp );
	m_aabb.UpdatePos( m_pos + m_aabbPosUp );
}

//-----------------------------------------------------
//　移動するときに
//-----------------------------------------------------
void PlayerCharacter::MoveObjUpdate()
{
	//向き補正
	float correctionAngle = m_setAngle - m_angle;
	if ( fabsf( correctionAngle ) > PI )
	{
		if ( correctionAngle > 0 )	correctionAngle = -(PI * 2 - correctionAngle);
		else						correctionAngle =  (PI * 2 + correctionAngle);
	}
	if ( fabsf( correctionAngle ) > FLT_EPSILON )
	{
		m_angle += correctionAngle * 0.1f;
	}
	ObjUpdate();
}

//-----------------------------------------------------
//　更新処理
//-----------------------------------------------------
void PlayerCharacter::Update()
{	
	StateUpdate();
	GroundCheack();
	ForwardCheack();

	switch ( m_state )
	{
		case PLAYERSTATE::Idle:	ObjUpdate();	break;
		case PLAYERSTATE::Move:	MoveObjUpdate();break;
		case PLAYERSTATE::Push:	ObjUpdate();	break;
		case PLAYERSTATE::Accsess:				break;
		case PLAYERSTATE::Worp:	ObjUpdate();	break;
		default:
			break;
	}
}

//*****************************************************************************
//	描画
//*****************************************************************************
void PlayerCharacter::Render()
{
	BaseCharacter::Render();


	//Sphere test;
	//test.SetPos( m_pos + m_obj->GetForwardVec()*4 );
	//test.SetRadius( 2 );
	//test.TestRender();

	//char str[ 64 ];	
	//sprintf( str , "%9.3f\n" , m_move.x );
	//IEX_DrawText( str , 0 , 20 , 40 , 40 , 0xFFFF0000 );	
	//sprintf( str , "%9.3f\n" , m_move.y );
	//IEX_DrawText( str , 0 , 20*2 , 40 , 40 , 0xFFFF0000 );
	//sprintf( str , "%9.3f\n" , m_move.z);
	//IEX_DrawText( str , 0 , 20*3 , 40 , 40 , 0xFFFF0000 );
}

void PlayerCharacter::RenderShadow()
{
	BaseCharacter::RenderShadow();
}