#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "Utilty/Utility.h"

#include "../Gimmick/GimmickManager.h"
#include "Character.h"

//*****************************************************************************
//	定数
//*****************************************************************************
const float BaseCharacter::TWO_PERSON_DIST = 3.5f;

//*****************************************************************************
//	初期化
//*****************************************************************************
void BaseCharacter::Initialize()
{
	//あたり判定周りの微調整
	m_sphere.SetRadius( 10 );
	m_spherePosUp = Vector3( 0 , 7 , 0 );
	m_aabbPosUp = Vector3( 0 , 5 , 0 );

	//テスト
	char* fileName = "DATA/Test/fontA.png";
	m_font = make_unique<GraphicFont>( fileName );
	m_font->setSize( 32 , 32 );
	m_font->setScale( 1.5f );
	m_font->setMargin( -12 );
}

//*****************************************************************************
//	更新
//*****************************************************************************
void BaseCharacter::Update()
{

}

//*****************************************************************************
//	描画
//*****************************************************************************
void BaseCharacter::Render()
{
	myshader->SetValue( "OutlineSize" , 0.05f );
	m_obj->Render( myshader , "toon" );

	//(＠_＠;) : 横江　消しとかないと
	//m_aabb.TestRender( m_pos + m_aabbPosUp );
	//m_sphere.TestRender();
}

void BaseCharacter::RenderShadow()
{
	m_obj->Render(myshader, "ShadowBuf");
}

void BaseCharacter::RenderDepth()
{
	shader->SetValue( "OutlineSize" , 0.05f );
	m_obj->Render( shader , "makeDepth" );
	//m_aabb.TestRender( m_pos + m_aabbPosUp );
}

//*****************************************************************************
//	Set関連
//*****************************************************************************
//-----------------------------------------------------
//	モーションをセットする
//	あとで列挙型を宣言してつけたらわかりやすい
//	enumを継承できたらわかりやすくなるんやけどね
//-----------------------------------------------------
void BaseCharacter::SetMotion( int motion )
{
	if ( m_obj->GetMotion() == motion ) return;
	m_obj->SetMotion( motion );
}

//-----------------------------------------------------
//	位置情報を個別でも入れれる
//-----------------------------------------------------
void BaseCharacter::SetPos(float x, float y, float z)
{
	m_pos = Vector3(x, y, z);
	m_obj->SetPos(m_pos);
}



