#include "iextreme.h"
#include "iextremePlus.h"
#include "Utilty/Utility.h"

#include "Character.h"

void HelpCharacter::Initialize()
{
	char* fileName = "DATA/CHR/Helpchan/Help.IEM";
	m_obj = make_unique<iex3DObj>( fileName );
	m_scale = Vector3One*4.0f;
	m_obj->SetScale( m_scale );
	BaseCharacter::Initialize();
}

void HelpCharacter::UpdateWait()
{
	SetMotion( HELP );
}

void HelpCharacter::UpdateFollow()
{	
	if		( m_player.lock()->GetPlayerState() == PLAYERSTATE::Idle ) SetMotion( HOLDHANDSIDEL );
	else if	( m_player.lock()->GetPlayerState() == PLAYERSTATE::Move ) SetMotion( HOLDHANDSWALK );

	Vector3 playerPos = m_player.lock()->GetPos();
	Vector3 playerRight;
	Vector3Cross( playerRight , Vector3Up , m_player.lock()->GetForwardVec() );
	playerRight.Normalize();
	Vector3 playerBackPos = -m_player.lock()->GetForwardVec() * 3.0f;
	m_pos = playerPos + playerRight * TWO_PERSON_DIST + playerBackPos;
	m_angle = m_player.lock()->GetAngle();
}

void HelpCharacter::StateUpdate()
{	
	switch ( m_state )
	{
		case HELPSTATE::Wait:
			UpdateWait();
			break;
		case HELPSTATE::Follow:
			UpdateFollow();
			break;
	}
}

void HelpCharacter::ObjUpdate()
{
	m_obj->SetPos( m_pos );
	m_obj->SetAngle( m_angle );
	m_obj->Animation();
	m_obj->Update();
	m_sphere.SetPos( m_pos + m_spherePosUp );
}

void HelpCharacter::Update()
{
	StateUpdate();
	ObjUpdate();
}
