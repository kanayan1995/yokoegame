#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../System/Framework.h"
#include "../Utilty/Utility.h"

#include "../Gimmick/GimmickManager.h"

#include "../Collision/Collision.h"
#include "../Collision/CollisionCheack.h"

#include "../Camera/Camera.h"

#include "../Character/Character.h"

#include "Graphics/Graphics.h"
#include "Input/Input.h"

#include "../Sound/Sound.h"
#include "../GameMode/GameMode.h"
#include "../Fade/Fade.h"

#include "StageSelect.h"
#include "sceneMain.h"
//*****************************************************************************************************************************
//
//	グローバル変数
//
//*****************************************************************************************************************************


#define SHADOW_SIZE 2048

//*****************************************************************************************************************************
//
//	初期化
//
//*****************************************************************************************************************************

bool sceneMain::Initialize()
{
	//　シャドウマップ
	m_shadow = make_unique<ShadowMap>(SHADOW_SIZE);

	//	半球ライティング設定
	myshader->SetValue("SkyColor", Vector3(0.3f, 0.3f, 0.3f));
	myshader->SetValue("GroundColor", Vector3(0.2f, 0.2f, 0.2f));

	iexSystem::Device->CreateDepthStencilSurface(SHADOW_SIZE, SHADOW_SIZE, D3DFMT_D16,
		D3DMULTISAMPLE_NONE, 0, FALSE, &ShadowZ, NULL);
	//	フレームバッファのポインタ保存
	iexSystem::GetDevice()->GetRenderTarget(0, &pBackBuffer);
	//	一時レンダリング用サーフェイス
	m_screen	= make_unique<iex2DObj>( 1280 , 720 , IEX2D_RENDERTARGET );
	m_depthTex		= make_unique<iex2DObj>( 1280 , 720 , IEX2D_RENDERTARGET );

	char* fileName;

	//	環境光
	iexLight::SetFog(800, 1000, 0);

	//　ライトの方向
	dir = Vector3(0.05f, -0.1f, 0.05f);
	myshader->SetValue( "DirLightVec" , dir );
	myshader->SetValue("ToonLevel", 0.4f);
	shader->SetValue( "DirLightVec" , dir );
	shader->SetValue( "ToonLevel" , 0.4f );

	//　ライトの明るさ				　方向　　赤　　 緑　　 青
	//	iexLight::DirLight(myshader , 1 , &dir , 0.5f , 0.5f , 0.25f );
	iexLight::DirLight(myshader, 1, &dir, 0.85f, 0.85f, 0.4f);
	iexLight::SetAmbient(0xFFFFFFFF);

	//メッシュテクスチャも含む読み込み
	OBJSTORAGE->Initialize();
	//Collision初期化
	Collision::Initialize();

	//	ステージ
	fileName = "DATA/Stage/Model/Floor.x";
	ground = make_shared<iexMesh>(fileName);
	ground->SetPos(0, 0, 10);
	ground->SetScale(40);

	//ギミック関係
	m_gimmickManager = make_shared<GimmickManager>();
	m_gimmickManager->Initialize();

	//	ステージ
	stage = make_unique<Stage>();
	//プレイヤーとヘルプちゃん用のオブジェクト（描画はしない）
	stage->SetIndex(0, "DATA/Stage/Model/box.x");
	stage->SetIndex(1, "DATA/Stage/Model/big_box.x");

	/*以下壁やサイコロのオブジェクトの設定*/
	stage->SetIndex(2, "DATA/Stage/Model/box.x");
	stage->SetIndex(3, "DATA/Stage/Model/big_box.x");
	stage->SetIndex(4, "DATA/Stage/Model/Cube.x");
	stage->SetIndex(5, "DATA/Stage/Model/WideWall_X.x");
	stage->SetIndex(6, "DATA/Stage/Model/WideWall_Z.x");
	stage->SetIndex(7, "DATA/Stage/Model/grid.x");
	stage->SetIndex(8, "DATA/Stage/Model/WallLong_Y.x");
	stage->SetIndex(9, "DATA/Stage/Model/DownHill_Left.x");
	stage->SetIndex(10, "DATA/Stage/Model/DownHill_Wall.x");
	stage->StageDataLoad(stageName, m_gimmickManager);

	//プレイヤー
	m_player = make_shared<PlayerCharacter>();
	m_player->Initialize();
	Vector3 p_pos = stage->GetPos_Chara( StageObjType::PLAYER );
	p_pos += Vector3( 0 , 0 , 0 );
	m_player->SetPos(p_pos.x, p_pos.y, p_pos.z);

	m_help = make_shared<HelpCharacter>();
	m_help->Initialize();

	m_help->SetAngle(PI);
	//m_help->setPos(52,0,52);
	Vector3 h_pos = stage->GetPos_Chara( StageObjType::HELPWOMAN );
	m_help->SetPos(h_pos.x, h_pos.y, h_pos.z);

	//カメラ設定
	m_camera = make_shared<Camera>();
	m_camera->SetGimmickManagerPointer(m_gimmickManager);
	m_camera->Initialize();

	//ゴール設定
	m_goal = make_shared<Goal>();
	m_goal->Initialize();
	m_goal->SetPos( stage->GetPos_Chara( StageObjType::PLAYER ) );


	//あたり判定用クラス
	m_collisionCheack = make_unique<CollisionCheack>();
	Collision::SetCollisionMesh( StageObjType::GRID , ground );
	multimap<GIMMICKTYPE , shared_ptr<BaseGimmick>> gimmiciList = m_gimmickManager->GetGimmickMap();
	for ( auto it = begin( gimmiciList ); it != end( gimmiciList ); it++ )
	{
		Collision::SetCollisionMesh( it->second->GetObjType() , it->second->GetCollisionMesh() );
	}


	//UIを表示するクラス
	m_uiManager = make_unique<UIManager>();
	m_uiManager->Initialize();

	//数字
	number = new GraphicNumbers("DATA/Test/Number.png");
	number->setSize(128, 128);
	number->setScale(0.5f);
	number->setMargin(-5);
	//number->setDigit(3);
	number->m_value = 123456;

	//フォント
	font = new GraphicFont("DATA/Test/fontA.png");
	font->setSize(32, 32);
	font->setScale(1.5f);
	font->setMargin(-12);

	//音再生
	Sound::PlayBGM( Sound::BGM::MAIN );

	//状態を管理するクラス
	m_gameMode = make_shared<GameMode>();
	m_gameMode->Initialize();

	//フェードインする
	Fade::FadeOut( m_fadeTime );
	m_isRetry	= false;
	m_isSelect	= false;

	SetPointer();
	MainUpdate();

	return true;
}

//*****************************************************************************************************************************
//
//		解放
//
//*****************************************************************************************************************************
void sceneMain::Release()
{
	SafeDelete( &number );
	SafeDelete( &font );
	OBJSTORAGE->Release();
	ShadowZ->Release();
	pBackBuffer->Release();
	Sound::StopBGM( Sound::BGM::MAIN );
	Sound::StopBGM( Sound::BGM::RESULT );
	Collision::Release();
}


//-----------------------------------------------------
//	ポインタをまとめてセットする
//-----------------------------------------------------
void sceneMain::SetPointer()
{
	m_camera->SetPlayerPointer( m_player );
	m_camera->SetGimmickManagerPointer( m_gimmickManager );
	m_gimmickManager->SetPlayerPointer( m_player );
	m_uiManager->SetCameraPointer( m_camera );	
	m_uiManager->SetGameModePointer( m_gameMode );
	m_uiManager->SetGimmickManagerPointer( m_gimmickManager );
	m_player->SetHelpPointer( m_help );
	m_player->SetCameraPointer( m_camera );
	m_player->SetGimmickManagerPointer( m_gimmickManager );
	m_help->SetPlayerPointer( m_player );
	m_goal->SetPlayerPointer( m_player );
	m_gameMode->SetPlayerPointer( m_player );
	m_gameMode->SetGoalPointer( m_goal );
	m_gameMode->SetCameraPointer( m_camera );
}

//*****************************************************************************************************************************
//
//		更新
//
//*****************************************************************************************************************************

//-----------------------------------------------------
//	メインでの更新
//	プレイヤーとかの座標が更新された情報が欲しいから
//	ギミックとかの後で更新しないいけない
//-----------------------------------------------------
void sceneMain::MainUpdate()
{
	m_player->Update();
	m_help->Update();
	m_gimmickManager->SetPlayerParameters( m_player );
	m_gimmickManager->Update();

	stage->Update();
	ground->Update();
	m_goal->Update();

	m_collisionCheack->Update( m_player , m_gimmickManager );
	m_camera->Update();
	m_gameMode->Update();
}

//-----------------------------------------------------
//	メニュー画面
//	まだ作っていない
//-----------------------------------------------------
void sceneMain::MenuUpdate()
{
	m_gameMode->Update();
}

//-----------------------------------------------------
//	カメラで見渡したりするState
//-----------------------------------------------------
void sceneMain::CameraUpdate()
{
	m_camera->Update();
	m_gameMode->Update();
}

//-----------------------------------------------------
//	クリア状態
//	次のシーンに移動するクラスとかを更新かな
//-----------------------------------------------------
void sceneMain::ClearUpdate()
{
	m_gameMode->Update();
}

//-----------------------------------------------------
//	やり直しをする時の更新
//-----------------------------------------------------
void sceneMain::ResetUpdate()
{
	m_gameMode->Update();
}

//-----------------------------------------------------
//　やり直しとかのためにとりあえずつけてる
//-----------------------------------------------------
void sceneMain::SceneReset()
{
	if ( Fade::GetFadeMode() == FADEMODE::MOVE ) return;	//フェード中は動かさない

	//リトライをする
	if ( GAMECONTROLER->ShareButtonPush() &&
		 Fade::GetFadeMode() == FADEMODE::STOP )
	{
		m_isRetry = true;
		Fade::FadeIn( m_fadeTime , 255 , 255 , 255 );
	}
	if ( Fade::isFadeInFinish() == true &&
		 m_isRetry == true )
	{
		MainFrame->ChangeScene( new StageSelect() );
		return;
	}

	//select画面に戻る
	if ( GAMECONTROLER->OptionsButtonPush() &&
		 Fade::GetFadeMode() == FADEMODE::STOP )
	{
		m_isSelect = true;
		Fade::FadeIn( m_fadeTime , 255 , 255 , 255 );
	}
	if ( Fade::isFadeInFinish() == true &&
		 m_isSelect == true )
	{
		this->Release();
		this->Initialize();
		return;
	}
}

//-----------------------------------------------------
//	まとめて更新
//	見やすくなった
//-----------------------------------------------------
void sceneMain::Update()
{
	switch ( m_gameMode->GetGameState() )
	{
		case GAMESTATE::MAIN:	MainUpdate();	break;
		case GAMESTATE::MENU:					break;
		case GAMESTATE::CAMERA:	CameraUpdate();	break;
		case GAMESTATE::CLEAR:	ClearUpdate();	break;
		case GAMESTATE::RESET:					break;
		default:
			assert( !"なんか意味わからんステートに来てるよ　何も更新しないよ" );
			break;
	}
	SceneReset();
}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************

void	sceneMain::Render()
{
	//　シャドウマップに書き込むモデル(影を描画するモデル)
	m_shadow->RenderShadowBuff(300, 300, 1, 400.0f, dir, m_player->getObj());
	m_player->RenderShadow();
	m_help->RenderShadow();
	m_gimmickManager->RenderShadow();
	stage->RenderShadow();
	//　上で設定したモデルをシャドウマップに描画(ステージに設定したモデルの影を描画する)
	m_shadow->RestoreShadowBuff();
	
	//	ビュー設定
	m_camera->SetViewport();
	m_camera->SetProjection( 0.8f, 0.1f, 300.0f );
	m_camera->Activate();
	m_camera->ClearZ( 0x505050 , 1 );
	myshader->SetValue( "matView" , matView );

	m_screen->RenderTarget();
	m_depthTex->RenderTarget( 1 );
	m_camera->Clear();

	//深度バッファ作成
	m_help->RenderDepth();
	m_player->RenderDepth();
	m_gimmickManager->RenderDepth();

	iexSystem::Device->SetRenderTarget( 0 , NULL );
	iexSystem::Device->SetRenderTarget( 1 , NULL );
	//	フレームバッファへ切り替え
	iexSystem::GetDevice()->SetRenderTarget( 0 , pBackBuffer );
	m_camera->ClearZ( 0xFF000000 , false );
	//テクスチャセット
	shader->SetValue( "DepthBuf" , m_depthTex.get() );

	iexSystem::Device->SetRenderState( D3DRS_ZWRITEENABLE , FALSE );
	m_screen->Render( 0 , 0 , 1280 , 720 , 0 , 0 , 1280 , 720 , shader2D , "copy" , 0xFFFFFFFF , 0.0f );
	iexSystem::Device->SetRenderState( D3DRS_ZWRITEENABLE , TRUE );

	//シェーダーをかけるためもう一度描画
	m_player->Render();
	m_gimmickManager->Render();
	m_help->Render();
	//地面
	ground->Render( myshader , "shadow" );
	//ステージ
	stage->Render();
	//ゴール関連
	m_goal->Render();
	//UI関連
	m_uiManager->Render();
	
	//テスト描画
	//Collision::Render();
	// 保存したデフューズのテクスチャ描画
	//m_screen->Render( 0 , 0 , 320 , 180 , 0 , 0 , 1280 , 720 , shader2D , "copy" );
	//m_depthTex->Render( 1280 - 320 , 0 , 320 , 180 , 0 , 0 , 1280 , 720 , shader2D , "copy" );

	//フェード
	Fade::Render();

	{
		//テスト用にシャドーマップ描画　
		//m_shadow->DebugRenderShadowBuff(0, 0, 500, 500);

		//int i = 0;
		//multimap<GIMMICKTYPE , shared_ptr<BaseGimmick>> gimmickList = m_gimmickManager->GetGimmickList();
		//for ( auto it = begin( gimmickList ); it != end( gimmickList ); it++ )
		//{
		//	i++;
		//	char str[ 64 ];	
		//	sprintf( str , "%9.3f\n" , it->second->GetQuaternion().x );
		//	IEX_DrawText( str , 0 , 20 * i , 40 , 40 , 0xFFFFFFFF );	
		//	sprintf( str , "%9.3f\n" , it->second->GetQuaternion().y );
		//	IEX_DrawText( str , 0 , 60 * i , 40 , 40 , 0xFFFFFFFF );		
		//	sprintf( str , "%9.3f\n" , it->second->GetQuaternion().z );
		//	IEX_DrawText( str , 0 , 100 * i , 40 , 40 , 0xFFFFFFFF );
		//	sprintf( str , "%9.3f\n" , it->second->GetQuaternion().w);
		//	IEX_DrawText( str , 0 , 140 * i , 40 , 40 , 0xFFFFFFFF );
		//	return;
		//}
	}
}




