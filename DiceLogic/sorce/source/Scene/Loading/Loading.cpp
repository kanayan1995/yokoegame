#include "iextreme.h"
#include "system/system.h"
#include"system/Framework.h"

#include "Loading.h"
#include <process.h>

bool Loading::m_loadEnd = false;

void Loading::LoadScene( void* next )
{
	m_loadEnd = true;
	Scene* scene = (Scene*)next;
	scene->Initialize();
	_endthread();
}

//*****************************************************************************
//	初期化
//*****************************************************************************
//外部で生成したSceneを入れる
bool Loading::Initialize( Scene* changeScene )
{
	iexLight::SetAmbient( 0x808080 );
	iexLight::SetFog( 800 , 1000 , 0 );

	fireG = new iex2DObj( "DATA\\FireG.png" );
	fireCount = 0;

	m_nextScene = changeScene;
	//別スレッド作成
	_beginthread( LoadScene , 0 , (void*)m_nextScene );
	return true;
}

//*****************************************************************************
//	更新
//*****************************************************************************
void Loading::Update()
{
	if ( m_loadEnd == true )
	{
		MainFrame->ChangeScene( m_nextScene );
		return;
	}
}

//*****************************************************************************
//	描画
//*****************************************************************************
void Loading::Render()
{
	IEX_DrawRect( 0 , 0 , 1280 , 720 , RS_COPY , 0xFF000000 );
	fireG->Render( 1100 , 460 , 128 , 256 , (fireCount / FireSpeed) * 128 , 0 , 128 , 256 );
}
