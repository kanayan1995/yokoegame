//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef _LOADING_H
#define _LOADING_H

class Loading : public Scene
{
private:
	static bool m_loadEnd;
	Scene *m_nextScene;	//次のシーンに行く
	static void LoadScene( void* next );

	int			fireCount;
	iex2DObj*	fireG;
	const static int FireSpeed = 2;
public:
	Loading()
	{

	}
	~Loading()
	{
		//炎のアニメーションで確保したデータをデリート
	delete fireG;
		if ( !m_loadEnd ) delete m_nextScene;
	}
	//ロードが終わったら次に進む


	bool Initialize(Scene* changeScene);	//初期化
	void Update();		//更新
	void Render();		//描画
};

#endif 