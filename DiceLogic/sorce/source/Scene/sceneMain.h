//*****************************************************************************************************************************
//
//		メインシーン
//
//*****************************************************************************************************************************

#ifndef __SCENEMAIN_H__
#define __SCENEMAIN_H__

#include "../Collision/Sphere.h"
#include "../Collision/CollisionCheack.h"
#include "../Stage/Stage.h"
#include "../UI/UIManager.h"
#include "../Goal/Goal.h"
#include "../GameMode/GameMode.h"
#include "../ShadowMap/ShadowMap.h"

#include "../ObjStorage/ObjStorage.h"

class Camera;
class GimmickManager;
class sceneMain : public Scene
{
private:
	//shared_ptr<iexMesh>		stage;					//ステージ
	shared_ptr<Stage>			stage;
	shared_ptr<iexMesh>			ground;					//ステージ
	shared_ptr<Camera>			m_camera;				//カメラ
	shared_ptr<GimmickManager>	m_gimmickManager;		//ギミックを設置したりするやつ
	shared_ptr<PlayerCharacter> m_player;				//プレイヤー
	shared_ptr<HelpCharacter>	m_help;					//ヘルプちゃん
	unique_ptr<ShadowMap>		m_shadow;				//シャドウマップ
	unique_ptr<iex2DObj>		m_screen;			//一時レンダリング用
	unique_ptr<iex2DObj>		m_depthTex;				//深度用テクスチャ

	shared_ptr<Goal>			m_goal;					//ゴール設置
	unique_ptr<CollisionCheack> m_collisionCheack;		//あたり判定用クラス
	unique_ptr<UIManager>		m_uiManager;			//UIを表示するクラス
	shared_ptr<GameMode>		m_gameMode;				//メインの状態を管理するクラス

	GraphicNumbers *number;
	GraphicFont *font;

	Surface* pBackBuffer;
	Surface* ShadowZ;

	Vector3 dir;

	char* stageName;

	float m_fadeTime;
	bool m_isRetry;			//trueならリトライに戻る
	bool m_isSelect;		//trueならセレクトに戻る

	//****************************************************
	//	テスト用
	//****************************************************
	Sphere sphere;
private:
	void SetPointer();
	//Stateによって更新する
	void MainUpdate();
	void MenuUpdate();
	void CameraUpdate();
	void ClearUpdate();
	void ResetUpdate();
	void SceneReset();
public:
	sceneMain( char* filename )
	{
		stageName = filename;

		stage				= nullptr;
		ground				= nullptr;
		m_camera			= nullptr;
		m_gimmickManager	= nullptr;
		m_player			= nullptr;
		m_help				= nullptr;
		m_goal				= nullptr;
		m_collisionCheack	= nullptr;
		m_uiManager			= nullptr;
		m_shadow			= nullptr;
		m_screen		= nullptr;
		m_gameMode			= nullptr;
		pBackBuffer			= nullptr;
		ShadowZ				= nullptr;

		m_fadeTime	= 1.0f; 
		m_isRetry	= false;
		m_isSelect	= false;
	};
	~sceneMain()
	{
		Release();
	}	

	bool Initialize();	//初期化
	void Release();		//解放

	//	更新・描画
	void Update();	//	更新
	void Render();	//	描画
	
};

#endif 

