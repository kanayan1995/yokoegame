#include "iextreme.h"
#include "system/system.h"
#include "StageSelect_ObjGroup.h"

//------------------------------------------
//			コンストラクタ
//------------------------------------------
SelectObj_Manager::SelectObj_Manager()
{
	ZeroMemory(objGroup, sizeof(ObjGroup)*OBJ_MAX);
	for (int i = 0; i < KIND_MAX; i++)obj[i] = nullptr;
	for (int i = 0; i < OBJ_MAX; i++)objGroup[i].type = NONE;
}



//------------------------------------------
//			初期化
//------------------------------------------
bool SelectObj_Manager::Init(int type,char*filename)
{
		obj[type] = make_shared<iex3DObj>(filename);
		if (obj[type] == nullptr)return false;
		return true;
}

//------------------------------------------
//			更新
//------------------------------------------
void SelectObj_Manager::Update()
{
	for (int i = 0; i < OBJ_MAX; i++)
	{
		if (objGroup[i].type == NONE)continue;
		objGroup[i].orizObj->SetPos(objGroup[i].pos);
		objGroup[i].orizObj->SetAngle(objGroup[i].angle);
		objGroup[i].orizObj->SetScale(objGroup[i].scale);
		objGroup[i].orizObj->Update();
	}
}

//------------------------------------------
//			描画
//------------------------------------------
void SelectObj_Manager::Render()
{
	for (int i = 0; i < OBJ_MAX; i++)
	{
		if (objGroup[i].type == NONE)continue;
		//objGroup[ i ].orizObj->Render( shader , "copy" );
		objGroup[i].orizObj->Render();
	}
}

//------------------------------------------
//			セット関数
//------------------------------------------
void SelectObj_Manager::Set(int type,int StageNum, Vector3&pos, Vector3&angle, float scale)
{
	for (int i = 0; i < OBJ_MAX; i++)
	{
		if (objGroup[i].type != NONE)continue;
		//種類・ステージ番号保存
		objGroup[i].type = type;
		objGroup[i].StageNum = StageNum;

		//クローン生成
		objGroup[i].orizObj = obj[type]->CloneShared();

		//位置・向き・大きさ
		objGroup[i].pos = pos;
		objGroup[i].angle = angle;
		objGroup[i].scale = scale;
		break;
	}
}
