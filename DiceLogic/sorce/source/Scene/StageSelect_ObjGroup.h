#pragma once

//---------------------------------------------------
//		オブジェクト構造体（こいつでオブジェ一括所持）
//----------------------------------------------------
class ObjGroup
{
public:
	shared_ptr<iex3DObj> orizObj;
	int type;
	int StageNum;
	Vector3 pos;
	Vector3 angle;
	float scale;

	Vector3 GetPos(){ return pos; }

};

//------------------------------------------
//		オブジェクト管理マネージャー
//------------------------------------------
class SelectObj_Manager
{
private:
	/*オブジェクトの最大数*/
	static const int OBJ_MAX = 20;
	/*出現する種類の最大数*/
	static const int KIND_MAX = 5;

	ObjGroup objGroup[OBJ_MAX];
	shared_ptr<iex3DObj> obj[KIND_MAX];
	int type;
	Vector3 pos;
	Vector3 angle;

	
public:
	SelectObj_Manager();
	~SelectObj_Manager(){};
	/*@1オブジェクトの種類 @2オブジェクトのファイル名*/
	bool Init(int type,char*filename);
	void Update();
	void Render();
	/*
*@1 オブジェクトの種類
*@2	ステージ番号
*@3	位置座標
*@4	向き
*@5	大きさ*/
	void Set(int type,int StageNum,Vector3&pos,Vector3&angle,float scale);

	//ステージ判別用列挙体
	enum{
		NONE = -1,
		STAGE1 = 1,
		STAGE2,
		STAGE3,
		STAGE4,
		STAGE5,
		STAGE6,
		STAGE7,
		STAGE8,
		STAGE9,
	};


	
	/*配置されてるクローンオブジェクトの位置取得*/
	Vector3 GetPos(int num)
	{
		Vector3 out = objGroup[num].orizObj->GetPos();
		return out;
	}

};

