//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　山中										//
//																					//
//---------------------------------------------------------------------------------	//

#ifndef _STAGESELECT_H_
#define _STAGESELECT_H_

#include "../Sound/Sound.h"
#include "StageSelect_ObjGroup.h"
#include "../Cursor/Cursor.h"


//-------------------------------------------
//			ステージセレクトクラス
//-------------------------------------------
class StageSelect :public Scene
{
private:
	

	unique_ptr<iexView> m_view;	//(＠_＠;) ; 横江 シーン遷移とために付けとく

	//固定値宣言
	static const int Image_Height = 128;
	static const int Image_Width = 128;
	


	//カーソルの宣言

	shared_ptr<Cursor> cursor;

	shared_ptr<SelectObj_Manager> objManager;
	shared_ptr<iex2DObj> backImage;

	
	shared_ptr<iex2DObj>obj;
	unique_ptr<iex2DObj> m_select;		//(＠_＠;) ; 横江 　とりあえずつけるだけやからあとで作り変えてね
	
	shared_ptr<iex3DObj> Back_Shelf;
	shared_ptr<iexMesh> Shelf;

	int type;

	float dx, dy, dw, dh;
	float sx, sy, width, height;

	float m_fadeTime;					//フェードの時間

public:

	/*シーンを遷移するかどうかのフラグ [true]遷移開始　[false]遷移しない*/
	bool TransferFlag = false;

	//初期化
	StageSelect()
	{
		TransferFlag = false;
		Sound::PlayBGM( Sound::BGM::SELECT );  //(＠_＠;) ; 横江 　とりあえずつけるだけやからあとで作り変えてね
		objManager = nullptr;
		obj = nullptr;
		dx = dy = 0;
		m_fadeTime = 1.0f;
	};

	~StageSelect()
	{
		Sound::StopBGM( Sound::BGM::SELECT );
	};
	bool Initialize();
	void Update();
	void Render();
	
};



#endif