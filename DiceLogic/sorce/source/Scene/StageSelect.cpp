#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../System/Framework.h"
#include "sceneMain.h"
#include "../Input/Input.h"
#include "../Scene/Loading/Loading.h"
#include "../Fade/Fade.h"

#include "StageSelect.h"

//-----------------------------------------------
//					初期化
//-----------------------------------------------
bool StageSelect::Initialize()
{
	//	環境光
	iexLight::SetAmbient(0x404040);
	iexLight::SetFog( 800 , 2000 , 0 );

	Vector3 dir( -0.4f , -0.4f , 1.0f );
	iexLight::DirLight(myshader , 1 , &dir , 1.5f , 1.5f , 0.5f );

	char* filename = "DATA//BG/grid.png";
	char* objct_filename = "DATA/StageSelect/Box_present.IEM";
	char* obj_filename = "DATA/StageSelect/Rack.IMO";

	//オブジェクトマネージャー
	objManager = make_shared<SelectObj_Manager>();

	//使うモデルが増えるたびにInitで宣言
	objManager->Init(0,  "DATA/StageSelect/Box_present.IEM");

	//プレゼントボックスの設置
	//objManager->Set(0,objManager->STAGE1, Vector3(-15,   2,  -20), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	//objManager->Set(0,objManager->STAGE2, Vector3( -9,   2,  -19), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	//objManager->Set(0,objManager->STAGE3, Vector3( -3,   2,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	//objManager->Set(0,objManager->STAGE4, Vector3(-15,  -4,  -20), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	//objManager->Set(0,objManager->STAGE5, Vector3( -9,  -4,  -19), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	//objManager->Set(0,objManager->STAGE6, Vector3( -3,  -4,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	//objManager->Set(0,objManager->STAGE7, Vector3(-15, -10,  -20), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	//objManager->Set(0,objManager->STAGE8, Vector3( -9, -10,  -19), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	//objManager->Set(0,objManager->STAGE9, Vector3( -3, -10,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE1, Vector3(-15,   2,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE2, Vector3( -9,   2,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE3, Vector3( -3,   2,  -18), Vector3(0.0f, 1.7f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE4, Vector3(-15,  -4,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE5, Vector3( -9,  -4,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE6, Vector3( -3,  -4,  -18), Vector3(0.0f, 1.7f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE7, Vector3(-15, -10,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE8, Vector3( -9, -10,  -18), Vector3(0.0f, 1.4f, 0.0f), 1.5f);
	objManager->Set(0,objManager->STAGE9, Vector3( -3, -10,  -18), Vector3(0.0f, 1.7f, 0.0f), 1.5f);

	
	//カーソルの宣言
	cursor = make_shared<Cursor>(0,Vector3(-15,   2,  -20));
	cursor->Init("DATA/UI/Cursor/Cursor.png");

	//棚の宣言
	Shelf = make_shared<iexMesh>(obj_filename);
	
	char* backfilename = "DATA/背景.png";
	backImage = make_shared<iex2DObj>(backfilename);

	//カメラ設定
	m_view = make_unique<iexView>();
	m_view->Set( Vector3( 0 , 0 , -50 ) , Vector3Zero );

	//Fade設定
	Fade::FadeOut( m_fadeTime , 255 , 255 , 255 );

	return true;
}
//-----------------------------------------------
//					更新
//-----------------------------------------------
void StageSelect::Update()
{
	//オブジェクトマネージャー更新
	objManager->Update();
	
	//カーソルの更新
	cursor->Update(objManager);

	//棚の更新
	Shelf->SetPos(-9, -10, -18 );
	//Shelf->SetAngle(3.0f);
	Shelf->SetAngle( PI );
	Shelf->SetScale(1.0f);
	Shelf->Update();


	//ステージ番号取得
	//シーンの遷移開始
	if ( GAMECONTROLER->CircleButtonPush() && 
		Fade::GetFadeMode() == FADEMODE::STOP )
	{
		Fade::FadeIn( m_fadeTime , 255 , 255 , 255 );
	}
	if ( Fade::isFadeInFinish() )
	{
		int num = cursor->GetNum();
		char* filename = cursor->GetStageName( num );
		MainFrame->ChangeScene( new sceneMain( filename ) );
	}
}

//-----------------------------------------------
//					描画
//-----------------------------------------------
void StageSelect::Render()
{
	//カメラのクリア
	m_view->Activate();
	m_view->Clear();
	
	

	//描画モード変更
	iexSystem::Device->SetRenderState(D3DRS_ZENABLE, false);
	backImage->Render();
	iexSystem::Device->SetRenderState(D3DRS_ZENABLE, true);

	//箱の描画
	objManager->Render();
	
	//棚の描画
	Shelf->Render();


	//カーソルの描画
	cursor->Render();

	
	Fade::Render();
}




