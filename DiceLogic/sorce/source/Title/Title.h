#ifndef _TITLE_H_
#define _TITLE_H_

//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　山中										//
//																					//
//---------------------------------------------------------------------------------	//

class Title : public Scene
{
private:
	/*タイトル画像*/
	unique_ptr<iex2DObj> TitleImage;
	char* filename;

	/*タイトル画像の高さ*/
	static const int TitleImage_Height = 512;
	/*タイトル画像の横幅*/
	static const int TitleImage_Width = 256;

public:

	//--------------------------------以下関数--------------------------------------//
	Title(){
		//初期化
		TitleImage = nullptr;
		
		//画像読み込み
		filename = "DATA\\Title\\Title.png";
	};
	~Title(){};
	bool Initialize();
	void Update();
	void Render();


};



#endif