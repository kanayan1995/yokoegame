#include "../../IEX/iextreme.h"
#include "../System/System.h"
#include "../System/Framework.h"
#include "../Input/Input.h"
#include "../Scene/StageSelect.h"
#include "../Fade/Fade.h"
#include "Title.h"


//-------------------------------------------------
//					初期化
//-------------------------------------------------
bool Title::Initialize()
{
	//	環境光
	iexLight::SetAmbient(0x404040);
	iexLight::SetFog( 800 , 2000 , 0 );

	Vector3 dir( -0.4f , -0.4f , 1.0f );
	iexLight::DirLight(myshader , 1 , &dir , 1.5f , 1.5f , 0.5f );
	TitleImage = make_unique<iex2DObj>(filename);
	if (TitleImage == nullptr)return false;

	return true;
}

//-------------------------------------------------
//					更新
//-------------------------------------------------
void Title::Update()
{
	//シーン遷移
	if (GAMECONTROLER->CircleButtonPush()){
		
		MainFrame->ChangeScene( new StageSelect() );
	
	}

	
}

//-------------------------------------------------
//					描画
//-------------------------------------------------
void Title::Render()
{
	TitleImage->Render(iexSystem::ScreenWidth / 2,iexSystem::ScreenHeight / 2, 512,256,0,0,512,256);
}