//----------------------------------------------------------------------------------//
//																					//
//			�����	:�@����															//
//																					//
//---------------------------------------------------------------------------------	//
#pragma once

#include <dsound.h>
#include <string>
#include <map>

using namespace std;

class SoundEffect
{
private:
	LPDIRECTSOUNDBUFFER buffer;
	LPDIRECTSOUND3DBUFFER buffer3d;

public:
	SoundEffect(char *filename, LPDIRECTSOUND ds, bool is3d = false);
};

class SFXManager
{
private:
	LPDIRECTSOUND8 ds;
	LPDIRECTSOUNDBUFFER primary;
	HWND hwnd;
	map<char*, SoundEffect*> sfxmap;


public:
	SFXManager();
	~SFXManager();

	void addSFX(char *filename, char *soundname, bool is3d = false);

	void Release();
};