#include "iextreme.h"
#include "Audio.h"
#include <assert.h>

SoundEffect::SoundEffect(char *filename, LPDIRECTSOUND ds, bool is3d)
{

}

SFXManager::SFXManager()
{
	hwnd = iexSystem::Window;
	if (DirectSoundCreate8(NULL, &ds, NULL) != DS_OK) {
		OutputDebugStringA("[SFXManager]エラー：デバイス生成エラー");
		assert(false);
	}
	ds->SetCooperativeLevel(hwnd, DSSCL_PRIORITY);

	primary = nullptr;

	DSBUFFERDESC	desc;
	ZeroMemory(&desc, sizeof(DSBUFFERDESC));
	desc.dwSize = sizeof(DSBUFFERDESC);
	desc.dwFlags = DSBCAPS_CTRL3D | DSBCAPS_PRIMARYBUFFER;
	ds->CreateSoundBuffer(&desc, &primary, NULL);


}

SFXManager::~SFXManager()
{
	Release();
}

void SFXManager::addSFX(char *filename, char *soundname, bool is3d)
{
	SoundEffect *sfx;
	sfx = new SoundEffect(filename, ds, is3d);

	sfxmap[soundname] = sfx;
}

void SFXManager::Release()
{
	
}