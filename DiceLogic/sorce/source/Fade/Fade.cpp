#include "iextreme.h"
#include "iextremePlus.h"
#include "Fade.h"

float Fade::m_fadeCount = 0;
float Fade::m_alpha		= 0;
int Fade::m_red			= 0;
int Fade::m_green		= 0;
int Fade::m_blue		= 0;
bool Fade::m_isFadeInFinish		= false;
bool Fade::m_isFadeOutFinish	= false;
FADEMODE Fade::m_fadeMode		= FADEMODE::STOP;

//*****************************************************************************
//	フェードイン
//	時間と色を入れる(AARRGGBB)
//*****************************************************************************

//-----------------------------------------------------
//	フェードイン（色を指定する）
//-----------------------------------------------------
void Fade::FadeIn( float secondTime , int red , int green , int blue )
{
	m_red	= red;
	m_green = green;
	m_blue	= blue;
	FadeIn( secondTime );
}

//-----------------------------------------------------
//	時間だけやと前の色引きつぐ
//-----------------------------------------------------
void Fade::FadeIn( float secondTime )
{
	m_alpha = FADE_IN_ALPHA;
	m_isFadeInFinish	= false;
	m_isFadeOutFinish	= false;
	m_fadeCount = FADE_OUT_ALPHA / FPS( secondTime );
	m_fadeMode	= FADEMODE::MOVE;
}

//*****************************************************************************
//	フェードアウト
//	時間と色を入れる(AARRGGBB)
//*****************************************************************************

//-----------------------------------------------------
//	フェードアウト（色指定）
//-----------------------------------------------------
void Fade::FadeOut( float secondTime , int red , int green , int blue )
{
	m_red	= red;
	m_green = green;
	m_blue	= blue;
	FadeOut( secondTime );
}

//-----------------------------------------------------
//	時間だけやと前の色引きつぐ
//-----------------------------------------------------
void Fade::FadeOut( float secondTime )
{
	m_alpha = FADE_OUT_ALPHA;
	m_isFadeOutFinish	= false;
	m_isFadeInFinish	= false;
	m_fadeCount = -FADE_OUT_ALPHA / FPS( secondTime );
	m_fadeMode	= FADEMODE::MOVE;
}

//*****************************************************************************
//	描画処理
//*****************************************************************************
void Fade::Render()
{
	m_alpha += m_fadeCount;
	if ( m_alpha > FADE_OUT_ALPHA )
	{
		m_isFadeOutFinish	= false;
		m_isFadeInFinish	= true;
		m_fadeCount			= 0;
		m_alpha				= FADE_OUT_ALPHA;
		m_fadeMode			= FADEMODE::STOP;
	}
	if ( m_alpha <= FADE_IN_ALPHA )
	{
		m_isFadeOutFinish	= true;
		m_isFadeInFinish	= false;
		m_fadeCount			= 0;
		m_alpha				= FADE_IN_ALPHA;
		m_fadeMode			= FADEMODE::STOP;
	}

	int alpha = (int)m_alpha;
	iexPolygon::Rect( 0 , 0 , iexSystem::ScreenWidth , iexSystem::ScreenHeight , RS_COPY , ARGB( alpha , m_red , m_green , m_blue ) );
}
