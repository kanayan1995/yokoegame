//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef _FADE_H__
#define _FADE_H__

enum  class FADEMODE
{
	STOP,	//フェードが止まっている
	MOVE,	//動いている時
};

class Fade
{
private:
	static float	m_fadeCount;			//アルファ値を変えるカウント
	static float	m_alpha;				//アルファ値
	static int		m_red;					//赤色成分
	static int		m_green;				//緑色成分
	static int		m_blue;					//青色成分
	static bool		m_isFadeInFinish;		//フェードが終わったかの確認
	static bool		m_isFadeOutFinish;		//フェードが終わったかの確認
	static FADEMODE m_fadeMode;				//フェードが動いているかの判定

	//定数値
	static const int FADE_OUT_ALPHA = 255;	//フェードインする時の始めの値
	static const int FADE_IN_ALPHA	= 0;	//フェードアウトするときの始めの値
public:
	Fade()
	{

	}
	~Fade()
	{

	}

	static void FadeIn(  float secondTime , int red , int green , int blue );	//この入れた時間でFadeInする（秒で入れる）
	static void FadeIn(  float secondTime );									//前に使った色を引き継ぐ
	static void FadeOut( float secondTime , int red , int green , int blue );	//この入れた時間でFadeOutする（秒で入れる）
	static void FadeOut( float secondTime  );									//前に使った色引き継ぐ
	static void Render();														//画面上に四角形ポリゴンを描画
public:	//-----ゲッター-----//
	static bool isFadeInFinish()	{ return m_isFadeInFinish; }
	static bool isFadeOutFinish()	{ return m_isFadeOutFinish; }
	static FADEMODE GetFadeMode()	{ return m_fadeMode; }
};

#endif