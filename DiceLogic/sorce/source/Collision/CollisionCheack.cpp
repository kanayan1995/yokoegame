#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"

#include "Collision.h"
#include "CollisionCheack.h"

//*****************************************************************************
//	プレイヤーとギミックの判定
//*****************************************************************************

//-----------------------------------------------------
//	ギミックとプレイヤーが当たっている時の処理
//	ギミック全体で管理しているがせっかくマップ使ってるから
//	それぞれで分けてもいいかも
//-----------------------------------------------------
void CollisionCheack::PlayerToGimmick( weak_ptr<PlayerCharacter> player , weak_ptr<GimmickManager> gimmick )
{
	int collisionCount = 0;		//あたり判定(レイピックまで)があればカウントを足す
	multimap<GIMMICKTYPE , shared_ptr<BaseGimmick>> gimmickList = gimmick.lock()->GetGimmickMap();
	int collisionCountMax = gimmickList.size();

	for ( auto it = begin( gimmickList ); it != end( gimmickList ); it++ )
	{
		//当たり判定があったときギミックの種類とギミック自体に当たっているかのチェックを入れる
		if ( Collision::CheackAABBToSphere( it->second->GetAABB() , player.lock()->GetSphere() ) == true )
		{
			Vector3 out;
			float dist = 30.0f;
			int materialNumber = Collision::RayPickLocal( it->second->GetObj() , &out , player.lock()->GetPos() , &player.lock()->GetForwardVec() , &dist );
			if ( materialNumber != -1 )
			{
				gimmick.lock()->SetWarpFaceType( it->second->GetFaceType( materialNumber ) );
				gimmick.lock()->SetGimmickType( it->first );
				//it->second->SetDiceRayVec( materialNumber );
				it->second->SetHoldPlayerPos( materialNumber , it->first );
				it->second->SetIsCollision( true );
				player.lock()->SetGimmickPointer( it->second );
				collisionCount++;
			}
			else
			{
				it->second->SetIsCollision( false );
			}
		}
		else
		{
			it->second->SetIsCollision( false );
		}
	}

	//誰とも当たっていなければギミックの種類を入れない
	if ( collisionCount == 0 )
	{
		player.lock()->SetGimmickPointer( nullptr );
		gimmick.lock()->SetWarpFaceType( DICEWARPTYPE::NONE );
		gimmick.lock()->SetGimmickType( GIMMICKTYPE::NONE );
	}
}

void CollisionCheack::Update(weak_ptr<PlayerCharacter> player , weak_ptr<GimmickManager> gimmick)
{
	PlayerToGimmick( player , gimmick );
}
