//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __AABB_H__
#define __AABB_H__

//------------------------------------------------------------------
//	直方体
//	位置情報からSETしたいのでこういう書き方
//------------------------------------------------------------------
class AABB
{
private:
	//****************************************************
	//	メンバ変数
	//****************************************************
	Vector3 m_min;		//最大値(中心から大きい方向にのびる)
	Vector3 m_max;		//最小値(中心から-方向に伸びる)
	Vector3 m_minValue;	//最大値にプラスする値
	Vector3 m_maxValue;	//最小値にマイナスして作る値
public:
	//****************************************************
	//	メンバ関数
	//****************************************************
	AABB()
	{
		m_min = Vector3One;
		m_max = Vector3One;
		m_minValue = Vector3One;
		m_maxValue = Vector3One;
	}
	~AABB()
	{

	}

	void TestRender();
	void TestRender( Vector3 pos );
public://-----セッター-----//
	//minとmaxはどれくらいの大きさかを指定して作る
	void SetMin( Vector3 min );	//最小値設定
	void SetMax( Vector3 max );	//最大値設定
	void SetMinAndMax( Vector3 scale );
	void UpdatePos( Vector3 pos );					//位置に大きさをだして調整
public://-----ゲッター-----//
	Vector3 GetMin() { return m_min; }
	Vector3 GetMax() { return m_max; }	
	Vector3 GetPos() { return m_max - m_maxValue; } //中心座標を取得
};

#endif 