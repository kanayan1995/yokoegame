//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __COLLISION_H__
#define __COLLISION_H__

enum class StageObjType;
class Sphere;
class OBB;
class AABB;

#include <list>
#include <map>
using namespace std;

//------------------------------------------------------------------
//	あたり判定クラス
//	判定だけ作る
//	このクラスは判定を作っているだけなので使って動かすのは別のところで
//------------------------------------------------------------------
class  Collision
{
private:
	//****************************************************
	//	メンバ変数
	//****************************************************
	static const int RAYPICK_THE_NUMBER = 10;
private:
	static multimap< StageObjType , weak_ptr<iexMesh>>	m_collisionMeshMap;	//マップでobjのタイプとメッシュを保存
	static multimap< StageObjType , AABB >				m_collisionAABBMap;	//マップでobjのタイプとAABBを保存
public:
	//****************************************************
	//	メンバ関数
	//****************************************************
	Collision()
	{

	}
	~Collision()
	{

	}
	static void Initialize();
	static void Release();
	static void Render();

	//*****************************************************************************
	// あたり判定を追加していく関数
	//*****************************************************************************
	//メッシュ関連
	static void SetCollisionMesh( StageObjType objType , shared_ptr<iexMesh> collisionMesh );
	static multimap< StageObjType , weak_ptr<iexMesh>>GetCollisionMeshMap()		{ return m_collisionMeshMap; }
	//AABB関連
	static void SetCollisionAABB( StageObjType objType , AABB aabb );
	static multimap< StageObjType , AABB > GetColisionAABBMap()	{ return m_collisionAABBMap; }

	//*****************************************************************************
	// レイピック関係
	//*****************************************************************************
	//ローカル座標に撃つレイピック
public:
	static int  RayPickLocal( weak_ptr<iexMesh> mesh , Vector3* out , const Vector3& pos , Vector3* vec , float* dist );	//モデル空間に撃つレイピック　(＠_＠;) : 横江
	static bool RayPickLocal( Vector3* out ,const Vector3& pos ,const Vector3& vec , const float& dist );					//モデル空間に撃つレイピック　(＠_＠;) : 横江
	static bool RayPickLocal( StageObjType objType , const Vector3& pos , const Vector3& vec , const float& dist );			//目当てのオブジェに中ったか確認したいとき
	//壁ずりベクトル
private:
	static bool WallHit( weak_ptr<iexMesh> mesh , Vector3* out ,const Vector3& pos , const Vector3& move , float* dist , int count );
public:
	static bool WallShear( Vector3* out , const Vector3& pos ,const Vector3& move , float* dist );
	
	//壁ずりベクトル
	//static bool RayPickLocal( iexMesh* obj , Vector3* out , Vector3* pos , Vector3* vec , float* dist );	//モデル空間に撃つレイピック
	//static Vector3 WallShear( iexMesh* obj , Vector3* out , Vector3* pos , Vector3* vec , float* dist );

	//*****************************************************************************
	// SphereとSphereで使う関数
	//*****************************************************************************
public:
	static bool CheackSphereToSphere( Sphere sphere1 , Sphere sphere2 , float cheackLenght );	//距離を指定
	static bool CheackSphereToSphere( Sphere sphere1 , Sphere sphere2 );						//球と球の半径で判断

	//*****************************************************************************
	// AABBとSphereで使う関数
	//*****************************************************************************	
public:
	static bool CheackAABBToSphere( AABB aabb , Sphere sphere );
	//*****************************************************************************
	// AABBとAABBで使う関数
	//*****************************************************************************	
	//AABBとAABBでのあたり判定
	static bool	CheackAABBToAABB( AABB aabb1 , AABB aabb2 );		
	static bool	CheackAABBToAABB( AABB aabb1 , AABB aabb2 , float scale);	//少しだけ小さくして判定したい場合
private:
	//壁ずり用のAABB同士のあたり判定
	static Vector3 GetAABBNormal( Vector3 aabb1Pos , Vector3 aabb1Min , Vector3 aabb1Max , Vector3 aabb2Min , Vector3 aabb2Max );
	static bool	ShearCheackAABBToAABB( Vector3 aabb1Min , Vector3 aabb1Max , Vector3 aabb2Min , Vector3 aabb2Max );
public:
	static Vector3 AABBShear( Vector3 move , Vector3 moveVec , AABB aabb1 , AABB aabb2 );	//あたったAABBに壁ずりすればいい


	//*****************************************************************************
	// OBBとSphereで使う関数
	//*****************************************************************************
private:
	static Vector3 GetOverEdgeLenght(float axisLength , Vector3 axis , Vector3 distVec );	//OBBで使うはみ出している分の距離取得
public:
	static bool CheackOBBToSphere( OBB obb , Sphere pos );	//当たっているかいないかをboolで返す

	//*****************************************************************************
	// OBB同士で使う関数
	//*****************************************************************************
private:
	static float CheackOBBAxisGetLength( float obbALenght , Vector3 obbAAxis , OBB obbB , Vector3 dist );	//OBBの各軸同士で判定
	static float CheackOBBAxisCrossGetLength( Vector3 obbAAxis , Vector3 obbBAxis , OBB obbA , OBB obbB , Vector3 dist );	//各軸から外積とって判定
public:
	static bool CheackOBBToOBB( OBB obbA , OBB obbB );	//OBB同士のあたり判定
};
#endif 