#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../Utilty/Utility.h"

#include "../Stage/Stage.h"
#include "Sphere.h"
#include "AABB.h"
#include "OBB.h"
#include "Collision.h"

multimap< StageObjType , weak_ptr<iexMesh>> Collision::m_collisionMeshMap;
multimap< StageObjType , AABB >				Collision::m_collisionAABBMap;
//***************************************************************************************************************
//
//	あたり判定用のモデルを格納する
//
//***************************************************************************************************************

//*****************************************************************************
//	初期化と解放
//*****************************************************************************

//-----------------------------------------------------
//	初期化
//-----------------------------------------------------
void Collision::Initialize()
{
	m_collisionMeshMap.clear();
	m_collisionAABBMap.clear();
}

//-----------------------------------------------------
//	解放
//-----------------------------------------------------
void Collision::Release()
{
	m_collisionMeshMap.clear();
	m_collisionAABBMap.clear();
}

//*****************************************************************************
//	mapに入っている物をテスト描画
//*****************************************************************************
void Collision::Render()
{
	for ( auto it = begin( m_collisionAABBMap ); it != end( m_collisionAABBMap ); it++ )
	{
		it->second.TestRender();
	}
	for ( auto it = begin( m_collisionMeshMap ); it != end( m_collisionMeshMap ); it++ )
	{
		it->second.lock()->Render();
	}
}

//*****************************************************************************
//	マルチマップにセットする
//*****************************************************************************
void Collision::SetCollisionMesh( StageObjType objType , shared_ptr<iexMesh> collisionMesh )
{
	m_collisionMeshMap.emplace( objType , collisionMesh );
}

void Collision::SetCollisionAABB( StageObjType objType , AABB aabb )
{
	m_collisionAABBMap.emplace( objType , aabb );
}

//http://marupeke296.com/COL_Basic_No5_WallVector.html
//***************************************************************************************************************
//
//	レイピック関係
//
//***************************************************************************************************************

//*****************************************************************************
//	取り合えずローカル座標に撃つレイピック
//*****************************************************************************
//-----------------------------------------------------
//	モデリング座標（ローカル座標）にレイを撃つ
//	距離と中った場所と当たった面の法線が返ってくる
//-----------------------------------------------------
//	mesh レイピックを撃ちたいメッシュ入れる		参照だけしたいかたweakポインタ
//	out  空の数値を入れる						あたった場所が返ってくる
//	pos  レイを撃つ場所							特に何も返ってこない
//	vec  レイを撃つ向き							当たった面の法線が返ってくる
//	dist レイが届く最大距離						当たったあとは面とposとの距離が返ってくる
//  return int型								material番号が返ってくる(当たらんかったら-1)
int Collision::RayPickLocal( weak_ptr<iexMesh> mesh , Vector3* out , const Vector3& pos , Vector3* vec , float* dist )
{
	//mesh.lock()->Update();
	Matrix mat = mesh.lock()->TransMatrix;
	Matrix invMat;
	Vector3 workPos = pos + Vector3( 0 , 2 , 0 );
	Vector3 workVec = *vec;
	workVec.Normalize();

	D3DXMatrixInverse( &invMat , NULL , &mat );		//モデリング空間の行列を作る
	workPos = TransformPos( workPos , invMat );		//モデリング空間に持ってくる
	workVec = Transform( workVec , invMat );		//モデリング空間に持ってくる
	Vector3 workOut;
	float workDist = *dist;

	int index = mesh.lock()->RayPick( &workOut , &workPos , &workVec , &workDist);
	if ( index == -1 ) return -1;

	workOut = TransformPos( workOut , mat );
	workVec = Transform( workVec , mat );

	//値変換さす
	*out = workOut;
	*vec = workVec;

	//距離計算　
	Vector3 d;
	d = workOut - pos;
	float cheackDist = d.Length();
	*dist = cheackDist;

	//material番号を取得する処理(RayPick関数で返ってきた値を使う)

	DWORD  *pAttr;
	mesh.lock()->GetMesh()->LockAttributeBuffer( D3DLOCK_READONLY , &pAttr );

	DWORD materialNumber = pAttr[ index ];
	mesh.lock()->GetMesh()->UnlockAttributeBuffer();
	return materialNumber;
}

//-----------------------------------------------------
//	リストに入れたものに対してレイを撃つ
//	モデリング座標（ローカル座標）にレイを撃つ
//	距離と中った場所と当たった面の法線が返ってくる
//-----------------------------------------------------
//	out  空の数値を入れる						あたった場所が返ってくる
//	pos  レイを撃つ場所							特に何も返ってこない
//	vec  レイを撃つ向き							当たった面の法線が返ってくる
//	dist レイが届く最大距離						当たったあとは面とposとの距離が返ってくる
bool Collision::RayPickLocal( Vector3* out , const Vector3 &pos ,const Vector3& vec ,const float& dist )
{
	for ( auto it = begin( m_collisionMeshMap ); it != end( m_collisionMeshMap ) ; it++ )
	{
		//it->lock()->Update();
		Matrix mat = it->second.lock()->TransMatrix;
		Matrix invMat;
		Vector3 workPos = pos + Vector3( 0 , 2 , 0 );
		Vector3 workVec = vec;
		workVec.Normalize();

		D3DXMatrixInverse( &invMat , NULL , &mat );		//モデリング空間の行列を作る
		workPos = TransformPos( workPos , invMat );		//モデリング空間に持ってくる
		workVec = Transform( workVec , invMat );		//モデリング空間に持ってくる
		Vector3 workOut;
		float workDist = dist;

		if ( it->second.lock()->RayPick( &workOut , &workPos , &workVec , &workDist ) == -1 ) continue;

		workOut = TransformPos( workOut , mat );
		workVec = Transform( workVec , mat );

		//値変換さす
		*out = workOut;
		return true;
	}
	*out = pos;
	return false;
}

bool Collision::RayPickLocal( StageObjType objType , const Vector3& pos , const Vector3& vec , const float& dist )
{
	for ( auto it = begin( m_collisionMeshMap ); it != end( m_collisionMeshMap ); it++ )
	{
		Matrix mat = it->second.lock()->TransMatrix;
		Matrix invMat;
		Vector3 workPos = pos + Vector3( 0 , 2 , 0 );
		Vector3 workVec = vec;
		workVec.Normalize();

		D3DXMatrixInverse( &invMat , NULL , &mat );		//モデリング空間の行列を作る
		workPos = TransformPos( workPos , invMat );		//モデリング空間に持ってくる
		workVec = Transform( workVec , invMat );		//モデリング空間に持ってくる
		
		Vector3 workOut;
		float workDist = dist;
		int index = it->second.lock()->RayPick( &workOut , &workPos , &workVec , &workDist );
		if ( index == -1 ) continue;
		
		if ( it->first == objType ) return true;
	}
	return false;
}

//*****************************************************************************
//	壁ずりベクトル(再起処理バ-ジョン)
//*****************************************************************************

//-----------------------------------------------------
//	再起処理
//	壁ずりでのベクトルを見て
//	その先に壁があるかをチェックしている
//-----------------------------------------------------
bool Collision::WallHit( weak_ptr<iexMesh> mesh , Vector3* out ,const Vector3& pos , const Vector3& move , float* dist , int count )
{
	if ( count <= 0 ) return true;

	Vector3 workOut;
	Vector3 workPos = pos;
	Vector3 workNormal = move;
	workNormal.Normalize();
	workNormal.y = 0;

	if ( mesh.lock()->RayPick( &workOut , &workPos , &workNormal , dist ) == -1 )
	{
		*out = move;
		return false;
	}
	workNormal.Normalize();

	//壁ずり作成
	Vector3 workMove = move;
	Vector3 wallRun;										//壁ずりベクトル
	float length = Vector3Dot( workNormal , -workMove );	//めり込んだ分の強さが出る 向きを逆にするの忘れない
	wallRun      = workMove + (workNormal * length);
	wallRun.y    = 0;
	count--;
	WallHit( mesh, out ,pos , wallRun , dist , count );
	return true;
}

//-----------------------------------------------------
//	壁ずりベクトル
//	移動量を返す
//	位置にoutで返ってくる値を+するようにして使う
//	ここの最適化処理を書くべき（距離判定で撃つか撃たないかを選ぶ）
//-----------------------------------------------------
//	out  空の数値を入れる                       あたった場所が返ってくる
//	pos  レイを撃つ場所							const
//	vec  レイを撃つ向き							const
//	dist レイが届く最大距離						当たったあとは面とposとの距離が返ってくる
bool Collision::WallShear( Vector3* out , const Vector3& pos , const Vector3& move , float* dist )
{
	//とりあえず値を保存しておく
	Vector3 workOut;
	Vector3 workPos = pos + Vector3( 0 , 2 , 0 );
	Vector3 workMove = move;		//壁に中る前の向き

	//壁ずり作成
	Vector3 wallRun;
	Vector3 addMove = move;

	for ( auto it = begin( m_collisionMeshMap ) ; it != end( m_collisionMeshMap ); it++ )
	{
		//レイをモデリング空間に持っていく準備
		Vector3 modelPos;
		it->second.lock()->Update();
		Matrix mat = it->second.lock()->TransMatrix;
		//ここで距離判定を書くべき

		Matrix invMat;
		D3DXMatrixInverse( &invMat , NULL , &mat );
		workMove = addMove;
		modelPos = TransformPos( workPos , invMat );
		workMove = Transform( workMove , invMat );
		WallHit( it->second , &workOut , modelPos , workMove , dist , RAYPICK_THE_NUMBER );
		//ワールドに戻す
		addMove = Transform( workOut , mat );
	}
	wallRun = addMove;
	wallRun.y = 0;

	*out = wallRun;
	//move値が変わっていなければまず当たっていない
	if ( fabsf( wallRun.x - move.x ) < FLT_EPSILON * 2
		&& fabsf( wallRun.z - move.z ) < FLT_EPSILON * 2 )
	{
		return false;
	}
	return true;
}

//-----------------------------------------------------
//	壁ずりベクトル
//	一応できてわいるけど壁を余裕でぬけるから作り直す
//-----------------------------------------------------
//bool Collision::WallShear( Vector3* out , const Vector3* pos , const Vector3* move , float* dist )
//{
//	//とりあえず値を保存しておく
//	Vector3 workOut;
//	Vector3 workPos = *pos;
//	workPos += Vector3( 0 , 1 , 0 );
//	Vector3 workNormal = *move;		//値が返ってきたら法線になるから
//	Vector3 workMove = *move;		//壁に中る前の向き
//	if ( RayPickLocal( &workOut , &workPos , &workNormal , dist ) == false )
//	{
//		workPos -= Vector3( 0 , 1 , 0 );
//		*out = workMove;
//		return false;
//	}
//
//	//端っこと正面の時
//	Vector3 normalMove = *move;
//	normalMove.Normalize();						
//	workNormal.Normalize();						//正規化（下の壁ずりでも使う）
//	float dot = Vector3Dot( workNormal , -normalMove );
//	if ( dot > 0.95f || dot < 0.2f )
//	{
//		*out = -workMove;
//		return true;
//	}
//
//	//壁ずり計算
//	Vector3 wallShear;										//壁ずりベクトル
//	float length = Vector3Dot( workNormal , -workMove );	//めり込んだ分の強さが出る 向きを逆にするの忘れない
//	wallShear = workMove + (workNormal * length);
//	wallShear.Normalize();
//	wallShear *= workMove.Length();
//	Vector3 sink = workNormal * (workMove.Length());	//めり込んだ分戻す　(＠_＠;) : 横江　ここはプレイヤー大きさそってかえなあかんかも
//	wallShear = sink + wallShear;							//壁ずり+めり込んだ分戻す
//	wallShear.y = 0;
//	*out = wallShear;
//	return true;
//}

////*****************************************************************************
////	壁ずりベクトル(objをこっちから指定する場合)
////*****************************************************************************
////-----------------------------------------------------
////	モデリング座標（ローカル座標）にレイを撃つ
////	距離と中った場所と当たった面の法線が返ってくる
////-----------------------------------------------------
////	obj  レイが当たってほしいメッシュを入れる	何も返ってこない
////	out  空の数値を入れる						あたった場所が返ってくる
////	pos  レイを撃つ場所							特に何も返ってこない
////	vec  レイを撃つ向き							当たった面の法線が返ってくる
////	dist レイが届く最大距離						当たったあとは面とposとの距離が返ってくる
//bool Collision::RayPickLocal( iexMesh* obj , Vector3* out , Vector3* pos , Vector3* vec , float* dist )
//{
//	Matrix mat = obj->TransMatrix;
//	Matrix invMat;
//	D3DXMatrixInverse( &invMat , NULL , &mat );			//モデリング空間の行列を作る
//	Vector3 workPos = TransformPos( *pos , invMat );	//モデリング空間に持ってくる
//	Vector3 workVec = Transform( *vec , invMat );		//モデリング空間に持ってくる
//	float workDist = *dist;
//
//	if ( obj->RayPick( out , &workPos , &workVec , &workDist ) == -1 ) return false;
//
//	workPos = TransformPos( *out , mat );				//ワールドに戻す
//	workVec = Transform( workVec , mat );				//ワールドに戻す
//	
//	//距離計算　
//	Vector3 d;
//	d = workPos - *pos;
//	float cheackDist = d.Length();
//	*dist = cheackDist;
//	return true;
//}
//
////-----------------------------------------------------
////	壁ずりベクトル
////	向きが取得できるからその向きに進ますようにする
////	長さが壁に届いているかの判定くらいあってもいいのかな？
////	こいつの中で壁ずりの進みぐあいまで計算するか
////-----------------------------------------------------
//Vector3 Collision::WallShear( iexMesh* obj , Vector3* out , Vector3* pos , Vector3* vec , float* dist )
//{
//	//とりあえず値を保存しておく
//	Vector3 workOut;
//	Vector3 workPos = *pos;
//	Vector3 workNormal = *vec;	//値が返ってきたら法線になるから
//	Vector3 workVec = *vec;		//壁に中る前の向き
//	float workDist = *dist;
//	if ( RayPickLocal( obj , &workOut , &workPos , &workNormal , &workDist ) == -1 )
//	{
//		return Vector3Zero;
//	}
//	pos = &workPos;	//当たったところにpos位置移動
//
//	workNormal.Normalize();
//	float length = Vector3Dot( workNormal , -workVec );	//めり込んだ分の強さが出る 向きを逆にするの忘れない
//	Vector3 wallShear; //壁ずりベクトル
//	wallShear = workVec + (workNormal*length);
//	wallShear.Normalize(); 
//	return wallShear;
//}

//***************************************************************************************************************
//
//	Sphere関係のあたり判定
//
//***************************************************************************************************************

//*****************************************************************************
//	SphereとSphere
//*****************************************************************************

//-----------------------------------------------------
//	sphere1とsphere2に判定を入れたいやつを入れて
//	cheackLenghtには気判定入れたい距離を入れる
//-----------------------------------------------------
bool Collision::CheackSphereToSphere( Sphere sphere1 , Sphere sphere2 , float cheackLenght )
{
	float lenght = Vector3Distance( sphere1.GetPos() , sphere2.GetPos() );	//球と球の距離取得
	if ( cheackLenght > lenght ) return true;
	return false;
}

//-----------------------------------------------------
//	sphere1とsphere2に判定を入れたいやつを入れて
//	球同士の判定を見て当たっているかの判断をする
//-----------------------------------------------------
bool Collision::CheackSphereToSphere( Sphere sphere1 , Sphere sphere2 )
{
	float cheackLenght = sphere1.GetRadius() + sphere2.GetRadius();
	if ( CheackSphereToSphere( sphere1 , sphere2 , cheackLenght ) == true ) return true;
	return false;
}

//***************************************************************************************************************
//
//	AABB関係
//
//***************************************************************************************************************


//*****************************************************************************
//	AABBとSphereのあたり判定
//*****************************************************************************
bool Collision::CheackAABBToSphere( AABB aabb , Sphere sphere )
{
	Vector3 spherePos = sphere.GetPos();
	Vector3 aabbMin = aabb.GetMin();
	Vector3 aabbMax = aabb.GetMax();
	float len = 0;
	if ( spherePos.x < aabbMin.x ) len += (spherePos.x - aabbMin.x)*(spherePos.x - aabbMin.x);
	if ( spherePos.x > aabbMax.x ) len += (spherePos.x - aabbMax.x)*(spherePos.x - aabbMax.x);
	if ( spherePos.y < aabbMin.y ) len += (spherePos.y - aabbMin.y)*(spherePos.y - aabbMin.y);
	if ( spherePos.y > aabbMax.y ) len += (spherePos.y - aabbMax.y)*(spherePos.y - aabbMax.y);
	if ( spherePos.z < aabbMin.z ) len += (spherePos.z - aabbMin.z)*(spherePos.z - aabbMin.z);
	if ( spherePos.z > aabbMax.z ) len += (spherePos.z - aabbMax.z)*(spherePos.z - aabbMax.z);
	len = sqrt( len );	//最短距離取得
	if ( len < sphere.GetRadius() ) return true;
	return false;
}

//*****************************************************************************
//	AABBとAABBの判定
//	あたっていたらtrue
//	あたっていなかったらfalse
//*****************************************************************************
bool Collision::CheackAABBToAABB( AABB aabb1 , AABB aabb2 )
{
	Vector3 aabb1Min = aabb1.GetMin();
	Vector3 aabb1Max = aabb1.GetMax();
	Vector3 aabb2Min = aabb2.GetMin();
	Vector3 aabb2Max = aabb2.GetMax();
	if ( aabb1Min.x > aabb2Max.x ) return false;
	if ( aabb1Max.x < aabb2Min.x ) return false;
	if ( aabb1Min.y > aabb2Max.y ) return false;
	if ( aabb1Max.y < aabb2Min.y ) return false;
	if ( aabb1Min.z > aabb2Max.z ) return false;
	if ( aabb1Max.z < aabb2Min.z ) return false;
	return true;
}

bool Collision::CheackAABBToAABB( AABB aabb1 , AABB aabb2 , float scale )
{
	Vector3 aabb1Min = aabb1.GetMin() - Vector3One * scale;
	Vector3 aabb1Max = aabb1.GetMax() + Vector3One * scale;
	Vector3 aabb2Min = aabb2.GetMin() - Vector3One * scale;
	Vector3 aabb2Max = aabb2.GetMax() + Vector3One * scale;
	if ( aabb1Min.x > aabb2Max.x ) return false;
	if ( aabb1Max.x < aabb2Min.x ) return false;
	if ( aabb1Min.y > aabb2Max.y ) return false;
	if ( aabb1Max.y < aabb2Min.y ) return false;
	if ( aabb1Min.z > aabb2Max.z ) return false;
	if ( aabb1Max.z < aabb2Min.z ) return false;
	return true;
}

//*****************************************************************************
//	AABBとAABBでの壁ずり
//*****************************************************************************

//-----------------------------------------------------
//	AABBとAABBの判定
//	壁ずりで使うのであたり判定とはべつで作る
//-----------------------------------------------------
bool Collision::ShearCheackAABBToAABB( Vector3 aabb1Min , Vector3 aabb1Max , Vector3 aabb2Min , Vector3 aabb2Max )
{
	if ( aabb1Min.x - 0.5f > aabb2Max.x ) return false;
	if ( aabb1Max.x + 0.5f < aabb2Min.x ) return false;
	if ( aabb1Min.y - 0.5f > aabb2Max.y ) return false;
	if ( aabb1Max.y + 0.5f < aabb2Min.y ) return false;
	if ( aabb1Min.z - 0.5f > aabb2Max.z ) return false;
	if ( aabb1Max.z + 0.5f < aabb2Min.z ) return false;
	return true;
}

//-----------------------------------------------------
//	AABBとAABBから法線を取得する
//-----------------------------------------------------
Vector3 Collision::GetAABBNormal( Vector3 aabb1Pos , Vector3 aabb1Min , Vector3 aabb1Max , Vector3 aabb2Min , Vector3 aabb2Max )
{
	float workLenght = 0;	//めり込み量を調整
	//-ｚ方向の法線（前）
	if ( aabb2Min.x < aabb1Max.x && aabb2Max.x > aabb1Min.x &&
		aabb2Min.y < aabb1Max.y && aabb2Max.y > aabb1Min.y )
	{
		if		( aabb2Min.z > aabb1Max.z - workLenght ) return Vector3Back;	 //前
		else if ( aabb2Max.z < aabb1Min.z + workLenght ) return Vector3Forward;	 //後ろ
	}

	//x軸方向の法線（右）
	if ( aabb2Min.y < aabb1Max.y && aabb2Max.y > aabb1Min.y &&
		aabb2Min.z < aabb1Max.z && aabb2Max.z > aabb1Min.z )
	{
		if		( aabb2Min.x > aabb1Max.x - workLenght )	return  Vector3Left;	//左
		else if ( aabb2Max.x < aabb1Min.x + workLenght )	return  Vector3Right;	//右
	}

	//ｙ軸方向の法線（上）
	if ( aabb2Min.x < aabb1Max.x || aabb2Max.x > aabb1Min.x &&
		aabb2Min.z < aabb1Max.z || aabb2Max.z > aabb1Min.z )
	{
		if		( aabb2Min.y > aabb1Max.y - workLenght ) return	Vector3Down;	//下
		else if ( aabb2Max.y < aabb1Min.y + workLenght ) return Vector3Up;		//上
	}
	return Vector3Zero;
}

//*****************************************************************************
//	AABBとAABB
//	当たっているAABBに沿って移動する
//	法線はAABB2の奴を取る
//*****************************************************************************
Vector3 Collision::AABBShear( Vector3 move , Vector3 moveVec , AABB aabb1 , AABB aabb2 )
{
	Vector3 aabb1Min = aabb1.GetMin();
	Vector3 aabb1Max = aabb1.GetMax();
	Vector3 aabb2Min = aabb2.GetMin();
	Vector3 aabb2Max = aabb2.GetMax();
	if ( ShearCheackAABBToAABB( aabb1Min , aabb1Max , aabb2Min , aabb2Max ) == true )
	{
		Vector3 aabb2Normal = GetAABBNormal( aabb1.GetPos() , aabb1Min , aabb1Max , aabb2Min , aabb2Max );
		if ( fabsf( aabb2Normal.x - moveVec.x ) < FLT_EPSILON &&
			fabsf( aabb2Normal.y - moveVec.y ) < FLT_EPSILON &&
			fabsf( aabb2Normal.z - moveVec.z ) < FLT_EPSILON )
		{
			return move;
		}

		float workDot = Vector3Dot( -move , aabb2Normal );
		Vector3 aabb2Shear = move + (aabb2Normal*workDot);
		//aabb2Shear += aabb2Normal*0.1f;
		return aabb2Shear;
	}
	else
	{
		return move;
	}
}

//参考文献　http://marupeke296.com/COL_3D_No12_OBBvsPoint.html
//***************************************************************************************************************
//
//	OBB関係のあたり判定
//
//***************************************************************************************************************

//*****************************************************************************
//	OBBとSphere
//*****************************************************************************

//-----------------------------------------------------
//	OBBと球での判定で使うもの
//	OBBの軸と半径を使って半径から1(m)はみ出してる分の距離を取得する
//	はみ出していない場合は0を取得（範囲内にいるということだから）
//-----------------------------------------------------
Vector3 Collision::GetOverEdgeLenght( float axisLength , Vector3 axis , Vector3 distVec )
{
	//各軸についてはみ出た部分のベクトルを算出
	if ( axisLength <= 0 ) return Vector3Zero;	//まず長さが入ってなかったら無理
	float constant;								//点から出る垂線が、指定した軸に交わる場所を出すのにいる定数倍　
	constant = Vector3Dot( distVec , axis ) / axisLength;

	constant = fabsf( constant );
	if ( constant > 1 )
	{
		return axis * ((1 - constant)*axisLength);
	}
	return Vector3Zero;
}

//-----------------------------------------------------
//	OBBと球の判定
//	OBBと点から最短距離がとれるので
//	その距離が半径より小さかったら当たっている
//	当たっていると			: true
//	当たっていなかったら	: false
//-----------------------------------------------------
bool Collision::CheackOBBToSphere( OBB obb , Sphere sphere )
{
	Vector3 workVec = Vector3Zero;		//最短距離の長さを求めるベクトル
	Vector3 workDist = sphere.GetPos() - obb.GetPos();
	workVec += GetOverEdgeLenght( obb.GetHalfLengthX() , obb.GetAxisX() , workDist );
	workVec += GetOverEdgeLenght( obb.GetHalfLengthY() , obb.GetAxisY() , workDist );
	workVec += GetOverEdgeLenght( obb.GetHalfLengthZ() , obb.GetAxisZ() , workDist );
																									
	float cheackDist = workVec.Length();
	if ( cheackDist < sphere.GetRadius() ) return true;
	return false;
}

//*****************************************************************************
//	OBBとOBB
//*****************************************************************************
//-----------------------------------------------------
//	投影線分を出す計算
//-----------------------------------------------------
inline float ProJectionLine( Vector3 axis , OBB obb )
{
	float d1 = fabsf( Vector3Dot( axis , obb.GetAxisX() * obb.GetHalfLengthX() ) );
	float d2 = fabsf( Vector3Dot( axis , obb.GetAxisY() * obb.GetHalfLengthY() ) );
	float d3 = fabsf( Vector3Dot( axis , obb.GetAxisZ() * obb.GetHalfLengthZ() ) );
	return d1 + d2 + d3;
}

//-----------------------------------------------------
//	OBBの各軸で当たっているかの判定をもつ
//-----------------------------------------------------
float Collision::CheackOBBAxisGetLength( float obbALenght , Vector3 obbAAxis , OBB obbB , Vector3 dist )
{
	float axisLength = obbALenght;								//
	float projectLine = ProJectionLine( obbAAxis , obbB );		//投影線分取得
	float interval = fabsf( Vector3Dot( dist , obbAAxis ) );	//中心点間の距離取得
	return axisLength + projectLine - interval;
}

//-----------------------------------------------------
//	OBBの各軸で当たっているかの判定をもつ
//-----------------------------------------------------
float Collision::CheackOBBAxisCrossGetLength( Vector3 obbAAxis , Vector3 obbBAxis ,OBB obbA ,  OBB obbB , Vector3 dist )
{
	Vector3 cross;
	Vector3Cross( cross , obbAAxis , obbBAxis );
	cross.Normalize();
	float projectLineA = ProJectionLine( cross , obbA );
	float projectLineB = ProJectionLine( cross , obbB );
	float interval = fabsf( Vector3Dot( dist , cross ) );
	return projectLineA + projectLineB - interval;
}

//-----------------------------------------------------
//	OBB同士のあたり判定
//	trueなら当たっている,falseならあたっていない
//-----------------------------------------------------
bool Collision::CheackOBBToOBB( OBB obbA , OBB obbB )
{
	Vector3 dist = obbA.GetPos() - obbB.GetPos();

	//obb1との分離軸判定
	if ( CheackOBBAxisGetLength( obbA.GetHalfLengthX() , obbA.GetAxisX() , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisGetLength( obbA.GetHalfLengthY() , obbA.GetAxisY() , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisGetLength( obbA.GetHalfLengthZ() , obbA.GetAxisZ() , obbB , dist ) < 0 ) return false;
	//obb2との分離軸判定
	if ( CheackOBBAxisGetLength( obbB.GetHalfLengthX() , obbB.GetAxisX() , obbA , dist ) < 0 ) return false;
	if ( CheackOBBAxisGetLength( obbB.GetHalfLengthY() , obbB.GetAxisY() , obbA , dist ) < 0 ) return false;
	if ( CheackOBBAxisGetLength( obbB.GetHalfLengthZ() , obbB.GetAxisZ() , obbA , dist ) < 0 ) return false;

	//双方の方向ベクトルに垂直な分離軸の判定
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisX() , obbB.GetAxisX() , obbA , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisX() , obbB.GetAxisY() , obbA , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisX() , obbB.GetAxisZ() , obbA , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisY() , obbB.GetAxisX() , obbA , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisY() , obbB.GetAxisY() , obbA , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisY() , obbB.GetAxisZ() , obbA , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisZ() , obbB.GetAxisX() , obbA , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisZ() , obbB.GetAxisY() , obbA , obbB , dist ) < 0 ) return false;
	if ( CheackOBBAxisCrossGetLength( obbA.GetAxisZ() , obbB.GetAxisZ() , obbA , obbB , dist ) < 0 ) return false;
	
	return true;
}

