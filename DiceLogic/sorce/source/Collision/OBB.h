//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __OBB_H__
#define __OBB_H__

//------------------------------------------------------------------
//	回転しても使える六面体
//	こいつの軸はMeshの向きに合わせて使おうと思っている
//------------------------------------------------------------------
class OBB
{
private:
	//****************************************************
	//	メンバ変数
	//****************************************************
	Vector3	m_pos;			//位置
	Vector3 m_axisX;		//軸X(right)
	Vector3 m_axisY;		//軸Y(up)
	Vector3 m_axisZ;		//軸Z(forward)
	float m_halfLenghtX;	//中心からの半径(X方面)
	float m_halfLenghtY;	//中心からの半径(Y方面)
	float m_halfLenghtZ;	//中心からの半径(Z方面)
public:
	//****************************************************
	//	メンバ関数
	//****************************************************
	OBB()
	{
		m_pos = Vector3Zero;
		m_axisX = Vector3Right;
		m_axisY = Vector3Up;
		m_axisZ = Vector3Forward;
		m_halfLenghtX = 1;
		m_halfLenghtY = 1;
		m_halfLenghtZ = 1;
	}
	OBB(float halfLenghtX , float halfLenghtY , float halfLenghtZ )
	{
		m_pos = Vector3Zero;
		m_axisX = Vector3Right;
		m_axisY = Vector3Up;
		m_axisZ = Vector3Forward;
		m_halfLenghtX = halfLenghtX;
		m_halfLenghtY = halfLenghtY;
		m_halfLenghtZ = halfLenghtZ;
	}
	~OBB()
	{
	}

	//****************************************************
	//	テスト描画（原点は中心座標）
	//****************************************************
	void TestRender();										//大きさ目視したいときに使う angle(degree)用
	void TestRender( Quaternion rotation );					//大きさ目視したいときに使う Quaternion用
	void TestRender( Quaternion rotation , Vector3 pos );	//大きさ目視したいときに使う Quaternion位置調整用

public: //---------セッター---------//	
	void SetAxisAndPos( Matrix mat );						//行列から軸、位置両方設定する
	//位置
	void SetPos( Vector3 pos ) { m_pos = pos; }				//位置設定
	//軸設定	
	void SetAxis( Matrix mat );								//行列から軸決定
	void SetAxisX( Vector3 axis ) { m_axisX = axis; }		//X軸設定尾
	void SetAxisY( Vector3 axis ) { m_axisY = axis; }		//Y軸設定
	void SetAxisZ( Vector3 axis ) { m_axisZ = axis; }		//Z軸設定
	//OBBの大きさを設定		
	void SetHalfLengthX( float halfLength ) { m_halfLenghtX = halfLength; }	//X軸への大きさ
	void SetHalfLengthY( float halfLength ) { m_halfLenghtY = halfLength; }	//Y軸への大きさ
	void SetHalfLengthZ( float halfLength ) { m_halfLenghtZ = halfLength; }	//Z軸への大きさ
	void SetHalfLength( Vector3 lengthXYZ );								//一回でまとめて入れたい場合
	void SetHalfLength( float length );										//一回でまとめて入れたい場合(全部同じ値)
public: //---------ゲッター--------//
	Vector3 GetPos() { return m_pos; }
	inline Vector3 GetAxisX() { return m_axisX; }
	inline Vector3 GetAxisY() { return m_axisY; }
	inline Vector3 GetAxisZ() { return m_axisZ; }
	float GetHalfLengthX() { return m_halfLenghtX; }
	float GetHalfLengthY() { return m_halfLenghtY; }
	float GetHalfLengthZ() { return m_halfLenghtZ; }
};


#endif 