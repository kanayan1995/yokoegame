#include "iextreme.h"
#include "iextremePlus.h"

#include "../ObjStorage/ObjStorage.h"
#include "AABB.h"

//****************************************************
//	
//****************************************************
void AABB::TestRender()
{
	Vector3 workScale = (m_maxValue - m_minValue) / 2;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetPos( m_max - m_maxValue );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetScale( workScale );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->Update();
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->Render( RS_MUL , 1.0f );
}

//****************************************************
//	位置入れて更新
//	こいつで確認できるのは正方形の場合
//****************************************************
void AABB::TestRender( Vector3 pos )
{
#ifdef _DEBUG
	Vector3 workScale = (m_maxValue - m_minValue) / 2;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetPos( pos );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetScale( workScale );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->Update();
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->Render( RS_MUL , 1.0f );
#endif
}

//****************************************************
//	両方に同じ大きさ入れる
//****************************************************
void AABB::SetMin( Vector3 min )
{
	m_min		= -min;
	m_minValue	= -min;
}

//****************************************************
//	両方に同じ大きさ入れる
//****************************************************
void AABB::SetMax( Vector3 max )
{
	m_max		= max;
	m_maxValue	= max;
}

//****************************************************
//	両方に同じ大きさ入れる
//****************************************************
void AABB::SetMinAndMax( Vector3 scale )
{
	m_min		= -scale;
	m_minValue	= -scale;
	m_max		= scale;
	m_maxValue	= scale;
}

//****************************************************
//	位置情報プラス大きさにする
//****************************************************
void AABB::UpdatePos( Vector3 pos )
{
	m_min = pos + m_minValue;
	m_max = pos + m_maxValue;
}


