#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../Utilty/Utility.h"
#include "../ObjStorage/ObjStorage.h"

#include "OBB.h"

//-----------------------------------------------------
//	行列から軸設定と位置を同時に設定できる
//-----------------------------------------------------
void OBB::SetAxisAndPos( Matrix mat )
{
	m_pos = Vector3( mat._41 , mat._42 , mat._43 );
	SetAxis( mat );
}

//-----------------------------------------------------
//	行列から軸を設定できる
//-----------------------------------------------------
void OBB::SetAxis( Matrix mat )
{
	m_axisX = Vector3( mat._11 , mat._12 , mat._13 );
	m_axisY = Vector3( mat._21 , mat._22 , mat._23 );
	m_axisZ = Vector3( mat._31 , mat._32 , mat._33 );
	m_axisX.Normalize();
	m_axisY.Normalize();
	m_axisZ.Normalize();
}

//-----------------------------------------------------
//	Vector3で長さ設定
//	x.y.xのz順で長さが入る
//-----------------------------------------------------
void OBB::SetHalfLength( Vector3 lengthXYZ )
{
	m_halfLenghtX = lengthXYZ.x;
	m_halfLenghtY = lengthXYZ.y;
	m_halfLenghtZ = lengthXYZ.z;
}

//-----------------------------------------------------
//	全部同じ長さが入る
//-----------------------------------------------------
void OBB::SetHalfLength( float length )
{
	m_halfLenghtX = length;
	m_halfLenghtY = length;
	m_halfLenghtZ = length;
}

//-----------------------------------------------------
//	テストで大きさ調べたいときこいつを呼ぶ
//	メンバ変数にiexMeshとか作りたくないから毎回生成する
//	まあテストで使うだけやからいいよね
//-----------------------------------------------------
void OBB::TestRender()
{
#ifdef _DEBUG
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetPos( m_pos );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->Update();
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._11 = m_axisX.x*m_halfLenghtX;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._12 = m_axisX.y*m_halfLenghtX;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._13 = m_axisX.z*m_halfLenghtX;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._21 = m_axisY.x*m_halfLenghtY;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._22 = m_axisY.y*m_halfLenghtY;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._23 = m_axisY.z*m_halfLenghtY;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._31 = m_axisZ.x*m_halfLenghtZ;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._32 = m_axisZ.y*m_halfLenghtZ;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->TransMatrix._33 = m_axisZ.z*m_halfLenghtZ;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->Render( RS_MUL , 1.0f );
#endif
}

//-----------------------------------------------------
//	回転情報がQuaternionやったときこっち
//-----------------------------------------------------
void OBB::TestRender( Quaternion rotation )
{
#ifdef _DEBUG
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetPos( m_pos );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetScale( m_halfLenghtX , m_halfLenghtY , m_halfLenghtZ );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetRotation( rotation );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->UpdateQuaternion();
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->Render( RS_MUL , 1.0f );
#endif
}

//-----------------------------------------------------
//	高さとか調整するときに使う
//	回転情報がQuaternionやったときこっち
//-----------------------------------------------------
void OBB::TestRender( Quaternion rotation , Vector3 pos )
{
#ifdef _DEBUG
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetPos( pos );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetScale( m_halfLenghtX , m_halfLenghtY , m_halfLenghtZ );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->SetRotation( rotation );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->UpdateQuaternion();
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::BOX )->Render( RS_MUL , 1.0f );
#endif
}
