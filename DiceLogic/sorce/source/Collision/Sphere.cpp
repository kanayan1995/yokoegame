#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"
#include "../Utilty/Utility.h"
#include "../ObjStorage/ObjStorage.h"

#include "Sphere.h"

//-----------------------------------------------------
//	テストで大きさ調べたいときこいつを呼ぶ
//	メンバ変数にiexMeshとか作りたくないから毎回生成する
//	まあテストで使うだけやからいいよね
//-----------------------------------------------------
void Sphere::TestRender()
{
#ifdef _DEBUG
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::SPHERE )->SetPos( m_pos );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::SPHERE )->SetScale( m_radius );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::SPHERE )->Update();
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::SPHERE )->Render();
#endif
}

//-----------------------------------------------------
//	球やから本来いらんメソッド
//	テストで使うときあると便利やから使う
//-----------------------------------------------------
void Sphere::TestRender( Vector3 scale )
{
#ifdef _DEBUG
	Vector3 scaleAdjust = Vector3One;
	scaleAdjust = scaleAdjust + scale;
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::SPHERE )->SetPos( m_pos );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::SPHERE )->SetScale( scaleAdjust );
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::SPHERE )->Update();
	OBJSTORAGE->GetCollisionMesh( COLIISIONCHEACK::SPHERE )->Render();
#endif
}


