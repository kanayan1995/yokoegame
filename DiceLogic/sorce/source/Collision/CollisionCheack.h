//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __COLLISIONCHEACK_H__
#define __COLLISIONCHEACK_H__

#include "../Character/Character.h"
#include "../Gimmick/GimmickManager.h"

class CollisionCheack
{
private:

public:
	CollisionCheack()
	{

	}
	~CollisionCheack()
	{

	}

	void PlayerToGimmick( weak_ptr<PlayerCharacter> player , weak_ptr<GimmickManager> gimmick );	//プレイヤーとギミックでの当たったときにおこる処理はここに書く
	void Update(weak_ptr<PlayerCharacter> player , weak_ptr<GimmickManager> gimmick);			//更新　こいつを呼ぶだけであたり判定関係は全部更新してほしい
};

#endif