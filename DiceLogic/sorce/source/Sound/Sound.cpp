#include "iextreme.h"
#include "Sound.h"

//*****************************************************************************
//	音初期化・解放
//*****************************************************************************

unique_ptr<iexSound> Sound::m_se  = nullptr;
unique_ptr<iexSound> Sound::m_bgm = nullptr;

//-----------------------------------------------------
//	SE初期化
//-----------------------------------------------------
void Sound::SetSE()
{
	m_se = make_unique<iexSound>();
	m_se->Set( (int)SE::CANCEL		, "DATA/Sound/SE/Cancel.wav");
	m_se->Set( (int)SE::DECISION	, "DATA/Sound/SE/Decision.wav" );
	m_se->Set( (int)SE::DICE		, "DATA/Sound/SE/Dice.wav" );
	m_se->Set( (int)SE::WARP		, "DATA/Sound/SE/Warp.wav" );
}

//-----------------------------------------------------
//　BGM初期化
//-----------------------------------------------------
void Sound::SetBGM()
{
	m_bgm = make_unique<iexSound>();
	m_bgm->Set( (int)BGM::MAIN		, "DATA/Sound/BGM/GameMain.wav"		);
	m_bgm->Set( (int)BGM::RESULT	, "DATA/Sound/BGM/Result.wav"		);
	m_bgm->SetVolume( (int)BGM::RESULT , -800);
	m_bgm->Set( (int)BGM::SELECT	, "DATA/Sound/BGM/StageSelect.wav"	);
	m_bgm->Set( (int)BGM::TITLE		, "DATA/Sound/BGM/Title.wav"		);
}

//-----------------------------------------------------
//　まとめて初期化
//-----------------------------------------------------
void Sound::Initialize()
{
	SetSE();
	SetBGM();
}

//-----------------------------------------------------
//　まとめて初期化
//-----------------------------------------------------
void Sound::Release()
{

}

//*****************************************************************************
//	SE関連
//*****************************************************************************

//-----------------------------------------------------
//　SE再生
//	デフォルトは単発再生
//  loopしたければloopをtrueでどうぞ
//-----------------------------------------------------
void Sound::PlaySE( SE type , bool loop )
{
	int num = (int)type;
	if ( !m_se->isPlay( num ) )
	{
		m_se->Play( num , loop );
	}
}

//-----------------------------------------------------
//　SEとめてどうぞ
//-----------------------------------------------------
void Sound::StopSE( SE type )
{
	int num = (int)type;
	if ( m_se->isPlay( num ) )
	{
		m_se->Stop( num );
	}
}

//-----------------------------------------------------
//　音量調整
//	単位がどのくらいで減るのかはまだ見てない
//-----------------------------------------------------
void Sound::SetSEVolume( SE type , int volume )
{
	int num = (int)type;
	m_se->SetVolume( num , volume );
}

//*****************************************************************************
//	BGM関連
//*****************************************************************************

//-----------------------------------------------------
//　BGM再生
//	BGMやしとりあえずループさせる
//-----------------------------------------------------
void Sound::PlayBGM( BGM type )
{
	int num = (int)type;
	if ( !m_bgm->isPlay( num ) )
	{
		m_bgm->Play( num , true );
	}
}

//-----------------------------------------------------
//	BGM止めてどうぞ
//-----------------------------------------------------
void Sound::StopBGM( BGM type )
{
	int num = (int)type;
	if ( m_bgm->isPlay( num ) )
	{
		m_bgm->Stop( num );
	}
}

//-----------------------------------------------------
//　音量調整
//	単位がどのくらいで減るのかはまだ見てない
//-----------------------------------------------------
void Sound::SetBGMVolume( BGM type , int volume )
{
	int num = (int)type;
	m_bgm->SetVolume( num , volume );
}
