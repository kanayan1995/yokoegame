//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __SOUND_H__
#define __SOUND_H__

class Sound
{
private:
	static unique_ptr<iexSound> m_se;		//SE用
	static unique_ptr<iexSound> m_bgm;		//BGM用
private:
	static void SetSE();
	static void SetBGM();
public:
	Sound()
	{
	}
	~Sound()
	{
	}
	//SE
	enum class SE : int
	{
		CANCEL = 0,	//キャンセル音
		DECISION,	//決定の音
		DICE,		//サイコロ転がすときの音
		WARP,		//ワープするときの音
	};
	//BGM
	enum class BGM : int
	{
		MAIN = 0,	//ゲームメインの時
		RESULT,		//リザルト
		SELECT,		//ステージセレクト
		TITLE		//タイトル
	};

	static void Initialize();							//初期化
	static void Release();								//解放
	static void PlaySE( SE num , bool loop = false );	//SEを鳴らす ループも選択
	static void StopSE( SE num );						//強制的に止める
	static void SetSEVolume( SE num , int volume );		//SEの調整
	static void PlayBGM( BGM num );						//音を再生する
	static void StopBGM( BGM num );						//音を止める
	static void SetBGMVolume( BGM num , int volume );	//BGMの調整
};



#endif 
