#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"

#include "ObjStorage.h"

//*****************************************************************************
// シングルトンパターン	生成　初期化　解放 
//*****************************************************************************

//-----------------------------------------------------
//		シングルトンパターン生成
//-----------------------------------------------------
ObjStorage* ObjStorage::GetInstance()
{
	static ObjStorage instance;
	return &instance;
}

//-----------------------------------------------------
//		初期化
//-----------------------------------------------------
void ObjStorage::Initialize()
{
	char* fileName = "DATA/Dice/diceCollision.IMO" ;
	m_diceMesh = make_shared<iexMesh>( fileName );
	fileName = "DATA/Dice/diceBigCollision.IMO" ;
	m_diceCollisionMesh = make_shared<iexMesh>( fileName );

	m_diceTextute[ (int)DICEWARPTYPE::ONE ]		= new iex2DObj( "DATA/Dice/Dice1.png" );
	m_diceTextute[ (int)DICEWARPTYPE::SIX ]		= new iex2DObj( "DATA/Dice/Dice6.png" );
	m_diceTextute[ (int)DICEWARPTYPE::TWO ]		= new iex2DObj( "DATA/Dice/Dice2.png" );
	m_diceTextute[ (int)DICEWARPTYPE::FIVE ]	= new iex2DObj( "DATA/Dice/Dice5.png" );
	m_diceTextute[ (int)DICEWARPTYPE::THREE ]	= new iex2DObj( "DATA/Dice/Dice3.png" );
	m_diceTextute[ (int)DICEWARPTYPE::FOWR ]	= new iex2DObj( "DATA/Dice/Dice4.png" );
	m_diceTextute[ (int)DICEWARPTYPE::NONE ]	= new iex2DObj( "DATA/Dice/redNone.png" );

	//あたり判定確認用
	fileName = "DATA/Test/Mesh/Collision/sphere.x";
	m_sphere = make_shared<iexMesh>( fileName );
	fileName = "DATA/Test/Mesh/Collision/Box.IMO";
	m_box = make_shared<iexMesh>( fileName );
}

//-----------------------------------------------------
//		解放
//-----------------------------------------------------
void ObjStorage::Release()
{
	m_diceMesh.reset();
	m_diceCollisionMesh.reset();
	for ( int i = 0; i < (int)DICEWARPTYPE::MAX; i++ )
	{
		if ( m_diceTextute[ i ] )
		{
			delete m_diceTextute[ i ];
		}
	}
	m_sphere.reset();
	m_box.reset();
}

//*****************************************************************************
//	テクスチャを取得する
//*****************************************************************************
Texture2D* ObjStorage::GetDiceTexture( DICEWARPTYPE faceType )
{
	return m_diceTextute[ (int)faceType ]->GetTexture();
}

//*****************************************************************************
//	ギミックのポインタを渡す
//*****************************************************************************
shared_ptr<iexMesh> ObjStorage::GetGimmickMesh( GIMMICKTYPE gimmickType )
{

	switch ( gimmickType )
	{
		case GIMMICKTYPE::ONEPERSONDICE:
			return m_diceMesh->CloneShared();
			break;
		case GIMMICKTYPE::TWOPERSONDICE:
			return m_diceCollisionMesh->CloneShared();
			break;
		default:
			assert( !"そのギミックのメッシュは用意してないよ" );
			break;
	}
	return nullptr;
}

//*****************************************************************************
//	確認用のメッシュ
//*****************************************************************************
shared_ptr<iexMesh> ObjStorage::GetCollisionMesh( COLIISIONCHEACK collisionType )
{
	switch ( collisionType )
	{
		case COLIISIONCHEACK::SPHERE:
			return m_sphere;
			break;
		case COLIISIONCHEACK::BOX:
			return m_box;
			break;
		default:
			assert( !"そのギミックのメッシュは用意してないよ" );
			break;
	}
	return nullptr;
}

