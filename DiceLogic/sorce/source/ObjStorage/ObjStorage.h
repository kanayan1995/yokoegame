#ifndef __OBJSTORAGE_H__
#define __OBJSTORAGE_H__

#include "../Gimmick/BaseGimmick.h"

enum class COLIISIONCHEACK
{
	SPHERE ,
	BOX ,
};

class ObjStorage
{
private:
	//****************************************************
	//	メンバ変数
	//****************************************************
	shared_ptr<iexMesh>	m_diceMesh;
	shared_ptr<iexMesh>	m_diceCollisionMesh;
	iex2DObj*	m_diceTextute[ (int)DICEWARPTYPE::MAX ];	//サイコロのテクスチャ

	//あたり判定をチェックする時に使うメッシュ
	shared_ptr<iexMesh> m_sphere;
	shared_ptr<iexMesh> m_box;
private:
	//****************************************************
	//	シングルトンパターン
	//****************************************************
	ObjStorage()
	{
		for ( int i = 0; i < (int)DICEWARPTYPE::MAX; i++ )
		{
			m_diceTextute[ i ] = nullptr;
		}
	}
	ObjStorage( const ObjStorage& org ){}
	ObjStorage& operator=(const ObjStorage& org ){}
public:
	static ObjStorage* GetInstance();
public:
	//****************************************************
	//	メンバ関数
	//****************************************************
	void Initialize();
	void Release();
	Texture2D* GetDiceTexture(DICEWARPTYPE faceType);
	shared_ptr<iexMesh> GetGimmickMesh( GIMMICKTYPE gimmickType );
	shared_ptr<iexMesh> GetCollisionMesh( COLIISIONCHEACK collisionType );
};

#define OBJSTORAGE ObjStorage::GetInstance()

#endif