#include "iextreme.h"
#include "iextremePlus.h"

#include "Input.h"

//*****************************************************************************
//	シングルトン実装　初期化　解放
//*****************************************************************************

//-----------------------------------------------------
//		シングルトン実装
//-----------------------------------------------------
InputControler* InputControler::GetInstance()
{
	static InputControler instance;
	return &instance;
}

//-----------------------------------------------------
//		解放
//-----------------------------------------------------
void InputControler::Release()
{

}

//*****************************************************************************
//	コントローラーでボタン設定を合わせる
//*****************************************************************************

void InputControler::SetControlerType( CONTROLERTYPE type )
{
	switch ( type )
	{
		case InputControler::CONTROLERTYPE::PS4CONTROLER:
			PS4ControlerInitialize();
			break;
		default:
			break;
	}
}

//-----------------------------------------------------
//	PS4コントローラーの時に使う初期化
//-----------------------------------------------------
void InputControler::PS4ControlerInitialize()
{
	AnalogUp	= KEY_UP;
	AnalogDown	= KEY_DOWN;
	AnalogRight	= KEY_RIGHT;
	AnalogLeft	= KEY_LEFT;
	Circle		= KEY_B;
	Cross		= KEY_A;
	Square		= KEY_C;
	Triangle	= KEY_D;
	L1			= KEY_L2;
	L2			= KEY_L1;
	R1			= KEY_R2;
	R2			= KEY_R1;
	Share		= KEY_START;
	Options		= KEY_SELECT;
}

//*****************************************************************************
//	スティック関連
//*****************************************************************************

//-----------------------------------------------------
//	左スティック用関数
//-----------------------------------------------------

float InputControler::AxisLeftStickX()
{
	float Axis1X = (float)KEY_GetAxisX() / 1000.0f;
	if ( Axis1X < InvalidArea && Axis1X > -InvalidArea )Axis1X = 0.0f;
	return Axis1X;
}

float InputControler::AxisLeftStickY()
{
	float Axis1Y = (float)KEY_GetAxisY() / 1000.0f;
	if ( Axis1Y < InvalidArea && Axis1Y > -InvalidArea )Axis1Y = 0.0f;
	return Axis1Y;
}

int InputControler::AxisLeftStickPushX()
{
	float Axis1X = (float)KEY_GetAxisX() / 1000.0f;
	if ( Axis1X > InvalidArea *2  )			return 1;
	else if ( Axis1X < -InvalidArea * 2 )	return -1;
	else									return 0;
}

int InputControler::AxisLeftStickPushY()
{
	float Axis1Y = (float)KEY_GetAxisY() / 1000.0f;
	if ( Axis1Y > InvalidArea * 2 )		return -1;
	else if ( Axis1Y < -InvalidArea * 2 )	return 1;
	else								    return 0;
}

//-----------------------------------------------------
//	右スティック関連
//-----------------------------------------------------

float InputControler::AxisRightStickX()
{
	float Axis2X = (float)KEY_GetAxisX2() / 1000.0f;
	if ( Axis2X < InvalidArea && Axis2X > -InvalidArea )Axis2X = 0.0f;
	return Axis2X;
}

float InputControler::AxisRightStickY()
{
	float Axis2Y = (float)KEY_GetAxisY2() / 1000.0f;
	if ( Axis2Y < InvalidArea && Axis2Y > -InvalidArea )Axis2Y = 0.0f;
	return Axis2Y;
}



//-----------------------------------------------------
//	アナログスティックを倒したか倒してないかを判定する
//-----------------------------------------------------
bool InputControler::AxisLeftStickRightPush()
{
	float AxisX = (float)KEY_GetAxisX() / 1000.0f;
	if ( AxisX > InvalidArea * 3 )
	{
		if ( pushFlag[ 0 ] == true ) return false;
		pushFlag[ 0 ] = true;
		return true;
	}
	else
	{
		pushFlag[ 0 ] = false;
		return false;
	}

}

bool InputControler::AxisLeftStickLeftPush()
{
	float AxisX = (float)KEY_GetAxisX() / 1000.0f;
	if ( AxisX < -InvalidArea * 3 )
	{
		if ( pushFlag[ 1 ] == true ) return false;
		pushFlag[ 1 ] = true;
		return true;
	}
	else
	{
		pushFlag[ 1 ] = false;
		return false;
	}
}

bool InputControler::AxisLeftStickUpPush()
{
	float AxisY = (float)KEY_GetAxisY() / 1000.0f;
	if ( AxisY < -InvalidArea * 3 )
	{
		if ( pushFlag[ 2 ] == true ) return false;
		pushFlag[ 2 ] = true;
		return true;
	}
	else
	{
		pushFlag[ 2 ] = false;
		return false;
	}
}

bool InputControler::AxisLeftStickDownPush()
{
	float AxisY = (float)KEY_GetAxisY() / 1000.0f;
	if ( AxisY > InvalidArea * 3 )
	{
		if ( pushFlag[ 3 ] == true ) return false;
		pushFlag[ 3 ] = true;
		return true;
	}
	else
	{
		pushFlag[ 3 ] = false;
		return false;
	}
}
//-----------------------------------------------------
//	アナログスティックを倒したか倒してないかを判定する
//-----------------------------------------------------
bool InputControler::AxisLeftStickRightHold()
{
	float AxisX = (float)KEY_GetAxisX() / 1000.0f;
	if ( AxisX > InvalidArea * 3 ) return true;
	return false;
}

bool InputControler::AxisLeftStickLeftHold()
{
	float AxisX = (float)KEY_GetAxisX() / 1000.0f;
	if ( AxisX < -InvalidArea * 3 ) return true;
	return false;
}

bool InputControler::AxisLeftStickUpHold()
{
	float AxisY = (float)KEY_GetAxisY() / 1000.0f;
	if ( AxisY < -InvalidArea * 3 ) return true;
	return false;
}

bool InputControler::AxisLeftStickDownHold()
{
	float AxisY = (float)KEY_GetAxisY() / 1000.0f;
	if ( AxisY > InvalidArea * 3 ) return true;
	return false;
}


//*****************************************************************************
//	アナログスティック関連
//*****************************************************************************

//-----------------------------------------------------
//		アナログスティック上ボタン関連
//-----------------------------------------------------
bool InputControler::AnalogStickUpPush()
{
	if ( KEY_Get( AnalogUp ) == 3 ) return true;
	return false;
}

bool InputControler::AnalogStickUpRelease()
{
	if ( KEY_Get( AnalogUp ) == 2 ) return true;
	return false;
}

bool InputControler::AnalogStickUpHold()
{
	if ( KEY_Get( AnalogUp ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		アナログスティック下ボタン関連
//-----------------------------------------------------
bool InputControler::AnalogStickDownPush()
{
	if ( KEY_Get( AnalogDown ) == 3 ) return true;
	return false;
}

bool InputControler::AnalogStickDownRelease()
{
	if ( KEY_Get( AnalogDown ) == 2 ) return true;
	return false;
}

bool InputControler::AnalogStickDownHold()
{
	if ( KEY_Get( AnalogDown ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		アナログスティック右ボタン関連
//-----------------------------------------------------
bool InputControler::AnalogStickRightPush()
{
	if ( KEY_Get( AnalogRight ) == 3 ) return true;
	return false;
}

bool InputControler::AnalogStickRightRelease()
{
	if ( KEY_Get( AnalogRight ) == 2 ) return true;
	return false;
}

bool InputControler::AnalogStickRightpHold()
{
	if ( KEY_Get( AnalogRight ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		アナログスティック左ボタン関連
//-----------------------------------------------------
bool InputControler::AnalogStickLeftPush()
{
	if ( KEY_Get( AnalogLeft ) == 3 ) return true;
	return false;
}

bool InputControler::AnalogStickLeftRelease()
{
	if ( KEY_Get( AnalogLeft ) == 2 ) return true;
	return false;
}

bool InputControler::AnalogStickLeftpHold()
{
	if ( KEY_Get( AnalogLeft ) == 1 ) return true;
	return false;
}

//*****************************************************************************
//	L1 L2 R1 R2 関連
//*****************************************************************************
//-----------------------------------------------------
//		L1関連
//-----------------------------------------------------
bool InputControler::L1ButtonPush()
{
	if ( KEY_Get( L1 ) == 3 ) return true;
	return false;
}

bool InputControler::L1ButtontRelease()
{
	if ( KEY_Get( L1 ) == 2 ) return true;
	return false;
}

bool InputControler::L1ButtontpHold()
{
	if ( KEY_Get( L1 ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		L2関連
//-----------------------------------------------------
bool InputControler::L2ButtonPush()
{
	if ( KEY_Get( L2 ) == 3 ) return true;
	return false;
}

bool InputControler::L2ButtontRelease()
{
	if ( KEY_Get( L2 ) == 2 ) return true;
	return false;
}

bool InputControler::L2ButtontpHold()
{
	if ( KEY_Get( L2 ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		R1関連
//-----------------------------------------------------
bool InputControler::R1ButtonPush()
{
	if ( KEY_Get( R1 ) == 3 ) return true;
	return false;
}

bool InputControler::R1ButtonRelease()
{
	if ( KEY_Get( R1 ) == 2 ) return true;
	return false;
}

bool InputControler::R1ButtonHold()
{
	if ( KEY_Get( R1 ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		R2関連
//-----------------------------------------------------
bool InputControler::R2ButtonPush()
{
	if ( KEY_Get( R2 ) == 3 ) return true;
	return false;
}

bool InputControler::R2ButtontRelease()
{
	if ( KEY_Get( R2 ) == 2 ) return true;
	return false;
}

bool InputControler::R2ButtontpHold()
{
	if ( KEY_Get( R2 ) == 1 ) return true;
	return false;
}

//*****************************************************************************
//	○　×　□　△ボタン関連
//*****************************************************************************
//-----------------------------------------------------
//		○ボタン
//-----------------------------------------------------
bool InputControler::CircleButtonPush()
{
	if ( KEY_Get( Circle ) == 3 ) return true;
	return false;
}

bool InputControler::CircleButtontRelease()
{
	if ( KEY_Get( Circle ) == 2 ) return true;
	return false;
}

bool InputControler::CircleButtontpHold()
{
	if ( KEY_Get( Circle ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		×ボタン
//-----------------------------------------------------
bool InputControler::CrossButtonPush()
{
	if ( KEY_Get( Cross ) == 3 ) return true;
	return false;
}

bool InputControler::CrossButtontRelease()
{
	if ( KEY_Get( Cross ) == 2 ) return true;
	return false;
}

bool InputControler::CrossButtontpHold()
{
	if ( KEY_Get( Cross ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		□ボタン
//-----------------------------------------------------
bool InputControler::SquareButtonPush()
{
	if ( KEY_Get( Square ) == 3 ) return true;
	return false;
}

bool InputControler::SquareButtontRelease()
{
	if ( KEY_Get( Square ) == 2 ) return true;
	return false;
}

bool InputControler::SquareButtontpHold()
{
	if ( KEY_Get( Square ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		□ボタン
//-----------------------------------------------------
bool InputControler::TriangleButtonPush()
{
	if ( KEY_Get( Triangle ) == 3 ) return true;
	return false;
}

bool InputControler::TriangleButtontRelease()
{
	if ( KEY_Get( Triangle ) == 2 ) return true;
	return false;
}

bool InputControler::TriangleButtontpHold()
{
	if ( KEY_Get( Triangle ) == 1 ) return true;
	return false;
}

//*****************************************************************************
//	Share Option
//*****************************************************************************
//-----------------------------------------------------
//		Shareボタン
//-----------------------------------------------------
bool InputControler::ShareButtonPush()
{
	if ( KEY_Get( Share ) == 3 ) return true;
	return false;
}

bool InputControler::ShareButtontRelease()
{
	if ( KEY_Get( Share ) == 2 ) return true;
	return false;
}

bool InputControler::ShareButtontpHold()
{
	if ( KEY_Get( Share ) == 1 ) return true;
	return false;
}

//-----------------------------------------------------
//		Optionsボタン
//-----------------------------------------------------
bool InputControler::OptionsButtonPush()
{
	if ( KEY_Get( Options ) == 3 ) return true;
	return false;
}

bool InputControler::OptionsButtontRelease()
{
	if ( KEY_Get( Options ) == 2 ) return true;
	return false;
}

bool InputControler::OptionsButtontpHold()
{
	if ( KEY_Get( Options ) == 1 ) return true;
	return false;
}







//*****************************************************************************
//　　　ボタン入力
//*****************************************************************************

////-----------------------------------------------------
////	ボタンを押したとき
////-----------------------------------------------------
//bool InputControler::BottonPush( char button )
//{
//	if ( KEY_Get( button ) == 3 ) return true;
//	return false;
//}
//
////-----------------------------------------------------
////	ボタンを話したとき
////-----------------------------------------------------
//bool InputControler::BottonRelease( char button )
//{
//	if ( KEY_Get( button ) == 2 ) return true;
//	return false;
//}
//
////-----------------------------------------------------
////	ボタンを押し続けたとき
////-----------------------------------------------------
//bool InputControler::BottonHold( char button )
//{
//	if ( KEY_Get( button ) == 1 ) return true;
//	return false;
//}
