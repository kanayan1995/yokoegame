//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __INPUTCONTROLER_H__
#define __INPUTCONTROLER_H__

//*************************************************************
//	PS4コントローラー用に作ったクラス
//*************************************************************
class InputControler
{
private:
	//*************************************************************
	//	メンバ変数
	//*************************************************************
	float InvalidArea;			//アナログスティックの遊び
	bool  pushFlag[ 4 ];		//共通にしてると怖いから4つ作る

	char AnalogUp;		//アナログ上
	char AnalogDown;	//アナログ下
	char AnalogLeft;	//アナログ左
	char AnalogRight;	//アナログ右
	char Circle;		//○
	char Cross;			//×
	char Square;		//□
	char Triangle;		//△
	char L1;			//L1ボタン
	char L2;			//L2ボタン
	char R1;			//R1ボタン
	char R2;			//R2ボタン
	char Share;			//SHAREボタン
	char Options;		//OPTIONSボタン
public:
	enum  class CONTROLERTYPE	//追加した時用
	{
		PS4CONTROLER,	//PS4コントローラーに合わせて初期化
	};
private:
	//*************************************************************
	//	シングルトン実装
	//*************************************************************
	InputControler() 
	{ 
		InvalidArea = 0.1f; 
		for ( int i = 0; i < 4; i++ )
		{
			pushFlag[i] = false;
		}
	}
	InputControler(const InputControler& org){}
	InputControler& operator=(const InputControler& org ){}

private:
	//*************************************************************
	//	メンバ関数
	//*************************************************************
	void PS4ControlerInitialize();			//コントローラー初期化
public:
	static InputControler* GetInstance();	//インスタンス取得
	void Release();							//解放

	//コントローラー初期化
	void SetControlerType( CONTROLERTYPE type );

	//*********************************************
	//	スティック関連
	//*********************************************	
	//アナログスティックの遊びを入れる　
	void SetInvalid(float invalid) { InvalidArea = invalid; }
	//左のスティック 
	//どれくらい倒したかの値を取得する
	float AxisLeftStickX();
	float AxisLeftStickY();
	//右のスティック
	float AxisRightStickX();
	float AxisRightStickY();
	//int型でどっちに向いているかだけ欲しい
	int AxisLeftStickPushX();
	int AxisLeftStickPushY();	//スティックは上を押すと-が返る(Xとは逆)

	//押したか押してないかを判定する
	bool AxisLeftStickRightPush();
	bool AxisLeftStickLeftPush();
	bool AxisLeftStickUpPush();		//上スティックを押すと−が入る
	bool AxisLeftStickDownPush();	//下スティックを押すと+が入る
	//押し続ける
	bool AxisLeftStickRightHold();
	bool AxisLeftStickLeftHold();
	bool AxisLeftStickUpHold();		//上スティックを押すと−が入る
	bool AxisLeftStickDownHold();	//下スティックを押すと+が入る

	//*********************************************
	//	アナログスティック関連
	//*********************************************
	//上ボタン
	bool AnalogStickUpPush(); 
	bool AnalogStickUpRelease(); 
	bool AnalogStickUpHold();
	//下ボタン
	bool AnalogStickDownPush();
	bool AnalogStickDownRelease();
	bool AnalogStickDownHold();
	//右ボタン
	bool AnalogStickRightPush();
	bool AnalogStickRightRelease();
	bool AnalogStickRightpHold();
	//左ボタン
	bool AnalogStickLeftPush();
	bool AnalogStickLeftRelease();
	bool AnalogStickLeftpHold();

	//*********************************************
	//	L1 L2 R1 R2 ボタン
	//*********************************************
	//L1
	bool L1ButtonPush();
	bool L1ButtontRelease();
	bool L1ButtontpHold();
	//L2
	bool L2ButtonPush();
	bool L2ButtontRelease();
	bool L2ButtontpHold();
	//R1
	bool R1ButtonPush();
	bool R1ButtonRelease();
	bool R1ButtonHold();
	//R2 
	bool R2ButtonPush();
	bool R2ButtontRelease();
	bool R2ButtontpHold();

	//*********************************************
	//	○　×　□　△ボタン関連
	//*********************************************
	//○ボタン
	bool CircleButtonPush();
	bool CircleButtontRelease();
	bool CircleButtontpHold();
	//×ボタン
	bool CrossButtonPush();
	bool CrossButtontRelease();
	bool CrossButtontpHold();
	//□ボタン
	bool SquareButtonPush();
	bool SquareButtontRelease();
	bool SquareButtontpHold();
	//△ボタン
	bool TriangleButtonPush();
	bool TriangleButtontRelease();
	bool TriangleButtontpHold();

	//*********************************************
	//	Share Optionsボタン
	//*********************************************
	//Shareボタン
	bool ShareButtonPush();
	bool ShareButtontRelease();
	bool ShareButtontpHold();
	//Optionボタン
	bool OptionsButtonPush();
	bool OptionsButtontRelease();
	bool OptionsButtontpHold();

};

#define GAMECONTROLER InputControler::GetInstance() 

#endif 
