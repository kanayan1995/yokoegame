//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//
#ifndef __CAMERA_H__
#define __CAMERA_H__

class PlayerCharacter;
class GimmickManager;

//*************************************************************
//	Cameraクラス
//*************************************************************
class Camera
{
private:
	unique_ptr<iexView>	m_view;	//カメラ

	float	m_viewAngle;		//カメラの向き
	float	m_viewHeight;		//カメラの高さ
	float	m_viewHeightMax;	//最大の高さ
	float	m_viewHeightMin;	//最少の高さ
	float	m_viewDist;			//ターゲットとの距離
	float	m_viewDistMax;		//最大距離
	float	m_viewDistMin;		//最少距離

	Vector3 m_pos;				//カメラの位置
	Vector3 m_target;			//こいつを視点にして見る
	Vector3 m_topPos;			//初めは高い所から見ていたい

	weak_ptr<PlayerCharacter>	m_player;			//プレイヤーのポインタ
	weak_ptr<GimmickManager>	m_gimmickManager;	//ギミックマネージャー(位置取りたいから)

	int m_targetCount;			//これが増えるとターゲットが変わる
	int m_gimmckCountMax;		//ギミックが何個あるか？

	Vector3 m_keepPos;			//動く前のカメラの位置保存
	float m_moveFrame;			//ギミックカメラを動かすカウント
	float m_moveFrameMax;			//ギミックカメラを動かす時に使う時間


public:
	enum class CAMERAMODE
	{
		PLAYER ,		//プレイヤーを視点にする
		FREE,			//事由に動かす
		GIMMICK,		//ギミックを選択する
		WARP,			//ワープ先を見るカメラ
	}m_cameraMode;

	enum  class WARPCAMERASTATE
	{
		PLAYER,		//全体を見るため初めはそのまま
		WARPPOS,	//移動を指すとワープ先へ
	}m_warpState;
public:
	Camera()
	{
		m_view = nullptr;
		m_viewAngle		= 0;
		m_viewHeight	= 0;
		m_viewHeightMax = 0;
		m_viewHeightMin = 0;
		m_viewDist		= 0;
		m_viewDistMax	= 0;
		m_viewDistMin	= 0;
		m_pos	 = Vector3Zero;
		m_target = Vector3Zero;
		m_player.lock()			= nullptr;
		m_gimmickManager.lock() = nullptr;
		m_targetCount = 0;
		m_gimmckCountMax = 0;
		m_cameraMode = CAMERAMODE::PLAYER;
	}
	~Camera()
	{
	}

	void Initialize();						//初期化
	void Update();							//更新
	void Activate() { m_view->Activate(); }	//開始
	void Clear()	{ m_view->Clear(); }	//一回消す

	void ClearZ(DWORD color = 0, bool bClearZ = true);		//視界クリア(橋口)
	void SetViewport();										//投影平面設定(橋口)
	void SetProjection(float FovY, float Near, float Far);	//投影設定(橋口)

private:
	void TargetLookAround();	//ターゲットの周りをきょろきょろする
	void TargetChange();		//ターゲットを切り替える

	void ChangeCameraMode();	//カメラのモードをボタンで切り替える
	void PlayerCamera();		//プレイヤーを視点にして動かすカメラ
	void FreeCamera();			//カメラを自由に動かす
	void WarpCamera();			//ワープカメラ　ワープ先を見つめる
	void CameraMode();			//カメラをモード分けする
public: //-----セッター-----//
	void SetTarget( Vector3 target )	{ m_target = target; }
	void SetPos( Vector3 pos )			{ m_pos = pos; }
	void SetTopPos( Vector3 top )		{ m_topPos = top; }
	void SetPlayerPointer( shared_ptr<PlayerCharacter> player )					{ m_player = player; }
	void SetGimmickManagerPointer( shared_ptr<GimmickManager> gimmickManager )	{ m_gimmickManager = gimmickManager; }
public: //-----ゲッター-----//
	Vector3 GetTarget()					{ return m_target; }	//ターゲット
	Vector3 GetPos()					{ return m_pos; }		//カメラの位置
	Vector3 GetCameraForTargetVec()
	{
		Vector3 workVec;
		workVec = m_target - m_pos;
		workVec.Normalize();
		return workVec;
	}
	CAMERAMODE GetCameraMode() { return m_cameraMode; }
};



#endif 