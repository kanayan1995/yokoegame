#include "iextreme.h"
#include "iextremePlus.h"
#include "system/system.h"

#include "Input/Input.h"
#include "../Gimmick/GimmickManager.h"
#include "../Character/Character.h"
#include "Camera.h"


//*****************************************************************************
//	初期化
//*****************************************************************************
void Camera::Initialize()
{
	m_view = make_unique<iexView>();

	//	視点初期設定
	m_viewAngle = 0.7f;
	m_viewHeight = 80;
	m_viewHeightMax = 100;
	m_viewHeightMin = 50;
	m_viewDist = -90;
	m_viewDistMax = -100;
	m_viewDistMin = -50;


	m_topPos = Vector3( 0 ,200 , -10.0f );
	m_pos = Vector3( 1 , 10 , 1 );
	m_target = Vector3Zero;

	m_gimmckCountMax = m_gimmickManager.lock()->GetGimmickCount() - 1;

	m_moveFrame = 0;
	m_moveFrameMax = 60;
}

//*****************************************************************************
//	Cameraを動かしたりする関連
//*****************************************************************************

//-----------------------------------------------------
//		キーパッドで動かす
//-----------------------------------------------------
void Camera::TargetLookAround()
{
	//距離を調整する
	if ( GAMECONTROLER->L2ButtontpHold() )
	{
		m_viewDist++;
	}
	if ( GAMECONTROLER->R2ButtontpHold() )
	{
		m_viewDist--;
	}
	if ( m_viewDistMax > m_viewDist ) m_viewDist = m_viewDistMax;
	if ( m_viewDistMin < m_viewDist ) m_viewDist = m_viewDistMin;

	//高さを変更する
	m_viewHeight -= GAMECONTROLER->AxisRightStickY() * 1.5f;
	if ( m_viewHeightMax < m_viewHeight ) m_viewHeight = m_viewHeightMax;
	if ( m_viewHeightMin > m_viewHeight ) m_viewHeight = m_viewHeightMin;

	//回転する
	m_viewAngle += GAMECONTROLER->AxisRightStickX() * 0.06f;
}

//-----------------------------------------------------
//	カメラ切り替え
//	ギミックマネージャー位置取得するときは0から取りたいから-1する
//-----------------------------------------------------
void Camera::TargetChange()
{
	if ( GAMECONTROLER->R1ButtonPush() )
	{
		m_moveFrame = 0;
		m_keepPos = m_target;
		m_targetCount++;
		if ( m_targetCount > m_gimmckCountMax ) m_targetCount = 0;
	}
	if ( GAMECONTROLER->L1ButtonPush() )
	{
		m_moveFrame = 0;
		m_keepPos = m_target;
		m_targetCount--;
		if ( m_targetCount < 0 ) m_targetCount = m_gimmckCountMax;
	}

	Vector3 gimmickPos = m_gimmickManager.lock()->GetGimmckPos( m_targetCount );

	m_moveFrame += 1.3f;
	if ( m_moveFrame <= m_moveFrameMax )
	{
		m_target = Vector3Lerp( m_keepPos , gimmickPos , 0 , m_moveFrameMax , m_moveFrame , EaseOut );
	}

	m_pos = m_target + Vector3( sinf( m_viewAngle )*m_viewDist , m_viewHeight , cosf( m_viewAngle )*m_viewDist );
}

//-----------------------------------------------------
//	カメラモードをボタンで分ける
//-----------------------------------------------------
void Camera::ChangeCameraMode()
{
	//プレイヤーカメラ
	if ( GAMECONTROLER->CrossButtonPush() )
	{
		m_cameraMode = CAMERAMODE::PLAYER;
	}
	//フリーカメラ
	if ( GAMECONTROLER->TriangleButtonPush() )
	{
		m_cameraMode = CAMERAMODE::FREE;
	}
	//ギミックカメラ
	if ( GAMECONTROLER->L1ButtonPush()
		|| GAMECONTROLER->R1ButtonPush() )
	{
		m_cameraMode = CAMERAMODE::GIMMICK;
	}
	//ワープカメラ
	if ( m_player.lock()->GetWarpState() == WARPSTATE::WARPCHOICE )
	{
		m_cameraMode = CAMERAMODE::WARP;
	}
}

//-----------------------------------------------------
//	プレイヤーを視点に見るカメラ
//-----------------------------------------------------
void Camera::PlayerCamera()
{
	m_target = m_player.lock()->GetPos();
	m_pos = m_target + Vector3( sinf( m_viewAngle )*m_viewDist , m_viewHeight , cosf( m_viewAngle )*m_viewDist );
}

//-----------------------------------------------------
//	カメラを自由に動かす
//	視点ごと動かす感じ
//-----------------------------------------------------
void Camera::FreeCamera()
{
	Vector3 move( GAMECONTROLER->AxisLeftStickX() , 0 , -GAMECONTROLER->AxisLeftStickY() );
	Vector3 cameraVec = Vector3Zero;
	cameraVec = m_target - m_pos;
	cameraVec.Normalize();
	cameraVec.y = 0;

	if ( GAMECONTROLER->AxisLeftStickX() != 0 )
	{		
		Vector3 right;
		Vector3Cross( right , Vector3Up , cameraVec );
		right.Normalize();
		right *= GAMECONTROLER->AxisLeftStickX();
		m_pos += right;
		m_target += right;
	}
	if ( GAMECONTROLER->AxisLeftStickY() != 0 )
	{	
		cameraVec *= -GAMECONTROLER->AxisLeftStickY();
		cameraVec = cameraVec;
		m_pos    += cameraVec;
		m_target += cameraVec;
	}
	m_pos = m_target + Vector3( sinf( m_viewAngle )*m_viewDist , m_viewHeight , cosf( m_viewAngle )*m_viewDist );
}

//-----------------------------------------------------
//	ワープ先を見つめるように
//-----------------------------------------------------
void Camera::WarpCamera()
{
	//switch ( m_warpState )
	//{
	//	case WARPCAMERASTATE::PLAYER:
	//		m_target = m_player.lock()->GetPos();
	//		m_pos = m_target + Vector3( sinf( m_viewAngle )*m_viewDist , m_viewHeight , cosf( m_viewAngle )*m_viewDist );
	//		break;
	//	case WARPCAMERASTATE::WARPPOS:
	//		break;
	//	default:
	//		break;
	//}
	m_pos = m_topPos;
	m_target = Vector3Zero;

	if ( m_player.lock()->GetWarpState() == WARPSTATE::WARPCHEACK )
	{
		m_cameraMode = CAMERAMODE::PLAYER;
		m_pos = m_player.lock()->GetPos() + m_player.lock()->GetForwardVec() * 4;
	}
}

//-----------------------------------------------------
//	カメラのモードを分ける
//-----------------------------------------------------
void Camera::CameraMode()
{
	switch ( m_cameraMode )
	{
		case CAMERAMODE::PLAYER:
			PlayerCamera();
			break;
		case CAMERAMODE::FREE:
			FreeCamera();
			break;
		case CAMERAMODE::GIMMICK:	
			TargetChange();
			break;
		case CAMERAMODE::WARP:
			WarpCamera();
			break;
		default:
			assert( !"そんなカメラモードないよ" );
			m_target = m_player.lock()->GetPos();
			break;
	}
}

//*****************************************************************************
//	更新
//*****************************************************************************
void Camera::Update()
{
	ChangeCameraMode();
	TargetLookAround();
	CameraMode();

	Vector3 posUp = Vector3Up * 5;
	m_view->Set( m_pos - posUp , m_target + posUp );

	char str[ 64 ];	
	sprintf( str , "%9.3f\n" , m_viewDist );
	IEX_DrawText( str , 0 , 100 , 40 , 40 , 0xFFFFFFFF );	
}


//*****************************************************************************
//	Set関連
//*****************************************************************************

//*******************************************************************************************************
//	橋口追加
//*******************************************************************************************************
//*****************************************************************************
//		視界クリア
//*****************************************************************************
//------------------------------------------------------
//	カラーバッファ＋Ｚバッファクリア
//------------------------------------------------------
void Camera::ClearZ( DWORD color, bool bClearZ )
{
	DWORD	flag = D3DCLEAR_TARGET;

	if( bClearZ ) flag |= D3DCLEAR_ZBUFFER;
	iexSystem::GetDevice()->Clear( 0, NULL, flag, color, 1.0f, 0 );
}

//*****************************************************************************
//		投影平面設定
//*****************************************************************************
void Camera::SetViewport()
{
	m_view->SetViewport();
}

void Camera::SetProjection(float FovY, float Near, float Far)
{
	m_view->SetProjection(FovY, Near, Far);
}