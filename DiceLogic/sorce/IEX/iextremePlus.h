//----------------------------------------------------------------------------------//
//																					//
//								製作者	:　横江										//
//																					//
//---------------------------------------------------------------------------------	//

#ifndef __IEXTREMEPLUS_H__
#define __IEXTREMEPLUS_H__

#include "iextreme.h"

//*****************************************************************************
//		基本的に共通して使うものをここに書きたい
//*****************************************************************************
#define FPS(second)		(second*(FLOAT)60.0f);		//秒でタイムとかを作りたいならこれをかける

//*****************************************************************************************************************************
//
//		数学関連
//
//*****************************************************************************************************************************

#define	PI					((FLOAT) 3.141592654f)
#define HALF_PI				((FLOAT) PI/2)
#define TO_DEGREE(radian)	(radian * (FLOAT) 180 / PI)		//ラジアンを度に変換
#define TO_RADIAN(degree)	(degree * (FLOAT) PI / 180)		//度からラジアンに変換

//-----------------------------------------------------
//	極端な方向に補正する
//	中途半端は困るので補正する ↑→↓←のどれかにする
//	正規化していれてね
//	ななめから入った場合は転がるのを拒否
//-----------------------------------------------------
inline Vector3 DirCorrection( Vector3 normalVec )
{
	Vector3 workVec = normalVec;
	if ( workVec.x > 0.5f  && workVec.z > 0.5f )  //右上斜め
	{
		if ( workVec.x > workVec.z )
		{
			workVec.x = 1.0f;
			workVec.z = 0.0f;
		}
		else if ( workVec.z > workVec.x )
		{
			workVec.x = 0.0f;
			workVec.z = 1.0f;
		}
	}
	if ( workVec.x < -0.5f && workVec.z < -0.5f ) //左下斜め
	{
		if ( workVec.x < workVec.z )
		{
			workVec.x = -1.0f;
			workVec.z = 0.0f;
		}
		else if ( workVec.z < workVec.x )
		{
			workVec.x = 0.0f;
			workVec.z = -1.0f;
		}
	}
	if ( workVec.x > 0.5f  && workVec.z < -0.5f ) //右下斜め
	{
		if ( workVec.x > -workVec.z )
		{
			workVec.x = 1.0f;
			workVec.z = 0.0f;
		}
		else if ( workVec.x < -workVec.z )
		{
			workVec.x = 0.0f;
			workVec.z = -1.0f;
		}
	}
	if ( workVec.x < -0.5f && workVec.z > 0.5f )//左上斜め
	{
		if ( -workVec.x > workVec.z )
		{
			workVec.x = -1.0f;
			workVec.z = 0.0f;
		}
		else if ( -workVec.x < workVec.z )
		{
			workVec.x = 0.0f;
			workVec.z = 1.0f;
		}
	}

	//斜めじゃないとき
	if ( workVec.x > 0.5f )
	{
		workVec.x = 1.0f;
		workVec.z = 0.0f;
	}
	else if ( workVec.x < -0.5f )
	{
		workVec.x = -1.0f;
		workVec.z = 0.0f;
	}
	else if ( workVec.z > 0.5f )
	{
		workVec.z = 1.0f;
		workVec.x = 0.0f;
	}
	else if ( workVec.z < -0.5f )
	{
		workVec.z = -1.0f;
		workVec.x = 0.0f;
	}
	return workVec;
}

//------------------------------------------------------
//	距離を取得
//------------------------------------------------------
inline float Vector3Distance( Vector3 v1 , Vector3 v2 )
{
	Vector3 dist = v1 - v2;
	return  dist.Length();
}

//------------------------------------------------------
//	絶対値を出す
//------------------------------------------------------
inline Vector3 Vector3Abs( Vector3 vec )
{
	Vector3 workVec = vec;
	workVec.x = fabsf( vec.x );
	workVec.y = fabsf( vec.y );
	workVec.z = fabsf( vec.z );
	return workVec;
}

//*****************************************************************************
//	よく使う値を宣言
//*****************************************************************************
#define Vector3Zero		Vector3( 0 , 0 , 0 )
#define Vector3One		Vector3( 1 , 1 , 1 )
#define Vector3Up		Vector3( 0 , 1 , 0 )
#define Vector3Down		Vector3( 0 ,-1 , 0 )
#define Vector3Right	Vector3( 1 , 0 , 0 )
#define Vector3Left		Vector3(-1 , 0 , 0 )
#define Vector3Forward	Vector3( 0 , 0 , 1 )
#define Vector3Back		Vector3( 0 , 0 ,-1 )

//*****************************************************************************
//	座標変換
//*****************************************************************************

//------------------------------------------------------
//	指定した空間に持っていく
//	位置は考えないものとする
//------------------------------------------------------
inline Vector3 Transform( Vector3& out , Matrix& mat )
{
	Vector3 workOut;
	workOut.x = out.x*mat._11 + out.y*mat._21 + out.z*mat._31;
	workOut.y = out.x*mat._12 + out.y*mat._22 + out.z*mat._32;
	workOut.z = out.x*mat._13 + out.y*mat._23 + out.z*mat._33;
	return workOut;
}

//------------------------------------------------------
//	指定した空間に持っていく
//	位置も一緒に移動させる
//------------------------------------------------------
inline Vector3 TransformPos( Vector3& out , Matrix& mat )
{
	Vector3 workOut;
	workOut.x = out.x*mat._11 + out.y*mat._21 + out.z*mat._31 + mat._41*1.0f;
	workOut.y = out.x*mat._12 + out.y*mat._22 + out.z*mat._32 + mat._42*1.0f;
	workOut.z = out.x*mat._13 + out.y*mat._23 + out.z*mat._33 + mat._43*1.0f;
	return workOut;
}

//補間関数
//http://marupeke296.com/TIPS_No19_interpolation.html
//*****************************************************************************
//	Lerp関数
//*****************************************************************************

//------------------------------------------------------
//	線形補間
//------------------------------------------------------
inline float LinearInterpolation( float time )
{
	return time;
}

//------------------------------------------------------
//	最初遅く、あとは早く
//------------------------------------------------------
inline float EaseIn( float time )
{
	return time*time;
}

//------------------------------------------------------
//	最初早い、あとは遅く
//------------------------------------------------------
inline float EaseOut( float time )
{
	return (time*2) - (time*time);
}

//------------------------------------------------------
//	緩やかにスタートして真ん中で早くなり最後はゆっくり
//------------------------------------------------------
inline float EaseInEaseOut( float time )
{
	return -2 * (time*time*time) + 3 * (time*time);
}

//------------------------------------------------------
//	指定したところに徐々に移動する
//------------------------------------------------------
inline Vector3 Vector3Lerp( Vector3 start , Vector3 end , float minFrame , float maxFrame , float nowFrame , float (*func)(float) )
{
	float time = (nowFrame - minFrame) / (maxFrame - minFrame);
	if ( time < 0.0f ) time = 0.0f;
	if ( time > 1.0f ) time = 1.0f;
	float rate = (*func)(time);
	return start * (1.0f - rate) + end * rate;;
}

//*****************************************************************************
//		クォータニオン
//*****************************************************************************

//------------------------------------------------------
//	指定した軸でQuaternionを作成する
//	作るだけやから計算処理はご自分で
//------------------------------------------------------
Quaternion QuaternionRotationAxis( Quaternion& q , Vector3 axis , float radian );

#endif