//********************************************************************
//																									
//		３Ｄ用シェーダー		
//
//********************************************************************
//------------------------------------------------------------------------------------------
//		環境関連
//------------------------------------------------------------------------------------------
float4x4 Projection;				//	投影変換行列
float4x4 ShadowProjection;			//	投影変換行列(影)
float4x4 TransMatrix;				//	ワールド変換行列
float3	 ViewPos;

//------------------------------------------------------------------------------------------
//		テクスチャサンプラー
//------------------------------------------------------------------------------------------
/*デフォルト*/
texture Texture;
sampler DecaleSamp = sampler_state
{
	Texture = <Texture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;

	AddressU = Wrap;
	AddressV = Wrap;
};
/*スペキュラマップテクスチャ*/
texture SpecularMap;
sampler SpecularSamp = sampler_state
{
	Texture = <SpecularMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;

	AddressU = Wrap;
	AddressV = Wrap;
};
/*シャドウマッピング*/
texture ShadowMap;
sampler ShadowSamp = sampler_state
{
	Texture = < ShadowMap >;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	BorderColor = 0xFFFFFFFF;
	AddressU = BORDER;
	AddressV = BORDER;
};
/*法線テクスチャサンプラー*/
texture NormalMap;
sampler NormalSamp = sampler_state
{
	Texture = <NormalMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;

	AddressU = Wrap;
	AddressV = Wrap;
};
/*高さテクスチャサンプラー*/
texture HeightMap;
sampler HeightSamp = sampler_state
{
	Texture = <HeightMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;

	AddressU = Wrap;
	AddressV = Wrap;
};

//------------------------------------------------------------------------------------------
//		頂点シェーダー
//------------------------------------------------------------------------------------------
struct VS_BASIC
{
	float4 Pos    : POSITION;
	float4 Color  : COLOR0;
	float2 Tex	  : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 Pos				:	POSITION;
	float4 Color			:	COLOR0;
	float2 Tex				:	TEXCOORD0;

	float3 vShadow		:	TEXCOORD1;				//	シャドウの法線

	float4	worldPos			:	TEXCOORD2;		//	ワールドの座標
	float3	worldNormal		:	TEXCOORD3;		//	ワールドの法線

	float3 vLight			:	TEXCOORD4;			//	ライトベクトル(頂点空間)
	float3 vE					:	TEXCOORD5;			//	視線ベクトル(頂点空間)
};

//		シャドウマッピング
struct VS_SHADOW
{
	float4 Pos				:	POSITION;
	float4 Color			:	TEXCOORD0;
};
//		トゥーン用頂点
struct VS_TOON
{
	float4 Pos				:	POSITION;
	float4 Color			:	COLOR0;
	float2 Tex				:	TEXCOORD0;

	float	Level : TEXCOORD1;

	float3 vShadow		:	TEXCOORD2;				//	シャドウの法線
};
float OutlineSize = 0.125f;		//	輪郭線の太さ
float ToonLevel = 0.7f;			//	影のレベル
float ToonShadow = 0.65f;	//	影の濃さ

struct VS_INPUT
{
	float4 Pos				:	POSITION;
	float4 Color			:	COLOR0;
	float3 Normal			:	NORMAL;
	float2 Tex				:	TEXCOORD0;
};
float3 AmbientColor = { 0.2f, 0.2f, 0.2f };

//------------------------------------------------------------------------------------------
//		ライティング
//------------------------------------------------------------------------------------------
//********************************
//	半球ライティング
//********************************
float3 SkyColor = { 0.48f, 0.5f, 0.5f };
float3 GroundColor = { 0.4f, 0.4f, 0.4f };

inline float4 HemiLight(float3 normal)
{
	float4 color;
	float rate = (normal.y*0.5f) + 0.5f;
	color.rgb = SkyColor * rate;
	color.rgb += GroundColor * (1 - rate);
	color.a = 1.0f;

	return color;
}
//********************************
//	平行光
//********************************
float3	DirLightVec = { -1, -1, 1 };
float3 LightDir = { 1.0f, -1.0f, 1.0f };
float3 DirLightColor = { 0.6f, 0.6f, 0.6f };

inline float3 DirLight(float3 dir, float3 normal)
{
	float3 light;
	float rate = max(1.0f, dot(-dir, normal));
	light = DirLightColor * rate;

	return light;
}
inline float3 DirLight2(float3 light, float3 normal)
{
	float3 vec = normalize(light);
	float intensity = max(0, dot(vec, -normal));

	return DirLightColor * intensity;
}
//********************************
//	スペキュラー
//********************************
inline float Specular(float3 pos, float3 normal)
{
	float   sp;

	float3	H = normalize(ViewPos - pos);
		H = normalize(H - LightDir);

	sp = dot(normal, H);
	sp = max(0, sp);
	sp = pow(sp, 10);

	return sp;
}
//********************************
//	シャドウマッピング
//********************************
float AdjustValue = -0.004f;
float Shadow = 0.5f;

inline float3 GetShadowTex(float3 Pos)
{
	float4x4 Proj;
	float3 Tex;
	// テクスチャ座標計算
	float4 ppp;
	ppp.xyz = Pos;
	ppp.w = 1;

	Tex = mul(ppp, ShadowProjection);

	Tex.y = -Tex.y;
	Tex.xy = 0.5f * Tex.xy + 0.5f;

	return Tex;
}

inline float GetShadow(float3 Tex)
{
	// シャドウマップから深度を取り出す
	float d = tex2D(ShadowSamp, Tex.xy);
	float l = (d < Tex.z + AdjustValue) ? Shadow : 1;

	return l;
}

//inline float GetShadow2(float3 Tex, float3 Tex2, float a)
//{
//	//　ここも深度値？
//	float d = Tex.z;
//
//	//　近累値
//	//　シャドウマップから深度を取り出す
//	float2 t = tex2D(ShadowSamp, Tex.xy).rg;
//		//深度値
//		float d1 = t.r;
//	//　分散
//	float variance = max(t.g - d1*d1, 0.0);
//
//	//　t - E
//	float te = d = d1;
//	//　チェビシェフの不等式使用
//	float l1 = varianc / (variance + te * te);
//
//	//　ここが確率論？
//	float2 l2 = (d1 < d - 0.002f) ? 0 : 1;
//		float l = (1 - max(l1, l2))*a;
//	//float l = (l-l2)*a;
//
//	//　濃度補正
//	l = 1 - (l*(1 - Shadow));
//
//	//　遠累値
//	float t2 = tex2D(ShadowSampL, Tex2.xy).r;	//　シャドウマップから深度を取り出す
//	l2 = (t2 < Tex2.z + AdjustValue) ? l*(a*(1 - ShadowL) + ShadowL) : l;
//
//	return l2;
//}

//********************************
//	フォンシェーディング
//********************************
float Fon(float3 pos, float3 N)
{
	float rate;

	float3 vE = normalize(ViewPos - pos);		//	視線ベクトル
		float3 vL = normalize(DirLightVec);
		float3 vLL = reflect(vL, N);

		rate = max(0, dot(vLL, vE));
	rate = pow(rate, 30);

	return rate;
}
//------------------------------------------------------------------------------------------
//		頂点シェーダー
//------------------------------------------------------------------------------------------
//********************************
//	基本シェーダー
//********************************
VS_OUTPUT VS_Basic(VS_INPUT In)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float3 P = mul(In.Pos, TransMatrix);

		float3x3	mat = TransMatrix;
		float3 N = mul(In.Normal, mat);
		N = normalize(N);

	Out.Pos = mul(In.Pos, Projection);
	Out.Color.rgb = DirLight(LightDir, N) + HemiLight(N);
	Out.Color.a = 1.0f;
	Out.Tex = In.Tex;

	return Out;
}
VS_OUTPUT VS_Basic_Shadow(VS_INPUT In)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float3 P = mul(In.Pos, TransMatrix);

	float3x3 mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);
	/*ライティング適用*/
	Out.Pos = mul(In.Pos, Projection);
	Out.Color.rgb = DirLight(LightDir, N) + HemiLight(N);
	Out.Color.a = 1.0f;
	Out.Tex = In.Tex;
	//	シャドウマップ適用
	float4 S = mul(In.Pos, TransMatrix);
	Out.vShadow = GetShadowTex(S);

	return Out;
}
//********************************
//	シャドウマッピング
//********************************
VS_SHADOW VS_ShadowBuf(float4 Pos : POSITION)
{
	VS_SHADOW Out = (VS_SHADOW)0;
	// 座標変換
	float4x4 mat;

	mat = mul(TransMatrix, ShadowProjection);
	//　
	Out.Pos = mul(Pos, mat);
	//　ここがいまひとつわからん
	Out.Color = Out.Pos.z;

	return Out;
}
//********************************
//	フォンシェーディング
//********************************
VS_OUTPUT VS_Fon(VS_INPUT In)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	Out.Pos = mul(In.Pos, Projection);
	Out.Tex = In.Tex;
	Out.Color.a = 1.0f;
	//	法線変換
	float3x3 mat = TransMatrix;
		float3 N = mul(In.Normal, mat);
		N = normalize(N);
	/*ライティング適用*/
	Out.Pos = mul(In.Pos, Projection);
	Out.Color.rgb = DirLight(LightDir, N) + HemiLight(N);
	Out.Color.a = 1.0f;
	Out.Tex = In.Tex;
	//	グローバルポジション&法線
	Out.worldPos = mul(In.Pos, TransMatrix);
	float3x3		m = (float3x3)TransMatrix;
		Out.worldNormal = mul(In.Normal, m);

	return Out;
}
//********************************
//	アウトライン
//********************************
VS_OUTPUT VS_Outline(VS_INPUT In)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	//	法線方向に拡大
	In.Normal = normalize(In.Normal);
	In.Pos.xyz += In.Normal * OutlineSize;

	Out.Pos = mul(In.Pos, Projection);
	Out.Tex = In.Tex;
	Out.Color.rgb = 0;
	Out.Color.a = 1;

	return Out;
}

float4 Toon_Color = { 1, 1, 1, 1 };


//********************************
//	トゥーン
//********************************
VS_TOON VS_Toon(VS_INPUT In)
{
	VS_TOON Out = (VS_TOON)0;

	Out.Pos = mul(In.Pos, Projection);
	Out.Tex = In.Tex;
	Out.Color = 1;

	//	法線変換
	float3x3 mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);
	//	ライト計算
	float3 vec = normalize(DirLightVec);
	Out.Level = dot(vec, -N);

	return Out;
}
//********************************
//	法線マップ,視差マップ
//********************************
VS_OUTPUT VS_FullFX(VS_INPUT In)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	Out.Pos = mul(In.Pos, Projection);
	Out.Color = In.Color;
	Out.Tex = In.Tex;

	float4 P = mul(In.Pos, TransMatrix);
	float3x3 mat = TransMatrix;
	float3 N = mul(In.Normal, mat);
	N = normalize(N);

	Out.Color.rgb = AmbientColor;
	Out.Color.rgb += HemiLight(N);
	//	頂点ローカル座標系算出
	float3 vx;
	float3 vy = { 0, 1, 0.001f };
	vx = cross(vy, N);
	vx = normalize(vx);
	vy = cross(N, vx);
	vy = normalize(N);

	//	ライトベクトル補正
	Out.vLight.x = dot(vx, DirLightVec);
	Out.vLight.y = dot(vy, DirLightVec);
	Out.vLight.z = dot(N, DirLightVec);

	//	視線ベクトル補正
	float3 E = P - ViewPos;			//	視線ベクトル
		Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	return Out;
}
//------------------------------------------------------------------------------------------
//		ピクセルシェーダー
//------------------------------------------------------------------------------------------
float4 PS_Basic(VS_OUTPUT In) : COLOR
{
	float4	OUT;
	//	ピクセル色決定
	OUT = In.Color * tex2D(DecaleSamp, In.Tex);

	return OUT;
}

float4 PS_Basic_Shadow(VS_OUTPUT In) : COLOR
{
	float4	OUT;
	//	ピクセル色決定
	OUT = In.Color * tex2D(DecaleSamp, In.Tex);
	if (OUT.a <= 0.2)
	{
		discard;
	}
	OUT.rgb *= GetShadow(In.vShadow);

	return OUT;
}

float4 PS_ShadowBuf(VS_SHADOW In) : COLOR
{
	return In.Color;
}

float4 PS_Fon(VS_OUTPUT In) : COLOR
{
	float4 OUT;
	//	ピクセル色設定
	OUT = In.Color * tex2D(DecaleSamp, In.Tex);

	//	ディフューズ適用
	In.worldNormal = normalize(In.worldNormal);
	OUT.rgb += Fon(In.worldPos, In.worldNormal);

	return OUT;
}

float4 PS_Outline(VS_OUTPUT In) : COLOR
{
	float4 OUT;
	//	ピクセル色決定
	OUT = In.Color;

	return OUT;
}

float4 PS_Toon(VS_TOON In) : COLOR
{
	float4 OUT;
	//	ピクセル色決定
	OUT = tex2D(DecaleSamp, In.Tex);

	//	レベル未満を影にする
	if (In.Level < 0) OUT.rgb *= ToonShadow;

	return OUT;
}

//********************************
//	視差マップ
//********************************
float4 PS_Parallax(VS_OUTPUT In):COLOR
{
	float4 OUT;
	float2 Tex = In.Tex;

	//　パララックスディスプレーメント
	float h = tex2D(HeightSamp, Tex).r - 0.5f;
	float3 E = normalize(In.vE);
	Tex.x -= 0.04f*h*E.x;
	Tex.y += 0.04f*h*E.y;

	//　法線取得
	float3 N = tex2D(NormalSamp, Tex).xyz*2.0f - 1.0f;

	//　ライト計算
	In.vLight = normalize(In.vLight);
	float3 light = DirLight2(In.vLight, N);

	//　ピクセル色決定
	OUT = In.Color*tex2D(DecaleSamp, Tex);
	OUT.rgb = (OUT.rgb + light);

	return OUT;
}

//------------------------------------------------------------------------------------------
//		テクニック
//------------------------------------------------------------------------------------------
technique copy
{
	pass P0
	{
		AlphaBlendEnable = false;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		CullMode = CCW;
		ZEnable = true;
		ZWriteEnable = true;

		VertexShader = compile vs_2_0 VS_Basic();
		PixelShader = compile ps_2_0 PS_Basic();
	}
}

technique add
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = One;
		ZWriteEnable = false;

		VertexShader = compile vs_2_0 VS_Basic();
		PixelShader = compile ps_2_0 PS_Basic();
	}
}

technique shadow
{
	pass P0
	{
		AlphaBlendEnable = false;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		CullMode = CCW;
		ZEnable = true;
		ZWriteEnable = true;

		VertexShader = compile vs_3_0 VS_Basic_Shadow();
		PixelShader = compile ps_3_0 PS_Basic_Shadow();
	}
};

technique fon
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		CullMode = CCW;
		ZEnable = true;
		ZWriteEnable = true;

		VertexShader = compile vs_2_0 VS_Fon();
		PixelShader = compile ps_2_0 PS_Fon();
	}
}

technique ShadowBuf
{
	pass Pass0
	{
		AlphaBlendEnable = false;
		ZWriteEnable = true;
		CullMode = none;

		VertexShader = compile vs_3_0 VS_ShadowBuf();
		PixelShader = compile ps_3_0 PS_ShadowBuf();
	}
}

technique toon
{
	//	輪郭線
	pass Outline
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		CullMode = CW;
		ZEnable = true;
		ZWriteEnable = true;

		VertexShader = compile vs_2_0 VS_Outline();
		PixelShader = compile ps_2_0 PS_Outline();
	}

	//	トゥーン
	pass Toon
	{
		CullMode = CCW;
		ZWriteEnable = true;

		VertexShader = compile vs_2_0 VS_Toon();
		PixelShader = compile ps_2_0 PS_Toon();
	}
}

technique parallax
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		CullMode = CCW;
		ZEnable = true;

		VertexShader = compile vs_2_0 VS_FullFX();
		PixelShader = compile ps_2_0 PS_Parallax();
	}
}

technique toonparallax
{
	//	輪郭線
	pass Outline
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		CullMode = CW;
		ZEnable = true;
		ZWriteEnable = true;

		VertexShader = compile vs_2_0 VS_Outline();
		PixelShader = compile ps_2_0 PS_Outline();
	}

	//	トゥーン
	pass Toon
	{
		CullMode = CCW;
		ZWriteEnable = true;

		VertexShader = compile vs_2_0 VS_Toon();
		PixelShader = compile ps_2_0 PS_Toon();
	}
	
	//　視差マップ
	pass Parallax
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		CullMode = CCW;
		ZEnable = true;

		VertexShader = compile vs_2_0 VS_FullFX();
		PixelShader = compile ps_2_0 PS_Parallax();
	}
};