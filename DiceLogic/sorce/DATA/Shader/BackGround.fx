//**************************************************************************************************
//
//		背景用シェーダー
//
//**************************************************************************************************

struct VIN_BG
{
	float4 Pos:POSITION;
	float3 Normal:NORMAL;
	float4 Color:COLOR0;
	float2 Tex:TEXCOORD0;
	float3 tangent:TANGENT;		//?
	float3 binormal:BINORMAL;	//?
};

struct POUT_BG
{
	float4 color:COLOR;
	float4 depth:COLOR1;
	float4 spec:COLOR2;
};

//**************************************************************************************************
//		頂点フォーマット
//**************************************************************************************************
struct VOUT_BG
{
	float4 Pos:POSITION;
	float4 Color:COLOR0;
	float2 Tex:TEXCOORD0;
	float3 Normal:TEXCOORD1;
	float4 wPos:TEXCOORD2;

	float4 Ambient:COLOR1;
	float3 vLight:TEXCOORD3;
	float3 vE:TEXCOORD4;

	float4 vShadow:TEXCOORD5;
	float3 vShadowL:TEXCOORD6;
};

//------------------------------------------------------
//	反射マップ
//------------------------------------------------------
texture RefMap;		//　反射マップテクスチャ
sampler RefSamp = sampler_state
{
	Texture = <RefMap>;
	MipFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = NONE;

	AddressU = Wrap;
	AddressV = Wrap;
};

//------------------------------------------------------
//	頂点カラー付シェーダ
//------------------------------------------------------
VOUT_BG VS_BGHigh(VIN_BG In)
{
	VOUT_BG Out;

	float4 P = mul(In.Pos, TransMatrix);

		float3x3 mat = TransMatrix;
		float3 N = mul(In.Normal, mat);
		N = normalize(N);
	Out.Normal = N;

	Out.Ambient.rgb = HemiLight(N);

	Out.Pos = mul(In.Pos, Projection);
	Out.Color = In.Color;
	Out.Tex = In.Tex;
	Out.wPos = Out.Pos;
	Out.wPos.xy = Out.wPos.xy*0.5f;

	//　接線・従法線
	float3 vx = In.tangent;
		float3 vy = In.binormal;

		//　ライトベクトル補正
		Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	//　視線ベクトル補正
	float3 E = (P - ViewPos);	//　視線ベクトル
		Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	//　シャドウバッファ
	Out.vShadow = GetShadowTex(P);
	Out.vShadow.w = (14.5f - Out.Pos.z)*0.15f;
	Out.vShadowL = GetShadowTexL(P);

	Out.vShadow.w = saturate(Out.vShadow.w);

	//　フォグ計算
	Out.Ambient.a = (FogFar - Out.Pos.z) / (FogFar - FogNear);
	Out.Ambient.a = saturate(Out.Ambient.a);

	return Out;
}

//------------------------------------------------------
//	頂点カラー付シェーダ
//------------------------------------------------------
VOUT_BG VS_BGHigh2(VIN_BG In)
{
	VOUT_BG Out;

	float4 P = mul(In.Pos, TransMatrix);

		float3x3 mat = TransMatrix;
		float3 N = mul(In.Normal, mat);
		N = normalize(N);
	Out.Normal = N;

	Out.Ambient.rgb = HemiLight(N);

	Out.Pos = mul(In.Pos, Projection);
	Out.Color = 1;
	Out.Tex = In.Tex;
	Out.wPos = Out.Pos;
	Out.wPos.xy = Out.wPos.xy*0.5f;

	//　接線・従法線
	float3 vx = In.tangent;
		float3 vy = In.binormal;

		//　ライトベクトル補正
		Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	//　視線ベクトル補正
	float3 E = (P - ViewPos);	//　視線ベクトル
		Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	//　シャドウバッファ
	Out.vShadow = GetShadowTex(P);
	Out.vShadow.w = (14.5f - Out.Pos.z)*0.15f;
	Out.vShadowL = GetShadowTexL(P);

	Out.vShadow.w = saturate(Out.vShadow.w);

	//　フォグ計算
	Out.Ambient.a = (FogFar - Out.Pos.z) / (FogFar - FogNear);
	Out.Ambient.a = saturate(Out.Ambient);

	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー	
//------------------------------------------------------
POUT_BG PS_BGHigh(VOUT_BG In)
{
	POUT_BG OUT;
	float2 Tex = In.Tex;

		//　パララックスディスプレースメント
		// 高さマップのサンプリング
		float h = tex2D(HeightSamp, Tex).r - 0.5f;
	float3 E = normalize(In.vE);
		In.vLight = normalize(In.vLight);

	Tex += SetPDisp(h, E.xy);
	float3 N = SetNormal(tex2D(NormalSamp, Tex));

		//　ライト計算
		float3 light = DirLight(In.vLight, N) + In.Ambient.rgb;
		//　ピクセル色決定
		OUT.color = SetColor(In.Color, tex2D(DecaleSamp, Tex));
	OUT.color.rgb = (OUT.color.rgb*light);

	//　シャドウマップ
	if (In.vLight.z < 0)
	{
		float shadow = GetShadow2(In.vShadow, In.vShadowL, In.vShadow.w);
		OUT.color.rg *= shadow*shadow;
		OUT.color.b *= shadow;
	}

	float4 sp_tex = tex2D(SpecularSamp, Tex);
		//　スペキュラ
		float3 R = normalize(reflect(E, N));
		float3 sp = pow(max(0, dot(R, -In.vLight)), 10)*sp_tex.rgb*DirLightColor;
		OUT.color.rgb += sp;

	float2 ref = In.wPos.xy / In.wPos.w + 0.5f;
		ref.y = -ref.y;
	float3 Env = tex2D(RefSamp, ref)*(OUT.color.rgb + 0.4f);	//　このマジックナンバーの意味とは？
		Env += Environment(E, N, In.Normal)*(OUT.color.rgb + 0.3f);

	OUT.color.rgb = OUT.color.rgb*sp_tex.a + Env*(1 - sp_tex.a);

	//　フォグ採用
	OUT.color.rgb = (OUT.color.rgb*In.Ambient.a) + (FogColor*(1 - In.Ambient.a));

	//　深度
	OUT.depth.xyz = In.wPos.z;
	OUT.depth.a = 1;

	//　スペキュラ描画
	OUT.spec.xyz = sp;
	OUT.spec.a = 1;

	return OUT;
}

technique bg_high
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZWriteEnable = true;
		CullMode = none;

		VertexShader = compile vs_3_0 VS_BGHigh();
		PixelShader = compile ps_3_0 PS_BGHigh();
	}
}

technique bg_high2
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZWriteEnable = true;
		CullMode = none;

		VertexShader = compile vs_3_0 VS_BGHigh2();
		PixelShader = compile ps_3_0 PS_BGHigh();
	}
}