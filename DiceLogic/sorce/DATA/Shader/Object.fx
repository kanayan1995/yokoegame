//********************************
//	セット関数用意
//********************************
//　ピクセル色決定
float4 SetColor(float4 col, float4 tex)
{
	return col*tex;
}

//　法線設定
float3 SetNormal(float3 tex)
{
	return tex*2.0f - 1.0f;
}

//　視差補正
float2 SetPDisp(float h, float2 E)
{
	return -0.05f*h*E;
}

//*****************************************
//	オブジェクト用シェーダ
//*****************************************
struct VIN_OBJECT
{
	float4 Pos:POSITION;
	float3 Normal:NORMAL;
	float4 Color:COLOR0;
	float2 Tex:TEXCOORD0;
};

struct VOUT_OBJECT
{
	float4 Pos:POSITION;
	float4 Color:COLOR0;
	float2 Tex:TEXCOORD0;
	float3 Normal:TEXCOORD1;

	float4 Ambient:COLOR1;
	float3 vLight:TEXCOORD2;
	float3 vE:TEXCOORD3;

	float4 Depth:TEXCOORD4;

	float4 vShadow:TEXCOORD5;
	float3 vShadowL:TEXCOORD6;
};

struct POUT_OBJECT
{
	float4 color:COLOR0;
	float4 depth:COLOR1;
	float4 spec:COLOR2;
};

inline float4 Environment(float3 dir, float3 normal, float2 center)
{
	dir = normalize(dir);
	normal = normalize(normal);
	float3 R = normalize(reflect(dir, normal));

	float4 color;

	float2 uv;
	uv = (R.xy + 1.0f)*0.5f;

	color = tex2D(EnvSamp, uv);
	return color;
}

//------------------------------------------------------
//	頂点シェーダ
//------------------------------------------------------
VOUT_OBJECT VS_ObjectHigh(VIN_OBJECT In)
{
	VOUT_OBJECT Out;

	float4 P = mul(In.Pos, TransMatrix);

		float3x3 mat = (float3x3)TransMatrix;
		float3 N = mul(In.Normal, mat);
		N = normalize(N);
	Out.Normal = N;

	Out.Ambient.rgb = HemiLight(N);

	Out.Pos = mul(In.Pos, Projection);
	Out.Color = 1.0f;
	Out.Tex = In.Tex;

	Out.Depth = Out.Pos;

	//　ライトベクトル補正
	float3 vx;
	float3 vy = { 0.0f, -1.0f, 0.01f };

		vx = cross(N, vy);
	vx = normalize(vx);

	vy = cross(vx, N);
	vy = normalize(vy);

	//　ライトベクトル補正
	Out.vLight.x = dot(vx, LightDir);
	Out.vLight.y = dot(vy, LightDir);
	Out.vLight.z = dot(N, LightDir);

	//　視差ベクトル補正
	float3 E = (P.xyz - ViewPos);	//　視線ベクトル
		Out.vE.x = dot(vx, E);
	Out.vE.y = dot(vy, E);
	Out.vE.z = dot(N, E);

	//　フォグ計算
	Out.Ambient.a = (FogFar - Out.Pos.z) / (FogFar - FogNear);
	Out.Ambient.a = saturate(Out.Ambient);

	//　シャドウバッファ
	Out.vShadow = GetShadowTex(P);
	Out.vShadow.w = (14.5f - Out.Pos.z)*0.15f;
	Out.vShadowL = GetShadowTexL(P).xyz;

	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー（影無し）	
//------------------------------------------------------
float4 PS_ObjectHigh(VOUT_OBJECT In):COLOR
{
	float4 OUT;

	float2 Tex = In.Tex;
	float3 E = normalize(In.vE);
	In.vLight = normalize(In.vLight);

	float3 N = SetNormal(tex2D(NormalSamp, Tex));
	//　ライト計算
	float3 light = DirLight(In.vLight, N) + In.Ambient.rgb;

	//　ピクセル色決定
	OUT = SetColor(In.Color, tex2D(DecaleSamp, Tex));
	OUT.rgb = OUT.rgb*light;

	float4 sp_tex = tex2D(SpecularSamp, Tex);
	//　スペキュラ
	float3  R = normalize( reflect( E, N ) );
	OUT.rgb += pow(max(0, dot(R, -In.vLight)), 10)*sp_tex.rgb*DirLightColor;

	//　環境マップ
	sp_tex.a *= sp_tex.a;
	float3 Env = Environment(E, N, In.Normal.xy)*(OUT.rgb + 0.4f);
	OUT.rgb = OUT.rgb*sp_tex.a + Env*(1 - sp_tex.a);

	//　フォグ採用
	OUT.rgb = (OUT.rgb*In.Ambient.a) + (FogColor*(In.Ambient.a));

	return OUT;
}

POUT_OBJECT PS_ObjectHighZ(VOUT_OBJECT In)
{
	POUT_OBJECT OUT;

	float2 Tex = In.Tex;
		float3 E = normalize(In.vE);
		In.vLight = normalize(In.vLight);

	float3 N = SetNormal(tex2D(NormalSamp, Tex));
		//　ライト計算
		float3 light = DirLight(In.vLight, N) + In.Ambient.rgb;

		//　ピクセル色決定
		OUT.color = SetColor(In.Color, tex2D(DecaleSamp, Tex));
	OUT.color.rgb = OUT.color.rgb*light;

	float4 sp_tex = tex2D(SpecularSamp, Tex);
		//　スペキュラ
		float3 R = normalize(reflect(E, N));
		float3 sp = pow(max(0, dot(R, -In.vLight)), 10)*sp_tex.rgb*DirLightColor;
		OUT.color.rgb += sp;
	//　環境マップ
	float3 Env = Environment(E, N, In.Normal)*(OUT.color.rgb + 0.3f);
		sp_tex.a *= sp_tex.a;
	OUT.color.rgb = OUT.color.rgb*sp_tex.a + Env*(1 - sp_tex.a);

	//　フォグ採用
	OUT.color.rgb = (OUT.color.rgb*In.Ambient.a) + (FogColor*(1 - In.Ambient.a));

	//　深度
	OUT.depth.xyz = In.Depth.z;
	OUT.depth.a = 1;

	//　スペキュラ描画
	OUT.spec.xyz = sp;
	OUT.spec.a = 1;

	return OUT;
}

POUT_OBJECT PS_ObjectHighZS(VOUT_OBJECT In)
{
	POUT_OBJECT OUT;

	float2 Tex = In.Tex;
		float3 E = normalize(In.vE);
		In.vLight = normalize(In.vLight);

	float3 N = SetNormal(tex2D(NormalSamp, Tex));
		//　ライト計算
		float3 light = DirLight(In.vLight, N) + In.Ambient.rgb;

		//　ピクセル色決定
		OUT.color = SetColor(In.Color, tex2D(DecaleSamp, Tex));
	OUT.color.rgb = OUT.color.rgb*light;

	float4 sp_tex = tex2D(SpecularSamp, Tex);
		//　スペキュラ
		float3 R = normalize(reflect(E, N));
		float3 sp = pow(max(0, dot(R, In.vLight)), 10)*sp_tex.rgb;
		OUT.color.rgb += sp;
	//　環境マップ
	float3 Env = Environment(E, N, In.Normal)*(OUT.color.rgb + 0.3f);
		sp_tex.a *= sp_tex.a;
	OUT.color.rgb = OUT.color.rgb*sp_tex.a + Env*(1 - sp_tex.a);

	//　シャドウマップ
	if (In.vLight.z < 0)
	{
		float shadow = GetShadow2(In.vShadow, In.vShadowL, In.vShadow.w);
		OUT.color.rgb *= shadow;
	}

	//　フォグ採用
	OUT.color.rgb = (OUT.color.rgb*In.Ambient.a) + (FogColor*(1 - In.Ambient.a));

	//　深度
	OUT.depth.xyz = In.Depth.z;
	OUT.depth.a = 1;

	//　スペキュラ描画
	OUT.spec.xyz = sp;
	OUT.spec.a = 1;

	return OUT;
}

//------------------------------------------------------
//		合成なし	
//------------------------------------------------------
technique object_high
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZWriteEnable = true;
		CullMode = CCW;

		VertexShader = compile vs_3_0 VS_ObjectHigh();
		PixelShader = compile ps_3_0 PS_ObjectHighZ();
	}
};

technique object_highS
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZWriteEnable = true;
		CullMode = CCW;

		VertexShader = compile vs_3_0 VS_ObjectHigh();
		PixelShader = compile ps_3_0 PS_ObjectHighZS();
	}
};

technique object_highA
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		ZWriteEnable = true;
		CullMode = None;
		AlphaRef = 128;
		AlphaFunc = GreaterEqual;
		VertexShader = compile vs_3_0 VS_ObjectHigh();
		PixelShader = compile ps_3_0 PS_ObjectHighZ();
	}
};