//*****************************************************************************************************************************
//
//		シャドウマップ作成
//
//*****************************************************************************************************************************
float4x4 ShadowProjection;
float4x4 ShadowProjectionL;

float AdjustValueL = -0.002f;
float AdjustValue = -0.03f;
float Shadow = 0.8f;
float ShadowL = 0.8f;

texture ShadowMap;
sampler ShadowSamp = sampler_state
{
	Texture = <ShadowMap>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	AddressU = CLAMP;
	AddressV = CLAMP;
};

texture ShadowMapL;
sampler ShadowSampL = sampler_state
{
	Texture = <ShadowMapL>;
	MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = NONE;

	AddressU = BORDER;
	AddressV = BORDER;
	BorderColor = 0xFFFFFFFF;
};

inline float4 GetShadowTex(float4 Pos)
{
	float4 Tex;

	//　テクスチャ座標計算
	Tex = mul(Pos, ShadowProjection);

	Tex.y = -Tex.y;
	Tex.xy = 0.5f*Tex.xy + 0.5f;
	Tex.xyz /= Tex.w;

	return Tex;
}

inline float4 GetShadowTexL(float4 Pos)
{
	float4 Tex;
	//　テクスチャ座標計算
	Tex = mul(Pos, ShadowProjectionL);

	Tex.y = -Tex.y;
	Tex.xy = 0.5f*Tex.xy + 0.5f;
	Tex.xyz /= Tex.w;

	return Tex;
}

inline float GetShadow(float3 Tex)
{
	float d = Tex.z;

	//　近累値
	float4 t = tex2D(ShadowSamp, Tex.xy);	//　シャドウマップから深度を取り出す
		float d1 = t.r;						//　深度値

	float l = (d1 < d + AdjustValue) ? 1 - Shadow : 0;

	return 1 - l;
}

inline float GetShadowS(float3 Tex)
{
	float d = Tex.z;

	//　近累値
	float4 t = tex2D(ShadowSampL, Tex.xy);	//　シャドウマップから深度を取り出す
	float d1 = t.r;						//　深度値
	
	float l = (d1 < d + AdjustValueL) ? 1 - ShadowL : 0;

	return 1 - l;
}

inline float GetShadow2(float3 Tex, float3 Tex2,float a)
{
	//　ここも深度値？
	float d = Tex.z;
	
	//	近景値
	// シャドウマップから深度を取り出す
	float2	t = tex2D(ShadowSamp, Tex.xy).rg;		
	// 深度値
	float	d1 = t.r;							
	// 分散
	float	variance = max(t.g - d1*d1, 0.0);	

	// t - E
	float	te = d - d1;
	// チェビシェフの不等式
	float	l1 = variance / (variance + te * te);		

	//　確率論？
	float l2 = (d1 < d -0.002f) ? 0 : 1;
	float l = (1-max(l1, l2))*a;
//	float l = (1-l2)*a;

	//	濃度補正	
	l = 1-(l*(1-Shadow));

	//	遠景値
	float	t2 = tex2D(ShadowSampL, Tex2.xy).r;	// シャドウマップから深度を取り出す
	l2 = (t2 < Tex2.z + AdjustValue) ? l*(a*(1-ShadowL)+ShadowL) : l;

	return l2;
}
//------------------------------------------------------
//	シャドウマップ作成
//------------------------------------------------------
struct VS_SHADOW
{
	float4 Pos	:POSITION;
	float2 Tex	:TEXCOORD0;
	float4 Color:COLOR1;	//　頂点色
};

//------------------------------------------------------
//		頂点シェーダー
//------------------------------------------------------
VS_SHADOW VS_ShadowBuf(float4 Pos:POSITION, float2 Tex : TEXCOORD0)
{
	VS_SHADOW Out;

	//　座標変換
	float4x4 mat = mul(TransMatrix, ShadowProjection);
	Out.Pos = mul(Pos, mat);
	
	/*チェビシェフの不等式を使う*/
	//　深度の期待値≒平均値
	Out.Color.r = Out.Pos.z;
	//　深度のべき乗の期待値
	Out.Color.g = Out.Pos.z*Out.Pos.z;
	Out.Color.b = 0;
	//　透明度
	Out.Color.a = 1;

	Out.Tex = Tex;
	return Out;
}

VS_SHADOW VS_ShadowBufL( float4 Pos : POSITION, float2 Tex : TEXCOORD0 )
{
	VS_SHADOW Out;
	
	// 座標変換
	float4x4	mat = mul(TransMatrix, ShadowProjectionL);
	Out.Pos = mul(Pos, mat);
	Out.Color = Out.Pos.z;

	Out.Tex = Tex;
	return Out;
}

//------------------------------------------------------
//		ピクセルシェーダー
//------------------------------------------------------
float4 PS_ShadowBuf(VS_SHADOW In):COLOR
{
	float4 OUT;
	OUT = In.Color;
	OUT.a = tex2D(DecaleSamp, In.Tex).a;
	return OUT;
}

//------------------------------------------------------
//		テクニック
//------------------------------------------------------
technique ShadowBuf
{
	pass Pass0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = one;
		DestBlend = ZERO;
		ZWriteEnable = true;
		CullMode = NONE;

		VertexShader = compile vs_3_0 VS_ShadowBuf();
		PixelShader = compile ps_3_0 PS_ShadowBuf();
	}
};

technique ShadowBufL
{
	pass Pass0
	{
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = ONE;
		DestBlend = ZERO;
		ZWriteEnable = true;
		CullMode = none;
		
		VertexShader = compile vs_3_0 VS_ShadowBufL();
		PixelShader  = compile ps_3_0 PS_ShadowBuf();
	}
};