//------------------------------------------------------
//		環境関連
//------------------------------------------------------
float4x4 TransMatrix;	//	変換行列
float4x4 matView;		//	変換行列
float4x4 Projection;	//	変換行列

float3	ViewPos;


texture Texture;
sampler DecaleSamp = sampler_state
{
    Texture = <Texture>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = WRAP;
    AddressV = WRAP;
};

sampler DecaleSamp2 = sampler_state
{
    Texture = <Texture>;
    MinFilter = POINT;
    MagFilter = POINT;
    MipFilter = NONE;

    AddressU = WRAP;
    AddressV = WRAP;
};

texture NormalMap;	//	法線マップテクスチャ
sampler NormalSamp = sampler_state
{
    Texture = <NormalMap>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = NONE;

    AddressU = Wrap;
    AddressV = Wrap;
};

texture Depth;
sampler DepthSamp = sampler_state
{
    Texture = <Depth>;
    MinFilter = POINT;
    MagFilter = POINT;
    MipFilter = NONE;

    AddressU = CLAMP;
    AddressV = CLAMP;
};



struct POUT
{
	float4 color	: COLOR0;
	float4 depth	: COLOR1;
	float4 spec		: COLOR2;
};


//*****************************************************************************
//	基本描画
//*****************************************************************************
struct V_BASIC
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;
};


// -------------------------------------------------------------
// 頂点シェーダプログラム
// -------------------------------------------------------------
V_BASIC VS_copy (
      float4 Pos    : POSITION,          // モデルの頂点
      float2 Tex    : TEXCOORD0,	         // テクスチャ座標
      float4 Color  : COLOR0
){
    V_BASIC Out;        // 出力データ
    
    // 位置座標
	Out.Pos = mul(Pos, Projection);
    Out.Tex = Tex;
    Out.Color = Color;
    return Out;
}

// -------------------------------------------------------------
// ピクセルシェーダプログラム
// -------------------------------------------------------------
float4 PS_copy(V_BASIC In) : COLOR
{
	return In.Color * tex2D( DecaleSamp, In.Tex );
}

technique copy
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_copy();
        PixelShader  = compile ps_3_0 PS_copy();
    }
}


//*****************************************************************************
//
//*****************************************************************************
float	wind;


//*****************************************************************************
//	空描画
//*****************************************************************************
float3	SkyColor;
float	Star;

// -------------------------------------------------------------
// 頂点シェーダプログラム
// -------------------------------------------------------------
V_BASIC VS_sky1 (
      float4 Pos    : POSITION,          // モデルの頂点
      float2 Tex    : TEXCOORD0,	     // テクスチャ座標
      float4 Color  : COLOR0
){
    V_BASIC Out;        // 出力データ
    
    // 位置座標
	Out.Pos = mul(Pos, Projection);
    Out.Tex = Tex;
    Out.Color.rgb = SkyColor;
    Out.Color.a   = 1;
    return Out;
}

// -------------------------------------------------------------
// ピクセルシェーダプログラム
// -------------------------------------------------------------
POUT PS_sky1(V_BASIC In)
{
	POUT	OUT;

	float3 orgS = tex2D( DecaleSamp, In.Tex ).rgb; 
	OUT.color.rgb = In.Color.rgb + (orgS * Star);	
	OUT.color.a = 1;

	//	深度
	OUT.depth = 1;

	//	スペキュラ描画
	OUT.spec.rgb = OUT.color.rgb*0.3f;
	OUT.spec.a = 1;

	return OUT;
}

technique sky
{
    pass P0
    {
		AlphaBlendEnable = false;
		BlendOp          = Add;
		SrcBlend         = one;
		DestBlend        = zero;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_sky1();
        PixelShader  = compile ps_3_0 PS_sky1();
    }
}



struct VS_SKY2
{
	float4 Pos		: POSITION;
	float4 Color	: COLOR0;
};

// -------------------------------------------------------------
// 頂点シェーダプログラム
// -------------------------------------------------------------
VS_SKY2 VS_sky2 (
      float4 Pos    : POSITION,          // モデルの頂点
      float4 Color  : COLOR0
){
    VS_SKY2 Out;        // 出力データ
    
    // 位置座標
	Out.Pos = mul(Pos, Projection);
    Out.Color = Color;
    return Out;
}

// -------------------------------------------------------------
// ピクセルシェーダプログラム
// -------------------------------------------------------------
float4 PS_sky2(VS_SKY2 In) : COLOR
{
	return In.Color;
}

technique sky2
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = one;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_sky2();
        PixelShader  = compile ps_3_0 PS_sky2();
    }
}


//*****************************************************************************
//	雲描画
//*****************************************************************************
float cloudRate;
float cloud_v;
// -------------------------------------------------------------
// 頂点シェーダプログラム
// -------------------------------------------------------------
V_BASIC VS_Cloud (
      float4 Pos    : POSITION,          // モデルの頂点
      float2 Tex    : TEXCOORD0,	         // テクスチャ座標
      float4 Color  : COLOR0
){
    V_BASIC Out;        // 出力データ
    
    // 位置座標
	Out.Pos = mul(Pos, Projection);
    Out.Tex = Tex;
    Out.Tex.y += cloud_v;
    Out.Color = Color;
	Out.Color.rgb *= (1.0f - cloudRate*0.5f);
    Out.Color.a *= cloudRate*cloudRate;
    return Out;
}

// -------------------------------------------------------------
// ピクセルシェーダプログラム
// -------------------------------------------------------------
float3 DDD = { 0.49f, -0.7f, -0.7f };

float4 PS_Cloud(V_BASIC In) : COLOR
{
	return In.Color * tex2D( DecaleSamp, In.Tex );
}


technique cloud
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_Cloud();
        PixelShader  = compile ps_3_0 PS_Cloud();
    }
}

// -------------------------------------------------------------
// 頂点シェーダプログラム
// -------------------------------------------------------------
V_BASIC VS_CloudX (
      float4 Pos    : POSITION,          // モデルの頂点
      float2 Tex    : TEXCOORD0,	         // テクスチャ座標
      float4 Color  : COLOR0
){
    V_BASIC Out;        // 出力データ
    
    // 位置座標
	Out.Pos = mul(Pos, Projection);
	Out.Pos.z *= 0.5f;
    Out.Tex = Tex;
    Out.Color = Color;

    return Out;
}

// -------------------------------------------------------------
// ピクセルシェーダプログラム
// -------------------------------------------------------------
float4 PS_CloudX(V_BASIC In) : COLOR
{
	return In.Color * tex2D( DecaleSamp, In.Tex );
}


technique cloudX
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_CloudX();
        PixelShader  = compile ps_3_0 PS_CloudX();
    }
}
technique cloudX2
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = one;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_CloudX();
        PixelShader  = compile ps_3_0 PS_CloudX();
    }
}


//*****************************************************************************
//	霧描画
//*****************************************************************************
float fog_u;
float fogRate;
float3 fogColor = { 1,1,1 };

struct V_FOG
{
    float4 Pos		: POSITION;
    float4 Color	: COLOR0;
    float2 Tex		: TEXCOORD0;
    float4 Tex2		: TEXCOORD1;
};
// -------------------------------------------------------------
// 頂点シェーダプログラム
// -------------------------------------------------------------
V_FOG VS_fog (
      float4 Pos    : POSITION,          // モデルの頂点
      float2 Tex    : TEXCOORD0,         // テクスチャ座標
      float4 Color  : COLOR0
){
	V_FOG Out;        // 出力データ

	// 位置座標
	Out.Pos = mul(Pos, Projection);
	Out.Tex = Tex;
	Out.Tex.x     += fog_u;
	Out.Color     = Color;
	Out.Color.rgb *= fogColor;
	Out.Color.a   *= fogRate;

	Out.Tex2   =  Out.Pos;
	Out.Tex2.y = -Out.Tex2.y;
	
	return Out;
}

// -------------------------------------------------------------
// ピクセルシェーダプログラム
// -------------------------------------------------------------
float4 PS_fog(V_FOG In) : COLOR
{
	float4 Color;
	Color = In.Color * tex2D( DecaleSamp, In.Tex );
	
	In.Tex2.xy = In.Tex2/In.Tex2.w *0.5f + 0.5f;
	float depth = distance( tex2D( DepthSamp, In.Tex2.xy).r, In.Tex2.z );
	float rate = depth*0.3f;
	rate = saturate( rate );

	Color.a *= rate;
	return Color;
}

technique fog
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZEnable		     = true;
		ZWriteEnable     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_fog();
        PixelShader  = compile ps_3_0 PS_fog();
    }
}

//*****************************************************************************
//	雨描画
//*****************************************************************************
float rain_v;
float rain_rate;

// -------------------------------------------------------------
// 頂点シェーダプログラム
// -------------------------------------------------------------
V_FOG VS_rain (
      float4 Pos    : POSITION,          // モデルの頂点
      float4 Tex    : TEXCOORD0,	         // テクスチャ座標
      float4 Color  : COLOR0
){
	V_FOG Out;        // 出力データ

	Pos.x -= Pos.y * wind * 0.5f;
	// 位置座標
	Out.Pos = mul(Pos, Projection);
	Out.Tex = Tex;
	Out.Tex.x += rain_rate;
	Out.Tex.y += rain_v;
	Out.Color = Color;

	Out.Tex2 =  Out.Pos;
	Out.Tex2.y = -Out.Tex2.y;

	return Out;
}

// -------------------------------------------------------------
// ピクセルシェーダプログラム
// -------------------------------------------------------------
float4 PS_rain(V_FOG In) : COLOR
{
	float4 Color;
	Color = In.Color * tex2D( DecaleSamp, In.Tex );
	
	In.Tex2.xy = In.Tex2/In.Tex2.w *0.5f + 0.5f;
	float depth = distance( tex2D( DepthSamp, In.Tex2.xy).r, In.Tex2.z );
	float rate = depth*0.5f;
	rate = saturate( rate );

	Color.a *= rate;
	return Color;
}

technique rain
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZEnable		     = true;
		ZWriteEnable     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_rain();
        PixelShader  = compile ps_3_0 PS_rain();
    }
}

//*****************************************************************************
//	雷様描画
//*****************************************************************************
float thunder_type;
float thunder_rate;
// -------------------------------------------------------------
// 頂点シェーダプログラム
// -------------------------------------------------------------
V_BASIC VS_ThunderC(
      float4 Pos    : POSITION,          // モデルの頂点
      float4 Tex    : TEXCOORD0,	         // テクスチャ座標
      float4 Color  : COLOR0
){
    V_BASIC Out;        // 出力データ
    
    // 位置座標
	Out.Pos = mul(Pos, Projection);
	Out.Tex = Tex;
	Out.Tex.y += cloud_v*15;

	float	alpha = 1.0f - thunder_rate*0.4f;
	Out.Color = Color;
	Out.Color.rgb *= saturate(alpha);
	return Out;
}

technique thunderC
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_ThunderC();
        PixelShader  = compile ps_3_0 PS_copy();
    }
}



V_BASIC VS_ThunderL(
      float4 Pos    : POSITION,          // モデルの頂点
      float4 Tex    : TEXCOORD0,	         // テクスチャ座標
      float4 Color  : COLOR0
){
    V_BASIC Out;        // 出力データ
    
    // 位置座標
	Out.Pos = mul(Pos, Projection);
	Out.Tex = Tex;

	float	alpha = 1.0f - thunder_rate*0.8f;
	Out.Color = Color;
	Out.Color.rgb *= saturate(alpha);
	return Out;
}

technique thunderL
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_ThunderL();
        PixelShader  = compile ps_3_0 PS_copy();
    }
}

V_BASIC VS_Thunder(
      float4 Pos    : POSITION,          // モデルの頂点
      float4 Tex    : TEXCOORD0,	         // テクスチャ座標
      float4 Color  : COLOR0
){
    V_BASIC Out;        // 出力データ
    
    // 位置座標
	Out.Pos = mul(Pos, Projection);
	Out.Tex = Tex;
	Out.Tex.x += thunder_type;

	float alpha = 1.0f - thunder_rate;
	Out.Color = Color;
	Out.Color.rgb *= saturate(alpha);
	return Out;
}

technique thunder
{
    pass P0
    {
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZEnable		     = false;
		// シェーダ
        VertexShader = compile vs_3_0 VS_Thunder();
        PixelShader  = compile ps_3_0 PS_copy();
    }
}

