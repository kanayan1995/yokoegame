﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour 
{
    public GameObject Pos1;
    public GameObject Pos2;
    public GameObject Pos3;

    public float speed;
    public float rotspeed;

    private Vector3 playerpos;
    private Quaternion rot;

    int flag = 0;

	// Use this for initialization
	void Start () 
    {
       
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (flag == 0)
        {
            Moving1();
        }

        if (flag == 1)
        {
            Moving2();
        }
	}

    public void Moving1()
    {
        playerpos = Pos2.transform.position;

        rot = Quaternion.LookRotation(playerpos);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed * Time.deltaTime);

        transform.position = Vector3.Lerp(transform.position, Pos2.transform.position, speed * Time.deltaTime);

        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    public void Moving2()
    {
        playerpos = Pos1.transform.position;

        rot = Quaternion.LookRotation(playerpos);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed * Time.deltaTime);

        transform.position = Vector3.Lerp(transform.position, Pos1.transform.position, speed * Time.deltaTime);

        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    public void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Pos6")
        {
            flag = 1;
        }
        else if (col.gameObject.tag == "Pos5")
        {
            flag = 0;
        }
    }
}
