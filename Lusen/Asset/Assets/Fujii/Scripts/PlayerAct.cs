﻿using UnityEngine;
using System.Collections;

public class PlayerAct : MonoBehaviour 
{
    //GameObject lightArea;

    public int p_flag = 0;

    public void OnCollisionStay(Collision col)
    {

        if (col.gameObject.tag == "SafeArea")
        {
            p_flag = 2;
        }
    }

    public void OnCollisionExit(Collision col)
    {

        if (col.gameObject.tag == "SafeArea")
        {
            p_flag = 0;
        }
    }
}
