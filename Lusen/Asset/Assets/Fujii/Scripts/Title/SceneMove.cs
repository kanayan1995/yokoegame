﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneMove : MonoBehaviour 
{
    private PlayerAct ply;
    private FadeManager fade;
    public AudioSource dead;

	// Use this for initialization
	void Start () 
    {
        GameObject player = GameObject.Find("TPS");
        ply = player.GetComponent<PlayerAct>();
        fade = GameObject.Find("FadeManager").GetComponent<FadeManager>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            
            if (ply.p_flag == 2 && ply.p_flag != null)
            {
                return;
            }
            if (!dead.isPlaying){
                dead.PlayOneShot(dead.clip);
            }
            fade.fadeColor = Color.red;
			FadeManager.Instance.LoadLevel( "GameOver" , 2.0f );
		}
    }
}
