﻿using UnityEngine;
using System.Collections;

public class Patrol1 : MonoBehaviour
{
    //指定地点の配置を配列で作成
    public Transform[] wayPoints1;
    private tuibi ptl;
    //指定地点
    public int currentRoot1;
    //プレイヤー
    private PlayerAct ply;

    //他オブジェクト衝突時のフラグ
    public int e_flag = 0;

    void Start()
    {
        //ply.GetComponent<PlayerAct>().p_flag = 0;
        ptl = gameObject.GetComponentInChildren<tuibi>();
        GameObject player = GameObject.Find("TPS");
        ply = player.GetComponent<PlayerAct>();
    }

    void Update()
    {
        Vector3 pos = wayPoints1[currentRoot1].position;

        if (Vector3.Distance(this.transform.position, pos) < 1.8f)
        {
            //Debug.Log(Vector3.Distance(this.transform.position, pos));
            currentRoot1 = (currentRoot1 < wayPoints1.Length - 1) ? currentRoot1 + 1 : 0;
        }

        this.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(pos);
    }

    void OnTriggerEnter(Collider col)
    {
        if (ply.p_flag == 2)
        {
            ptl.enabled = true;
        }
    }
}