﻿using UnityEngine;
using System.Collections;

public class move : MonoBehaviour 
{
    private float y;
    
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * 0.15f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0, -1f, 0));
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position += - transform.forward * 0.15f;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, 1f, 0));
        }
	}
}
