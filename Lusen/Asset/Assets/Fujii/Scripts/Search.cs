﻿using UnityEngine;
using System.Collections;

public class Search : MonoBehaviour 
{
    public float speed;
    public float rotspeed;

    private Vector3 playerpos;
    private Vector3 pos;
    private Quaternion rot;

    private Vector3 basePosition;
    private Vector3 baseRotation;

    GameObject enemy;
    public GameObject character;
    public int flag = 0;



	// Use this for initialization
	void Start () 
    {
        playerpos = new Vector3(0, 0, 0);
        pos = new Vector3(0, 0, 0);
        enemy = GameObject.Find("Enemy");
        //enemy.transform.localPosition = new Vector3(0, 0, 0);
        basePosition = enemy.transform.localPosition;
        baseRotation = enemy.transform.localEulerAngles;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (flag == 1)
        {
            Horming();
        }

        if (flag == 0)
        {
            enemy.transform.position = basePosition;
            enemy.transform.rotation = new Quaternion(baseRotation.x, baseRotation.y, baseRotation.z, 90);
        }
	}

    public void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            flag = 1;
            character = col.gameObject;
        }
    }

    public void OnTriggerExit()
    {
        flag = 0;

        enemy.transform.position = basePosition;
        enemy.transform.rotation = new Quaternion(baseRotation.x, baseRotation.y, baseRotation.z, 90);
    }

    public void Horming()
    {
        playerpos = character.gameObject.transform.position;

        pos = playerpos - enemy.transform.position;
        rot = Quaternion.LookRotation(pos);
        enemy.transform.rotation = Quaternion.Lerp(enemy.transform.rotation, rot, rotspeed * Time.deltaTime);

        enemy.transform.position = Vector3.Lerp(enemy.transform.position, character.transform.position, speed * Time.deltaTime);

        enemy.transform.position = new Vector3(enemy.transform.position.x, enemy.transform.position.y, enemy.transform.position.z);
    }
}
