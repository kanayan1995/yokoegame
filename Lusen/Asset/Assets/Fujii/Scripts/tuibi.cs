﻿using UnityEngine;
using System.Collections;

public class tuibi : MonoBehaviour 
{
    public Transform target;
    //public Transform resetPos;
    public GameObject Enemy;
    [SerializeField]
    private int flag = 0;
    GameObject plyr;

    //public AudioClip alert;

	// Use this for initialization
	void Start () 
    {
        plyr = GameObject.Find("TPS");
	}
	
	// Update is called once per frame
	void Update () 
    {
        //プレイヤーを見失ったとき
        if (flag == 0)
        {
            Enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = 6;
        }

        //プレイヤー見つけたとき
        if (flag == 1)
        {
            Vector3 pos1 = target.position;

            Enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(pos1);
            Enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = 9f;

            //ここでパトーロールはずす
            Enemy.GetComponent<Patrol1>().enabled = false;
            //this.gameObject.GetComponent<AudioSource>().PlayOneShot(alert);

            //p_flag2がsafeAreaにいる
            if (plyr.GetComponent<PlayerAct>().p_flag != null && plyr.GetComponent<PlayerAct>().p_flag == 2)
            {
                flag = 0;
                Enemy.GetComponent<Patrol1>().enabled = true;
                this.GetComponent<tuibi>().enabled = false;
            }
        }
	}

    public void OnTriggerStay (Collider col)
    {
        //プレイヤー見つけたときflagが1になる
        if (col.gameObject.tag == "Player")
        {
            flag = 1;
        }
    }

    public void OnTriggerExit(Collider col)
    {
        //見失ったとき　パトーロールをはる
        if (col.gameObject.tag == "Player")
        {
            flag = 0;
            Enemy.GetComponent<Patrol1>().enabled = true;
        }
    }
}
