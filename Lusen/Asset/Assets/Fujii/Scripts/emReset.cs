﻿using UnityEngine;
using System.Collections;

public class emReset : MonoBehaviour 
{
    public GameObject enemy01;

    private Vector3 basePosition;
    private Vector3 baseRotation;

	// Use this for initialization
	void Start () 
    {
       
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            enemy01.transform.position = basePosition;
            enemy01.transform.rotation = new Quaternion(baseRotation.x, baseRotation.y, baseRotation.z, 90);
        }
    }
}
