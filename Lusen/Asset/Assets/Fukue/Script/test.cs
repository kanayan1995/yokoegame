﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour
{
    public Vector3 m_Pos;
	// Use this for initialization
	void Start ()
    {
        m_Pos = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.localPosition = m_Pos;
        m_Pos.y += 2.0f;
        if(m_Pos.y > 2000)
        {
            Destroy(gameObject);
        }
	}
}
