﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    bool m_GameClear = false;
    private FadeManager fade;

	// Use this for initialization
	void Start ()
    {
        fade = GameObject.Find("FadeManager").GetComponent<FadeManager>();
	}

    IEnumerator WaitAndReloadScene()
    {      
        fade.fadeColor = Color.white;
        yield return new WaitForSeconds(2.0f);
        //SceneManager.LoadScene("GameClear");
        FadeManager.Instance.LoadLevel("GameClear", 2.0f);
    }

        // Update is called once per frame
    void Update ()
    {
        if (m_GameClear) return;
    }
    void OnTriggerEnter(Collider other)
    {
        //GetComponent<Rigidbody>().Sleep();


        m_GameClear = true;
        StartCoroutine(WaitAndReloadScene());

    }
}
