﻿using UnityEngine;
using System.Collections;

public class Tracking2 : MonoBehaviour
{
    // プレイヤーを代入
    public Transform m_Player;
    //移動速度
    public float m_WalkSpeed;
    //ターゲット
    Transform m_Target;
    //距離用一時的変数
    float m_TmpDis = 0;
    //最も近いオブジェクトの距離
    float m_NearDis = 0;
    //目標のオブジェクトとの距離
    float m_TarDis = 0;
    //ターゲットの位置
    Vector3 m_TargetPos;
    //ターゲットとの距離
    Vector3 m_Direction;
    //処理中断時間
    float m_Time = 0.0f;

    // 回転速度
    float m_RotateSpeed = 60.0f;
    float m_TimeToNewDirection = 0.0f;
    // 向きを変える間隔
    float m_DirectionTraveTime = 2.0f;

    private CharacterController m_Controller;



    // Use this for initialization
    void Start ()
    {
        // "Player"の位置を取得
        m_Player = GameObject.FindGameObjectWithTag("Player").transform;

        // 扱いやすいようにキャラクターコントローラーをキャッシュしておく
        m_Controller = GetComponent<CharacterController>();
    }

    void Search()
    {
        // playerとの距離を取得
        m_NearDis = Vector3.Distance(m_Player.transform.position, this.transform.position);

        // "Surviver"tagを持つオブジェクトを１つずつチェック
        foreach (GameObject obs in
            GameObject.FindGameObjectsWithTag("Surviver"))
        {
            //自身と取得したオブジェクトの距離を取得
            m_TmpDis = Vector3.Distance(obs.transform.position, this.transform.position);

            //オブジェクトの距離が近くて、５以内であればオブジェクトの値を取得
            //一時変数に距離を格納
            if (m_NearDis > m_TmpDis && m_TmpDis < 50)
            {
                m_Target = obs.transform;

                // 移動処理
                return;
            }
            if(m_NearDis < 20)
            {
                m_Target = m_Player.transform;
                return;
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        // ターゲットを発見した場合追跡を行う
        if (m_Target)
        {
            // targetと自身の距離を取得
            m_TarDis = Vector3.Distance(m_Target.transform.position, this.transform.position);

            // ターゲットの位置
            m_TargetPos = m_Target.position;

            //方向
            m_Direction = m_TargetPos - transform.position;

            //単位化
            m_Direction = m_Direction.normalized;

            //プレイヤーの方を向く
            //m_Controller.SimpleMove(m_Direction);
            transform.Translate(Vector3.forward * m_WalkSpeed * Time.deltaTime);

            // ターゲットが範囲外にいる場合は追跡を中止
            if (m_TarDis > 20)
            {
                m_Target = null;
            }
        }
        else
        {
            Search();

            if(Time.time > m_TimeToNewDirection)
            {
                // 乱数に応じて」、右回転か左回転かを決定
                if (Random.Range(0, 11) > 5)
                    transform.Rotate(new Vector3(0, 5, 0), m_RotateSpeed);
                else
                    transform.Rotate(new Vector3(0, -5, 0), m_RotateSpeed);

                m_TimeToNewDirection = Time.time + m_DirectionTraveTime;
            }

            
        }
	}
}
