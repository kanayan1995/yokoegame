﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneSwich : MonoBehaviour
{
	public GameObject enemy;
	private bool moveFlag;
	public float speed;
	private FadeManager fade;

	// Use this for initialization
	void Start()
	{
		moveFlag = false;
		fade = GameObject.Find( "FadeManager" ).GetComponent<FadeManager>();
	}
	// Update is called once per frame
	void Update()
	{
		if( moveFlag == true ) {
			enemy.transform.position += enemy.transform.forward * speed;
		}
	}

	//ゲーム画面に移行
	public void OnClickGameStart()
	{
		// SceneManager.LoadScene("1");
		moveFlag = true;
		fade.fadeColor = Color.black;
		FadeManager.Instance.LoadLevel( "MainScene" , 2.0f );
	}



	//ゲームを終了
	public void OnClickExid()
	{
		Application.Quit();
	}

	public void OnClickTako()
	{
		//SceneManager.LoadScene("Title");
		fade.fadeColor = Color.black;
		FadeManager.Instance.LoadLevel( "TitleScene" , 2.0f );
	}
	public void OnClickIka()
	{
		//SceneManager.LoadScene("GameOver");
		FadeManager.Instance.LoadLevel( "GameOver" , 2.0f );
	}
	public void OnClickGameClear()
	{
		FadeManager.Instance.LoadLevel( "GameClear" , 2.0f );
	}
}
