﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Ending : MonoBehaviour
{
	private FadeManager fade;

	void Start()
	{
		StartCoroutine( DelayMethod( 36.0f ) );
		fade = GameObject.Find( "FadeManager" ).GetComponent<FadeManager>();
	}

	void Update()
	{

	}
	private IEnumerator DelayMethod( float waitTime )
	{
		yield return new WaitForSeconds( waitTime );
		fade.fadeColor = Color.white;
		FadeManager.Instance.LoadLevel( "TitleScene" , 2.0f );
	}

}

