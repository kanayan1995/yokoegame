﻿using UnityEngine;
using System.Collections;

public class test2 : MonoBehaviour {

    private int _frame = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _frame++;

        //30フレーム毎に表示・非表示を繰り返す
        if (_frame / 30 % 2 == 0)
        {
            GetComponent<Renderer>().enabled = false;
        }
        else
        {
            GetComponent<Renderer>().enabled = true;
        }

    }
}
