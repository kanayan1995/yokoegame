﻿using UnityEngine;
using System.Collections;

public class GetLight : MonoBehaviour
{
	//プレイヤーの情報
	private Player player;					  //プレイヤーを取得
	private float checkLength;				  //取得されるかを判定する距離

	//他のスクリプト
	private ChangeEmission changeEmission;    //光消すスクリプト
	private GetLightPower getLightPower;      //光のパワーが保存されている

	//視野に入っていたらとかに使う
	private bool cheakLook;									//カメラから見えているかどうか
	private const string CAMERA_TAG_NAME = "MainCamera";	//メインカメラのタグ

	//光が消える時間
	public float lostEmissionTime = 2.0f;					//光が消えるまでにかかる時間

	//ライトのステート
	public enum LIGHT_STATE
	{
		STAY,           //存在しているだけ
		GETLIGHTPOWER,	//光を取得される
		MAKEEFFECT,		//光を取得される ここでエフェクトもだしたい
	}
	private LIGHT_STATE lightState;     //ライトのステート

	public GameObject powerEffect;		//光取得エフェクト

	//----------------------------------------
	//			初期化
	//----------------------------------------
	void Start()
	{
		//プレイヤー関連
		player = GameObject.Find( "Player" ).GetComponent<Player>();
		checkLength = player.GetLightGetDist();
		//スクリプト
		changeEmission = GetComponent<ChangeEmission>();
		changeEmission.SetLostTime( lostEmissionTime );		//きえるまでの時間を指定
		getLightPower = GetComponent<GetLightPower>();
		//初期ステート　待機状態
		lightState = LIGHT_STATE.STAY;
	}

	//----------------------------------------
	//			State（状態）
	//----------------------------------------
	void State()
	{
		switch( lightState ) {
			case LIGHT_STATE.STAY:
				Stay();
				break;
			case LIGHT_STATE.GETLIGHTPOWER:
				GetLightPower();
				break;
			case LIGHT_STATE.MAKEEFFECT:
				MakeEffect();
        break;
		}
	}

	//----------------------------------------
	//			待機状態
	//			この間にプレイヤーとの距離測って近かったら取得される
	//			プレイヤーの状態が光取得じゃなかったら更新しない
	//			ここでついでに消えるフラグを付ける　case分が増えるから
	//----------------------------------------
	void Stay()
	{
		if( player.GetMode() != Player.PLAYER_MODE.GET_LIGHT ) return;
		Vector3 playerPos = player.GetLightGetPos();
		Vector3 lightPos = this.transform.position;
		float workDist = Vector3.Distance( playerPos , lightPos );
		//プレイヤーとの距離が近かったら取得モードに入る 
		if( workDist < player.GetLightGetDist() ) {
			changeEmission.SetLostFlagTrue();
			lightState = LIGHT_STATE.GETLIGHTPOWER;
    }
	}

	//----------------------------------------
	//			ここでとりあえず光のパワー取得
	//----------------------------------------
	void GetLightPower()
	{
		player.SetLightPlusPower( getLightPower.GetLightPowerValue() );
		lightState = LIGHT_STATE.MAKEEFFECT;
		Instantiate( powerEffect , transform.position , Quaternion.identity );
	}

	//----------------------------------------
	//			光取得のエフェクト出したい(希望)
	//----------------------------------------
	void MakeEffect()
	{
		if( changeEmission.GetCheckLost() == true ) {
			//Destroy( gameObject );
		}
	}

	//----------------------------------------
	//			更新
	//----------------------------------------
	//void Update()
	void FixedUpdate()
	{
		if( cheakLook == false ) return;    //視野範囲に入っていなかったら更新しない

		State();
	}

	//----------------------------------------
	//			カメラの視野に入っていた時に動く処理
	//----------------------------------------
	private void OnWillRenderObject()
	{
		if( Camera.current.tag == CAMERA_TAG_NAME ) {
			cheakLook = true;
		}
	}
}
