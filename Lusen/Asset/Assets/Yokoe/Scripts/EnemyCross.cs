﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class EnemyCross : MonoBehaviour
{
	public GameObject enemy;
	private bool startFlag;
	private float destoryTime;
	private float destoryTimeMax = 5.0f;

	void Start()
	{
		startFlag = false;
		destoryTime = 0;
	}

	// Update is called once per frame
	//void Update()
	void FixedUpdate()
	{
		if( startFlag == false ) return;
		transform.position += transform.forward;

		destoryTime += Time.deltaTime;
		if( destoryTime > destoryTimeMax ) Destroy( this.gameObject );
	}

	void OnTriggerEnter( Collider collider )
	{
		if( collider.transform.tag == "Player" ) {
			startFlag = true;
			enemy.SetActive( true );
		}
	}
}
