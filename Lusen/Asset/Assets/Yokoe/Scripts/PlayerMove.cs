﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class PlayerMove : MonoBehaviour
{
	public float walkSpeed = 1.0f;	//歩く速さ保持		
	public float runSpeed  = 1.2f;	//走る速さ保持
	private float speed;            //ここにwalkSpeed,runSpeedの値を入れる
	public float angleSpeed;		//回転する速さ

	//音関係	
	PlayerSound playerSound;    //関数で再生させるだけ

	private enum MOVE_STATE
	{
		WALK,
		RUN
	}
	private MOVE_STATE moveState; //歩くか走っているか

	private ThrowPos throwPos;

	//----------------------------------------
	//			マウスの移動量
	//----------------------------------------
	void MousePosLook()
	{
		Vector3 workPos = Input.mousePosition;    //Vector3でマウス位置座標を取得する 
		workPos.z = 10;             //Z軸修正 		
		Vector3 workMousePos;
		workMousePos = Camera.main.ScreenToWorldPoint( workPos );//マウス位置座標をスクリーン座標からワールド座標に変換する 
		Vector3 workDir = workMousePos - Camera.main.transform.position;
		workDir.Normalize();
		Camera.main.transform.forward = workDir;
	}

	//----------------------------------------
	//			初期化
	//----------------------------------------
	void Start()
	{
		moveState = MOVE_STATE.WALK;
		playerSound = gameObject.GetComponentInChildren<PlayerSound>();
		speed = walkSpeed;
		throwPos = gameObject.GetComponentInChildren<ThrowPos>();

  }

	//----------------------------------------
	//			走っているか歩いているか
	//----------------------------------------
	void StateCheak()
	{
		if( Input.GetMouseButton( 2 ) ) moveState = MOVE_STATE.RUN;
		else moveState = MOVE_STATE.WALK;
	}

	//----------------------------------------
	//			ステートによってスピードを入れる
	//----------------------------------------
	void SpeedCheak()
	{
		switch( moveState ) {
			case MOVE_STATE.WALK:
				speed = walkSpeed;
				break;
			case MOVE_STATE.RUN:
				speed = runSpeed;
				break;
		}
	}

	//----------------------------------------
	//			走っているか歩いているかで音を分ける
	//----------------------------------------
	void CheakSound()
	{
		if( moveState == MOVE_STATE.WALK ) playerSound.PlayWalk();
		else playerSound.PlayRun();
	}

	//----------------------------------------
	//			動く
	//			スピードも調整できるように
	//----------------------------------------
	void Move(float setSpeed)
	{
		if( Input.GetKey( KeyCode.W ) ) {
			transform.position += Camera.main.transform.forward  * setSpeed;
			CheakSound();
    }
		if( Input.GetKey( KeyCode.S ) ) {
			transform.position -= Camera.main.transform.forward * setSpeed;
			CheakSound();
		}
	}

	//----------------------------------------
	//			移動
	//----------------------------------------
	void MoveControl()
	{
		StateCheak();   //ステート変えるかどうか
		SpeedCheak();		//これを入れてからMoveを入れる
		Move( speed );
		if( Input.GetKey( KeyCode.A ) ) transform.rotation *= new Quaternion( 0 , -angleSpeed , 0 , 1 );
		if( Input.GetKey( KeyCode.D ) ) transform.rotation *= new Quaternion( 0 , angleSpeed , 0 , 1 );

        ////マウスでのカメラ調整
        //float mouseXraw = Input.GetAxisRaw("Mouse X");
        //float mouseYraw = Input.GetAxisRaw("Mouse Y");
        //Camera.main.transform.Rotate( -mouseYraw , mouseXraw , 0 , Space.Self );
        //Camera.main.transform.rotation = new Quaternion( Camera.main.transform.rotation.x , Camera.main.transform.rotation.y , 0 , Camera.main.transform.rotation.w );
	}

	// Update is called once per frame
	//void Update()
	void FixedUpdate()
	{
		MoveControl();
	}
}
