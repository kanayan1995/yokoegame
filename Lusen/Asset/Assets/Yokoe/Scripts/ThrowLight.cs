﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class ThrowLight : MonoBehaviour
{
	private Rigidbody rigid;              //物理
	private Vector3 throwFore;            //投げる時に加わる力

	//子オブジェクト
	private GameObject throwLight;        //子オブジェクトにある"ThrowLight"
	private const string THROW_OBJ_NAME = "ThrowLight";
	private GameObject safeZone;          //子オブジェクトにある"SafeZone"
	private const string SAFEZONE_OBJ_NAME = "SafeZone";

	//ライトを消す
	//lifeTimeは結構いろんなとこで使っている　共有emission lightRange  Destoryする時間　changeScale　に使っている
	public float lifeTime = 5.0f;               //ライトを消す時に使う時間　 
	private ChangeLightRenge pointLightRange;		//ポイントライトの範囲を小さくする
	private ChangeLightRenge spotLightRange;		//スポットライトの範囲を小さくする
	private ChangeEmission   emission;				  //物体の輝きをなくす		

	//ぶつかってから上に上がる
	private HeightUP heightUp;                  //浮き上がることをしてくれるスクリプト

	//大きさを調整するスクリプト
	private ChangeScale changeScale;            //大きさを変えるスクリプト(今回は小さくする)

	//あたり判定
	private SphereCollider sphereColl;			//あたり判定
	public GameObject collisionHalf;            //あたり判定の半径

	public AudioSource soundCreate;             //safeZoneが作られるときの音

	//光っている範囲（目に入れるやつ）				
	public GameObject lightRange;                //光っているやつ

	//投げたときに動く状態
	public enum LIGHTSTATE
	{
		THROW,          //投げれらてる時
		STOP,           //当たったとき
		LIGHTING,       //光を出す時
		HEIGHTMOVE,     //少し上に上がる
		HEIGHTSTAY,     //上がるまで待ってる
		DELETEFLUG,     //消えるフラグを出す
		DELETECOUNT,    //消えるカウント
		DELETESTAY,     //消えるのを待つ
		STAY,
	}
	private LIGHTSTATE lightState;        //投げるライトのステート

	private float deleteTimeMax = 10.0f;  //設置してから消えるまで
	private float deleteTime;             //設置してから動く時間 これをついでに消す時にも使う

	private bool gaitouFlag = false;

	//あんまりやりたくないけどこれが確実
	//あたり判定を急に消すとバグが起こる気がするので書く
	//光がplayerが入っているかどうか判断するとかアホやけどしゃあない
	private PlayerAct playerAct;
	private bool playerInSafeArea;

	//加算システムをonにするかしないか プレイヤーが教える
	private bool addSystemFlag;

	//----------------------------------------
	//			初期化
	//----------------------------------------
	void Awake()
	{
		rigid = GetComponent<Rigidbody>();
		//輝きの範囲
		emission = GetComponentInChildren<ChangeEmission>();
		////浮き上がるのに使う変数
		heightUp = GetComponentInChildren<HeightUP>();
		//子オブジェクト(ThrowLight)にいるポイントライトの範囲
		throwLight = transform.FindChild( THROW_OBJ_NAME ).gameObject;
		pointLightRange = throwLight.GetComponentInChildren<ChangeLightRenge>();
		//子オブジェクト(safeZone)にあるSafeZoneにいるスポットライトを取得
		safeZone = transform.FindChild( SAFEZONE_OBJ_NAME ).gameObject;
		spotLightRange = safeZone.GetComponentInChildren<ChangeLightRenge>();
		//子オブジェクト(safeZone)にあるSafeZoneにいるlightObjectに取りに行く
		changeScale = safeZone.GetComponentInChildren<ChangeScale>();
		//あたり判定
		sphereColl = gameObject.GetComponent<SphereCollider>();
		lightState = LIGHTSTATE.THROW;
		//レイヤーマスクは初めはMakeSafeArea
		gameObject.layer = LayerMask.NameToLayer( "MakeSafeArea" );
		//あたり判定を急に消すとバグが起こる気がするので書く
		playerAct = GameObject.Find( "TPS" ).GetComponent<PlayerAct>();
		playerInSafeArea = false;
    }

	//----------------------------------------
	//			止まらす		
	//----------------------------------------
	void Stop()
	{

	}

	//----------------------------------------
	//			更新
	//----------------------------------------
	//void Update()
	void FixedUpdate()
	{
		State();
	}

	//----------------------------------------
	//			ステート（状態）
	//----------------------------------------
	void State()
	{
		switch( lightState )
		{
			case LIGHTSTATE.THROW:
				break;
			case LIGHTSTATE.STOP:
				MoveStop();
				break;
			case LIGHTSTATE.LIGHTING:
				break;
			case LIGHTSTATE.HEIGHTMOVE:
				HeightMove();
				break;
			case LIGHTSTATE.HEIGHTSTAY:
				HightTop();
				break;
			case LIGHTSTATE.DELETEFLUG:
				if( gaitouFlag == false )	//無理やり付けたし
				{
					DeleteTimer();
					DeleteFlag();
				}
				break;
			case LIGHTSTATE.DELETECOUNT:
				DeleteCount();
				break;
			case LIGHTSTATE.DELETESTAY:
				DestoryObj();
				break;
		}
	}

	//----------------------------------------
	//			地面についてから上に動かす処理
	//			処理自体は別のスクリプトに任している
	//----------------------------------------
	void HeightMove()
	{
		heightUp.SetHeightUpFlagTrue();
		lightState = LIGHTSTATE.HEIGHTSTAY;
		soundCreate.PlayOneShot( soundCreate.clip );
		sphereColl.radius = collisionHalf.transform.localPosition.z;
		transform.tag = "SafeArea";
	}

	//----------------------------------------
	//		  上がりきるまで待っている
	//			上がり切ったら光が消えるStateに変える
	//			上がりきったかどうかは別スクリプトのHeightUPスクリプトから取ってくる
	//			そしてこの瞬間あたり判定の大きさを変える
	//----------------------------------------
	void HightTop()
	{
		if( heightUp.GetHeightMax() == true ) {
			lightState = LIGHTSTATE.DELETEFLUG;
            //sphereColl.radius = collisionHalf.transform.localPosition.z;
            ////sphereColl.center = throwLight.transform.localPosition;
            //transform.tag = "SafeArea";
    }
	}

	//----------------------------------------
	//			発生した時カウントする
	//----------------------------------------
	void DeleteCount()
	{
		if( deleteTime < deleteTimeMax ) {
			deleteTime += Time.deltaTime;
		}
		else {
			lightState = LIGHTSTATE.DELETEFLUG;
			deleteTime = 0;
		}
	}

	//----------------------------------------
	//			光を表現するために使っているオブジェクとの消える時間を設定する
	//			使っている変数同じやしまとめた
	//----------------------------------------
	void DeleteTimer()
	{
		emission.SetLostTime( lifeTime );
		pointLightRange.SetLostTime( lifeTime );
		spotLightRange.SetLostTime( lifeTime );
		changeScale.SetMinusScale( lifeTime );
	}

	//----------------------------------------
	//			光が消える前に動かす処理
	//			時間設定やったり
	//----------------------------------------
	void DeleteFlag()
	{
		pointLightRange.SetLostFlagTrue();
		spotLightRange.SetLostFlagTrue();
		emission.SetLostFlagTrue();
		changeScale.SetChangeFlagTrue();
		lightState = LIGHTSTATE.DELETESTAY;
	}

	//----------------------------------------
	//			光の中に入っていたときに光が消えたとき
	//----------------------------------------
	void PlayerActFlagChange()
	{
		if( playerInSafeArea == true ) {
			playerAct.p_flag = 0;
		}
	}

	//----------------------------------------
	//			光が消える前に動かす処理
	//----------------------------------------
	void DestoryObj()
	{
		deleteTime += Time.deltaTime;
		if( deleteTime > lifeTime ) {
			Destroy( this.gameObject );
			PlayerActFlagChange();
		}
	}

	//----------------------------------------
	//			発生した時に力を与える
	//----------------------------------------
	public void SetAddThrowForce( Vector3 setThrowForce )
	{
		throwFore = setThrowForce;
		rigid.AddForce( throwFore );
	}

	//----------------------------------------
	//			あたり判定当たった瞬間の処理
	//----------------------------------------
	void OnCollisionEnter( Collision collision )
	{
		switch( collision.transform.tag ) {
			case "SafeArea":
				if( addSystemFlag == true )
				{
					SetLifeTimePlus( collision.gameObject.GetComponent<ThrowLight>().GetLifeTime() );
					Destroy( collision.gameObject );
				}
				break;
			case "Stage":
				transform.tag = "SafeArea";
				lightState = LIGHTSTATE.STOP;
				gameObject.layer = LayerMask.NameToLayer( "SafeArea" );
				break;
			case "Gaitou":
				//lightState = LIGHTSTATE.STAY;
				gaitouFlag = true;
				break;
			case "Player":
				playerInSafeArea = true;
				break;
		}
	}

	//----------------------------------------
	//			あたり判定が外れた瞬間の処理
	//----------------------------------------
	public void OnCollisionExit( Collision col )
	{

		if( col.gameObject.tag == "Player" ) {
			playerInSafeArea = false;
		}
	}

	//----------------------------------------
	//			何かに中ったときにつける処理
	//----------------------------------------
	void MoveStop()
	{
		//物理関係を切る
		rigid.velocity = Vector3.zero;
		rigid.angularVelocity = Vector3.zero;
		rigid.mass = 0;
		rigid.useGravity = false;
		rigid.constraints = RigidbodyConstraints.FreezeAll;
        rigid.isKinematic = true;
		//transform.rotation = Quaternion.identity;
		lightState = LIGHTSTATE.HEIGHTMOVE;
		safeZone.SetActive( true );
	}

	//----------------------------------------
	//		セッター・ゲッター
	//----------------------------------------
	void SetLifeTimePlus(float plusLifeTime)
	{
		lifeTime += plusLifeTime;
	}
	float GetLifeTime() { return lifeTime; }
	public void SetSafeZoneDir(Vector3 setHitPosDir) { lightRange.transform.up = setHitPosDir; }
	public void SetAddSystem(bool addSystem )		 { addSystemFlag = addSystem; }
}
