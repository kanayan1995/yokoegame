﻿using UnityEngine;
using System.Collections;
//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //

//それぞれが光の持っている力を保持しておくスクリプト
public class GetLightPower : MonoBehaviour
{
	public int lightPower;		//オブジェクトが持っている光のパワー
	public int GetLightPowerValue() { return lightPower; }
}
