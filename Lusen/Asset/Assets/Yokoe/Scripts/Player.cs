﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class Player : MonoBehaviour
{
	//光を取得するときに使うもの
	public GameObject playerHeight;		  //目線らへんに高さ調整
	public GameObject getLightDist;		  //ライトを取得できる距離(距離として取るのはZ値だけ)

	//光を投げる時に使うもの
	public GameObject throwLight;		  //投げるライト(オブジェクト)
	private Vector3 mousePosition;		  //マウスの位置
	//光を投げたり取得したりするときに使うパラメータ
	private int lightPowerMax;			  //保持できる光の最大値
	private int lightPower;				  //今保持している光の量
	private const int useLightPower = 10; //光を出す時に使う量

	//音関係のスクリプト
	private PlayerSound playerSound;

	//投げる時に使うスクリプト
	ThrowRigidbody throwRigidbody;

	//マウスを押している時に出る光の範囲
	public GameObject lightRange;

	//プレイヤーの状態
	public enum PLAYER_MODE
	{
		IDEL,
		MOVE,
		GET_LIGHT,
	}
	private PLAYER_MODE playerMode;

	//光を投げる時飛ぶ場所を取得したい
	private ThrowPos throwPos;

	private bool breakClock;    //時計がこわれた

	private bool addSystemFlag;	//加算システムを作るか　（trueならON falseならOFF）

	//----------------------------------------
	//			初期化
	//----------------------------------------
	void Start()
	{
		playerMode = PLAYER_MODE.IDEL;
		lightPowerMax = 120;
		lightPower = 100;
		playerSound = gameObject.transform.root.gameObject.GetComponentInChildren<PlayerSound>(); //一旦親行ってから
		throwRigidbody = GetComponent<ThrowRigidbody>();
		//光を飛ばす場所を取得したい
		throwPos = gameObject.GetComponent<ThrowPos>();
		breakClock = false;
		addSystemFlag = true;
    }

	//----------------------------------------
	//			マウスの位置を検索
	//----------------------------------------
	void SearchMousePosition()
	{
		Vector3 workPos;
		workPos = Input.mousePosition;
		workPos.z = 10.0f;
		mousePosition = Camera.main.ScreenToWorldPoint( workPos );
	}
	//----------------------------------------
	//　右クリックを押したとき　プレイヤーが光取得状態になる
	//----------------------------------------

	void MouseRightClick()
	{
		if( Input.GetMouseButton( 1 ) )
		{
			playerMode = PLAYER_MODE.GET_LIGHT;
		}
		else
		{
			playerMode = PLAYER_MODE.IDEL;
		}

	}

	//----------------------------------------
	//　左クリックした時 自分からマウスの向きを取って発射している
	//　ここで光を投げると自分の持っているパワーもも減る
	//　ThrowLightを複製してRigidBodyにAddforceしている
	//	ついでに音も出す
	//----------------------------------------
	void MouseLeftClick()
	{
		if( lightPower < useLightPower ) return; //光のパワーが足らなかったら発動しない

		//マウスを押し続けている時
		if( Input.GetMouseButton( 0 ) )
		{
			lightRange.transform.position = throwPos.GetHitPointPos();
			Vector3 workDir = throwPos.GetHitNormal();
			lightRange.transform.up = workDir;
			lightRange.SetActive( true );
		}
		else lightRange.SetActive( false );
		//マウスを離したとき時
		if( Input.GetMouseButtonUp( 0 ) )
		{
			lightPower -= useLightPower;
			GameObject workLight;
			workLight = (GameObject)Instantiate( throwLight , throwRigidbody.GetThrowPos() , Quaternion.identity );
			workLight.GetComponent<ThrowLight>().SetAddThrowForce( throwRigidbody.GetThrowPower() );
			workLight.GetComponent<ThrowLight>().SetSafeZoneDir( throwPos.GetHitNormal() );
			workLight.GetComponent<ThrowLight>().SetAddSystem( addSystemFlag );
			playerSound.PlayThrow();
		}
	}

	//----------------------------------------
	//			ステート
	//----------------------------------------
	void State()
	{
		MouseRightClick();
		MouseLeftClick();

		switch( playerMode ) {
			case PLAYER_MODE.IDEL:
				break;
			case PLAYER_MODE.MOVE:
				break;
			case PLAYER_MODE.GET_LIGHT:
				break;
		}

	}

	//----------------------------------------
	//			テスト用関数
	//----------------------------------------
	void Debug()
	{
		//ゲージ回復
		if( Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space) ) {
			lightPower += 10;
			lightPower = Mathf.Min( lightPower , lightPowerMax ); //小さいほうの値を取る
		}

		//加算システムのON,OFF
		if( Input.GetKeyDown( KeyCode.E ) )
		{
			addSystemFlag = true;
			playerSound.PlayAbsorption();
        }
		if( Input.GetKeyDown( KeyCode.R ) )
		{
			addSystemFlag = false;
			playerSound.PlayAbsorption();
		}
	}

	//----------------------------------------
	//			更新
	//----------------------------------------
	//void Update()
	void FixedUpdate()
	{
		State();
		//デバック用関数
		Debug();
		//時計が壊れるフラグ　（ゴミコード）
		if (breakClock == false && lightPower == 0) {
			breakClock = true;
		}
	}

	//----------------------------------------
	//			セッター　・　ゲッター
	//----------------------------------------
	public PLAYER_MODE GetMode() { return playerMode; }
	public Vector3 GetLightGetPos() { return playerHeight.transform.position; }
	public float GetLightGetDist() { return Mathf.Abs(getLightDist.transform.localPosition.z); } //ライトを取得する距離
	public int GetLightPower() { return lightPower; }
	public bool GetBreakClock() { return breakClock; }

	//----------------------------------------
	//			光を取得するときに使う
	//----------------------------------------
	public void SetLightPlusPower(int setPlusPower) {
		lightPower += setPlusPower;
		lightPower = Mathf.Min( lightPower , lightPowerMax ); //小さいほうの値を取る
		playerSound.PlayAbsorption();
	}
}
