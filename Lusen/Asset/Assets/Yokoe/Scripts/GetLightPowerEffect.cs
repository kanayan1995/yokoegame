﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class GetLightPowerEffect : MonoBehaviour
{
	private Player player;                  //プレイヤーを取得

	public enum EFFECTSTATE
	{
		MOVEUP,
		GETPOWER,
		DELETE,
	}
	EFFECTSTATE state;

	private float moveUpTimeMax = 2.0f;				//上に上がる時間
	private float moveUpTime;						//時間更新

	//大きさ関連
	private float minusScalelTime = 2.0f;           //小さくなる時間
	private Vector3 minusScale;                     //1フレームに小さくなる

	private float deleteDist;						//消える距離


	//----------------------------------------
	//			初期化
	//----------------------------------------
	void Start()
	{
		player = GameObject.Find( "Player" ).GetComponent<Player>();
		moveUpTime = 0.0f;
		minusScale = this.transform.localScale / ( minusScalelTime * 60.0f );
		deleteDist = 1.0f;
	}

	//----------------------------------------
	//			少し上に上がってから近づく
	//----------------------------------------
	void MoveUp()
	{
		transform.position += Vector3.up * 0.02f;
		moveUpTime += Time.deltaTime;
		if (moveUpTime > moveUpTimeMax)
		{
			state = EFFECTSTATE.GETPOWER;
		}
	}

	//----------------------------------------
	//			取得するために近づいてくる
	//----------------------------------------
	void GetPower()
	{
		Vector3 playerPos = player.GetLightGetPos();
		Vector3 lightPos = this.transform.position;
		float workDist = Vector3.Distance( playerPos , lightPos );
		Vector3 workDir;
		workDir = playerPos - lightPos;
		Vector3.Normalize( workDir );
		this.transform.position += workDir * 0.04f;
		//スケール関連
		this.transform.localScale -= minusScale;
		if( workDist < deleteDist ) state = EFFECTSTATE.DELETE;
	}

	//----------------------------------------
	//			消える処理
	//----------------------------------------
	void Delete()
	{
		Destroy( this.gameObject );
	}

	//----------------------------------------
	//			ステート更新
	//----------------------------------------
	void State()
	{
		switch(state)
		{
			case EFFECTSTATE.MOVEUP:
				MoveUp();
				break;
			case EFFECTSTATE.GETPOWER:
				GetPower();
				break;
			case EFFECTSTATE.DELETE:
				Delete();
				break;
		}
	}

	//----------------------------------------
	//			更新
	//----------------------------------------
	//void Update()
	void FixedUpdate()
	{
		State();
	}
}
