﻿using UnityEngine;
using System.Collections;

public class FrameLate : MonoBehaviour {

	// Use this for initialization
	void Start () {

        QualitySettings.vSyncCount = 1;		// VSync（垂直同期）をONにする
        Application.targetFrameRate = 60;  // フレームレートを設定
    }
}
