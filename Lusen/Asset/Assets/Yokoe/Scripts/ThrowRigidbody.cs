﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class ThrowRigidbody : MonoBehaviour
{
	//投げる時に使う変数
	private ThrowPos throwPos;      //投げる時に使うパラメーター
	private Vector3 throwPosition;  //投げる位置
	private Vector3 hitPos;         //着地地点
	private float lengthMax;        //最大で飛ぶ距離

	//飛ばす時に使うもの　おもにベクトルとか	
	private float heightDirMax = 45*Mathf.Deg2Rad;	//最大の上の角度
	private Vector3 throwDirMax;					//最大の角度 y軸に45が限界
	private Vector3 throwDir;						//投げる向き
	private float throwPowerMax = 552;				//最大のパワー	
	private float throwPower;						//パワー
	private Vector3 throwAdd;						//こいつで飛ばす　これをプレーヤーに渡す
	//private Vector3 mouseDir;						//マウスへの向き
	private float rate;								//割合
	private float ratePlus;							//投げる位置が自分に近いほど値を大きくする
	private float ratePlusMax = 1.5f;

	//----------------------------------------
	//			初期化
	//----------------------------------------
	void Start()
	{
		//投げる時に使う変数 ThrowPosから取ってくる
		throwPos = gameObject.GetComponent<ThrowPos>();
		throwPosition = throwPos.GetShotPos();
		hitPos = throwPos.GetHitPointPos();
		lengthMax = throwPos.GetLenghtMax();

		//投げる時に使う
		throwDirMax = new Vector3( 0 , 0 , 0 );
		rate = 0;
	}

	//----------------------------------------
	//			投げるのに必要なものを更新
	//----------------------------------------
	void ThrowDataUpdete()
	{
		throwPosition = throwPos.GetShotPos();
		hitPos = throwPos.GetHitPointPos();
		lengthMax = throwPos.GetLenghtMax();
		throwDir = throwPos.GetMouseDir();
	}

	//----------------------------------------
	//			長さ調整
	//			割合を出して最大から飛ばす力を調整している
	//----------------------------------------
	void SetThrowAdd()
	{
		//距離で割る(HitPos)を最大の長さで割る	これで割合を取っている
		rate = throwPos.GetHitPosDist( ) / lengthMax;
		//最大を距離で割って近ければ値を大きくする
		ratePlus = lengthMax / throwPos.GetHitPosDist();
		ratePlus = Mathf.Min( ratePlus , ratePlusMax );
		//最大に割合をかける
		throwPower = throwPowerMax * rate * ratePlus;
		throwDir.y = heightDirMax * rate;
	}

	//----------------------------------------
	//			更新
	//----------------------------------------
	//void Update()
	void FixedUpdate()
	{
        ThrowDataUpdete();
		SetThrowAdd();
		//投げる強さ更新
		throwAdd = throwDir * throwPower;
	}

	//----------------------------------------
	//			セッター・ゲッター
	//----------------------------------------
	public Vector3 GetThrowPower() { return throwAdd; }
	public Vector3 GetThrowPos() { return throwPosition; }
}
