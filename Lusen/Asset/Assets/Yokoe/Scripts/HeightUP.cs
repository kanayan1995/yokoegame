﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class HeightUP : MonoBehaviour
{
	//ぶつかってから上に上がる
	public GameObject heightPos;          //浮き上がる高さ
	private float heightTimeMax = 2.0f;   //浮き上がるのにかかる時間
	private float heightTime;             //浮き上がるのに使う経過時間
	private float plusHeight;             //浮き上がるのに使う変数
	private bool heightUpFlag;            //浮き上がるかのフラグ
	private bool heightUpMaxFlag;					//最大まで浮き上がっているかのフラグ

	// Use this for initialization
	void Start()
	{
		//浮き上がるのに使う変数
		heightTime = 0.0f;
		plusHeight = heightPos.transform.localPosition.y / ( heightTimeMax * 30f );
		heightUpFlag = false;
		heightUpMaxFlag = false;
	}

	// Update is called once per frame
	//void Update()
	void FixedUpdate()
	{
		if( heightUpFlag == false ) return;

		heightTime += Time.deltaTime;
		if( heightTime < heightTimeMax ) {
			Vector3 workDir = heightPos.transform.position - transform.position;
			workDir.Normalize();
			transform.position += workDir * plusHeight;
		}
		else {
			heightUpMaxFlag = true;
		}
	}

	public void SetHeightUpFlagTrue() { heightUpFlag = true; }
	public bool GetHeightMax() { return heightUpMaxFlag; }
}
