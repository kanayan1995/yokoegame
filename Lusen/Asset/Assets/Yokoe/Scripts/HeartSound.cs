﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class HeartSound : MonoBehaviour
{
	public AudioSource heartSound;
	private GameObject player;  //プレイヤーの位置が欲しいのでFindで探す
	private Vector3 playerPos;  //プレイヤーの位置取得
	public GameObject enemy;    //何もしない敵を設置
	public GameObject dist;			//距離をGameObjectで見る
	private float distMax ;      //音鳴らす距離 distのｚ座標で長さ検知

	void Start()
	{
		player = GameObject.Find( "TPS" );
		distMax = 35.0f;
	}

	// Update is called once per frame
	//void Update()
	void FixedUpdate()

	{
		playerPos = player.transform.position;   //プレイヤーの場所を探す
		float workDist = Vector3.Distance(playerPos , enemy.transform.position);
		//距離が近かったら音鳴らす
		if( workDist < distMax ) {
			heartSound.volume = 1.0f - ( workDist / distMax );
			if( !heartSound.isPlaying ) heartSound.PlayOneShot( heartSound.clip );
		}
}	}

