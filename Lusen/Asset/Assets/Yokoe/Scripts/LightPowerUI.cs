﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class LightPowerUI : MonoBehaviour
{
	public GameObject[] lightUI = new GameObject[26];
	private Player player;    //プレイヤーのスクリプト取得
	private  int lightPower;  //投げれる光のパワーがあるか
	private bool breacClock;  //時計が壊れたか

	// Use this for initialization
	void Start()
	{
		for( int i = 0 ; i < 26 ; i++ ) {
			lightUI[i].SetActive( false );
			lightUI[0].SetActive( true );
		}
		player = gameObject.GetComponent<Player>();
		lightPower = player.GetLightPower(); //現在の持っている力取得
	}

	// Update is called once per frame
	//void Update()
	void FixedUpdate()
	{
		lightPower = player.GetLightPower(); //現在の持っている力取得
		breacClock = player.GetBreakClock();
		if( breacClock == false ) {
			for( int i = 0 ; i < 13 ; i++ ) {
				if( lightPower >= 10 * i ) lightUI[i].SetActive( true );
				else lightUI[i].SetActive( false );
			}
		}
		else {
			for( int i = 0 ; i < 13 ; i++ ) {
				if( lightPower >= 10 * i ) lightUI[i + 13].SetActive( true );
				else lightUI[i + 13].SetActive( false );
			}
		}
	}
}
