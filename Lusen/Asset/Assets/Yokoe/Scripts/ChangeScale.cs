﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class ChangeScale : MonoBehaviour
{
	private Vector3 keepScale;        //大きさを保持
	private Vector3 changeVaule;      //大きさ変換
	private float changeScaleTime;    //大きさを変える時間
	private float changeScaleTimeMax; //小さくするまでにかかる時間
	private bool changeFlag;          //大きさを変え始める

	public enum VALUE
	{
		PLUS,   //大きくする
		MINUS,  //小さくする
		eMax,   //どちらでもない
	}
	private VALUE valueType;          //minusにするかplusにするか

	// Use this for initialization
	void Start()
	{
		keepScale = transform.localScale;
		changeFlag = false;
		changeVaule = keepScale / ( changeScaleTimeMax * 60 ); //SetMinusScaleに同じこと書いてる
	}

	//----------------------------------------
	//			更新
	//			関数でフラグをtreuにしないと動かない
	//----------------------------------------
	void Update()
	{
		if( changeFlag == false ) return;

		changeScaleTime += Time.deltaTime;
		if( changeScaleTime < changeScaleTimeMax ) {
			transform.localScale -= changeVaule;
		}
  }

	//----------------------------------------
	//			小さくするとき(秒)を入れる
	//----------------------------------------
	public void SetMinusScale( float setChangeTime )
	{
		changeScaleTimeMax = setChangeTime;
		changeVaule = keepScale / ( changeScaleTimeMax * 60 );//SetMinusScaleに同じこと書いてる
	}

	//----------------------------------------
	//			大きくするとき(秒)を入れる
	//----------------------------------------
	public void SetPlusScale( float setChangeTime )
	{

	}

	public void SetChangeFlagTrue()
	{
		changeFlag = true;
	}
}
