﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class OneSound : MonoBehaviour
{
	public AudioSource sound; //SE

	void OnTriggerEnter( Collider collider )
	{
		switch( collider.transform.tag ) {
			case "Player":
				sound.PlayOneShot( sound.clip );
				break;
		}
	}
}