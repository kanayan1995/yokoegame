﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class PlayerSound : MonoBehaviour
{
	public AudioSource soundWalk;         //歩く音
	public AudioSource soundRun;          //走る音
	public AudioSource soundThrow;        //投げる音
	public AudioSource soundAbsorption;   //光取得音

	//----------------------------------------
	//			変数別で音鳴らす関数を作る
	//----------------------------------------
	public void PlayWalk() { if( !soundWalk.isPlaying ) soundWalk.PlayOneShot( soundWalk.clip ); }
	public void PlayRun() { if( !soundRun.isPlaying ) soundRun.PlayOneShot( soundRun.clip ); }
	public void PlayThrow() { soundThrow.PlayOneShot( soundThrow.clip ); }
	public void PlayAbsorption() { soundAbsorption.PlayOneShot( soundAbsorption.clip ); }
}
