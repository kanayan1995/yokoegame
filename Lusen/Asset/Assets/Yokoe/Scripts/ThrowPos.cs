﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
//マウスからレイを撃って中った位置を求める
public class ThrowPos : MonoBehaviour
{
	//public GameObject box;          //テスト用
	private Vector3 mousePosition;			//マウスの位置
	private Vector3 rayDir;					//カメラからのマウスの位置座標
	private Vector3 cameraPos;				//カメラの位置
	private RaycastHit hit;					//ヒットした情報を取得する変数
	private Vector3 keepHitPos;				//当たったところの位置情報を保持する
	private Vector3 shotPos;				//飛ばすところの位置
	private float rayCastLenghtMax = 20.0f; //RayCastHitが適用できる距離	最大で飛ばす距離
	private const float frontValue = 1.0f;  //少しだけ前に出す
	private float hitPointDist;				//カメラからhit.pointまでの距離
	private Vector3 keepHitDir;				//当たった面の法線

	public LayerMask mask;					//レイヤーマスク指定

	//----------------------------------------
	//			初期化
	//----------------------------------------
	void Start()
	{

	}

	//----------------------------------------
	//			マウスのからワールド座標を取る
	//			mousePositionに入れる
	//----------------------------------------
	void GetMousePos()
	{
		Vector3 workPos = Input.mousePosition;    //Vector3でマウス位置座標を取得する 
		workPos.z = rayCastLenghtMax;             //Z軸修正 
	//マウス位置座標をスクリーン座標からワールド座標に変換する 
		mousePosition = Camera.main.ScreenToWorldPoint( workPos );
	}

	//----------------------------------------
	//			カメラからマウスに向けての向きを取得する(ちゃんと正規化)
	//			レイピックうつときに使いたいから
	//----------------------------------------
	void GetRayDir()
	{
		rayDir = mousePosition - Camera.main.transform.position;
		rayDir.Normalize();
	}

	//----------------------------------------
	//			カメラの位置取得
	//			ついでに発射する位置を保持しておく
	//----------------------------------------
	void GetCameraPos()
	{
	//ここマジであかん
	//forwordでとらな
		cameraPos = Camera.main.transform.position;
		//cameraPos.z += 1;
		Vector3 forward = Camera.main.transform.forward;
		forward.Normalize();
		cameraPos += forward;
		shotPos = cameraPos;
	}

	//----------------------------------------
	//			RayCastをうつ準備
	//----------------------------------------
	void SetRay()
	{
		GetMousePos();
		GetRayDir();
		GetCameraPos();
	}

	//----------------------------------------
	//			RayCastを撃つ
	//			準備したやつもちゃんと動かしてから
	//----------------------------------------
	void ExecuteRayCast()
	{
		SetRay();
		if( Physics.Raycast( cameraPos , rayDir , out hit , rayCastLenghtMax , mask ) ) {
			//box.transform.position = hit.point;
			keepHitPos = hit.point;
			keepHitDir = hit.normal;
    }
		else {  //範囲に入っていない時
			if( Physics.Raycast( mousePosition , Vector3.down , out hit , rayCastLenghtMax , mask) ) {
				//box.transform.position = hit.point;
				keepHitPos = hit.point;
				keepHitDir = hit.normal;
			}
		}
	}

	//----------------------------------------
	//			更新
	//----------------------------------------
	//void Update()
	void FixedUpdate()
	{
		ExecuteRayCast();
		float work;
		work = Vector3.Distance( keepHitPos , shotPos );
  }

	//----------------------------------------
	//			セッター・ゲッター
	//----------------------------------------
	public Vector3 GetHitPointPos() { return keepHitPos; }
	public Vector3 GetShotPos() { return shotPos; }
	public float GetLenghtMax() { return rayCastLenghtMax; }
	public Vector3 GetMouseDir() { return rayDir; }  //マウスへの向き
	public float GetHitPosDist() { return Vector3.Distance( keepHitPos , shotPos ); } //照準の長さと発射
	public Vector3 GetHitNormal() { return keepHitDir; }
}
