﻿using UnityEngine;
using System.Collections;

//-------------------------------------//
//									   //
//				製作者 : 横江		   //
//								       //
//-------------------------------------//
public class ChangeLightRenge : MonoBehaviour
{
	private Light changeLight;      //ライトを取得
	private float keepRange;        //初めの大きさを保持
	private float minusRange;       //時間で小さくするためのやつ
	private float lostTime;			//何秒で消すか
	private bool lostFlag;			//ライトのRangeを小さくするフラグ

	// Use this for initialization
	void Start()
	{
		changeLight = GetComponent<Light>();
		keepRange = changeLight.range;
		minusRange = keepRange / ( lostTime * 60 );
		lostFlag = false;
	}

	//----------------------------------------
	//			更新
	//			SetLostFlagTrue()を呼ばないと動かない
	//----------------------------------------
	//void Update()
	void FixedUpdate()
	{
		if( lostFlag == false ) return;

		changeLight.range -= minusRange;
		if( changeLight.range < 0 ) {
			changeLight.range = 0;
		}
	}

	//----------------------------------------
	//			この関数を呼ぶまで動かない
	//----------------------------------------
	public void SetLostFlagTrue() { lostFlag = true; }

	//----------------------------------------
	//			消えるまでの時間を入れる
	//			ここで時間を入れるとその時間で消えるように下げるLightのRangeを下げるようにしている
	//			入れるのはフレームではなく時間（秒)
	//----------------------------------------
	public void SetLostTime( float setLostTime ) 
	{
		lostTime = setLostTime;
		minusRange = keepRange / ( lostTime * 60 );
	}
}
