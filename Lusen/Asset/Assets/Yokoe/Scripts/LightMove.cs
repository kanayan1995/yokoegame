﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class LightMove : MonoBehaviour
{		
	private Player player;					//プレイヤーを取得
	public GameObject checkLength;			//取得されるかを判定する距離

	//視野に入っていたらとかに使う
	private bool cheakLook;									//カメラから見えているかどうか
	private const string CAMERA_TAG_NAME = "MainCamera";	//メインカメラのタグ
		
	//光関係
	private Renderer render;						//Renderer取得
	private Color emissionColor;					//もともとの光源の色
	private Color minusColor;						//光源を減らしていく時間
	public float minusEmissionTime = 2.0f;			//光源の値を減らすやつ

	//大きさ関係
	public float minusScalelTime = 2.0f;			//小さくなる時間
	private Vector3 keepScale;						//もともとの大きさ保持
	private Vector3 minusScale;						//スケールを減らしていくやつ

	private const float FPS = 60.0f;

	public enum LIGHTMODE	
	{
		IDLE,		//何もないとき
		LOSTLIGHT,  //光をなくすとき
		ABSORD,     //プレイヤーに吸収される
	}
	private LIGHTMODE lightMode;          //ライトの状態

	private ChangeLightRenge changeLightRange;  //ポイントライトの大きさ調整

	void Start()
	{
		player = GameObject.Find( "player" ).GetComponent<Player>();
		cheakLook = true;
		//光関係
		render = GetComponent<Renderer>();
		emissionColor = render.material.GetColor( "_EmissionColor" );
		minusColor = emissionColor / ( minusEmissionTime * FPS );   //指定した時間(フレーム数に直してる)で光が消える
		lightMode = LIGHTMODE.IDLE;
		//スケール調整
		keepScale = this.transform.localScale;
		minusScale = keepScale / ( minusScalelTime * FPS ); //指定した時間(フレーム数に直してる)で小さくなる
		changeLightRange = GetComponentInChildren<ChangeLightRenge>();
	}

	// Update is called once per frame
	//void Update()
	void FixedUpdate()
	{
		if( cheakLook == false ) return;		//カメラに写ってなかったら動かさない

		switch(lightMode)
		{
			case LIGHTMODE.IDLE:        //通常
				//プレイヤーの状態がライトを取得になっていたら動かす
				if( player.GetMode() == Player.PLAYER_MODE.GET_LIGHT ) {
					Idel();
				}
				break;
			case LIGHTMODE.LOSTLIGHT:   //光消す
				MinusEmmision();
				break;
			case LIGHTMODE.ABSORD:      //プレイヤーに近づく
				PlayerDirMove();
				break;
    }

		cheakLook = false;
	}

	//自分がただ存在しているだけの時
	void Idel()
	{
		Vector3 playerPos = player.GetLightGetPos();
		Vector3 lightPos = this.transform.position;
		float workDist = Vector3.Distance( playerPos , lightPos );
		//プレイヤーとの距離が近かったら取得モードに入る ポイントライトのフラグも立てる
		if( workDist < player.GetLightGetDist()) {
			lightMode = LIGHTMODE.LOSTLIGHT;
			changeLightRange.SetLostTime( minusEmissionTime);
			changeLightRange.SetLostFlagTrue();
		}
	}

	//エミッション（光の量）を0にしていく
	void MinusEmmision()
	{
		if( emissionColor.r > 0.0f ) {
			render.material.EnableKeyword( "_EMISSION" ); //キーワードの有効化を忘れない
			emissionColor -= minusColor;
			render.material.SetColor( "_EmissionColor" , emissionColor );
		}
		else { //光の量が0になると自分に近づくようになる
			lightMode = LIGHTMODE.ABSORD;
		}
	}

	//プレイヤーの方向に進む
	void PlayerDirMove()
	{
		//移動関連
		Vector3 playerPos = player.GetLightGetPos();
		Vector3 lightPos = this.transform.position;
		float workDist = Vector3.Distance( playerPos , lightPos );
		Vector3 workDir;
		workDir = playerPos - lightPos;
		Vector3.Normalize( workDir );
		this.transform.position += workDir * 0.04f;
		//スケール関連
		this.transform.localScale -= minusScale;
		if( transform.localScale.x < 0 ) Destroy( this.gameObject );
	}

	//カメラの視野に入っていた時に動く処理
	private void OnWillRenderObject()
	{
		if( Camera.current.tag == CAMERA_TAG_NAME ) {
			cheakLook = true;
		}
	}

	public float GetLightMinusTime() { return minusEmissionTime * 60.0f; } //この中でフレーム数に変えてる
}
