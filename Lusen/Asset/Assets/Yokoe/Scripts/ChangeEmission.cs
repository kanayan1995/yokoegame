﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class ChangeEmission : MonoBehaviour
{
	private Renderer render;      //Emissionいじるときに使う
	private Color emissionColor;  //Emissionの色
	private Color minusColor;     //時間を指定して減らすための変数
	private float lostTime;       //輝きが消えるまでの時間 消えたかどうかを確認するために使う
	private bool lostFlag;        //消すのを動かすのに使うフラグ

	//最後に消えたかの確認を行うために使う
	private float cheakTimer;     //輝きがなくなったかどうかを時間で判定するために使う
	private bool cheakLostFlag;		//輝きがなくなったらtrueになる

	public int materialNumber = 0;	//materialが複数ある場合何番目にするか　基本は0になっている

	//----------------------------------------
	//			materialが複数あった場合どのmaterialかを判断するために使う
	//----------------------------------------
	void SetEmission()
	{
		if( materialNumber == 0 ) {
			emissionColor = render.material.GetColor( "_EmissionColor" );
		}
		else {
			emissionColor = render.materials[materialNumber].GetColor( "_EmissionColor" );
		}
	}

	//----------------------------------------
	//			初期化
	//----------------------------------------
	void Start()
	{
		render = GetComponent<Renderer>();
		SetEmission();
    minusColor = emissionColor / ( lostTime * 60 );

		lostFlag = false;
		cheakTimer = 0;
		cheakLostFlag = false;
	}



	//----------------------------------------
	//			光が消えたフラグをたたす
	//			消えるまでに使ったタイムを使って判断する
	//----------------------------------------
	void CheakTimer()
	{
		cheakTimer += Time.deltaTime;
		if (cheakTimer > lostTime) {
			cheakLostFlag = true;
		}
	}

	//----------------------------------------
	//			更新
	//----------------------------------------
	//void Update()
	void FixedUpdate()
	{
		if( lostFlag == false ) return;

		//輝きを消す処理
		render.material.EnableKeyword( "_EMISSION" ); //キーワードの有効化を忘れない
		emissionColor -= minusColor;
		if( emissionColor.r < 0 ) emissionColor.r = 0;
		if( emissionColor.g < 0 ) emissionColor.g = 0;
		if( emissionColor.b < 0 ) emissionColor.b = 0;
		render.material.SetColor( "_EmissionColor" , emissionColor );

		//消えたかをチェックする関数
		CheakTimer();
	}

	//----------------------------------------
	//			時間を設定する（秒）で
	//			この中で減る量も決めるので重要
	//----------------------------------------
	public void SetLostTime( float setLostTime )  //入れる時間は秒
	{
		lostTime = setLostTime;
		minusColor = emissionColor / ( lostTime * 60 );
	}

	//----------------------------------------
	//			これを呼び出すことで動かす
	//----------------------------------------
	public void SetLostFlagTrue()
	{
		lostFlag = true;
	}

	public bool GetCheckLost() { return cheakLostFlag; }
}
