﻿using UnityEngine;
using System.Collections;

//------------------------------------- //
//										//
//				製作者 : 横江			//
//										//
//------------------------------------- //
public class DetectCollisions : MonoBehaviour
{
	private CharacterController controller;

	// Use this for initialization
	void Start()
	{
		controller = gameObject.GetComponent<CharacterController>();
	}

	// Update is called once per frame
	void Update()
	{
		controller.detectCollisions = false; 
	}
}
